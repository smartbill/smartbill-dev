require './SmartbillAPIService.rb'
require 'soap/mapping'

module SmartbillAPIServiceMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new
  NsWsApiSmartbillComBr = "http://ws.api.smartbill.com.br/"

  EncodedRegistry.register(
    :class => ReabrirClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "reabrirClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ReabrirClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "reabrirClienteServicoConsumoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => SmartbillObjetoRetorno,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "smartbillObjetoRetorno"),
    :schema_element => [
      ["codigo", ["Status", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["mensagem", ["SOAP::SOAPString", XSD::QName.new(nil, "mensagem")], [0, 1]],
      ["modelo", ["SOAP::SOAPString", XSD::QName.new(nil, "modelo")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["webservice", ["SOAP::SOAPString", XSD::QName.new(nil, "webservice")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTotalClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNFFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteNFFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteNFFilter"),
    :schema_element => [
      ["clienteCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTotalClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteNFResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarEndereco"),
    :schema_element => [
      ["arg0", ["EnderecoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => EnderecoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "enderecoFilter"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["enderecoDeFaturamento", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "enderecoDeFaturamento")], [0, 1]],
      ["tipoEndereco", ["TipoEndereco", XSD::QName.new(nil, "tipoEndereco")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarEnderecoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarEnderecoResponse"),
    :schema_element => [
      ["v_return", ["Endereco[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => Endereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "endereco"),
    :schema_element => [
      ["bairro", ["SOAP::SOAPString", XSD::QName.new(nil, "bairro")], [0, 1]],
      ["cep", ["SOAP::SOAPString", XSD::QName.new(nil, "cep")], [0, 1]],
      ["cidade", ["Cidade", XSD::QName.new(nil, "cidade")], [0, 1]],
      ["complemento", ["SOAP::SOAPString", XSD::QName.new(nil, "complemento")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["enderecoDeFaturamento", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "enderecoDeFaturamento")]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["logradouro", ["SOAP::SOAPString", XSD::QName.new(nil, "logradouro")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["numero", ["SOAP::SOAPInt", XSD::QName.new(nil, "numero")], [0, 1]],
      ["tipoEndereco", ["TipoEndereco", XSD::QName.new(nil, "tipoEndereco")], [0, 1]],
      ["uf", ["Uf", XSD::QName.new(nil, "uf")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Cidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cidade"),
    :schema_element => [
      ["disponivelParaCadastro", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "disponivelParaCadastro")]],
      ["ibge", ["SOAP::SOAPLong", XSD::QName.new(nil, "ibge")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["uf", ["Uf", XSD::QName.new(nil, "uf")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Uf,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "uf"),
    :schema_element => [
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["sigla", ["SOAP::SOAPString", XSD::QName.new(nil, "sigla")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarIndiceEconomicoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomicoValor"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarIndiceEconomicoValorResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomicoValorResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomicoValor[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => IndiceEconomicoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "indiceEconomicoValor"),
    :schema_element => [
      ["ano", ["SOAP::SOAPInt", XSD::QName.new(nil, "ano")]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mes", ["SOAP::SOAPInt", XSD::QName.new(nil, "mes")]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valorIndiceMes", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorIndiceMes")], [0, 1]],
      ["valorIndiceUltimos12Meses", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorIndiceUltimos12Meses")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClonarClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clonarClienteServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClonarClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clonarClienteServicoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoConsumoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoConsumoFilter"),
    :schema_element => [
      ["anoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "anoReferencia")], [0, 1]],
      ["clienteServicoUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteServicoUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["mesReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "mesReferencia")], [0, 1]],
      ["tipoConsumo", ["TipoConsumo", XSD::QName.new(nil, "tipoConsumo")], [0, 1]],
      ["tipoConsumoEstado", ["TipoConsumoEstado", XSD::QName.new(nil, "tipoConsumoEstado")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoConsumoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoConsumo[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoConsumo"),
    :schema_element => [
      ["anoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "anoReferencia")], [0, 1]],
      ["clienteServicoUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteServicoUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mesReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "mesReferencia")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["quantidades", ["ClienteServicoConsumoQuantidade[]", XSD::QName.new(nil, "quantidades")], [0, nil]],
      ["tipoConsumo", ["TipoConsumo", XSD::QName.new(nil, "tipoConsumo")], [0, 1]],
      ["tipoConsumoEstado", ["TipoConsumoEstado", XSD::QName.new(nil, "tipoConsumoEstado")], [0, 1]],
      ["unidadeMedida", ["UnidadeMedida", XSD::QName.new(nil, "unidadeMedida")], [0, 1]],
      ["unidadeMetrica", ["UnidadeMetrica", XSD::QName.new(nil, "unidadeMetrica")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoConsumoQuantidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoConsumoQuantidade"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["fimConsumo", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "fimConsumo")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["identificadorFaturamento", ["SOAP::SOAPString", XSD::QName.new(nil, "identificadorFaturamento")], [0, 1]],
      ["inicioConsumo", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "inicioConsumo")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["quantidade", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "quantidade")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNFFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteNFResponse"),
    :schema_element => [
      ["v_return", ["ClienteNF[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteNF"),
    :schema_element => [
      ["alertaRetorno", ["SOAP::SOAPString", XSD::QName.new(nil, "alertaRetorno")], [0, 1]],
      ["aliquotaCOFINS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaCOFINS")], [0, 1]],
      ["aliquotaCSLL", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaCSLL")], [0, 1]],
      ["aliquotaINSS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSS")], [0, 1]],
      ["aliquotaIR", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaIR")], [0, 1]],
      ["aliquotaISS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISS")], [0, 1]],
      ["aliquotaISSRetido", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSRetido")], [0, 1]],
      ["aliquotaPIS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaPIS")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["codigoCNAE", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoCNAE")], [0, 1]],
      ["codigoServicoNF", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoServicoNF")], [0, 1]],
      ["codigoVerificacao", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoVerificacao")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["dataEmissao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataEmissao")], [0, 1]],
      ["descricaoRPS", ["SOAP::SOAPString", XSD::QName.new(nil, "descricaoRPS")], [0, 1]],
      ["erroRetorno", ["SOAP::SOAPString", XSD::QName.new(nil, "erroRetorno")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["incentivadorCultural", ["SOAP::SOAPString", XSD::QName.new(nil, "incentivadorCultural")], [0, 1]],
      ["loteNF", ["LoteNF", XSD::QName.new(nil, "loteNF")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["motivoCancelamento", ["SOAP::SOAPString", XSD::QName.new(nil, "motivoCancelamento")], [0, 1]],
      ["naturezaOperacao", ["SOAP::SOAPString", XSD::QName.new(nil, "naturezaOperacao")], [0, 1]],
      ["numeroNFS", ["SOAP::SOAPInteger", XSD::QName.new(nil, "numeroNFS")], [0, 1]],
      ["numeroRPS", ["SOAP::SOAPLong", XSD::QName.new(nil, "numeroRPS")], [0, 1]],
      ["regimeTributario", ["SOAP::SOAPString", XSD::QName.new(nil, "regimeTributario")], [0, 1]],
      ["serieRPS", ["SOAP::SOAPString", XSD::QName.new(nil, "serieRPS")], [0, 1]],
      ["servicosCR", ["ClienteServicoCR[]", XSD::QName.new(nil, "servicosCR")], [0, nil]],
      ["simplesNacional", ["SOAP::SOAPString", XSD::QName.new(nil, "simplesNacional")], [0, 1]],
      ["textoEnvio", ["SOAP::SOAPString", XSD::QName.new(nil, "textoEnvio")], [0, 1]],
      ["textoRetorno", ["SOAP::SOAPString", XSD::QName.new(nil, "textoRetorno")], [0, 1]],
      ["tipoAmbiente", ["TipoAmbiente", XSD::QName.new(nil, "tipoAmbiente")], [0, 1]],
      ["tipoNFCancelamento", ["TipoNFCancelamento", XSD::QName.new(nil, "tipoNFCancelamento")], [0, 1]],
      ["tipoNFEstado", ["TipoNFEstado", XSD::QName.new(nil, "tipoNFEstado")], [0, 1]],
      ["tributacaoMunicipio", ["SOAP::SOAPString", XSD::QName.new(nil, "tributacaoMunicipio")], [0, 1]],
      ["urlConsulta", ["SOAP::SOAPString", XSD::QName.new(nil, "urlConsulta")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valorCOFINS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorCOFINS")], [0, 1]],
      ["valorCSLL", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorCSLL")], [0, 1]],
      ["valorDeducao", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorDeducao")], [0, 1]],
      ["valorINSS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorINSS")], [0, 1]],
      ["valorIR", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorIR")], [0, 1]],
      ["valorISS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorISS")], [0, 1]],
      ["valorISSRetido", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorISSRetido")], [0, 1]],
      ["valorPIS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorPIS")], [0, 1]],
      ["valorServicos", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorServicos")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => LoteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "loteNF"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["numero", ["SOAP::SOAPLong", XSD::QName.new(nil, "numero")], [0, 1]],
      ["urlEnvio", ["SOAP::SOAPString", XSD::QName.new(nil, "urlEnvio")], [0, 1]],
      ["urlRetorno", ["SOAP::SOAPString", XSD::QName.new(nil, "urlRetorno")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoCR"),
    :schema_element => [
      ["anoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "anoReferencia")], [0, 1]],
      ["clienteServico", ["ClienteServico", XSD::QName.new(nil, "clienteServico")], [0, 1]],
      ["clienteServicoConsumo", ["ClienteServicoConsumo", XSD::QName.new(nil, "clienteServicoConsumo")], [0, 1]],
      ["clienteServicoParcelaAtivacao", ["ClienteServicoParcelaAtivacao", XSD::QName.new(nil, "clienteServicoParcelaAtivacao")], [0, 1]],
      ["clienteServicoParcelaServico", ["ClienteServicoParcelaServico", XSD::QName.new(nil, "clienteServicoParcelaServico")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["dataEmissao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataEmissao")], [0, 1]],
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mesReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "mesReferencia")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["projecaoFaturamento", ["ProjecaoFaturamento", XSD::QName.new(nil, "projecaoFaturamento")], [0, 1]],
      ["referencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "referencia")], [0, 1]],
      ["serie", ["SOAP::SOAPString", XSD::QName.new(nil, "serie")], [0, 1]],
      ["subSerie", ["SOAP::SOAPString", XSD::QName.new(nil, "subSerie")], [0, 1]],
      ["tipoCREstado", ["TipoCREstado", XSD::QName.new(nil, "tipoCREstado")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valorAtivacaoTotal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorAtivacaoTotal")], [0, 1]],
      ["valorServicoTotal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorServicoTotal")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServico"),
    :schema_element => [
      ["acrescimos", ["ClienteServicoAcrescimo[]", XSD::QName.new(nil, "acrescimos")], [0, nil]],
      ["aliquotaCOFINS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaCOFINS")], [0, 1]],
      ["aliquotaCSLL", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaCSLL")], [0, 1]],
      ["aliquotaINSS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSS")], [0, 1]],
      ["aliquotaIR", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaIR")], [0, 1]],
      ["aliquotaISS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISS")], [0, 1]],
      ["aliquotaISSRetido", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSRetido")], [0, 1]],
      ["aliquotaPIS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaPIS")], [0, 1]],
      ["codigoConsolidaFatura", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoConsolidaFatura")], [0, 1]],
      ["codigoContrato", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoContrato")], [0, 1]],
      ["contatos", ["Contato[]", XSD::QName.new(nil, "contatos")], [0, nil]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["dataAdesao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataAdesao")], [0, 1]],
      ["dataCancelamento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataCancelamento")], [0, 1]],
      ["dataPrimeiroVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataPrimeiroVencimento")], [0, 1]],
      ["dataSuspensaoFim", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataSuspensaoFim")], [0, 1]],
      ["dataSuspensaoInicio", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataSuspensaoInicio")], [0, 1]],
      ["dataVigenciaFim", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaFim")], [0, 1]],
      ["dataVigenciaInicio", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaInicio")], [0, 1]],
      ["descontos", ["ClienteServicoDesconto[]", XSD::QName.new(nil, "descontos")], [0, nil]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["fatorDeReajusteDaRenovacao", ["IndiceEconomico", XSD::QName.new(nil, "fatorDeReajusteDaRenovacao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["motivoCancelamento", ["MotivoCancelamento", XSD::QName.new(nil, "motivoCancelamento")], [0, 1]],
      ["motivoSuspensao", ["MotivoSuspensao", XSD::QName.new(nil, "motivoSuspensao")], [0, 1]],
      ["municipioPrestacaoServico", ["Cidade", XSD::QName.new(nil, "municipioPrestacaoServico")], [0, 1]],
      ["naturezaOperacao", ["SOAP::SOAPString", XSD::QName.new(nil, "naturezaOperacao")], [0, 1]],
      ["observacao", ["SOAP::SOAPString", XSD::QName.new(nil, "observacao")], [0, 1]],
      ["parcelasAtivacao", ["ClienteServicoParcelaAtivacao[]", XSD::QName.new(nil, "parcelasAtivacao")], [0, nil]],
      ["parcelasServico", ["ClienteServicoParcelaServico[]", XSD::QName.new(nil, "parcelasServico")], [0, nil]],
      ["periodicidade", ["Periodicidade", XSD::QName.new(nil, "periodicidade")], [0, 1]],
      ["quantidade", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "quantidade")], [0, 1]],
      ["quantidadeTipoVigencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "quantidadeTipoVigencia")], [0, 1]],
      ["renovacaoAutomatica", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "renovacaoAutomatica")]],
      ["servico", ["Servico", XSD::QName.new(nil, "servico")], [0, 1]],
      ["servicoEstado", ["ServicoEstado", XSD::QName.new(nil, "servicoEstado")], [0, 1]],
      ["tarifaMinima", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "tarifaMinima")], [0, 1]],
      ["taxaAtivacao", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "taxaAtivacao")], [0, 1]],
      ["tipoConsumo", ["TipoConsumo", XSD::QName.new(nil, "tipoConsumo")], [0, 1]],
      ["tipoFaturamento", ["TipoFaturamento", XSD::QName.new(nil, "tipoFaturamento")], [0, 1]],
      ["tipoVigencia", ["TipoVigencia", XSD::QName.new(nil, "tipoVigencia")], [0, 1]],
      ["unidadeMedida", ["UnidadeMedida", XSD::QName.new(nil, "unidadeMedida")], [0, 1]],
      ["unidadeMetrica", ["UnidadeMetrica", XSD::QName.new(nil, "unidadeMetrica")], [0, 1]],
      ["usarFatorDeReajusteAutomatico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "usarFatorDeReajusteAutomatico")]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]],
      ["valores", ["ClienteServicoValor[]", XSD::QName.new(nil, "valores")], [0, nil]],
      ["vendedor", ["Usuario", XSD::QName.new(nil, "vendedor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoAcrescimo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoAcrescimo"),
    :schema_element => [
      ["dataVigenciaFim", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaFim")], [0, 1]],
      ["dataVigenciaInicio", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaInicio")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["tipoValor", ["TipoValor", XSD::QName.new(nil, "tipoValor")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Contato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "contato"),
    :schema_element => [
      ["celularDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "celularDdd")], [0, 1]],
      ["celularNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "celularNumero")], [0, 1]],
      ["cpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cpf")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["email", ["SOAP::SOAPString", XSD::QName.new(nil, "email")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["faxDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "faxDdd")], [0, 1]],
      ["faxNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "faxNumero")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["recebeCobranca", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "recebeCobranca")]],
      ["telefoneDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "telefoneDdd")], [0, 1]],
      ["telefoneNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "telefoneNumero")], [0, 1]],
      ["tipoContato", ["TipoContato", XSD::QName.new(nil, "tipoContato")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoDesconto,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoDesconto"),
    :schema_element => [
      ["dataVigenciaFim", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaFim")], [0, 1]],
      ["dataVigenciaInicio", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaInicio")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["tipoValor", ["TipoValor", XSD::QName.new(nil, "tipoValor")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => IndiceEconomico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "indiceEconomico"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["tipoIndiceEconomico", ["TipoIndiceEconomico", XSD::QName.new(nil, "tipoIndiceEconomico")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valores", ["IndiceEconomicoValor[]", XSD::QName.new(nil, "valores")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => MotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "motivoCancelamento"),
    :schema_element => [
      ["contabilizarNoRelatorio", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "contabilizarNoRelatorio")]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => MotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "motivoSuspensao"),
    :schema_element => [
      ["contabilizarNoRelatorio", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "contabilizarNoRelatorio")]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoParcelaAtivacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoParcelaAtivacao"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoParcela"),
    :schema_element => [
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["parcela", ["SOAP::SOAPInt", XSD::QName.new(nil, "parcela")]],
      ["tipoParcela", ["TipoParcela", XSD::QName.new(nil, "tipoParcela")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoParcela,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoParcela"),
    :schema_element => [
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["parcela", ["SOAP::SOAPInt", XSD::QName.new(nil, "parcela")]],
      ["tipoParcela", ["TipoParcela", XSD::QName.new(nil, "tipoParcela")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoParcelaServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoParcelaServico"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoParcela"),
    :schema_element => [
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["parcela", ["SOAP::SOAPInt", XSD::QName.new(nil, "parcela")]],
      ["tipoParcela", ["TipoParcela", XSD::QName.new(nil, "tipoParcela")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Periodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "periodicidade"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["exibirNoFormularioDeContratoExterno", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "exibirNoFormularioDeContratoExterno")]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["percentualDesconto", ["SOAP::SOAPDouble", XSD::QName.new(nil, "percentualDesconto")], [0, 1]],
      ["tipoPeriodicidade", ["TipoPeriodicidade", XSD::QName.new(nil, "tipoPeriodicidade")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Servico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servico"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["codigoServicoNF", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoServicoNF")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["descricaoNF", ["SOAP::SOAPString", XSD::QName.new(nil, "descricaoNF")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["familia", ["Familia", XSD::QName.new(nil, "familia")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["servicos", ["Servico[]", XSD::QName.new(nil, "servicos")], [0, nil]],
      ["tarifaMinima", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "tarifaMinima")], [0, 1]],
      ["taxaAtivacao", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "taxaAtivacao")], [0, 1]],
      ["tipoConsumo", ["TipoConsumo", XSD::QName.new(nil, "tipoConsumo")], [0, 1]],
      ["tipoServico", ["TipoServico", XSD::QName.new(nil, "tipoServico")], [0, 1]],
      ["tributacaoMunicipio", ["SOAP::SOAPString", XSD::QName.new(nil, "tributacaoMunicipio")], [0, 1]],
      ["unidadeMedida", ["UnidadeMedida", XSD::QName.new(nil, "unidadeMedida")], [0, 1]],
      ["unidadeMetrica", ["UnidadeMetrica", XSD::QName.new(nil, "unidadeMetrica")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]],
      ["valores", ["ServicoValor[]", XSD::QName.new(nil, "valores")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => Familia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "familia"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["memorialDescritivo", ["SOAP::SOAPString", XSD::QName.new(nil, "memorialDescritivo")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ServicoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoValor"),
    :schema_element => [
      ["faixaFinal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "faixaFinal")], [0, 1]],
      ["faixaInicial", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "faixaInicial")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoValor"),
    :schema_element => [
      ["faixaFinal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "faixaFinal")], [0, 1]],
      ["faixaInicial", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "faixaInicial")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Usuario,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "usuario"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["email", ["SOAP::SOAPString", XSD::QName.new(nil, "email")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["ips", ["SOAP::SOAPString[]", XSD::QName.new(nil, "ips")], [0, nil]],
      ["menus", ["Menu[]", XSD::QName.new(nil, "menus")], [0, nil]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["othersGerenteUuids", ["SOAP::SOAPString[]", XSD::QName.new(nil, "othersGerenteUuids")], [0, nil]],
      ["regras", ["Regra[]", XSD::QName.new(nil, "regras")], [0, nil]],
      ["senha", ["SOAP::SOAPString", XSD::QName.new(nil, "senha")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Menu,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "menu"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["id", ["SOAP::SOAPString", XSD::QName.new(nil, "id")], [0, 1]],
      ["image", ["SOAP::SOAPString", XSD::QName.new(nil, "image")], [0, 1]],
      ["label", ["SOAP::SOAPString", XSD::QName.new(nil, "label")], [0, 1]],
      ["menuConfig", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "menuConfig")]],
      ["menuItem", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "menuItem")]],
      ["menuPai", ["Menu", XSD::QName.new(nil, "menuPai")], [0, 1]],
      ["ordem", ["SOAP::SOAPInt", XSD::QName.new(nil, "ordem")]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["value", ["SOAP::SOAPString", XSD::QName.new(nil, "value")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Regra,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "regra"),
    :schema_element => [
      ["chave", ["SOAP::SOAPString", XSD::QName.new(nil, "chave")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AtivarClienteServicoPorClienteServicoUuid,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "ativarClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AtivarClienteServicoPorClienteServicoUuidResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "ativarClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarMotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["MotivoSuspensao", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarMotivoSuspensaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoSuspensaoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarFamilia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarFamilia"),
    :schema_element => [
      ["arg0", ["Familia", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarFamiliaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarFamiliaResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarTotalServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServico"),
    :schema_element => [
      ["arg0", ["ServicoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ServicoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoFilter"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["excluindoIds", ["SOAP::SOAPString[]", XSD::QName.new(nil, "excluindoIds")], [0, nil]],
      ["familia", ["Familia", XSD::QName.new(nil, "familia")], [0, 1]],
      ["generalFilter", ["GeneralFilter", XSD::QName.new(nil, "generalFilter")], [0, 1]],
      ["servicosAdicional", ["Servico", XSD::QName.new(nil, "servicosAdicional")], [0, 1]],
      ["tiposServico", ["TipoServico[]", XSD::QName.new(nil, "tiposServico")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => GeneralFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "generalFilter"),
    :schema_element => [
      ["likeCampos", ["SOAP::SOAPString[]", XSD::QName.new(nil, "likeCampos")], [0, nil]],
      ["likeValor", ["SOAP::SOAPString", XSD::QName.new(nil, "likeValor")], [0, 1]],
      ["maxElements", ["SOAP::SOAPInt", XSD::QName.new(nil, "maxElements")], [0, 1]],
      ["orderBy", ["SOAP::SOAPString", XSD::QName.new(nil, "orderBy")], [0, 1]],
      ["page", ["SOAP::SOAPInt", XSD::QName.new(nil, "page")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTotalServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServicoResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterServicoResponse"),
    :schema_element => [
      ["v_return", ["Servico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRelacionamento"),
    :schema_element => [
      ["arg0", ["RelacionamentoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RelacionamentoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "relacionamentoFilter"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["tipoRelacionamento", ["TipoRelacionamento", XSD::QName.new(nil, "tipoRelacionamento")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarRelacionamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRelacionamentoResponse"),
    :schema_element => [
      ["v_return", ["Relacionamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => Relacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "relacionamento"),
    :schema_element => [
      ["clienteCRTransacaoUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRTransacaoUuid")], [0, 1]],
      ["clienteCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRUuid")], [0, 1]],
      ["clienteNFUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteNFUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["comentario", ["SOAP::SOAPString", XSD::QName.new(nil, "comentario")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["emailUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "emailUuid")], [0, 1]],
      ["enviarEmail", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "enviarEmail")]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoRelacionamento", ["TipoRelacionamento", XSD::QName.new(nil, "tipoRelacionamento")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRelacionamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterRelacionamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRelacionamentoResponse"),
    :schema_element => [
      ["v_return", ["Relacionamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarSegmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarSegmento"),
    :schema_element => [
      ["arg0", ["Segmento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Segmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "segmento"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["criticidade", ["Criticidade", XSD::QName.new(nil, "criticidade")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarSegmentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarSegmentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRelacionamento"),
    :schema_element => [
      ["arg0", ["Relacionamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarRelacionamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRelacionamentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarRegra,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegra"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarRegraResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegraResponse"),
    :schema_element => [
      ["v_return", ["Regra[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarContato"),
    :schema_element => [
      ["arg0", ["Contato", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarContatoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarContatoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => GerarNFsEmissao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "gerarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => GerarNFsEmissaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "gerarNFsEmissaoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AdicionarMotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["MotivoSuspensao", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarMotivoSuspensaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoSuspensaoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterCodigoCNAE,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCodigoCNAE"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterCodigoCNAEResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCodigoCNAEResponse"),
    :schema_element => [
      ["v_return", ["CodigoCNAE", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => CodigoCNAE,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "codigoCNAE"),
    :schema_element => [
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteCRCadastro,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCRCadastro"),
    :schema_element => [
      ["arg0", ["ClienteCRCadastro", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteCRCadastro,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRCadastro"),
    :schema_element => [
      ["clienteCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRUuid")], [0, 1]],
      ["clientePrestador", ["ClientePrestador", XSD::QName.new(nil, "clientePrestador")], [0, 1]],
      ["clienteTomador", ["ClienteTomador", XSD::QName.new(nil, "clienteTomador")], [0, 1]],
      ["contatoPrestador", ["ContatoPrestador", XSD::QName.new(nil, "contatoPrestador")], [0, 1]],
      ["contatoTomador", ["ContatoTomador", XSD::QName.new(nil, "contatoTomador")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["enderecoPrestador", ["EnderecoPrestador", XSD::QName.new(nil, "enderecoPrestador")], [0, 1]],
      ["enderecoTomador", ["EnderecoTomador", XSD::QName.new(nil, "enderecoTomador")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClientePrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clientePrestador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["cnpjCpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cnpjCpf")], [0, 1]],
      ["emailGeral", ["SOAP::SOAPString", XSD::QName.new(nil, "emailGeral")], [0, 1]],
      ["ieRg", ["SOAP::SOAPString", XSD::QName.new(nil, "ieRg")], [0, 1]],
      ["im", ["SOAP::SOAPString", XSD::QName.new(nil, "im")], [0, 1]],
      ["nomeFantasia", ["SOAP::SOAPString", XSD::QName.new(nil, "nomeFantasia")], [0, 1]],
      ["orgaoPublico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "orgaoPublico")]],
      ["razaoSocial", ["SOAP::SOAPString", XSD::QName.new(nil, "razaoSocial")], [0, 1]],
      ["tipoPessoa", ["TipoPessoa", XSD::QName.new(nil, "tipoPessoa")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => CopyFields,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ClienteTomador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteTomador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["cnpjCpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cnpjCpf")], [0, 1]],
      ["emailGeral", ["SOAP::SOAPString", XSD::QName.new(nil, "emailGeral")], [0, 1]],
      ["ieRg", ["SOAP::SOAPString", XSD::QName.new(nil, "ieRg")], [0, 1]],
      ["im", ["SOAP::SOAPString", XSD::QName.new(nil, "im")], [0, 1]],
      ["nomeFantasia", ["SOAP::SOAPString", XSD::QName.new(nil, "nomeFantasia")], [0, 1]],
      ["orgaoPublico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "orgaoPublico")]],
      ["razaoSocial", ["SOAP::SOAPString", XSD::QName.new(nil, "razaoSocial")], [0, 1]],
      ["tipoPessoa", ["TipoPessoa", XSD::QName.new(nil, "tipoPessoa")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ContatoPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "contatoPrestador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["celularDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "celularDdd")], [0, 1]],
      ["celularNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "celularNumero")], [0, 1]],
      ["cpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cpf")], [0, 1]],
      ["email", ["SOAP::SOAPString", XSD::QName.new(nil, "email")], [0, 1]],
      ["faxDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "faxDdd")], [0, 1]],
      ["faxNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "faxNumero")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["telefoneDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "telefoneDdd")], [0, 1]],
      ["telefoneNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "telefoneNumero")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ContatoTomador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "contatoTomador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["celularDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "celularDdd")], [0, 1]],
      ["celularNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "celularNumero")], [0, 1]],
      ["cpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cpf")], [0, 1]],
      ["email", ["SOAP::SOAPString", XSD::QName.new(nil, "email")], [0, 1]],
      ["faxDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "faxDdd")], [0, 1]],
      ["faxNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "faxNumero")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["telefoneDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "telefoneDdd")], [0, 1]],
      ["telefoneNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "telefoneNumero")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => EnderecoPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "enderecoPrestador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["bairro", ["SOAP::SOAPString", XSD::QName.new(nil, "bairro")], [0, 1]],
      ["cep", ["SOAP::SOAPString", XSD::QName.new(nil, "cep")], [0, 1]],
      ["cidade", ["Cidade", XSD::QName.new(nil, "cidade")], [0, 1]],
      ["complemento", ["SOAP::SOAPString", XSD::QName.new(nil, "complemento")], [0, 1]],
      ["logradouro", ["SOAP::SOAPString", XSD::QName.new(nil, "logradouro")], [0, 1]],
      ["numero", ["SOAP::SOAPInt", XSD::QName.new(nil, "numero")], [0, 1]],
      ["uf", ["Uf", XSD::QName.new(nil, "uf")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => EnderecoTomador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "enderecoTomador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["bairro", ["SOAP::SOAPString", XSD::QName.new(nil, "bairro")], [0, 1]],
      ["cep", ["SOAP::SOAPString", XSD::QName.new(nil, "cep")], [0, 1]],
      ["cidade", ["Cidade", XSD::QName.new(nil, "cidade")], [0, 1]],
      ["complemento", ["SOAP::SOAPString", XSD::QName.new(nil, "complemento")], [0, 1]],
      ["logradouro", ["SOAP::SOAPString", XSD::QName.new(nil, "logradouro")], [0, 1]],
      ["numero", ["SOAP::SOAPInt", XSD::QName.new(nil, "numero")], [0, 1]],
      ["uf", ["Uf", XSD::QName.new(nil, "uf")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteCRCadastroResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCRCadastroResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarRamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRamoAtividade"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarRamoAtividadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRamoAtividadeResponse"),
    :schema_element => [
      ["v_return", ["RamoAtividade[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => RamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "ramoAtividade"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["segmentos", ["Segmento[]", XSD::QName.new(nil, "segmentos")], [0, nil]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => BaixarCobranca,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "baixarCobranca"),
    :schema_element => [
      ["arg0", ["Lancamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Lancamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "lancamento"),
    :schema_element => [
      ["clienteCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRUuid")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["data", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "data")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["tipoLancamento", ["TipoLancamento", XSD::QName.new(nil, "tipoLancamento")], [0, 1]],
      ["tipoMeioPagamento", ["TipoMeioPagamento", XSD::QName.new(nil, "tipoMeioPagamento")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => BaixarCobrancaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "baixarCobrancaResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => RemoverClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCR", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverClienteServicoCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoCRResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarTotalClienteCRTransacaoBoleto,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCRTransacaoBoleto"),
    :schema_element => [
      ["arg0", ["ClienteCRTransacaoBoletoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteCRTransacaoBoletoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacaoBoletoFilter"),
    :schema_element => [
      ["clienteCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTotalClienteCRTransacaoBoletoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCRTransacaoBoletoResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterRegimeTributario,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegimeTributario"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterRegimeTributarioResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegimeTributarioResponse"),
    :schema_element => [
      ["v_return", ["RegimeTributario", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RegimeTributario,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "regimeTributario"),
    :schema_element => [
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerServico"),
    :schema_element => [
      ["arg0", ["Servico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerServicoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarTotalArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalArquivo"),
    :schema_element => [
      ["arg0", ["ArquivoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ArquivoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "arquivoFilter"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["generalFilter", ["GeneralFilter", XSD::QName.new(nil, "generalFilter")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTotalArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalArquivoResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  EncodedRegistry.register(
    :class => ConsultarCobranca,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "consultarCobranca"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ConsultarCobrancaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "consultarCobrancaResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterContato"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterContatoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterContatoResponse"),
    :schema_element => [
      ["v_return", ["Contato", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteCRCadastro,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRCadastro"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteCRCadastroResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRCadastroResponse"),
    :schema_element => [
      ["v_return", ["ClienteCRCadastro", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarServicoAliquotaPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarServicoAliquotaPrestador"),
    :schema_element => [
      ["arg0", ["ServicoAliquotaPrestadorFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ServicoAliquotaPrestadorFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoAliquotaPrestadorFilter"),
    :schema_element => [
      ["cidadeUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "cidadeUuid")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["generalFilter", ["GeneralFilter", XSD::QName.new(nil, "generalFilter")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarServicoAliquotaPrestadorResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarServicoAliquotaPrestadorResponse"),
    :schema_element => [
      ["v_return", ["ServicoAliquotaPrestador[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ServicoAliquotaPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoAliquotaPrestador"),
    :schema_element => [
      ["aliquotaINSSEX", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSSEX")], [0, 1]],
      ["aliquotaINSSPF", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSSPF")], [0, 1]],
      ["aliquotaINSSRetidoPJ", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSSRetidoPJ")], [0, 1]],
      ["aliquotaINSSRetidoPU", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSSRetidoPU")], [0, 1]],
      ["aliquotaIRRetidoPJ", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaIRRetidoPJ")], [0, 1]],
      ["aliquotaIRRetidoPU", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaIRRetidoPU")], [0, 1]],
      ["aliquotaISSEX", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSEX")], [0, 1]],
      ["aliquotaISSPF", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSPF")], [0, 1]],
      ["aliquotaISSPJ", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSPJ")], [0, 1]],
      ["aliquotaISSPU", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSPU")], [0, 1]],
      ["aliquotaISSRetidoPJ", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSRetidoPJ")], [0, 1]],
      ["aliquotaISSRetidoPU", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSRetidoPU")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["incideINSSPJ", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "incideINSSPJ")]],
      ["incideINSSPU", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "incideINSSPU")]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["retemINSSPJ", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemINSSPJ")]],
      ["retemINSSPU", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemINSSPU")]],
      ["retemIRPJ", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemIRPJ")]],
      ["retemIRPU", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemIRPU")]],
      ["retemISSPJ", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSPJ")]],
      ["retemISSPU", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSPU")]],
      ["retemISSPrestadorCadastrado", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSPrestadorCadastrado")]],
      ["retemISSPrestadorMunicipio", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSPrestadorMunicipio")]],
      ["retemISSPrestadorNaoCadastrado", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSPrestadorNaoCadastrado")]],
      ["retemISSTomadorIgualPrestador", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSTomadorIgualPrestador")]],
      ["retemPCCPJ", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemPCCPJ")]],
      ["retemPCCPU", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemPCCPU")]],
      ["servicoAliquotaTomadors", ["ServicoAliquotaTomador[]", XSD::QName.new(nil, "servicoAliquotaTomadors")], [0, nil]],
      ["tributacaoMunicipio", ["SOAP::SOAPString", XSD::QName.new(nil, "tributacaoMunicipio")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ServicoAliquotaTomador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoAliquotaTomador"),
    :schema_element => [
      ["aliquotaISSRetido", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSRetido")], [0, 1]],
      ["cidade", ["Cidade", XSD::QName.new(nil, "cidade")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterArquivo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterArquivoResponse"),
    :schema_element => [
      ["v_return", ["Arquivo", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Arquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "arquivo"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["src", ["SOAP::SOAPString", XSD::QName.new(nil, "src")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarSegmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarSegmento"),
    :schema_element => [
      ["arg0", ["Segmento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarSegmentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarSegmentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterFamilia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterFamilia"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterFamiliaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterFamiliaResponse"),
    :schema_element => [
      ["v_return", ["Familia", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterTemplateEmail,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmail"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterTemplateEmailResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmailResponse"),
    :schema_element => [
      ["v_return", ["TemplateEmail", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => TemplateEmail,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "templateEmail"),
    :schema_element => [
      ["assunto", ["SOAP::SOAPString", XSD::QName.new(nil, "assunto")], [0, 1]],
      ["comCopia", ["SOAP::SOAPString", XSD::QName.new(nil, "comCopia")], [0, 1]],
      ["comCopiaOculta", ["SOAP::SOAPString", XSD::QName.new(nil, "comCopiaOculta")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mensagem", ["SOAP::SOAPString", XSD::QName.new(nil, "mensagem")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoTemplateEmail", ["TipoTemplateEmail", XSD::QName.new(nil, "tipoTemplateEmail")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => CancelarNFsEmissao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => CancelarNFsEmissaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarNFsEmissaoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarCliente"),
    :schema_element => [
      ["arg0", ["Cliente", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Cliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cliente"),
    :schema_element => [
      ["bloqueioFaturamento", ["BloqueioFaturamento", XSD::QName.new(nil, "bloqueioFaturamento")], [0, 1]],
      ["cartoes", ["ClienteCartao[]", XSD::QName.new(nil, "cartoes")], [0, nil]],
      ["cicloFaturamento", ["CicloFaturamento", XSD::QName.new(nil, "cicloFaturamento")], [0, 1]],
      ["cnpjCpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cnpjCpf")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["comoConheceu", ["SOAP::SOAPString", XSD::QName.new(nil, "comoConheceu")], [0, 1]],
      ["complementoDoCorpoDaNFe", ["SOAP::SOAPString", XSD::QName.new(nil, "complementoDoCorpoDaNFe")], [0, 1]],
      ["contatos", ["Contato[]", XSD::QName.new(nil, "contatos")], [0, nil]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["diaVencimento", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaVencimento")], [0, 1]],
      ["documentoFiscal", ["DocumentoFiscal", XSD::QName.new(nil, "documentoFiscal")], [0, 1]],
      ["domicilioFaturamento", ["Cidade", XSD::QName.new(nil, "domicilioFaturamento")], [0, 1]],
      ["dominio", ["SOAP::SOAPString", XSD::QName.new(nil, "dominio")], [0, 1]],
      ["emailGeral", ["SOAP::SOAPString", XSD::QName.new(nil, "emailGeral")], [0, 1]],
      ["enderecos", ["Endereco[]", XSD::QName.new(nil, "enderecos")], [0, nil]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["ieRg", ["SOAP::SOAPString", XSD::QName.new(nil, "ieRg")], [0, 1]],
      ["im", ["SOAP::SOAPString", XSD::QName.new(nil, "im")], [0, 1]],
      ["indicadoPor", ["SOAP::SOAPString", XSD::QName.new(nil, "indicadoPor")], [0, 1]],
      ["meioPagamento", ["MeioPagamento", XSD::QName.new(nil, "meioPagamento")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nomeFantasia", ["SOAP::SOAPString", XSD::QName.new(nil, "nomeFantasia")], [0, 1]],
      ["orgaoPublico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "orgaoPublico")]],
      ["ramoAtividade", ["RamoAtividade", XSD::QName.new(nil, "ramoAtividade")], [0, 1]],
      ["razaoSocial", ["SOAP::SOAPString", XSD::QName.new(nil, "razaoSocial")], [0, 1]],
      ["telefoneDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "telefoneDdd")], [0, 1]],
      ["telefoneNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "telefoneNumero")], [0, 1]],
      ["tipoPessoa", ["TipoPessoa", XSD::QName.new(nil, "tipoPessoa")], [0, 1]],
      ["urlPropria", ["SOAP::SOAPString", XSD::QName.new(nil, "urlPropria")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteCartao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCartao"),
    :schema_element => [
      ["ano", ["SOAP::SOAPInt", XSD::QName.new(nil, "ano")], [0, 1]],
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["chaveCompra", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveCompra")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mes", ["SOAP::SOAPInt", XSD::QName.new(nil, "mes")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["numero", ["SOAP::SOAPString", XSD::QName.new(nil, "numero")], [0, 1]],
      ["principal", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "principal")]],
      ["security", ["SOAP::SOAPString", XSD::QName.new(nil, "security")], [0, 1]],
      ["tipoBandeira", ["TipoBandeira", XSD::QName.new(nil, "tipoBandeira")], [0, 1]],
      ["titular", ["SOAP::SOAPString", XSD::QName.new(nil, "titular")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => CicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cicloFaturamento"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["diaEnvioBoleto", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaEnvioBoleto")]],
      ["diaEnvioBoletoFatorMesBase", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaEnvioBoletoFatorMesBase")]],
      ["diaEnvioNFE", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaEnvioNFE")]],
      ["diaEnvioNFEFatorMesBase", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaEnvioNFEFatorMesBase")]],
      ["diaFechamentoConsumo", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaFechamentoConsumo")]],
      ["diaFechamentoConsumoFatorMesBase", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaFechamentoConsumoFatorMesBase")]],
      ["diaFechamentoFatura", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaFechamentoFatura")]],
      ["diaFechamentoFaturaFatorMesBase", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaFechamentoFaturaFatorMesBase")]],
      ["diaVencimento", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaVencimento")]],
      ["diaVencimentoFatorMesBase", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaVencimentoFatorMesBase")]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => MeioPagamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "meioPagamento"),
    :schema_element => [
      ["agencia", ["SOAP::SOAPString", XSD::QName.new(nil, "agencia")], [0, 1]],
      ["agenciaDigito", ["SOAP::SOAPString", XSD::QName.new(nil, "agenciaDigito")], [0, 1]],
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["banco", ["TipoBanco", XSD::QName.new(nil, "banco")], [0, 1]],
      ["conta", ["SOAP::SOAPString", XSD::QName.new(nil, "conta")], [0, 1]],
      ["contaDigito", ["SOAP::SOAPString", XSD::QName.new(nil, "contaDigito")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["exibirNoFormularioDeContratoExterno", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "exibirNoFormularioDeContratoExterno")]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["instrucoesDeCobrancaLinha1", ["SOAP::SOAPString", XSD::QName.new(nil, "instrucoesDeCobrancaLinha1")], [0, 1]],
      ["instrucoesDeCobrancaLinha2", ["SOAP::SOAPString", XSD::QName.new(nil, "instrucoesDeCobrancaLinha2")], [0, 1]],
      ["instrucoesDeCobrancaLinha3", ["SOAP::SOAPString", XSD::QName.new(nil, "instrucoesDeCobrancaLinha3")], [0, 1]],
      ["instrucoesDeCobrancaLinha4", ["SOAP::SOAPString", XSD::QName.new(nil, "instrucoesDeCobrancaLinha4")], [0, 1]],
      ["instrucoesDeCobrancaLinha5", ["SOAP::SOAPString", XSD::QName.new(nil, "instrucoesDeCobrancaLinha5")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["moraMensalAposVencimento", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "moraMensalAposVencimento")], [0, 1]],
      ["multaAposVencimento", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "multaAposVencimento")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["nossoNumeroInicial", ["SOAP::SOAPLong", XSD::QName.new(nil, "nossoNumeroInicial")], [0, 1]],
      ["numeroCarteira", ["SOAP::SOAPString", XSD::QName.new(nil, "numeroCarteira")], [0, 1]],
      ["numeroConvenio", ["SOAP::SOAPString", XSD::QName.new(nil, "numeroConvenio")], [0, 1]],
      ["quantidadeDiasCobrancaAposVencimento", ["SOAP::SOAPInt", XSD::QName.new(nil, "quantidadeDiasCobrancaAposVencimento")]],
      ["quantidadeDiasConsiderarVencido", ["SOAP::SOAPInt", XSD::QName.new(nil, "quantidadeDiasConsiderarVencido")]],
      ["quantidadeDiasEnvioAntecipado", ["SOAP::SOAPInt", XSD::QName.new(nil, "quantidadeDiasEnvioAntecipado")]],
      ["tarifaBoleto", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "tarifaBoleto")], [0, 1]],
      ["tipoMeioPagamento", ["TipoMeioPagamento", XSD::QName.new(nil, "tipoMeioPagamento")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AdicionarRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRelacionamento"),
    :schema_element => [
      ["arg0", ["Relacionamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarRelacionamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRelacionamentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarCicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCicloFaturamento"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarCicloFaturamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCicloFaturamentoResponse"),
    :schema_element => [
      ["v_return", ["CicloFaturamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterConfiguracao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterConfiguracao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterConfiguracaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterConfiguracaoResponse"),
    :schema_element => [
      ["v_return", ["Configuracao", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Configuracao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "configuracao"),
    :schema_element => [
      ["cadastrosPrefeitura", ["Cidade[]", XSD::QName.new(nil, "cadastrosPrefeitura")], [0, nil]],
      ["caminhoCabecalhoEmail", ["SOAP::SOAPString", XSD::QName.new(nil, "caminhoCabecalhoEmail")], [0, 1]],
      ["caminhoLogo", ["SOAP::SOAPString", XSD::QName.new(nil, "caminhoLogo")], [0, 1]],
      ["certificadoDigitalCaminho", ["SOAP::SOAPString", XSD::QName.new(nil, "certificadoDigitalCaminho")], [0, 1]],
      ["certificadoDigitalSenha", ["SOAP::SOAPString", XSD::QName.new(nil, "certificadoDigitalSenha")], [0, 1]],
      ["codigoCNAE", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoCNAE")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["domicilioFaturamento", ["Cidade", XSD::QName.new(nil, "domicilioFaturamento")], [0, 1]],
      ["emailCobranca", ["SOAP::SOAPString", XSD::QName.new(nil, "emailCobranca")], [0, 1]],
      ["emailFinanceiro", ["SOAP::SOAPString", XSD::QName.new(nil, "emailFinanceiro")], [0, 1]],
      ["entityIdMyFinance", ["SOAP::SOAPInt", XSD::QName.new(nil, "entityIdMyFinance")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["incentivadorCultural", ["SOAP::SOAPString", XSD::QName.new(nil, "incentivadorCultural")], [0, 1]],
      ["merchantKeyMundipagg", ["SOAP::SOAPString", XSD::QName.new(nil, "merchantKeyMundipagg")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["numeroLOTE", ["SOAP::SOAPLong", XSD::QName.new(nil, "numeroLOTE")], [0, 1]],
      ["numeroRPS", ["SOAP::SOAPLong", XSD::QName.new(nil, "numeroRPS")], [0, 1]],
      ["referenciaCR", ["SOAP::SOAPInt", XSD::QName.new(nil, "referenciaCR")], [0, 1]],
      ["referenciaCSCR", ["SOAP::SOAPInt", XSD::QName.new(nil, "referenciaCSCR")], [0, 1]],
      ["regimeTributario", ["SOAP::SOAPString", XSD::QName.new(nil, "regimeTributario")], [0, 1]],
      ["senhaMastersaf", ["SOAP::SOAPString", XSD::QName.new(nil, "senhaMastersaf")], [0, 1]],
      ["serieCR", ["SOAP::SOAPString", XSD::QName.new(nil, "serieCR")], [0, 1]],
      ["serieCSCR", ["SOAP::SOAPString", XSD::QName.new(nil, "serieCSCR")], [0, 1]],
      ["serieRPS", ["SOAP::SOAPString", XSD::QName.new(nil, "serieRPS")], [0, 1]],
      ["seriesCliente", ["Serie[]", XSD::QName.new(nil, "seriesCliente")], [0, nil]],
      ["seriesContrato", ["Serie[]", XSD::QName.new(nil, "seriesContrato")], [0, nil]],
      ["simplesNacional", ["SOAP::SOAPString", XSD::QName.new(nil, "simplesNacional")], [0, 1]],
      ["smtpEmailDeCobranca", ["SOAP::SOAPString", XSD::QName.new(nil, "smtpEmailDeCobranca")], [0, 1]],
      ["subSerieCR", ["SOAP::SOAPString", XSD::QName.new(nil, "subSerieCR")], [0, 1]],
      ["subSerieCSCR", ["SOAP::SOAPString", XSD::QName.new(nil, "subSerieCSCR")], [0, 1]],
      ["tipoAmbiente", ["TipoAmbiente", XSD::QName.new(nil, "tipoAmbiente")], [0, 1]],
      ["tipoStatusEmailCobranca", ["TipoStatusEmailCobranca", XSD::QName.new(nil, "tipoStatusEmailCobranca")], [0, 1]],
      ["tokenMyFinance", ["SOAP::SOAPString", XSD::QName.new(nil, "tokenMyFinance")], [0, 1]],
      ["usuarioMastersaf", ["SOAP::SOAPString", XSD::QName.new(nil, "usuarioMastersaf")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["webserviceMastersaf", ["SOAP::SOAPString", XSD::QName.new(nil, "webserviceMastersaf")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Serie,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "serie"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["subSeries", ["SubSerie[]", XSD::QName.new(nil, "subSeries")], [0, nil]],
      ["tamanhoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "tamanhoReferencia")]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPString", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => SubSerie,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "subSerie"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["referencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "referencia")]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPString", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerRelacionamento"),
    :schema_element => [
      ["arg0", ["Relacionamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverRelacionamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerRelacionamentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => RemoverContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerContato"),
    :schema_element => [
      ["arg0", ["Contato", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverContatoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerContatoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterSimplesNacional,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterSimplesNacional"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterSimplesNacionalResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterSimplesNacionalResponse"),
    :schema_element => [
      ["v_return", ["SimplesNacional", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => SimplesNacional,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "simplesNacional"),
    :schema_element => [
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarMotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["MotivoCancelamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarMotivoCancelamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoCancelamentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNF", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteNFResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCR", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCR"),
    :schema_element => [
      ["anoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "anoReferencia")], [0, 1]],
      ["boletos", ["ClienteCRTransacaoBoleto[]", XSD::QName.new(nil, "boletos")], [0, nil]],
      ["cartoes", ["ClienteCRTransacaoCartao[]", XSD::QName.new(nil, "cartoes")], [0, nil]],
      ["cliente", ["Cliente", XSD::QName.new(nil, "cliente")], [0, 1]],
      ["clienteCRCadastroUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRCadastroUuid")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["dataBaixa", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataBaixa")], [0, 1]],
      ["dataCancelamento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataCancelamento")], [0, 1]],
      ["dataEmissao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataEmissao")], [0, 1]],
      ["dataEnvio", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataEnvio")], [0, 1]],
      ["dataEnvioAtraso", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataEnvioAtraso")], [0, 1]],
      ["dataPagamento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataPagamento")], [0, 1]],
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["dataVencimentoOriginal", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimentoOriginal")], [0, 1]],
      ["depositos", ["ClienteCRTransacaoDeposito[]", XSD::QName.new(nil, "depositos")], [0, nil]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mesReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "mesReferencia")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["motivoCancelamento", ["MotivoCancelamento", XSD::QName.new(nil, "motivoCancelamento")], [0, 1]],
      ["notasFiscais", ["ClienteNF[]", XSD::QName.new(nil, "notasFiscais")], [0, nil]],
      ["projecaoFaturamento", ["ProjecaoFaturamento", XSD::QName.new(nil, "projecaoFaturamento")], [0, 1]],
      ["referencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "referencia")], [0, 1]],
      ["serie", ["SOAP::SOAPString", XSD::QName.new(nil, "serie")], [0, 1]],
      ["servicosCR", ["ClienteServicoCR[]", XSD::QName.new(nil, "servicosCR")], [0, nil]],
      ["subSerie", ["SOAP::SOAPString", XSD::QName.new(nil, "subSerie")], [0, 1]],
      ["tipoCREstado", ["TipoCREstado", XSD::QName.new(nil, "tipoCREstado")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valorDesconto", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorDesconto")], [0, 1]],
      ["valorIOF", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorIOF")], [0, 1]],
      ["valorImposto", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorImposto")], [0, 1]],
      ["valorJurosMora", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorJurosMora")], [0, 1]],
      ["valorMulta", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorMulta")], [0, 1]],
      ["valorPago", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorPago")], [0, 1]],
      ["valorTarifaBoleto", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorTarifaBoleto")], [0, 1]],
      ["valorTotal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorTotal")], [0, 1]],
      ["valorTotalOriginal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorTotalOriginal")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteCRTransacaoBoleto,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacaoBoleto"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacao"),
    :schema_element => [
      ["consulta", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "consulta")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoClienteCRTransacao", ["TipoClienteCRTransacao", XSD::QName.new(nil, "tipoClienteCRTransacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["boletoUrl", ["SOAP::SOAPString", XSD::QName.new(nil, "boletoUrl")], [0, 1]],
      ["chavePedido", ["SOAP::SOAPString", XSD::QName.new(nil, "chavePedido")], [0, 1]],
      ["chaveRequisicao", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveRequisicao")], [0, 1]],
      ["chaveTransacao", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveTransacao")], [0, 1]],
      ["codigoBarra", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoBarra")], [0, 1]],
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["nossoNumero", ["SOAP::SOAPLong", XSD::QName.new(nil, "nossoNumero")], [0, 1]],
      ["referenciaPedido", ["SOAP::SOAPString", XSD::QName.new(nil, "referenciaPedido")], [0, 1]],
      ["referenciaTransacao", ["SOAP::SOAPString", XSD::QName.new(nil, "referenciaTransacao")], [0, 1]],
      ["situacaoPedido", ["TipoStatusGateway", XSD::QName.new(nil, "situacaoPedido")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteCRTransacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacao"),
    :schema_element => [
      ["consulta", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "consulta")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoClienteCRTransacao", ["TipoClienteCRTransacao", XSD::QName.new(nil, "tipoClienteCRTransacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteCRTransacaoCartao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacaoCartao"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacao"),
    :schema_element => [
      ["consulta", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "consulta")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoClienteCRTransacao", ["TipoClienteCRTransacao", XSD::QName.new(nil, "tipoClienteCRTransacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["chaveCompra", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveCompra")], [0, 1]],
      ["chavePedido", ["SOAP::SOAPString", XSD::QName.new(nil, "chavePedido")], [0, 1]],
      ["chaveRequisicao", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveRequisicao")], [0, 1]],
      ["chaveTransacao", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveTransacao")], [0, 1]],
      ["codigoAdquirente", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoAdquirente")], [0, 1]],
      ["codigoAutorizacao", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoAutorizacao")], [0, 1]],
      ["dataTransacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataTransacao")], [0, 1]],
      ["identificacaoTransacao", ["SOAP::SOAPString", XSD::QName.new(nil, "identificacaoTransacao")], [0, 1]],
      ["mensagemAdquirente", ["SOAP::SOAPString", XSD::QName.new(nil, "mensagemAdquirente")], [0, 1]],
      ["numero", ["SOAP::SOAPString", XSD::QName.new(nil, "numero")], [0, 1]],
      ["referenciaPedido", ["SOAP::SOAPString", XSD::QName.new(nil, "referenciaPedido")], [0, 1]],
      ["referenciaTransacao", ["SOAP::SOAPString", XSD::QName.new(nil, "referenciaTransacao")], [0, 1]],
      ["situacaoPedido", ["TipoStatusGateway", XSD::QName.new(nil, "situacaoPedido")], [0, 1]],
      ["tipoBandeira", ["TipoBandeira", XSD::QName.new(nil, "tipoBandeira")], [0, 1]],
      ["titular", ["SOAP::SOAPString", XSD::QName.new(nil, "titular")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteCRTransacaoDeposito,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacaoDeposito"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacao"),
    :schema_element => [
      ["consulta", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "consulta")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoClienteCRTransacao", ["TipoClienteCRTransacao", XSD::QName.new(nil, "tipoClienteCRTransacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["situacaoPedido", ["TipoStatusGateway", XSD::QName.new(nil, "situacaoPedido")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCRResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AdicionarCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarCliente"),
    :schema_element => [
      ["arg0", ["Cliente", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarCodigoCNAE,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCodigoCNAE"),
    :schema_element => [
      ["arg0", ["CodigoCNAEFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => CodigoCNAEFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "codigoCNAEFilter"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarCodigoCNAEResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCodigoCNAEResponse"),
    :schema_element => [
      ["v_return", ["CodigoCNAE[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterNaturezaOperacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterNaturezaOperacao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterNaturezaOperacaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterNaturezaOperacaoResponse"),
    :schema_element => [
      ["v_return", ["NaturezaOperacao", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => NaturezaOperacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "naturezaOperacao"),
    :schema_element => [
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoCR"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteServicoCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoCR", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => GerarCobranca,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "gerarCobranca"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => GerarCobrancaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "gerarCobrancaResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteNF"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteNFResponse"),
    :schema_element => [
      ["v_return", ["ClienteNF", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => EmailCobranca,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "emailCobranca"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => EmailCobrancaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "emailCobrancaResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarSegmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarSegmento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarSegmentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarSegmentoResponse"),
    :schema_element => [
      ["v_return", ["Segmento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarFamilia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarFamilia"),
    :schema_element => [
      ["arg0", ["Familia", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarFamiliaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarFamiliaResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarMeioPagamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMeioPagamento"),
    :schema_element => [
      ["arg0", ["MeioPagamentoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => MeioPagamentoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "meioPagamentoFilter"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")], [0, 1]],
      ["tiposMeioPagamento", ["TipoMeioPagamento[]", XSD::QName.new(nil, "tiposMeioPagamento")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarMeioPagamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMeioPagamentoResponse"),
    :schema_element => [
      ["v_return", ["MeioPagamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCR", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCRResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCR"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteCR", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarRamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRamoAtividade"),
    :schema_element => [
      ["arg0", ["RamoAtividade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarRamoAtividadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRamoAtividadeResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterUuidUsuario,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterUuidUsuario"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterUuidUsuarioResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterUuidUsuarioResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterMeioPagamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMeioPagamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterMeioPagamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMeioPagamentoResponse"),
    :schema_element => [
      ["v_return", ["MeioPagamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterEndereco"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterEnderecoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterEnderecoResponse"),
    :schema_element => [
      ["v_return", ["Endereco", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => SolicitarClienteCartao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "solicitarClienteCartao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => SolicitarClienteCartaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "solicitarClienteCartaoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => CancelarClienteServicoPorClienteServicoUuid,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => CancelarClienteServicoPorClienteServicoUuidResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarEndereco"),
    :schema_element => [
      ["arg0", ["Endereco", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarEnderecoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarEnderecoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => RegraClienteServicoImpostos,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "regraClienteServicoImpostos"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RegraClienteServicoImpostosResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "regraClienteServicoImpostosResponse"),
    :schema_element => [
      ["v_return", ["ClienteServico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterGerente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterGerente"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterGerenteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterGerenteResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTotalClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCRFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteCRFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRFilter"),
    :schema_element => [
      ["anoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "anoReferencia")], [0, 1]],
      ["boleto", ["SOAP::SOAPLong", XSD::QName.new(nil, "boleto")], [0, 1]],
      ["cicloFaturamento", ["CicloFaturamento", XSD::QName.new(nil, "cicloFaturamento")], [0, 1]],
      ["clienteServicoCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteServicoCRUuid")], [0, 1]],
      ["clienteServicoUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteServicoUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["enviado", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "enviado")], [0, 1]],
      ["generalFilter", ["GeneralFilter", XSD::QName.new(nil, "generalFilter")], [0, 1]],
      ["mesReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "mesReferencia")], [0, 1]],
      ["notaFiscal", ["SOAP::SOAPInteger", XSD::QName.new(nil, "notaFiscal")], [0, 1]],
      ["pago", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "pago")], [0, 1]],
      ["possuiNFEmitido", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "possuiNFEmitido")], [0, 1]],
      ["projecaoFaturamento", ["ProjecaoFaturamento[]", XSD::QName.new(nil, "projecaoFaturamento")], [0, nil]],
      ["rangeDataPagamento", ["SOAP::SOAPDateTime[]", XSD::QName.new(nil, "rangeDataPagamento")], [0, nil]],
      ["rangeDataVencimento", ["SOAP::SOAPDateTime[]", XSD::QName.new(nil, "rangeDataVencimento")], [0, nil]],
      ["rangeDataVencimentoOriginal", ["SOAP::SOAPDateTime[]", XSD::QName.new(nil, "rangeDataVencimentoOriginal")], [0, nil]],
      ["tipoCREstado", ["TipoCREstado[]", XSD::QName.new(nil, "tipoCREstado")], [0, nil]],
      ["vencido", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "vencido")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTotalClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCRResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  EncodedRegistry.register(
    :class => SuspenderClienteServicoPorClienteServicoUuid,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "suspenderClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => SuspenderClienteServicoPorClienteServicoUuidResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "suspenderClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AdicionarEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarEndereco"),
    :schema_element => [
      ["arg0", ["Endereco", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarEnderecoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarEnderecoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => RemoverCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerCliente"),
    :schema_element => [
      ["arg0", ["Cliente", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterPadraoPreenchimento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterPadraoPreenchimento"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterPadraoPreenchimentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterPadraoPreenchimentoResponse"),
    :schema_element => [
      ["v_return", ["PadraoPreenchimento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => PadraoPreenchimento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "padraoPreenchimento"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["bloqueioFaturamento", ["BloqueioFaturamento", XSD::QName.new(nil, "bloqueioFaturamento")], [0, 1]],
      ["cicloFaturamento", ["CicloFaturamento", XSD::QName.new(nil, "cicloFaturamento")], [0, 1]],
      ["cidade", ["Cidade", XSD::QName.new(nil, "cidade")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["documentoFiscal", ["DocumentoFiscal", XSD::QName.new(nil, "documentoFiscal")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["familia", ["Familia", XSD::QName.new(nil, "familia")], [0, 1]],
      ["fatorDeReajusteDaRenovacao", ["IndiceEconomico", XSD::QName.new(nil, "fatorDeReajusteDaRenovacao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["meioPagamento", ["MeioPagamento", XSD::QName.new(nil, "meioPagamento")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["naturezaOperacao", ["SOAP::SOAPString", XSD::QName.new(nil, "naturezaOperacao")], [0, 1]],
      ["orgaoPublico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "orgaoPublico")]],
      ["periodicidade", ["Periodicidade", XSD::QName.new(nil, "periodicidade")], [0, 1]],
      ["quantidadeTipoVigencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "quantidadeTipoVigencia")], [0, 1]],
      ["ramoAtividade", ["RamoAtividade", XSD::QName.new(nil, "ramoAtividade")], [0, 1]],
      ["recebeCobranca", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "recebeCobranca")]],
      ["renovacaoAutomatica", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "renovacaoAutomatica")]],
      ["tipoContato", ["TipoContato", XSD::QName.new(nil, "tipoContato")], [0, 1]],
      ["tipoEndereco", ["TipoEndereco", XSD::QName.new(nil, "tipoEndereco")], [0, 1]],
      ["tipoFaturamento", ["TipoFaturamento", XSD::QName.new(nil, "tipoFaturamento")], [0, 1]],
      ["tipoPessoa", ["TipoPessoa", XSD::QName.new(nil, "tipoPessoa")], [0, 1]],
      ["tipoServico", ["TipoServico", XSD::QName.new(nil, "tipoServico")], [0, 1]],
      ["tipoVigencia", ["TipoVigencia", XSD::QName.new(nil, "tipoVigencia")], [0, 1]],
      ["usarFatorDeReajusteAutomatico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "usarFatorDeReajusteAutomatico")]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverMotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["MotivoSuspensao", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverMotivoSuspensaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoSuspensaoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarArquivo"),
    :schema_element => [
      ["arg0", ["Arquivo", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarArquivoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterServicoAliquotaPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterServicoAliquotaPrestador"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterServicoAliquotaPrestadorResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterServicoAliquotaPrestadorResponse"),
    :schema_element => [
      ["v_return", ["ServicoAliquotaPrestador", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTemplateEmail,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTemplateEmail"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarTemplateEmailResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTemplateEmailResponse"),
    :schema_element => [
      ["v_return", ["TemplateEmail[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarIndiceEconomico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomico"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarIndiceEconomicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomicoResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomico[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterTributacaoMunicipio,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTributacaoMunicipio"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterTributacaoMunicipioResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTributacaoMunicipioResponse"),
    :schema_element => [
      ["v_return", ["TributacaoMunicipio", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => TributacaoMunicipio,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tributacaoMunicipio"),
    :schema_element => [
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarFamilia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarFamilia"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarFamiliaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarFamiliaResponse"),
    :schema_element => [
      ["v_return", ["Familia[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarPeriodicidade"),
    :schema_element => [
      ["arg0", ["PeriodicidadeFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => PeriodicidadeFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "periodicidadeFilter"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarPeriodicidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarPeriodicidadeResponse"),
    :schema_element => [
      ["v_return", ["Periodicidade[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarConfiguracao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarConfiguracao"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarConfiguracaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarConfiguracaoResponse"),
    :schema_element => [
      ["v_return", ["Configuracao[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServicoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoFilter"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["codigoContrato", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoContrato")], [0, 1]],
      ["excluindoUnidadesMetricas", ["UnidadeMetrica[]", XSD::QName.new(nil, "excluindoUnidadesMetricas")], [0, nil]],
      ["servicoEstado", ["ServicoEstado", XSD::QName.new(nil, "servicoEstado")], [0, 1]],
      ["servicoUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "servicoUuid")], [0, 1]],
      ["tipoConsumoEstado", ["TipoConsumoEstado", XSD::QName.new(nil, "tipoConsumoEstado")], [0, 1]],
      ["tipoServico", ["TipoServico", XSD::QName.new(nil, "tipoServico")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServico[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarRamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRamoAtividade"),
    :schema_element => [
      ["arg0", ["RamoAtividade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarRamoAtividadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRamoAtividadeResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarUf,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarUf"),
    :schema_element => [
      ["arg0", ["UfFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => UfFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "ufFilter"),
    :schema_element => [
      ["sigla", ["SOAP::SOAPString", XSD::QName.new(nil, "sigla")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarUfResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarUfResponse"),
    :schema_element => [
      ["v_return", ["Uf[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverRamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerRamoAtividade"),
    :schema_element => [
      ["arg0", ["RamoAtividade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverRamoAtividadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerRamoAtividadeResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarPeriodicidade"),
    :schema_element => [
      ["arg0", ["Periodicidade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarPeriodicidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarPeriodicidadeResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => RemoverClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNF", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteNFResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ReativarCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "reativarCliente"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "arg1")]]
    ]
  )

  EncodedRegistry.register(
    :class => ReativarClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "reativarClienteResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AdicionarClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCR", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarClienteServicoCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoCRResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterCicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCicloFaturamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterCicloFaturamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCicloFaturamentoResponse"),
    :schema_element => [
      ["v_return", ["CicloFaturamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumo", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoConsumoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarServico"),
    :schema_element => [
      ["arg0", ["Servico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarServicoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterUf,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterUf"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterUfResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterUfResponse"),
    :schema_element => [
      ["v_return", ["Uf", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTotalServicoAliquotaPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServicoAliquotaPrestador"),
    :schema_element => [
      ["arg0", ["ServicoAliquotaPrestadorFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTotalServicoAliquotaPrestadorResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServicoAliquotaPrestadorResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverFamilia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerFamilia"),
    :schema_element => [
      ["arg0", ["Familia", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverFamiliaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerFamiliaResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarContato"),
    :schema_element => [
      ["arg0", ["ContatoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ContatoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "contatoFilter"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarContatoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarContatoResponse"),
    :schema_element => [
      ["v_return", ["Contato[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => EnviarNFsEmissao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "enviarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => EnviarNFsEmissaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "enviarNFsEmissaoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterSegmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterSegmento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterSegmentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterSegmentoResponse"),
    :schema_element => [
      ["v_return", ["Segmento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterMotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterMotivoSuspensaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoSuspensaoResponse"),
    :schema_element => [
      ["v_return", ["MotivoSuspensao", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNF", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteNFResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => RemoverMotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["MotivoCancelamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverMotivoCancelamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoCancelamentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => RemoverSegmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerSegmento"),
    :schema_element => [
      ["arg0", ["Segmento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverSegmentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerSegmentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterRegra,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegra"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterRegraResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegraResponse"),
    :schema_element => [
      ["v_return", ["Regra", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterIndiceEconomico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterIndiceEconomicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomicoResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarArquivo"),
    :schema_element => [
      ["arg0", ["Arquivo", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarArquivoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarCicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarCicloFaturamento"),
    :schema_element => [
      ["arg0", ["CicloFaturamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarCicloFaturamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarCicloFaturamentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AdicionarClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCR", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteCRResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => RemoverClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumo", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoConsumoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarTributacaoMunicipio,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTributacaoMunicipio"),
    :schema_element => [
      ["arg0", ["TributacaoMunicipioFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => TributacaoMunicipioFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tributacaoMunicipioFilter"),
    :schema_element => [
      ["cidadeUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "cidadeUuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarTributacaoMunicipioResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTributacaoMunicipioResponse"),
    :schema_element => [
      ["v_return", ["TributacaoMunicipio[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarSimplesNacional,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarSimplesNacional"),
    :schema_element => [
      ["arg0", ["SimplesNacionalFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => SimplesNacionalFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "simplesNacionalFilter"),
    :schema_element => [
      ["cidadeUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "cidadeUuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarSimplesNacionalResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarSimplesNacionalResponse"),
    :schema_element => [
      ["v_return", ["SimplesNacional[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarPeriodicidade"),
    :schema_element => [
      ["arg0", ["Periodicidade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarPeriodicidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarPeriodicidadeResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AdicionarClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumo", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoConsumoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterIndiceEconomicoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomicoValor"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterIndiceEconomicoValorResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomicoValorResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomicoValor", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarArquivo"),
    :schema_element => [
      ["arg0", ["ArquivoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarArquivoResponse"),
    :schema_element => [
      ["v_return", ["Arquivo[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => FecharClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "fecharClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => FecharClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "fecharClienteServicoConsumoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterRamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRamoAtividade"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterRamoAtividadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRamoAtividadeResponse"),
    :schema_element => [
      ["v_return", ["RamoAtividade", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerPeriodicidade"),
    :schema_element => [
      ["arg0", ["Periodicidade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverPeriodicidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerPeriodicidadeResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCRFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteCR[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarCidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCidade"),
    :schema_element => [
      ["arg0", ["CidadeFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => CidadeFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cidadeFilter"),
    :schema_element => [
      ["disponivelParaCadastro", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "disponivelParaCadastro")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["uf", ["Uf", XSD::QName.new(nil, "uf")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarCidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCidadeResponse"),
    :schema_element => [
      ["v_return", ["Cidade[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerEndereco"),
    :schema_element => [
      ["arg0", ["Endereco", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverEnderecoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerEnderecoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AutorizarClienteCartao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "autorizarClienteCartao"),
    :schema_element => [
      ["arg0", ["ClienteCartao", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AutorizarClienteCartaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "autorizarClienteCartaoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AdicionarClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarNaturezaOperacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarNaturezaOperacao"),
    :schema_element => [
      ["arg0", ["NaturezaOperacaoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => NaturezaOperacaoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "naturezaOperacaoFilter"),
    :schema_element => [
      ["cidadeUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "cidadeUuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarNaturezaOperacaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarNaturezaOperacaoResponse"),
    :schema_element => [
      ["v_return", ["NaturezaOperacao[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterTemplateEmailPorTipo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmailPorTipo"),
    :schema_element => [
      ["arg0", ["TipoTemplateEmail", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterTemplateEmailPorTipoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmailPorTipoResponse"),
    :schema_element => [
      ["v_return", ["TemplateEmail", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCRFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteServicoCRFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoCRFilter"),
    :schema_element => [
      ["uuidCliente", ["SOAP::SOAPString", XSD::QName.new(nil, "uuidCliente")], [0, 1]],
      ["uuidClienteCR", ["SOAP::SOAPString", XSD::QName.new(nil, "uuidClienteCR")], [0, 1]],
      ["uuidClienteServico", ["SOAP::SOAPString", XSD::QName.new(nil, "uuidClienteServico")], [0, 1]],
      ["uuidClienteServicoConsumo", ["SOAP::SOAPString", XSD::QName.new(nil, "uuidClienteServicoConsumo")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarClienteServicoCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoCR[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverCicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerCicloFaturamento"),
    :schema_element => [
      ["arg0", ["CicloFaturamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverCicloFaturamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerCicloFaturamentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoConsumoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoConsumo", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCR", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteServicoCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoCRResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarMotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoCancelamento"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarMotivoCancelamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoCancelamentoResponse"),
    :schema_element => [
      ["v_return", ["MotivoCancelamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarMotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["MotivoCancelamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarMotivoCancelamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoCancelamentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterMotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterMotivoCancelamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoCancelamentoResponse"),
    :schema_element => [
      ["v_return", ["MotivoCancelamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarCicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarCicloFaturamento"),
    :schema_element => [
      ["arg0", ["CicloFaturamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarCicloFaturamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarCicloFaturamentoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AdicionarContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarContato"),
    :schema_element => [
      ["arg0", ["Contato", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarContatoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarContatoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCliente"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteResponse"),
    :schema_element => [
      ["v_return", ["Cliente", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => CancelarClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteCR"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => CancelarClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteCRResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => AlterarClienteServicoPorClienteServicoUuid,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AlterarClienteServicoPorClienteServicoUuidResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarRegimeTributario,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegimeTributario"),
    :schema_element => [
      ["arg0", ["RegimeTributarioFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RegimeTributarioFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "regimeTributarioFilter"),
    :schema_element => [
      ["cidadeUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "cidadeUuid")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarRegimeTributarioResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegimeTributarioResponse"),
    :schema_element => [
      ["v_return", ["RegimeTributario[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteCRTransacaoBoleto,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRTransacaoBoleto"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterClienteCRTransacaoBoletoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRTransacaoBoletoResponse"),
    :schema_element => [
      ["v_return", ["ClienteCRTransacaoBoleto", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverClienteCartao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCartao"),
    :schema_element => [
      ["arg0", ["ClienteCartao", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverClienteCartaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCartaoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCliente"),
    :schema_element => [
      ["arg0", ["ClienteFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClienteFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteFilter"),
    :schema_element => [
      ["cpfCnpj", ["SOAP::SOAPString", XSD::QName.new(nil, "cpfCnpj")], [0, 1]],
      ["generalFilter", ["GeneralFilter", XSD::QName.new(nil, "generalFilter")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteResponse"),
    :schema_element => [
      ["v_return", ["Cliente[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerArquivo"),
    :schema_element => [
      ["arg0", ["Arquivo", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RemoverArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerArquivoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterCidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCidade"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterCidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCidadeResponse"),
    :schema_element => [
      ["v_return", ["Cidade", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClonarServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clonarServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ClonarServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clonarServicoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ObterPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterPeriodicidade"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ObterPeriodicidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterPeriodicidadeResponse"),
    :schema_element => [
      ["v_return", ["Periodicidade", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarServico"),
    :schema_element => [
      ["arg0", ["ServicoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarServicoResponse"),
    :schema_element => [
      ["v_return", ["Servico[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => ListarMotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoSuspensao"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ListarMotivoSuspensaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoSuspensaoResponse"),
    :schema_element => [
      ["v_return", ["MotivoSuspensao[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarServico"),
    :schema_element => [
      ["arg0", ["Servico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => AdicionarServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarServicoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => ConsultarNFsEmissao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "consultarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ConsultarNFsEmissaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "consultarNFsEmissaoResponse"),
    :schema_element => []
  )

  EncodedRegistry.register(
    :class => SmartbillAPIMessage,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "SmartbillAPIMessage"),
    :schema_element => [
      ["mensagem", ["SOAP::SOAPString", XSD::QName.new(nil, "mensagem")]],
      ["classe", ["SOAP::SOAPString", XSD::QName.new(nil, "classe")]],
      ["metodo", ["SOAP::SOAPString", XSD::QName.new(nil, "metodo")]],
      ["codigo", ["Status", XSD::QName.new(nil, "codigo")]],
      ["objetoRetorno", ["SmartbillObjetoRetorno", XSD::QName.new(nil, "objetoRetorno")]],
      ["objetosRetorno", ["SmartbillObjetoRetorno[]", XSD::QName.new(nil, "objetosRetorno")], [0, nil]],
      ["smartbillStackTrace", ["SOAP::SOAPString", XSD::QName.new(nil, "smartbillStackTrace")]]
    ]
  )

  EncodedRegistry.register(
    :class => Status,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "status")
  )

  EncodedRegistry.register(
    :class => TipoEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoEndereco")
  )

  EncodedRegistry.register(
    :class => TipoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoConsumo")
  )

  EncodedRegistry.register(
    :class => TipoConsumoEstado,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoConsumoEstado")
  )

  EncodedRegistry.register(
    :class => UnidadeMedida,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "unidadeMedida")
  )

  EncodedRegistry.register(
    :class => UnidadeMetrica,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "unidadeMetrica")
  )

  EncodedRegistry.register(
    :class => TipoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoValor")
  )

  EncodedRegistry.register(
    :class => TipoContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoContato")
  )

  EncodedRegistry.register(
    :class => TipoIndiceEconomico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoIndiceEconomico")
  )

  EncodedRegistry.register(
    :class => TipoParcela,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoParcela")
  )

  EncodedRegistry.register(
    :class => TipoPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoPeriodicidade")
  )

  EncodedRegistry.register(
    :class => TipoServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoServico")
  )

  EncodedRegistry.register(
    :class => ServicoEstado,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoEstado")
  )

  EncodedRegistry.register(
    :class => TipoFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoFaturamento")
  )

  EncodedRegistry.register(
    :class => TipoVigencia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoVigencia")
  )

  EncodedRegistry.register(
    :class => ProjecaoFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "projecaoFaturamento")
  )

  EncodedRegistry.register(
    :class => TipoCREstado,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoCREstado")
  )

  EncodedRegistry.register(
    :class => TipoAmbiente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoAmbiente")
  )

  EncodedRegistry.register(
    :class => TipoNFCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoNFCancelamento")
  )

  EncodedRegistry.register(
    :class => TipoNFEstado,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoNFEstado")
  )

  EncodedRegistry.register(
    :class => TipoRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoRelacionamento")
  )

  EncodedRegistry.register(
    :class => Criticidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "criticidade")
  )

  EncodedRegistry.register(
    :class => TipoPessoa,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoPessoa")
  )

  EncodedRegistry.register(
    :class => TipoLancamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoLancamento")
  )

  EncodedRegistry.register(
    :class => TipoMeioPagamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoMeioPagamento")
  )

  EncodedRegistry.register(
    :class => TipoTemplateEmail,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoTemplateEmail")
  )

  EncodedRegistry.register(
    :class => BloqueioFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "bloqueioFaturamento")
  )

  EncodedRegistry.register(
    :class => TipoBandeira,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoBandeira")
  )

  EncodedRegistry.register(
    :class => DocumentoFiscal,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "documentoFiscal")
  )

  EncodedRegistry.register(
    :class => TipoBanco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoBanco")
  )

  EncodedRegistry.register(
    :class => TipoStatusEmailCobranca,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoStatusEmailCobranca")
  )

  EncodedRegistry.register(
    :class => TipoStatusGateway,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoStatusGateway")
  )

  EncodedRegistry.register(
    :class => TipoClienteCRTransacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoClienteCRTransacao")
  )

  LiteralRegistry.register(
    :class => ReabrirClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "reabrirClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ReabrirClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "reabrirClienteServicoConsumoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => SmartbillObjetoRetorno,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "smartbillObjetoRetorno"),
    :schema_element => [
      ["codigo", ["Status", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["mensagem", ["SOAP::SOAPString", XSD::QName.new(nil, "mensagem")], [0, 1]],
      ["modelo", ["SOAP::SOAPString", XSD::QName.new(nil, "modelo")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["webservice", ["SOAP::SOAPString", XSD::QName.new(nil, "webservice")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNFFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteNFFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteNFFilter"),
    :schema_element => [
      ["clienteCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteNFResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarEndereco"),
    :schema_element => [
      ["arg0", ["EnderecoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => EnderecoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "enderecoFilter"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["enderecoDeFaturamento", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "enderecoDeFaturamento")], [0, 1]],
      ["tipoEndereco", ["TipoEndereco", XSD::QName.new(nil, "tipoEndereco")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarEnderecoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarEnderecoResponse"),
    :schema_element => [
      ["v_return", ["Endereco[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => Endereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "endereco"),
    :schema_element => [
      ["bairro", ["SOAP::SOAPString", XSD::QName.new(nil, "bairro")], [0, 1]],
      ["cep", ["SOAP::SOAPString", XSD::QName.new(nil, "cep")], [0, 1]],
      ["cidade", ["Cidade", XSD::QName.new(nil, "cidade")], [0, 1]],
      ["complemento", ["SOAP::SOAPString", XSD::QName.new(nil, "complemento")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["enderecoDeFaturamento", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "enderecoDeFaturamento")]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["logradouro", ["SOAP::SOAPString", XSD::QName.new(nil, "logradouro")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["numero", ["SOAP::SOAPInt", XSD::QName.new(nil, "numero")], [0, 1]],
      ["tipoEndereco", ["TipoEndereco", XSD::QName.new(nil, "tipoEndereco")], [0, 1]],
      ["uf", ["Uf", XSD::QName.new(nil, "uf")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Cidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cidade"),
    :schema_element => [
      ["disponivelParaCadastro", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "disponivelParaCadastro")]],
      ["ibge", ["SOAP::SOAPLong", XSD::QName.new(nil, "ibge")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["uf", ["Uf", XSD::QName.new(nil, "uf")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Uf,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "uf"),
    :schema_element => [
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["sigla", ["SOAP::SOAPString", XSD::QName.new(nil, "sigla")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarIndiceEconomicoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomicoValor"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarIndiceEconomicoValorResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomicoValorResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomicoValor[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => IndiceEconomicoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "indiceEconomicoValor"),
    :schema_element => [
      ["ano", ["SOAP::SOAPInt", XSD::QName.new(nil, "ano")]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mes", ["SOAP::SOAPInt", XSD::QName.new(nil, "mes")]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valorIndiceMes", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorIndiceMes")], [0, 1]],
      ["valorIndiceUltimos12Meses", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorIndiceUltimos12Meses")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClonarClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clonarClienteServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClonarClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clonarClienteServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoConsumoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoConsumoFilter"),
    :schema_element => [
      ["anoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "anoReferencia")], [0, 1]],
      ["clienteServicoUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteServicoUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["mesReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "mesReferencia")], [0, 1]],
      ["tipoConsumo", ["TipoConsumo", XSD::QName.new(nil, "tipoConsumo")], [0, 1]],
      ["tipoConsumoEstado", ["TipoConsumoEstado", XSD::QName.new(nil, "tipoConsumoEstado")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoConsumoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoConsumo[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoConsumo"),
    :schema_element => [
      ["anoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "anoReferencia")], [0, 1]],
      ["clienteServicoUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteServicoUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mesReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "mesReferencia")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["quantidades", ["ClienteServicoConsumoQuantidade[]", XSD::QName.new(nil, "quantidades")], [0, nil]],
      ["tipoConsumo", ["TipoConsumo", XSD::QName.new(nil, "tipoConsumo")], [0, 1]],
      ["tipoConsumoEstado", ["TipoConsumoEstado", XSD::QName.new(nil, "tipoConsumoEstado")], [0, 1]],
      ["unidadeMedida", ["UnidadeMedida", XSD::QName.new(nil, "unidadeMedida")], [0, 1]],
      ["unidadeMetrica", ["UnidadeMetrica", XSD::QName.new(nil, "unidadeMetrica")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoConsumoQuantidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoConsumoQuantidade"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["fimConsumo", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "fimConsumo")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["identificadorFaturamento", ["SOAP::SOAPString", XSD::QName.new(nil, "identificadorFaturamento")], [0, 1]],
      ["inicioConsumo", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "inicioConsumo")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["quantidade", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "quantidade")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNFFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteNFResponse"),
    :schema_element => [
      ["v_return", ["ClienteNF[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteNF"),
    :schema_element => [
      ["alertaRetorno", ["SOAP::SOAPString", XSD::QName.new(nil, "alertaRetorno")], [0, 1]],
      ["aliquotaCOFINS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaCOFINS")], [0, 1]],
      ["aliquotaCSLL", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaCSLL")], [0, 1]],
      ["aliquotaINSS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSS")], [0, 1]],
      ["aliquotaIR", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaIR")], [0, 1]],
      ["aliquotaISS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISS")], [0, 1]],
      ["aliquotaISSRetido", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSRetido")], [0, 1]],
      ["aliquotaPIS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaPIS")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["codigoCNAE", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoCNAE")], [0, 1]],
      ["codigoServicoNF", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoServicoNF")], [0, 1]],
      ["codigoVerificacao", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoVerificacao")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["dataEmissao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataEmissao")], [0, 1]],
      ["descricaoRPS", ["SOAP::SOAPString", XSD::QName.new(nil, "descricaoRPS")], [0, 1]],
      ["erroRetorno", ["SOAP::SOAPString", XSD::QName.new(nil, "erroRetorno")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["incentivadorCultural", ["SOAP::SOAPString", XSD::QName.new(nil, "incentivadorCultural")], [0, 1]],
      ["loteNF", ["LoteNF", XSD::QName.new(nil, "loteNF")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["motivoCancelamento", ["SOAP::SOAPString", XSD::QName.new(nil, "motivoCancelamento")], [0, 1]],
      ["naturezaOperacao", ["SOAP::SOAPString", XSD::QName.new(nil, "naturezaOperacao")], [0, 1]],
      ["numeroNFS", ["SOAP::SOAPInteger", XSD::QName.new(nil, "numeroNFS")], [0, 1]],
      ["numeroRPS", ["SOAP::SOAPLong", XSD::QName.new(nil, "numeroRPS")], [0, 1]],
      ["regimeTributario", ["SOAP::SOAPString", XSD::QName.new(nil, "regimeTributario")], [0, 1]],
      ["serieRPS", ["SOAP::SOAPString", XSD::QName.new(nil, "serieRPS")], [0, 1]],
      ["servicosCR", ["ClienteServicoCR[]", XSD::QName.new(nil, "servicosCR")], [0, nil]],
      ["simplesNacional", ["SOAP::SOAPString", XSD::QName.new(nil, "simplesNacional")], [0, 1]],
      ["textoEnvio", ["SOAP::SOAPString", XSD::QName.new(nil, "textoEnvio")], [0, 1]],
      ["textoRetorno", ["SOAP::SOAPString", XSD::QName.new(nil, "textoRetorno")], [0, 1]],
      ["tipoAmbiente", ["TipoAmbiente", XSD::QName.new(nil, "tipoAmbiente")], [0, 1]],
      ["tipoNFCancelamento", ["TipoNFCancelamento", XSD::QName.new(nil, "tipoNFCancelamento")], [0, 1]],
      ["tipoNFEstado", ["TipoNFEstado", XSD::QName.new(nil, "tipoNFEstado")], [0, 1]],
      ["tributacaoMunicipio", ["SOAP::SOAPString", XSD::QName.new(nil, "tributacaoMunicipio")], [0, 1]],
      ["urlConsulta", ["SOAP::SOAPString", XSD::QName.new(nil, "urlConsulta")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valorCOFINS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorCOFINS")], [0, 1]],
      ["valorCSLL", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorCSLL")], [0, 1]],
      ["valorDeducao", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorDeducao")], [0, 1]],
      ["valorINSS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorINSS")], [0, 1]],
      ["valorIR", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorIR")], [0, 1]],
      ["valorISS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorISS")], [0, 1]],
      ["valorISSRetido", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorISSRetido")], [0, 1]],
      ["valorPIS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorPIS")], [0, 1]],
      ["valorServicos", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorServicos")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => LoteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "loteNF"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["numero", ["SOAP::SOAPLong", XSD::QName.new(nil, "numero")], [0, 1]],
      ["urlEnvio", ["SOAP::SOAPString", XSD::QName.new(nil, "urlEnvio")], [0, 1]],
      ["urlRetorno", ["SOAP::SOAPString", XSD::QName.new(nil, "urlRetorno")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoCR"),
    :schema_element => [
      ["anoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "anoReferencia")], [0, 1]],
      ["clienteServico", ["ClienteServico", XSD::QName.new(nil, "clienteServico")], [0, 1]],
      ["clienteServicoConsumo", ["ClienteServicoConsumo", XSD::QName.new(nil, "clienteServicoConsumo")], [0, 1]],
      ["clienteServicoParcelaAtivacao", ["ClienteServicoParcelaAtivacao", XSD::QName.new(nil, "clienteServicoParcelaAtivacao")], [0, 1]],
      ["clienteServicoParcelaServico", ["ClienteServicoParcelaServico", XSD::QName.new(nil, "clienteServicoParcelaServico")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["dataEmissao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataEmissao")], [0, 1]],
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mesReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "mesReferencia")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["projecaoFaturamento", ["ProjecaoFaturamento", XSD::QName.new(nil, "projecaoFaturamento")], [0, 1]],
      ["referencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "referencia")], [0, 1]],
      ["serie", ["SOAP::SOAPString", XSD::QName.new(nil, "serie")], [0, 1]],
      ["subSerie", ["SOAP::SOAPString", XSD::QName.new(nil, "subSerie")], [0, 1]],
      ["tipoCREstado", ["TipoCREstado", XSD::QName.new(nil, "tipoCREstado")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valorAtivacaoTotal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorAtivacaoTotal")], [0, 1]],
      ["valorServicoTotal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorServicoTotal")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServico"),
    :schema_element => [
      ["acrescimos", ["ClienteServicoAcrescimo[]", XSD::QName.new(nil, "acrescimos")], [0, nil]],
      ["aliquotaCOFINS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaCOFINS")], [0, 1]],
      ["aliquotaCSLL", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaCSLL")], [0, 1]],
      ["aliquotaINSS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSS")], [0, 1]],
      ["aliquotaIR", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaIR")], [0, 1]],
      ["aliquotaISS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISS")], [0, 1]],
      ["aliquotaISSRetido", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSRetido")], [0, 1]],
      ["aliquotaPIS", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaPIS")], [0, 1]],
      ["codigoConsolidaFatura", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoConsolidaFatura")], [0, 1]],
      ["codigoContrato", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoContrato")], [0, 1]],
      ["contatos", ["Contato[]", XSD::QName.new(nil, "contatos")], [0, nil]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["dataAdesao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataAdesao")], [0, 1]],
      ["dataCancelamento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataCancelamento")], [0, 1]],
      ["dataPrimeiroVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataPrimeiroVencimento")], [0, 1]],
      ["dataSuspensaoFim", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataSuspensaoFim")], [0, 1]],
      ["dataSuspensaoInicio", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataSuspensaoInicio")], [0, 1]],
      ["dataVigenciaFim", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaFim")], [0, 1]],
      ["dataVigenciaInicio", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaInicio")], [0, 1]],
      ["descontos", ["ClienteServicoDesconto[]", XSD::QName.new(nil, "descontos")], [0, nil]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["fatorDeReajusteDaRenovacao", ["IndiceEconomico", XSD::QName.new(nil, "fatorDeReajusteDaRenovacao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["motivoCancelamento", ["MotivoCancelamento", XSD::QName.new(nil, "motivoCancelamento")], [0, 1]],
      ["motivoSuspensao", ["MotivoSuspensao", XSD::QName.new(nil, "motivoSuspensao")], [0, 1]],
      ["municipioPrestacaoServico", ["Cidade", XSD::QName.new(nil, "municipioPrestacaoServico")], [0, 1]],
      ["naturezaOperacao", ["SOAP::SOAPString", XSD::QName.new(nil, "naturezaOperacao")], [0, 1]],
      ["observacao", ["SOAP::SOAPString", XSD::QName.new(nil, "observacao")], [0, 1]],
      ["parcelasAtivacao", ["ClienteServicoParcelaAtivacao[]", XSD::QName.new(nil, "parcelasAtivacao")], [0, nil]],
      ["parcelasServico", ["ClienteServicoParcelaServico[]", XSD::QName.new(nil, "parcelasServico")], [0, nil]],
      ["periodicidade", ["Periodicidade", XSD::QName.new(nil, "periodicidade")], [0, 1]],
      ["quantidade", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "quantidade")], [0, 1]],
      ["quantidadeTipoVigencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "quantidadeTipoVigencia")], [0, 1]],
      ["renovacaoAutomatica", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "renovacaoAutomatica")]],
      ["servico", ["Servico", XSD::QName.new(nil, "servico")], [0, 1]],
      ["servicoEstado", ["ServicoEstado", XSD::QName.new(nil, "servicoEstado")], [0, 1]],
      ["tarifaMinima", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "tarifaMinima")], [0, 1]],
      ["taxaAtivacao", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "taxaAtivacao")], [0, 1]],
      ["tipoConsumo", ["TipoConsumo", XSD::QName.new(nil, "tipoConsumo")], [0, 1]],
      ["tipoFaturamento", ["TipoFaturamento", XSD::QName.new(nil, "tipoFaturamento")], [0, 1]],
      ["tipoVigencia", ["TipoVigencia", XSD::QName.new(nil, "tipoVigencia")], [0, 1]],
      ["unidadeMedida", ["UnidadeMedida", XSD::QName.new(nil, "unidadeMedida")], [0, 1]],
      ["unidadeMetrica", ["UnidadeMetrica", XSD::QName.new(nil, "unidadeMetrica")], [0, 1]],
      ["usarFatorDeReajusteAutomatico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "usarFatorDeReajusteAutomatico")]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]],
      ["valores", ["ClienteServicoValor[]", XSD::QName.new(nil, "valores")], [0, nil]],
      ["vendedor", ["Usuario", XSD::QName.new(nil, "vendedor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoAcrescimo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoAcrescimo"),
    :schema_element => [
      ["dataVigenciaFim", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaFim")], [0, 1]],
      ["dataVigenciaInicio", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaInicio")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["tipoValor", ["TipoValor", XSD::QName.new(nil, "tipoValor")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Contato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "contato"),
    :schema_element => [
      ["celularDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "celularDdd")], [0, 1]],
      ["celularNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "celularNumero")], [0, 1]],
      ["cpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cpf")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["email", ["SOAP::SOAPString", XSD::QName.new(nil, "email")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["faxDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "faxDdd")], [0, 1]],
      ["faxNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "faxNumero")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["recebeCobranca", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "recebeCobranca")]],
      ["telefoneDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "telefoneDdd")], [0, 1]],
      ["telefoneNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "telefoneNumero")], [0, 1]],
      ["tipoContato", ["TipoContato", XSD::QName.new(nil, "tipoContato")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoDesconto,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoDesconto"),
    :schema_element => [
      ["dataVigenciaFim", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaFim")], [0, 1]],
      ["dataVigenciaInicio", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVigenciaInicio")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["tipoValor", ["TipoValor", XSD::QName.new(nil, "tipoValor")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => IndiceEconomico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "indiceEconomico"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["tipoIndiceEconomico", ["TipoIndiceEconomico", XSD::QName.new(nil, "tipoIndiceEconomico")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valores", ["IndiceEconomicoValor[]", XSD::QName.new(nil, "valores")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => MotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "motivoCancelamento"),
    :schema_element => [
      ["contabilizarNoRelatorio", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "contabilizarNoRelatorio")]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => MotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "motivoSuspensao"),
    :schema_element => [
      ["contabilizarNoRelatorio", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "contabilizarNoRelatorio")]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoParcelaAtivacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoParcelaAtivacao"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoParcela"),
    :schema_element => [
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["parcela", ["SOAP::SOAPInt", XSD::QName.new(nil, "parcela")]],
      ["tipoParcela", ["TipoParcela", XSD::QName.new(nil, "tipoParcela")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoParcela,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoParcela"),
    :schema_element => [
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["parcela", ["SOAP::SOAPInt", XSD::QName.new(nil, "parcela")]],
      ["tipoParcela", ["TipoParcela", XSD::QName.new(nil, "tipoParcela")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoParcelaServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoParcelaServico"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoParcela"),
    :schema_element => [
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["parcela", ["SOAP::SOAPInt", XSD::QName.new(nil, "parcela")]],
      ["tipoParcela", ["TipoParcela", XSD::QName.new(nil, "tipoParcela")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Periodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "periodicidade"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["exibirNoFormularioDeContratoExterno", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "exibirNoFormularioDeContratoExterno")]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["percentualDesconto", ["SOAP::SOAPDouble", XSD::QName.new(nil, "percentualDesconto")], [0, 1]],
      ["tipoPeriodicidade", ["TipoPeriodicidade", XSD::QName.new(nil, "tipoPeriodicidade")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Servico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servico"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["codigoServicoNF", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoServicoNF")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["descricaoNF", ["SOAP::SOAPString", XSD::QName.new(nil, "descricaoNF")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["familia", ["Familia", XSD::QName.new(nil, "familia")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["servicos", ["Servico[]", XSD::QName.new(nil, "servicos")], [0, nil]],
      ["tarifaMinima", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "tarifaMinima")], [0, 1]],
      ["taxaAtivacao", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "taxaAtivacao")], [0, 1]],
      ["tipoConsumo", ["TipoConsumo", XSD::QName.new(nil, "tipoConsumo")], [0, 1]],
      ["tipoServico", ["TipoServico", XSD::QName.new(nil, "tipoServico")], [0, 1]],
      ["tributacaoMunicipio", ["SOAP::SOAPString", XSD::QName.new(nil, "tributacaoMunicipio")], [0, 1]],
      ["unidadeMedida", ["UnidadeMedida", XSD::QName.new(nil, "unidadeMedida")], [0, 1]],
      ["unidadeMetrica", ["UnidadeMetrica", XSD::QName.new(nil, "unidadeMetrica")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]],
      ["valores", ["ServicoValor[]", XSD::QName.new(nil, "valores")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => Familia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "familia"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["memorialDescritivo", ["SOAP::SOAPString", XSD::QName.new(nil, "memorialDescritivo")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ServicoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoValor"),
    :schema_element => [
      ["faixaFinal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "faixaFinal")], [0, 1]],
      ["faixaInicial", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "faixaInicial")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoValor"),
    :schema_element => [
      ["faixaFinal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "faixaFinal")], [0, 1]],
      ["faixaInicial", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "faixaInicial")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Usuario,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "usuario"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["email", ["SOAP::SOAPString", XSD::QName.new(nil, "email")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["ips", ["SOAP::SOAPString[]", XSD::QName.new(nil, "ips")], [0, nil]],
      ["menus", ["Menu[]", XSD::QName.new(nil, "menus")], [0, nil]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["othersGerenteUuids", ["SOAP::SOAPString[]", XSD::QName.new(nil, "othersGerenteUuids")], [0, nil]],
      ["regras", ["Regra[]", XSD::QName.new(nil, "regras")], [0, nil]],
      ["senha", ["SOAP::SOAPString", XSD::QName.new(nil, "senha")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Menu,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "menu"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["id", ["SOAP::SOAPString", XSD::QName.new(nil, "id")], [0, 1]],
      ["image", ["SOAP::SOAPString", XSD::QName.new(nil, "image")], [0, 1]],
      ["label", ["SOAP::SOAPString", XSD::QName.new(nil, "label")], [0, 1]],
      ["menuConfig", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "menuConfig")]],
      ["menuItem", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "menuItem")]],
      ["menuPai", ["Menu", XSD::QName.new(nil, "menuPai")], [0, 1]],
      ["ordem", ["SOAP::SOAPInt", XSD::QName.new(nil, "ordem")]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["value", ["SOAP::SOAPString", XSD::QName.new(nil, "value")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Regra,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "regra"),
    :schema_element => [
      ["chave", ["SOAP::SOAPString", XSD::QName.new(nil, "chave")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AtivarClienteServicoPorClienteServicoUuid,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "ativarClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AtivarClienteServicoPorClienteServicoUuidResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "ativarClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarMotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["MotivoSuspensao", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarMotivoSuspensaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoSuspensaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarFamilia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarFamilia"),
    :schema_element => [
      ["arg0", ["Familia", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarFamiliaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarFamiliaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarTotalServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServico"),
    :schema_element => [
      ["arg0", ["ServicoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ServicoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoFilter"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["excluindoIds", ["SOAP::SOAPString[]", XSD::QName.new(nil, "excluindoIds")], [0, nil]],
      ["familia", ["Familia", XSD::QName.new(nil, "familia")], [0, 1]],
      ["generalFilter", ["GeneralFilter", XSD::QName.new(nil, "generalFilter")], [0, 1]],
      ["servicosAdicional", ["Servico", XSD::QName.new(nil, "servicosAdicional")], [0, 1]],
      ["tiposServico", ["TipoServico[]", XSD::QName.new(nil, "tiposServico")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => GeneralFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "generalFilter"),
    :schema_element => [
      ["likeCampos", ["SOAP::SOAPString[]", XSD::QName.new(nil, "likeCampos")], [0, nil]],
      ["likeValor", ["SOAP::SOAPString", XSD::QName.new(nil, "likeValor")], [0, 1]],
      ["maxElements", ["SOAP::SOAPInt", XSD::QName.new(nil, "maxElements")], [0, 1]],
      ["orderBy", ["SOAP::SOAPString", XSD::QName.new(nil, "orderBy")], [0, 1]],
      ["page", ["SOAP::SOAPInt", XSD::QName.new(nil, "page")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServicoResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterServicoResponse"),
    :schema_element => [
      ["v_return", ["Servico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRelacionamento"),
    :schema_element => [
      ["arg0", ["RelacionamentoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RelacionamentoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "relacionamentoFilter"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["tipoRelacionamento", ["TipoRelacionamento", XSD::QName.new(nil, "tipoRelacionamento")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarRelacionamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRelacionamentoResponse"),
    :schema_element => [
      ["v_return", ["Relacionamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => Relacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "relacionamento"),
    :schema_element => [
      ["clienteCRTransacaoUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRTransacaoUuid")], [0, 1]],
      ["clienteCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRUuid")], [0, 1]],
      ["clienteNFUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteNFUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["comentario", ["SOAP::SOAPString", XSD::QName.new(nil, "comentario")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["emailUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "emailUuid")], [0, 1]],
      ["enviarEmail", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "enviarEmail")]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoRelacionamento", ["TipoRelacionamento", XSD::QName.new(nil, "tipoRelacionamento")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRelacionamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRelacionamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRelacionamentoResponse"),
    :schema_element => [
      ["v_return", ["Relacionamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarSegmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarSegmento"),
    :schema_element => [
      ["arg0", ["Segmento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Segmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "segmento"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["criticidade", ["Criticidade", XSD::QName.new(nil, "criticidade")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarSegmentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarSegmentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRelacionamento"),
    :schema_element => [
      ["arg0", ["Relacionamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarRelacionamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRelacionamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarRegra,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegra"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarRegraResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegraResponse"),
    :schema_element => [
      ["v_return", ["Regra[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarContato"),
    :schema_element => [
      ["arg0", ["Contato", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarContatoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarContatoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => GerarNFsEmissao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "gerarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GerarNFsEmissaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "gerarNFsEmissaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarMotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["MotivoSuspensao", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarMotivoSuspensaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoSuspensaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterCodigoCNAE,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCodigoCNAE"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterCodigoCNAEResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCodigoCNAEResponse"),
    :schema_element => [
      ["v_return", ["CodigoCNAE", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CodigoCNAE,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "codigoCNAE"),
    :schema_element => [
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteCRCadastro,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCRCadastro"),
    :schema_element => [
      ["arg0", ["ClienteCRCadastro", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteCRCadastro,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRCadastro"),
    :schema_element => [
      ["clienteCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRUuid")], [0, 1]],
      ["clientePrestador", ["ClientePrestador", XSD::QName.new(nil, "clientePrestador")], [0, 1]],
      ["clienteTomador", ["ClienteTomador", XSD::QName.new(nil, "clienteTomador")], [0, 1]],
      ["contatoPrestador", ["ContatoPrestador", XSD::QName.new(nil, "contatoPrestador")], [0, 1]],
      ["contatoTomador", ["ContatoTomador", XSD::QName.new(nil, "contatoTomador")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["enderecoPrestador", ["EnderecoPrestador", XSD::QName.new(nil, "enderecoPrestador")], [0, 1]],
      ["enderecoTomador", ["EnderecoTomador", XSD::QName.new(nil, "enderecoTomador")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClientePrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clientePrestador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["cnpjCpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cnpjCpf")], [0, 1]],
      ["emailGeral", ["SOAP::SOAPString", XSD::QName.new(nil, "emailGeral")], [0, 1]],
      ["ieRg", ["SOAP::SOAPString", XSD::QName.new(nil, "ieRg")], [0, 1]],
      ["im", ["SOAP::SOAPString", XSD::QName.new(nil, "im")], [0, 1]],
      ["nomeFantasia", ["SOAP::SOAPString", XSD::QName.new(nil, "nomeFantasia")], [0, 1]],
      ["orgaoPublico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "orgaoPublico")]],
      ["razaoSocial", ["SOAP::SOAPString", XSD::QName.new(nil, "razaoSocial")], [0, 1]],
      ["tipoPessoa", ["TipoPessoa", XSD::QName.new(nil, "tipoPessoa")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CopyFields,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ClienteTomador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteTomador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["cnpjCpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cnpjCpf")], [0, 1]],
      ["emailGeral", ["SOAP::SOAPString", XSD::QName.new(nil, "emailGeral")], [0, 1]],
      ["ieRg", ["SOAP::SOAPString", XSD::QName.new(nil, "ieRg")], [0, 1]],
      ["im", ["SOAP::SOAPString", XSD::QName.new(nil, "im")], [0, 1]],
      ["nomeFantasia", ["SOAP::SOAPString", XSD::QName.new(nil, "nomeFantasia")], [0, 1]],
      ["orgaoPublico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "orgaoPublico")]],
      ["razaoSocial", ["SOAP::SOAPString", XSD::QName.new(nil, "razaoSocial")], [0, 1]],
      ["tipoPessoa", ["TipoPessoa", XSD::QName.new(nil, "tipoPessoa")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ContatoPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "contatoPrestador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["celularDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "celularDdd")], [0, 1]],
      ["celularNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "celularNumero")], [0, 1]],
      ["cpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cpf")], [0, 1]],
      ["email", ["SOAP::SOAPString", XSD::QName.new(nil, "email")], [0, 1]],
      ["faxDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "faxDdd")], [0, 1]],
      ["faxNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "faxNumero")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["telefoneDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "telefoneDdd")], [0, 1]],
      ["telefoneNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "telefoneNumero")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ContatoTomador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "contatoTomador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["celularDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "celularDdd")], [0, 1]],
      ["celularNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "celularNumero")], [0, 1]],
      ["cpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cpf")], [0, 1]],
      ["email", ["SOAP::SOAPString", XSD::QName.new(nil, "email")], [0, 1]],
      ["faxDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "faxDdd")], [0, 1]],
      ["faxNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "faxNumero")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["telefoneDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "telefoneDdd")], [0, 1]],
      ["telefoneNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "telefoneNumero")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => EnderecoPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "enderecoPrestador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["bairro", ["SOAP::SOAPString", XSD::QName.new(nil, "bairro")], [0, 1]],
      ["cep", ["SOAP::SOAPString", XSD::QName.new(nil, "cep")], [0, 1]],
      ["cidade", ["Cidade", XSD::QName.new(nil, "cidade")], [0, 1]],
      ["complemento", ["SOAP::SOAPString", XSD::QName.new(nil, "complemento")], [0, 1]],
      ["logradouro", ["SOAP::SOAPString", XSD::QName.new(nil, "logradouro")], [0, 1]],
      ["numero", ["SOAP::SOAPInt", XSD::QName.new(nil, "numero")], [0, 1]],
      ["uf", ["Uf", XSD::QName.new(nil, "uf")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => EnderecoTomador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "enderecoTomador"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "copyFields"),
    :schema_element => [
      ["bairro", ["SOAP::SOAPString", XSD::QName.new(nil, "bairro")], [0, 1]],
      ["cep", ["SOAP::SOAPString", XSD::QName.new(nil, "cep")], [0, 1]],
      ["cidade", ["Cidade", XSD::QName.new(nil, "cidade")], [0, 1]],
      ["complemento", ["SOAP::SOAPString", XSD::QName.new(nil, "complemento")], [0, 1]],
      ["logradouro", ["SOAP::SOAPString", XSD::QName.new(nil, "logradouro")], [0, 1]],
      ["numero", ["SOAP::SOAPInt", XSD::QName.new(nil, "numero")], [0, 1]],
      ["uf", ["Uf", XSD::QName.new(nil, "uf")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteCRCadastroResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCRCadastroResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarRamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRamoAtividade"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarRamoAtividadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRamoAtividadeResponse"),
    :schema_element => [
      ["v_return", ["RamoAtividade[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => RamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "ramoAtividade"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["segmentos", ["Segmento[]", XSD::QName.new(nil, "segmentos")], [0, nil]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => BaixarCobranca,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "baixarCobranca"),
    :schema_element => [
      ["arg0", ["Lancamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Lancamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "lancamento"),
    :schema_element => [
      ["clienteCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRUuid")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["data", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "data")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["tipoLancamento", ["TipoLancamento", XSD::QName.new(nil, "tipoLancamento")], [0, 1]],
      ["tipoMeioPagamento", ["TipoMeioPagamento", XSD::QName.new(nil, "tipoMeioPagamento")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => BaixarCobrancaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "baixarCobrancaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCR", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteServicoCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteCRTransacaoBoleto,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCRTransacaoBoleto"),
    :schema_element => [
      ["arg0", ["ClienteCRTransacaoBoletoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteCRTransacaoBoletoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacaoBoletoFilter"),
    :schema_element => [
      ["clienteCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteCRTransacaoBoletoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCRTransacaoBoletoResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRegimeTributario,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegimeTributario"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRegimeTributarioResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegimeTributarioResponse"),
    :schema_element => [
      ["v_return", ["RegimeTributario", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RegimeTributario,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "regimeTributario"),
    :schema_element => [
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerServico"),
    :schema_element => [
      ["arg0", ["Servico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarTotalArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalArquivo"),
    :schema_element => [
      ["arg0", ["ArquivoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ArquivoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "arquivoFilter"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["generalFilter", ["GeneralFilter", XSD::QName.new(nil, "generalFilter")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalArquivoResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => ConsultarCobranca,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "consultarCobranca"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ConsultarCobrancaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "consultarCobrancaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterContato"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterContatoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterContatoResponse"),
    :schema_element => [
      ["v_return", ["Contato", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCRCadastro,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRCadastro"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCRCadastroResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRCadastroResponse"),
    :schema_element => [
      ["v_return", ["ClienteCRCadastro", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarServicoAliquotaPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarServicoAliquotaPrestador"),
    :schema_element => [
      ["arg0", ["ServicoAliquotaPrestadorFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ServicoAliquotaPrestadorFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoAliquotaPrestadorFilter"),
    :schema_element => [
      ["cidadeUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "cidadeUuid")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["generalFilter", ["GeneralFilter", XSD::QName.new(nil, "generalFilter")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarServicoAliquotaPrestadorResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarServicoAliquotaPrestadorResponse"),
    :schema_element => [
      ["v_return", ["ServicoAliquotaPrestador[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ServicoAliquotaPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoAliquotaPrestador"),
    :schema_element => [
      ["aliquotaINSSEX", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSSEX")], [0, 1]],
      ["aliquotaINSSPF", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSSPF")], [0, 1]],
      ["aliquotaINSSRetidoPJ", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSSRetidoPJ")], [0, 1]],
      ["aliquotaINSSRetidoPU", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaINSSRetidoPU")], [0, 1]],
      ["aliquotaIRRetidoPJ", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaIRRetidoPJ")], [0, 1]],
      ["aliquotaIRRetidoPU", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaIRRetidoPU")], [0, 1]],
      ["aliquotaISSEX", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSEX")], [0, 1]],
      ["aliquotaISSPF", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSPF")], [0, 1]],
      ["aliquotaISSPJ", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSPJ")], [0, 1]],
      ["aliquotaISSPU", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSPU")], [0, 1]],
      ["aliquotaISSRetidoPJ", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSRetidoPJ")], [0, 1]],
      ["aliquotaISSRetidoPU", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSRetidoPU")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["incideINSSPJ", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "incideINSSPJ")]],
      ["incideINSSPU", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "incideINSSPU")]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["retemINSSPJ", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemINSSPJ")]],
      ["retemINSSPU", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemINSSPU")]],
      ["retemIRPJ", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemIRPJ")]],
      ["retemIRPU", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemIRPU")]],
      ["retemISSPJ", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSPJ")]],
      ["retemISSPU", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSPU")]],
      ["retemISSPrestadorCadastrado", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSPrestadorCadastrado")]],
      ["retemISSPrestadorMunicipio", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSPrestadorMunicipio")]],
      ["retemISSPrestadorNaoCadastrado", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSPrestadorNaoCadastrado")]],
      ["retemISSTomadorIgualPrestador", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemISSTomadorIgualPrestador")]],
      ["retemPCCPJ", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemPCCPJ")]],
      ["retemPCCPU", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "retemPCCPU")]],
      ["servicoAliquotaTomadors", ["ServicoAliquotaTomador[]", XSD::QName.new(nil, "servicoAliquotaTomadors")], [0, nil]],
      ["tributacaoMunicipio", ["SOAP::SOAPString", XSD::QName.new(nil, "tributacaoMunicipio")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ServicoAliquotaTomador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoAliquotaTomador"),
    :schema_element => [
      ["aliquotaISSRetido", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "aliquotaISSRetido")], [0, 1]],
      ["cidade", ["Cidade", XSD::QName.new(nil, "cidade")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterArquivo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterArquivoResponse"),
    :schema_element => [
      ["v_return", ["Arquivo", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Arquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "arquivo"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["src", ["SOAP::SOAPString", XSD::QName.new(nil, "src")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarSegmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarSegmento"),
    :schema_element => [
      ["arg0", ["Segmento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarSegmentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarSegmentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterFamilia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterFamilia"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterFamiliaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterFamiliaResponse"),
    :schema_element => [
      ["v_return", ["Familia", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTemplateEmail,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmail"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTemplateEmailResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmailResponse"),
    :schema_element => [
      ["v_return", ["TemplateEmail", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => TemplateEmail,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "templateEmail"),
    :schema_element => [
      ["assunto", ["SOAP::SOAPString", XSD::QName.new(nil, "assunto")], [0, 1]],
      ["comCopia", ["SOAP::SOAPString", XSD::QName.new(nil, "comCopia")], [0, 1]],
      ["comCopiaOculta", ["SOAP::SOAPString", XSD::QName.new(nil, "comCopiaOculta")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mensagem", ["SOAP::SOAPString", XSD::QName.new(nil, "mensagem")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoTemplateEmail", ["TipoTemplateEmail", XSD::QName.new(nil, "tipoTemplateEmail")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CancelarNFsEmissao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CancelarNFsEmissaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarNFsEmissaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarCliente"),
    :schema_element => [
      ["arg0", ["Cliente", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Cliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cliente"),
    :schema_element => [
      ["bloqueioFaturamento", ["BloqueioFaturamento", XSD::QName.new(nil, "bloqueioFaturamento")], [0, 1]],
      ["cartoes", ["ClienteCartao[]", XSD::QName.new(nil, "cartoes")], [0, nil]],
      ["cicloFaturamento", ["CicloFaturamento", XSD::QName.new(nil, "cicloFaturamento")], [0, 1]],
      ["cnpjCpf", ["SOAP::SOAPString", XSD::QName.new(nil, "cnpjCpf")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["comoConheceu", ["SOAP::SOAPString", XSD::QName.new(nil, "comoConheceu")], [0, 1]],
      ["complementoDoCorpoDaNFe", ["SOAP::SOAPString", XSD::QName.new(nil, "complementoDoCorpoDaNFe")], [0, 1]],
      ["contatos", ["Contato[]", XSD::QName.new(nil, "contatos")], [0, nil]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["diaVencimento", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaVencimento")], [0, 1]],
      ["documentoFiscal", ["DocumentoFiscal", XSD::QName.new(nil, "documentoFiscal")], [0, 1]],
      ["domicilioFaturamento", ["Cidade", XSD::QName.new(nil, "domicilioFaturamento")], [0, 1]],
      ["dominio", ["SOAP::SOAPString", XSD::QName.new(nil, "dominio")], [0, 1]],
      ["emailGeral", ["SOAP::SOAPString", XSD::QName.new(nil, "emailGeral")], [0, 1]],
      ["enderecos", ["Endereco[]", XSD::QName.new(nil, "enderecos")], [0, nil]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["ieRg", ["SOAP::SOAPString", XSD::QName.new(nil, "ieRg")], [0, 1]],
      ["im", ["SOAP::SOAPString", XSD::QName.new(nil, "im")], [0, 1]],
      ["indicadoPor", ["SOAP::SOAPString", XSD::QName.new(nil, "indicadoPor")], [0, 1]],
      ["meioPagamento", ["MeioPagamento", XSD::QName.new(nil, "meioPagamento")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nomeFantasia", ["SOAP::SOAPString", XSD::QName.new(nil, "nomeFantasia")], [0, 1]],
      ["orgaoPublico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "orgaoPublico")]],
      ["ramoAtividade", ["RamoAtividade", XSD::QName.new(nil, "ramoAtividade")], [0, 1]],
      ["razaoSocial", ["SOAP::SOAPString", XSD::QName.new(nil, "razaoSocial")], [0, 1]],
      ["telefoneDdd", ["SOAP::SOAPInt", XSD::QName.new(nil, "telefoneDdd")], [0, 1]],
      ["telefoneNumero", ["SOAP::SOAPString", XSD::QName.new(nil, "telefoneNumero")], [0, 1]],
      ["tipoPessoa", ["TipoPessoa", XSD::QName.new(nil, "tipoPessoa")], [0, 1]],
      ["urlPropria", ["SOAP::SOAPString", XSD::QName.new(nil, "urlPropria")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteCartao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCartao"),
    :schema_element => [
      ["ano", ["SOAP::SOAPInt", XSD::QName.new(nil, "ano")], [0, 1]],
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["chaveCompra", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveCompra")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mes", ["SOAP::SOAPInt", XSD::QName.new(nil, "mes")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["numero", ["SOAP::SOAPString", XSD::QName.new(nil, "numero")], [0, 1]],
      ["principal", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "principal")]],
      ["security", ["SOAP::SOAPString", XSD::QName.new(nil, "security")], [0, 1]],
      ["tipoBandeira", ["TipoBandeira", XSD::QName.new(nil, "tipoBandeira")], [0, 1]],
      ["titular", ["SOAP::SOAPString", XSD::QName.new(nil, "titular")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cicloFaturamento"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["diaEnvioBoleto", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaEnvioBoleto")]],
      ["diaEnvioBoletoFatorMesBase", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaEnvioBoletoFatorMesBase")]],
      ["diaEnvioNFE", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaEnvioNFE")]],
      ["diaEnvioNFEFatorMesBase", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaEnvioNFEFatorMesBase")]],
      ["diaFechamentoConsumo", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaFechamentoConsumo")]],
      ["diaFechamentoConsumoFatorMesBase", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaFechamentoConsumoFatorMesBase")]],
      ["diaFechamentoFatura", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaFechamentoFatura")]],
      ["diaFechamentoFaturaFatorMesBase", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaFechamentoFaturaFatorMesBase")]],
      ["diaVencimento", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaVencimento")]],
      ["diaVencimentoFatorMesBase", ["SOAP::SOAPInt", XSD::QName.new(nil, "diaVencimentoFatorMesBase")]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => MeioPagamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "meioPagamento"),
    :schema_element => [
      ["agencia", ["SOAP::SOAPString", XSD::QName.new(nil, "agencia")], [0, 1]],
      ["agenciaDigito", ["SOAP::SOAPString", XSD::QName.new(nil, "agenciaDigito")], [0, 1]],
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["banco", ["TipoBanco", XSD::QName.new(nil, "banco")], [0, 1]],
      ["conta", ["SOAP::SOAPString", XSD::QName.new(nil, "conta")], [0, 1]],
      ["contaDigito", ["SOAP::SOAPString", XSD::QName.new(nil, "contaDigito")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["exibirNoFormularioDeContratoExterno", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "exibirNoFormularioDeContratoExterno")]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["instrucoesDeCobrancaLinha1", ["SOAP::SOAPString", XSD::QName.new(nil, "instrucoesDeCobrancaLinha1")], [0, 1]],
      ["instrucoesDeCobrancaLinha2", ["SOAP::SOAPString", XSD::QName.new(nil, "instrucoesDeCobrancaLinha2")], [0, 1]],
      ["instrucoesDeCobrancaLinha3", ["SOAP::SOAPString", XSD::QName.new(nil, "instrucoesDeCobrancaLinha3")], [0, 1]],
      ["instrucoesDeCobrancaLinha4", ["SOAP::SOAPString", XSD::QName.new(nil, "instrucoesDeCobrancaLinha4")], [0, 1]],
      ["instrucoesDeCobrancaLinha5", ["SOAP::SOAPString", XSD::QName.new(nil, "instrucoesDeCobrancaLinha5")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["moraMensalAposVencimento", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "moraMensalAposVencimento")], [0, 1]],
      ["multaAposVencimento", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "multaAposVencimento")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["nossoNumeroInicial", ["SOAP::SOAPLong", XSD::QName.new(nil, "nossoNumeroInicial")], [0, 1]],
      ["numeroCarteira", ["SOAP::SOAPString", XSD::QName.new(nil, "numeroCarteira")], [0, 1]],
      ["numeroConvenio", ["SOAP::SOAPString", XSD::QName.new(nil, "numeroConvenio")], [0, 1]],
      ["quantidadeDiasCobrancaAposVencimento", ["SOAP::SOAPInt", XSD::QName.new(nil, "quantidadeDiasCobrancaAposVencimento")]],
      ["quantidadeDiasConsiderarVencido", ["SOAP::SOAPInt", XSD::QName.new(nil, "quantidadeDiasConsiderarVencido")]],
      ["quantidadeDiasEnvioAntecipado", ["SOAP::SOAPInt", XSD::QName.new(nil, "quantidadeDiasEnvioAntecipado")]],
      ["tarifaBoleto", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "tarifaBoleto")], [0, 1]],
      ["tipoMeioPagamento", ["TipoMeioPagamento", XSD::QName.new(nil, "tipoMeioPagamento")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRelacionamento"),
    :schema_element => [
      ["arg0", ["Relacionamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarRelacionamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRelacionamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarCicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCicloFaturamento"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarCicloFaturamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCicloFaturamentoResponse"),
    :schema_element => [
      ["v_return", ["CicloFaturamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterConfiguracao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterConfiguracao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterConfiguracaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterConfiguracaoResponse"),
    :schema_element => [
      ["v_return", ["Configuracao", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Configuracao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "configuracao"),
    :schema_element => [
      ["cadastrosPrefeitura", ["Cidade[]", XSD::QName.new(nil, "cadastrosPrefeitura")], [0, nil]],
      ["caminhoCabecalhoEmail", ["SOAP::SOAPString", XSD::QName.new(nil, "caminhoCabecalhoEmail")], [0, 1]],
      ["caminhoLogo", ["SOAP::SOAPString", XSD::QName.new(nil, "caminhoLogo")], [0, 1]],
      ["certificadoDigitalCaminho", ["SOAP::SOAPString", XSD::QName.new(nil, "certificadoDigitalCaminho")], [0, 1]],
      ["certificadoDigitalSenha", ["SOAP::SOAPString", XSD::QName.new(nil, "certificadoDigitalSenha")], [0, 1]],
      ["codigoCNAE", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoCNAE")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["domicilioFaturamento", ["Cidade", XSD::QName.new(nil, "domicilioFaturamento")], [0, 1]],
      ["emailCobranca", ["SOAP::SOAPString", XSD::QName.new(nil, "emailCobranca")], [0, 1]],
      ["emailFinanceiro", ["SOAP::SOAPString", XSD::QName.new(nil, "emailFinanceiro")], [0, 1]],
      ["entityIdMyFinance", ["SOAP::SOAPInt", XSD::QName.new(nil, "entityIdMyFinance")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["incentivadorCultural", ["SOAP::SOAPString", XSD::QName.new(nil, "incentivadorCultural")], [0, 1]],
      ["merchantKeyMundipagg", ["SOAP::SOAPString", XSD::QName.new(nil, "merchantKeyMundipagg")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["numeroLOTE", ["SOAP::SOAPLong", XSD::QName.new(nil, "numeroLOTE")], [0, 1]],
      ["numeroRPS", ["SOAP::SOAPLong", XSD::QName.new(nil, "numeroRPS")], [0, 1]],
      ["referenciaCR", ["SOAP::SOAPInt", XSD::QName.new(nil, "referenciaCR")], [0, 1]],
      ["referenciaCSCR", ["SOAP::SOAPInt", XSD::QName.new(nil, "referenciaCSCR")], [0, 1]],
      ["regimeTributario", ["SOAP::SOAPString", XSD::QName.new(nil, "regimeTributario")], [0, 1]],
      ["senhaMastersaf", ["SOAP::SOAPString", XSD::QName.new(nil, "senhaMastersaf")], [0, 1]],
      ["serieCR", ["SOAP::SOAPString", XSD::QName.new(nil, "serieCR")], [0, 1]],
      ["serieCSCR", ["SOAP::SOAPString", XSD::QName.new(nil, "serieCSCR")], [0, 1]],
      ["serieRPS", ["SOAP::SOAPString", XSD::QName.new(nil, "serieRPS")], [0, 1]],
      ["seriesCliente", ["Serie[]", XSD::QName.new(nil, "seriesCliente")], [0, nil]],
      ["seriesContrato", ["Serie[]", XSD::QName.new(nil, "seriesContrato")], [0, nil]],
      ["simplesNacional", ["SOAP::SOAPString", XSD::QName.new(nil, "simplesNacional")], [0, 1]],
      ["smtpEmailDeCobranca", ["SOAP::SOAPString", XSD::QName.new(nil, "smtpEmailDeCobranca")], [0, 1]],
      ["subSerieCR", ["SOAP::SOAPString", XSD::QName.new(nil, "subSerieCR")], [0, 1]],
      ["subSerieCSCR", ["SOAP::SOAPString", XSD::QName.new(nil, "subSerieCSCR")], [0, 1]],
      ["tipoAmbiente", ["TipoAmbiente", XSD::QName.new(nil, "tipoAmbiente")], [0, 1]],
      ["tipoStatusEmailCobranca", ["TipoStatusEmailCobranca", XSD::QName.new(nil, "tipoStatusEmailCobranca")], [0, 1]],
      ["tokenMyFinance", ["SOAP::SOAPString", XSD::QName.new(nil, "tokenMyFinance")], [0, 1]],
      ["usuarioMastersaf", ["SOAP::SOAPString", XSD::QName.new(nil, "usuarioMastersaf")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["webserviceMastersaf", ["SOAP::SOAPString", XSD::QName.new(nil, "webserviceMastersaf")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Serie,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "serie"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["subSeries", ["SubSerie[]", XSD::QName.new(nil, "subSeries")], [0, nil]],
      ["tamanhoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "tamanhoReferencia")]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPString", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SubSerie,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "subSerie"),
    :schema_element => [
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["referencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "referencia")]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valor", ["SOAP::SOAPString", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerRelacionamento"),
    :schema_element => [
      ["arg0", ["Relacionamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverRelacionamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerRelacionamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerContato"),
    :schema_element => [
      ["arg0", ["Contato", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverContatoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerContatoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterSimplesNacional,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterSimplesNacional"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterSimplesNacionalResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterSimplesNacionalResponse"),
    :schema_element => [
      ["v_return", ["SimplesNacional", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SimplesNacional,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "simplesNacional"),
    :schema_element => [
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarMotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["MotivoCancelamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarMotivoCancelamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoCancelamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNF", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteNFResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCR", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCR"),
    :schema_element => [
      ["anoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "anoReferencia")], [0, 1]],
      ["boletos", ["ClienteCRTransacaoBoleto[]", XSD::QName.new(nil, "boletos")], [0, nil]],
      ["cartoes", ["ClienteCRTransacaoCartao[]", XSD::QName.new(nil, "cartoes")], [0, nil]],
      ["cliente", ["Cliente", XSD::QName.new(nil, "cliente")], [0, 1]],
      ["clienteCRCadastroUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteCRCadastroUuid")], [0, 1]],
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["dataBaixa", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataBaixa")], [0, 1]],
      ["dataCancelamento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataCancelamento")], [0, 1]],
      ["dataEmissao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataEmissao")], [0, 1]],
      ["dataEnvio", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataEnvio")], [0, 1]],
      ["dataEnvioAtraso", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataEnvioAtraso")], [0, 1]],
      ["dataPagamento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataPagamento")], [0, 1]],
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["dataVencimentoOriginal", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimentoOriginal")], [0, 1]],
      ["depositos", ["ClienteCRTransacaoDeposito[]", XSD::QName.new(nil, "depositos")], [0, nil]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["mesReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "mesReferencia")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["motivoCancelamento", ["MotivoCancelamento", XSD::QName.new(nil, "motivoCancelamento")], [0, 1]],
      ["notasFiscais", ["ClienteNF[]", XSD::QName.new(nil, "notasFiscais")], [0, nil]],
      ["projecaoFaturamento", ["ProjecaoFaturamento", XSD::QName.new(nil, "projecaoFaturamento")], [0, 1]],
      ["referencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "referencia")], [0, 1]],
      ["serie", ["SOAP::SOAPString", XSD::QName.new(nil, "serie")], [0, 1]],
      ["servicosCR", ["ClienteServicoCR[]", XSD::QName.new(nil, "servicosCR")], [0, nil]],
      ["subSerie", ["SOAP::SOAPString", XSD::QName.new(nil, "subSerie")], [0, 1]],
      ["tipoCREstado", ["TipoCREstado", XSD::QName.new(nil, "tipoCREstado")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["valorDesconto", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorDesconto")], [0, 1]],
      ["valorIOF", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorIOF")], [0, 1]],
      ["valorImposto", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorImposto")], [0, 1]],
      ["valorJurosMora", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorJurosMora")], [0, 1]],
      ["valorMulta", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorMulta")], [0, 1]],
      ["valorPago", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorPago")], [0, 1]],
      ["valorTarifaBoleto", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorTarifaBoleto")], [0, 1]],
      ["valorTotal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorTotal")], [0, 1]],
      ["valorTotalOriginal", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valorTotalOriginal")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteCRTransacaoBoleto,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacaoBoleto"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacao"),
    :schema_element => [
      ["consulta", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "consulta")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoClienteCRTransacao", ["TipoClienteCRTransacao", XSD::QName.new(nil, "tipoClienteCRTransacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["boletoUrl", ["SOAP::SOAPString", XSD::QName.new(nil, "boletoUrl")], [0, 1]],
      ["chavePedido", ["SOAP::SOAPString", XSD::QName.new(nil, "chavePedido")], [0, 1]],
      ["chaveRequisicao", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveRequisicao")], [0, 1]],
      ["chaveTransacao", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveTransacao")], [0, 1]],
      ["codigoBarra", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoBarra")], [0, 1]],
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["nossoNumero", ["SOAP::SOAPLong", XSD::QName.new(nil, "nossoNumero")], [0, 1]],
      ["referenciaPedido", ["SOAP::SOAPString", XSD::QName.new(nil, "referenciaPedido")], [0, 1]],
      ["referenciaTransacao", ["SOAP::SOAPString", XSD::QName.new(nil, "referenciaTransacao")], [0, 1]],
      ["situacaoPedido", ["TipoStatusGateway", XSD::QName.new(nil, "situacaoPedido")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteCRTransacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacao"),
    :schema_element => [
      ["consulta", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "consulta")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoClienteCRTransacao", ["TipoClienteCRTransacao", XSD::QName.new(nil, "tipoClienteCRTransacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteCRTransacaoCartao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacaoCartao"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacao"),
    :schema_element => [
      ["consulta", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "consulta")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoClienteCRTransacao", ["TipoClienteCRTransacao", XSD::QName.new(nil, "tipoClienteCRTransacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["chaveCompra", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveCompra")], [0, 1]],
      ["chavePedido", ["SOAP::SOAPString", XSD::QName.new(nil, "chavePedido")], [0, 1]],
      ["chaveRequisicao", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveRequisicao")], [0, 1]],
      ["chaveTransacao", ["SOAP::SOAPString", XSD::QName.new(nil, "chaveTransacao")], [0, 1]],
      ["codigoAdquirente", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoAdquirente")], [0, 1]],
      ["codigoAutorizacao", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoAutorizacao")], [0, 1]],
      ["dataTransacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataTransacao")], [0, 1]],
      ["identificacaoTransacao", ["SOAP::SOAPString", XSD::QName.new(nil, "identificacaoTransacao")], [0, 1]],
      ["mensagemAdquirente", ["SOAP::SOAPString", XSD::QName.new(nil, "mensagemAdquirente")], [0, 1]],
      ["numero", ["SOAP::SOAPString", XSD::QName.new(nil, "numero")], [0, 1]],
      ["referenciaPedido", ["SOAP::SOAPString", XSD::QName.new(nil, "referenciaPedido")], [0, 1]],
      ["referenciaTransacao", ["SOAP::SOAPString", XSD::QName.new(nil, "referenciaTransacao")], [0, 1]],
      ["situacaoPedido", ["TipoStatusGateway", XSD::QName.new(nil, "situacaoPedido")], [0, 1]],
      ["tipoBandeira", ["TipoBandeira", XSD::QName.new(nil, "tipoBandeira")], [0, 1]],
      ["titular", ["SOAP::SOAPString", XSD::QName.new(nil, "titular")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteCRTransacaoDeposito,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacaoDeposito"),
    :schema_basetype => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRTransacao"),
    :schema_element => [
      ["consulta", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "consulta")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["tipoClienteCRTransacao", ["TipoClienteCRTransacao", XSD::QName.new(nil, "tipoClienteCRTransacao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]],
      ["dataVencimento", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "dataVencimento")], [0, 1]],
      ["situacaoPedido", ["TipoStatusGateway", XSD::QName.new(nil, "situacaoPedido")], [0, 1]],
      ["valor", ["SOAP::SOAPDecimal", XSD::QName.new(nil, "valor")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarCliente"),
    :schema_element => [
      ["arg0", ["Cliente", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarCodigoCNAE,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCodigoCNAE"),
    :schema_element => [
      ["arg0", ["CodigoCNAEFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CodigoCNAEFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "codigoCNAEFilter"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarCodigoCNAEResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCodigoCNAEResponse"),
    :schema_element => [
      ["v_return", ["CodigoCNAE[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterNaturezaOperacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterNaturezaOperacao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterNaturezaOperacaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterNaturezaOperacaoResponse"),
    :schema_element => [
      ["v_return", ["NaturezaOperacao", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => NaturezaOperacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "naturezaOperacao"),
    :schema_element => [
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoCR"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteServicoCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoCR", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GerarCobranca,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "gerarCobranca"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GerarCobrancaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "gerarCobrancaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteNF"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteNFResponse"),
    :schema_element => [
      ["v_return", ["ClienteNF", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => EmailCobranca,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "emailCobranca"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => EmailCobrancaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "emailCobrancaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarSegmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarSegmento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarSegmentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarSegmentoResponse"),
    :schema_element => [
      ["v_return", ["Segmento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarFamilia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarFamilia"),
    :schema_element => [
      ["arg0", ["Familia", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarFamiliaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarFamiliaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarMeioPagamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMeioPagamento"),
    :schema_element => [
      ["arg0", ["MeioPagamentoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => MeioPagamentoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "meioPagamentoFilter"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")], [0, 1]],
      ["tiposMeioPagamento", ["TipoMeioPagamento[]", XSD::QName.new(nil, "tiposMeioPagamento")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarMeioPagamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMeioPagamentoResponse"),
    :schema_element => [
      ["v_return", ["MeioPagamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCR", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCR"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteCR", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarRamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRamoAtividade"),
    :schema_element => [
      ["arg0", ["RamoAtividade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarRamoAtividadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRamoAtividadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterUuidUsuario,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterUuidUsuario"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterUuidUsuarioResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterUuidUsuarioResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMeioPagamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMeioPagamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMeioPagamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMeioPagamentoResponse"),
    :schema_element => [
      ["v_return", ["MeioPagamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterEndereco"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterEnderecoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterEnderecoResponse"),
    :schema_element => [
      ["v_return", ["Endereco", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SolicitarClienteCartao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "solicitarClienteCartao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SolicitarClienteCartaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "solicitarClienteCartaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => CancelarClienteServicoPorClienteServicoUuid,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CancelarClienteServicoPorClienteServicoUuidResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarEndereco"),
    :schema_element => [
      ["arg0", ["Endereco", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarEnderecoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarEnderecoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RegraClienteServicoImpostos,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "regraClienteServicoImpostos"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RegraClienteServicoImpostosResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "regraClienteServicoImpostosResponse"),
    :schema_element => [
      ["v_return", ["ClienteServico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterGerente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterGerente"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterGerenteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterGerenteResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCRFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteCRFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteCRFilter"),
    :schema_element => [
      ["anoReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "anoReferencia")], [0, 1]],
      ["boleto", ["SOAP::SOAPLong", XSD::QName.new(nil, "boleto")], [0, 1]],
      ["cicloFaturamento", ["CicloFaturamento", XSD::QName.new(nil, "cicloFaturamento")], [0, 1]],
      ["clienteServicoCRUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteServicoCRUuid")], [0, 1]],
      ["clienteServicoUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteServicoUuid")], [0, 1]],
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["enviado", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "enviado")], [0, 1]],
      ["generalFilter", ["GeneralFilter", XSD::QName.new(nil, "generalFilter")], [0, 1]],
      ["mesReferencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "mesReferencia")], [0, 1]],
      ["notaFiscal", ["SOAP::SOAPInteger", XSD::QName.new(nil, "notaFiscal")], [0, 1]],
      ["pago", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "pago")], [0, 1]],
      ["possuiNFEmitido", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "possuiNFEmitido")], [0, 1]],
      ["projecaoFaturamento", ["ProjecaoFaturamento[]", XSD::QName.new(nil, "projecaoFaturamento")], [0, nil]],
      ["rangeDataPagamento", ["SOAP::SOAPDateTime[]", XSD::QName.new(nil, "rangeDataPagamento")], [0, nil]],
      ["rangeDataVencimento", ["SOAP::SOAPDateTime[]", XSD::QName.new(nil, "rangeDataVencimento")], [0, nil]],
      ["rangeDataVencimentoOriginal", ["SOAP::SOAPDateTime[]", XSD::QName.new(nil, "rangeDataVencimentoOriginal")], [0, nil]],
      ["tipoCREstado", ["TipoCREstado[]", XSD::QName.new(nil, "tipoCREstado")], [0, nil]],
      ["vencido", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "vencido")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCRResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => SuspenderClienteServicoPorClienteServicoUuid,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "suspenderClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SuspenderClienteServicoPorClienteServicoUuidResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "suspenderClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarEndereco"),
    :schema_element => [
      ["arg0", ["Endereco", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarEnderecoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarEnderecoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerCliente"),
    :schema_element => [
      ["arg0", ["Cliente", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterPadraoPreenchimento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterPadraoPreenchimento"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterPadraoPreenchimentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterPadraoPreenchimentoResponse"),
    :schema_element => [
      ["v_return", ["PadraoPreenchimento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => PadraoPreenchimento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "padraoPreenchimento"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")]],
      ["bloqueioFaturamento", ["BloqueioFaturamento", XSD::QName.new(nil, "bloqueioFaturamento")], [0, 1]],
      ["cicloFaturamento", ["CicloFaturamento", XSD::QName.new(nil, "cicloFaturamento")], [0, 1]],
      ["cidade", ["Cidade", XSD::QName.new(nil, "cidade")], [0, 1]],
      ["criacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "criacao")], [0, 1]],
      ["documentoFiscal", ["DocumentoFiscal", XSD::QName.new(nil, "documentoFiscal")], [0, 1]],
      ["exclusao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "exclusao")], [0, 1]],
      ["familia", ["Familia", XSD::QName.new(nil, "familia")], [0, 1]],
      ["fatorDeReajusteDaRenovacao", ["IndiceEconomico", XSD::QName.new(nil, "fatorDeReajusteDaRenovacao")], [0, 1]],
      ["gerenteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "gerenteUuid")], [0, 1]],
      ["meioPagamento", ["MeioPagamento", XSD::QName.new(nil, "meioPagamento")], [0, 1]],
      ["modificacao", ["SOAP::SOAPDateTime", XSD::QName.new(nil, "modificacao")], [0, 1]],
      ["naturezaOperacao", ["SOAP::SOAPString", XSD::QName.new(nil, "naturezaOperacao")], [0, 1]],
      ["orgaoPublico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "orgaoPublico")]],
      ["periodicidade", ["Periodicidade", XSD::QName.new(nil, "periodicidade")], [0, 1]],
      ["quantidadeTipoVigencia", ["SOAP::SOAPInt", XSD::QName.new(nil, "quantidadeTipoVigencia")], [0, 1]],
      ["ramoAtividade", ["RamoAtividade", XSD::QName.new(nil, "ramoAtividade")], [0, 1]],
      ["recebeCobranca", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "recebeCobranca")]],
      ["renovacaoAutomatica", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "renovacaoAutomatica")]],
      ["tipoContato", ["TipoContato", XSD::QName.new(nil, "tipoContato")], [0, 1]],
      ["tipoEndereco", ["TipoEndereco", XSD::QName.new(nil, "tipoEndereco")], [0, 1]],
      ["tipoFaturamento", ["TipoFaturamento", XSD::QName.new(nil, "tipoFaturamento")], [0, 1]],
      ["tipoPessoa", ["TipoPessoa", XSD::QName.new(nil, "tipoPessoa")], [0, 1]],
      ["tipoServico", ["TipoServico", XSD::QName.new(nil, "tipoServico")], [0, 1]],
      ["tipoVigencia", ["TipoVigencia", XSD::QName.new(nil, "tipoVigencia")], [0, 1]],
      ["usarFatorDeReajusteAutomatico", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "usarFatorDeReajusteAutomatico")]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverMotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["MotivoSuspensao", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverMotivoSuspensaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoSuspensaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarArquivo"),
    :schema_element => [
      ["arg0", ["Arquivo", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarArquivoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterServicoAliquotaPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterServicoAliquotaPrestador"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterServicoAliquotaPrestadorResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterServicoAliquotaPrestadorResponse"),
    :schema_element => [
      ["v_return", ["ServicoAliquotaPrestador", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTemplateEmail,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTemplateEmail"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarTemplateEmailResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTemplateEmailResponse"),
    :schema_element => [
      ["v_return", ["TemplateEmail[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarIndiceEconomico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomico"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarIndiceEconomicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomicoResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomico[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTributacaoMunicipio,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTributacaoMunicipio"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTributacaoMunicipioResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTributacaoMunicipioResponse"),
    :schema_element => [
      ["v_return", ["TributacaoMunicipio", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => TributacaoMunicipio,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tributacaoMunicipio"),
    :schema_element => [
      ["codigo", ["SOAP::SOAPString", XSD::QName.new(nil, "codigo")], [0, 1]],
      ["descricao", ["SOAP::SOAPString", XSD::QName.new(nil, "descricao")], [0, 1]],
      ["uuid", ["SOAP::SOAPString", XSD::QName.new(nil, "uuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarFamilia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarFamilia"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarFamiliaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarFamiliaResponse"),
    :schema_element => [
      ["v_return", ["Familia[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarPeriodicidade"),
    :schema_element => [
      ["arg0", ["PeriodicidadeFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => PeriodicidadeFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "periodicidadeFilter"),
    :schema_element => [
      ["ativo", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "ativo")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarPeriodicidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarPeriodicidadeResponse"),
    :schema_element => [
      ["v_return", ["Periodicidade[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarConfiguracao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarConfiguracao"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarConfiguracaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarConfiguracaoResponse"),
    :schema_element => [
      ["v_return", ["Configuracao[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServicoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoFilter"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]],
      ["codigoContrato", ["SOAP::SOAPString", XSD::QName.new(nil, "codigoContrato")], [0, 1]],
      ["excluindoUnidadesMetricas", ["UnidadeMetrica[]", XSD::QName.new(nil, "excluindoUnidadesMetricas")], [0, nil]],
      ["servicoEstado", ["ServicoEstado", XSD::QName.new(nil, "servicoEstado")], [0, 1]],
      ["servicoUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "servicoUuid")], [0, 1]],
      ["tipoConsumoEstado", ["TipoConsumoEstado", XSD::QName.new(nil, "tipoConsumoEstado")], [0, 1]],
      ["tipoServico", ["TipoServico", XSD::QName.new(nil, "tipoServico")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServico[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarRamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRamoAtividade"),
    :schema_element => [
      ["arg0", ["RamoAtividade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarRamoAtividadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRamoAtividadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarUf,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarUf"),
    :schema_element => [
      ["arg0", ["UfFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => UfFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "ufFilter"),
    :schema_element => [
      ["sigla", ["SOAP::SOAPString", XSD::QName.new(nil, "sigla")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarUfResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarUfResponse"),
    :schema_element => [
      ["v_return", ["Uf[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverRamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerRamoAtividade"),
    :schema_element => [
      ["arg0", ["RamoAtividade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverRamoAtividadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerRamoAtividadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarPeriodicidade"),
    :schema_element => [
      ["arg0", ["Periodicidade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarPeriodicidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarPeriodicidadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNF", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteNFResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ReativarCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "reativarCliente"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "arg1")]]
    ]
  )

  LiteralRegistry.register(
    :class => ReativarClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "reativarClienteResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCR", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServicoCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterCicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCicloFaturamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterCicloFaturamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCicloFaturamentoResponse"),
    :schema_element => [
      ["v_return", ["CicloFaturamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumo", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoConsumoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarServico"),
    :schema_element => [
      ["arg0", ["Servico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterUf,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterUf"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterUfResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterUfResponse"),
    :schema_element => [
      ["v_return", ["Uf", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalServicoAliquotaPrestador,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServicoAliquotaPrestador"),
    :schema_element => [
      ["arg0", ["ServicoAliquotaPrestadorFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalServicoAliquotaPrestadorResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServicoAliquotaPrestadorResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverFamilia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerFamilia"),
    :schema_element => [
      ["arg0", ["Familia", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverFamiliaResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerFamiliaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarContato"),
    :schema_element => [
      ["arg0", ["ContatoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ContatoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "contatoFilter"),
    :schema_element => [
      ["clienteUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "clienteUuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarContatoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarContatoResponse"),
    :schema_element => [
      ["v_return", ["Contato[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => EnviarNFsEmissao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "enviarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => EnviarNFsEmissaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "enviarNFsEmissaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterSegmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterSegmento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterSegmentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterSegmentoResponse"),
    :schema_element => [
      ["v_return", ["Segmento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMotivoSuspensaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoSuspensaoResponse"),
    :schema_element => [
      ["v_return", ["MotivoSuspensao", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteNF,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNF", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteNFResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteNFResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverMotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["MotivoCancelamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverMotivoCancelamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoCancelamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverSegmento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerSegmento"),
    :schema_element => [
      ["arg0", ["Segmento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverSegmentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerSegmentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterRegra,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegra"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRegraResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegraResponse"),
    :schema_element => [
      ["v_return", ["Regra", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterIndiceEconomico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterIndiceEconomicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomicoResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarArquivo"),
    :schema_element => [
      ["arg0", ["Arquivo", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarArquivoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarCicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarCicloFaturamento"),
    :schema_element => [
      ["arg0", ["CicloFaturamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarCicloFaturamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarCicloFaturamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCR", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumo", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoConsumoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarTributacaoMunicipio,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTributacaoMunicipio"),
    :schema_element => [
      ["arg0", ["TributacaoMunicipioFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => TributacaoMunicipioFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tributacaoMunicipioFilter"),
    :schema_element => [
      ["cidadeUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "cidadeUuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTributacaoMunicipioResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarTributacaoMunicipioResponse"),
    :schema_element => [
      ["v_return", ["TributacaoMunicipio[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarSimplesNacional,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarSimplesNacional"),
    :schema_element => [
      ["arg0", ["SimplesNacionalFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SimplesNacionalFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "simplesNacionalFilter"),
    :schema_element => [
      ["cidadeUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "cidadeUuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarSimplesNacionalResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarSimplesNacionalResponse"),
    :schema_element => [
      ["v_return", ["SimplesNacional[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarPeriodicidade"),
    :schema_element => [
      ["arg0", ["Periodicidade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarPeriodicidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarPeriodicidadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumo", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoConsumoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterIndiceEconomicoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomicoValor"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterIndiceEconomicoValorResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomicoValorResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomicoValor", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarArquivo"),
    :schema_element => [
      ["arg0", ["ArquivoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarArquivoResponse"),
    :schema_element => [
      ["v_return", ["Arquivo[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => FecharClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "fecharClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => FecharClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "fecharClienteServicoConsumoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterRamoAtividade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRamoAtividade"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRamoAtividadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterRamoAtividadeResponse"),
    :schema_element => [
      ["v_return", ["RamoAtividade", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerPeriodicidade"),
    :schema_element => [
      ["arg0", ["Periodicidade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverPeriodicidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerPeriodicidadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCRFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteCR[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarCidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCidade"),
    :schema_element => [
      ["arg0", ["CidadeFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CidadeFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cidadeFilter"),
    :schema_element => [
      ["disponivelParaCadastro", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "disponivelParaCadastro")], [0, 1]],
      ["nome", ["SOAP::SOAPString", XSD::QName.new(nil, "nome")], [0, 1]],
      ["uf", ["Uf", XSD::QName.new(nil, "uf")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarCidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCidadeResponse"),
    :schema_element => [
      ["v_return", ["Cidade[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerEndereco"),
    :schema_element => [
      ["arg0", ["Endereco", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverEnderecoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerEnderecoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AutorizarClienteCartao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "autorizarClienteCartao"),
    :schema_element => [
      ["arg0", ["ClienteCartao", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutorizarClienteCartaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "autorizarClienteCartaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarNaturezaOperacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarNaturezaOperacao"),
    :schema_element => [
      ["arg0", ["NaturezaOperacaoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => NaturezaOperacaoFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "naturezaOperacaoFilter"),
    :schema_element => [
      ["cidadeUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "cidadeUuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarNaturezaOperacaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarNaturezaOperacaoResponse"),
    :schema_element => [
      ["v_return", ["NaturezaOperacao[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTemplateEmailPorTipo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmailPorTipo"),
    :schema_element => [
      ["arg0", ["TipoTemplateEmail", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTemplateEmailPorTipoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmailPorTipoResponse"),
    :schema_element => [
      ["v_return", ["TemplateEmail", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCRFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteServicoCRFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteServicoCRFilter"),
    :schema_element => [
      ["uuidCliente", ["SOAP::SOAPString", XSD::QName.new(nil, "uuidCliente")], [0, 1]],
      ["uuidClienteCR", ["SOAP::SOAPString", XSD::QName.new(nil, "uuidClienteCR")], [0, 1]],
      ["uuidClienteServico", ["SOAP::SOAPString", XSD::QName.new(nil, "uuidClienteServico")], [0, 1]],
      ["uuidClienteServicoConsumo", ["SOAP::SOAPString", XSD::QName.new(nil, "uuidClienteServicoConsumo")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServicoCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoCR[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverCicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerCicloFaturamento"),
    :schema_element => [
      ["arg0", ["CicloFaturamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverCicloFaturamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerCicloFaturamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterClienteServicoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteServicoConsumoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoConsumoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoConsumo", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCR", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarMotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoCancelamento"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarMotivoCancelamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoCancelamentoResponse"),
    :schema_element => [
      ["v_return", ["MotivoCancelamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarMotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["MotivoCancelamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarMotivoCancelamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoCancelamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterMotivoCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMotivoCancelamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoCancelamentoResponse"),
    :schema_element => [
      ["v_return", ["MotivoCancelamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarCicloFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarCicloFaturamento"),
    :schema_element => [
      ["arg0", ["CicloFaturamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarCicloFaturamentoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarCicloFaturamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarContato"),
    :schema_element => [
      ["arg0", ["Contato", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarContatoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarContatoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCliente"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteResponse"),
    :schema_element => [
      ["v_return", ["Cliente", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CancelarClienteCR,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteCR"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CancelarClienteCRResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoPorClienteServicoUuid,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoPorClienteServicoUuidResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarRegimeTributario,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegimeTributario"),
    :schema_element => [
      ["arg0", ["RegimeTributarioFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RegimeTributarioFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "regimeTributarioFilter"),
    :schema_element => [
      ["cidadeUuid", ["SOAP::SOAPString", XSD::QName.new(nil, "cidadeUuid")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarRegimeTributarioResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegimeTributarioResponse"),
    :schema_element => [
      ["v_return", ["RegimeTributario[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCRTransacaoBoleto,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRTransacaoBoleto"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCRTransacaoBoletoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRTransacaoBoletoResponse"),
    :schema_element => [
      ["v_return", ["ClienteCRTransacaoBoleto", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteCartao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCartao"),
    :schema_element => [
      ["arg0", ["ClienteCartao", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteCartaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCartaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarCliente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarCliente"),
    :schema_element => [
      ["arg0", ["ClienteFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClienteFilter,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clienteFilter"),
    :schema_element => [
      ["cpfCnpj", ["SOAP::SOAPString", XSD::QName.new(nil, "cpfCnpj")], [0, 1]],
      ["generalFilter", ["GeneralFilter", XSD::QName.new(nil, "generalFilter")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteResponse"),
    :schema_element => [
      ["v_return", ["Cliente[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverArquivo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerArquivo"),
    :schema_element => [
      ["arg0", ["Arquivo", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverArquivoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "removerArquivoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterCidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCidade"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterCidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterCidadeResponse"),
    :schema_element => [
      ["v_return", ["Cidade", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClonarServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clonarServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClonarServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "clonarServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterPeriodicidade"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterPeriodicidadeResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "obterPeriodicidadeResponse"),
    :schema_element => [
      ["v_return", ["Periodicidade", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarServico"),
    :schema_element => [
      ["arg0", ["ServicoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarServicoResponse"),
    :schema_element => [
      ["v_return", ["Servico[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarMotivoSuspensao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoSuspensao"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarMotivoSuspensaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoSuspensaoResponse"),
    :schema_element => [
      ["v_return", ["MotivoSuspensao[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarServico"),
    :schema_element => [
      ["arg0", ["Servico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarServicoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ConsultarNFsEmissao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "consultarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ConsultarNFsEmissaoResponse,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "consultarNFsEmissaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => SmartbillAPIMessage,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "SmartbillAPIMessage"),
    :schema_element => [
      ["mensagem", ["SOAP::SOAPString", XSD::QName.new(nil, "mensagem")]],
      ["classe", ["SOAP::SOAPString", XSD::QName.new(nil, "classe")]],
      ["metodo", ["SOAP::SOAPString", XSD::QName.new(nil, "metodo")]],
      ["codigo", ["Status", XSD::QName.new(nil, "codigo")]],
      ["objetoRetorno", ["SmartbillObjetoRetorno", XSD::QName.new(nil, "objetoRetorno")]],
      ["objetosRetorno", ["SmartbillObjetoRetorno[]", XSD::QName.new(nil, "objetosRetorno")], [0, nil]],
      ["smartbillStackTrace", ["SOAP::SOAPString", XSD::QName.new(nil, "smartbillStackTrace")]]
    ]
  )

  LiteralRegistry.register(
    :class => Status,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "status")
  )

  LiteralRegistry.register(
    :class => TipoEndereco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoEndereco")
  )

  LiteralRegistry.register(
    :class => TipoConsumo,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoConsumo")
  )

  LiteralRegistry.register(
    :class => TipoConsumoEstado,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoConsumoEstado")
  )

  LiteralRegistry.register(
    :class => UnidadeMedida,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "unidadeMedida")
  )

  LiteralRegistry.register(
    :class => UnidadeMetrica,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "unidadeMetrica")
  )

  LiteralRegistry.register(
    :class => TipoValor,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoValor")
  )

  LiteralRegistry.register(
    :class => TipoContato,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoContato")
  )

  LiteralRegistry.register(
    :class => TipoIndiceEconomico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoIndiceEconomico")
  )

  LiteralRegistry.register(
    :class => TipoParcela,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoParcela")
  )

  LiteralRegistry.register(
    :class => TipoPeriodicidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoPeriodicidade")
  )

  LiteralRegistry.register(
    :class => TipoServico,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoServico")
  )

  LiteralRegistry.register(
    :class => ServicoEstado,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "servicoEstado")
  )

  LiteralRegistry.register(
    :class => TipoFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoFaturamento")
  )

  LiteralRegistry.register(
    :class => TipoVigencia,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoVigencia")
  )

  LiteralRegistry.register(
    :class => ProjecaoFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "projecaoFaturamento")
  )

  LiteralRegistry.register(
    :class => TipoCREstado,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoCREstado")
  )

  LiteralRegistry.register(
    :class => TipoAmbiente,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoAmbiente")
  )

  LiteralRegistry.register(
    :class => TipoNFCancelamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoNFCancelamento")
  )

  LiteralRegistry.register(
    :class => TipoNFEstado,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoNFEstado")
  )

  LiteralRegistry.register(
    :class => TipoRelacionamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoRelacionamento")
  )

  LiteralRegistry.register(
    :class => Criticidade,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "criticidade")
  )

  LiteralRegistry.register(
    :class => TipoPessoa,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoPessoa")
  )

  LiteralRegistry.register(
    :class => TipoLancamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoLancamento")
  )

  LiteralRegistry.register(
    :class => TipoMeioPagamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoMeioPagamento")
  )

  LiteralRegistry.register(
    :class => TipoTemplateEmail,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoTemplateEmail")
  )

  LiteralRegistry.register(
    :class => BloqueioFaturamento,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "bloqueioFaturamento")
  )

  LiteralRegistry.register(
    :class => TipoBandeira,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoBandeira")
  )

  LiteralRegistry.register(
    :class => DocumentoFiscal,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "documentoFiscal")
  )

  LiteralRegistry.register(
    :class => TipoBanco,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoBanco")
  )

  LiteralRegistry.register(
    :class => TipoStatusEmailCobranca,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoStatusEmailCobranca")
  )

  LiteralRegistry.register(
    :class => TipoStatusGateway,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoStatusGateway")
  )

  LiteralRegistry.register(
    :class => TipoClienteCRTransacao,
    :schema_type => XSD::QName.new(NsWsApiSmartbillComBr, "tipoClienteCRTransacao")
  )

  LiteralRegistry.register(
    :class => AdicionarArquivo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarArquivo"),
    :schema_element => [
      ["arg0", ["Arquivo", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarArquivoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarArquivoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarCicloFaturamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarCicloFaturamento"),
    :schema_element => [
      ["arg0", ["CicloFaturamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarCicloFaturamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarCicloFaturamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarCliente,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarCliente"),
    :schema_element => [
      ["arg0", ["Cliente", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCR", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarClienteNF,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNF", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteNFResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteNFResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarClienteResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServicoCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCR", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServicoCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServicoConsumo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumo", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServicoConsumoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoConsumoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarClienteServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarClienteServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarContato,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarContato"),
    :schema_element => [
      ["arg0", ["Contato", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarContatoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarContatoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarEndereco,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarEndereco"),
    :schema_element => [
      ["arg0", ["Endereco", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarEnderecoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarEnderecoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarFamilia,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarFamilia"),
    :schema_element => [
      ["arg0", ["Familia", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarFamiliaResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarFamiliaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarMotivoCancelamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["MotivoCancelamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarMotivoCancelamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoCancelamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarMotivoSuspensao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["MotivoSuspensao", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarMotivoSuspensaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarMotivoSuspensaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarPeriodicidade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarPeriodicidade"),
    :schema_element => [
      ["arg0", ["Periodicidade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarPeriodicidadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarPeriodicidadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarRamoAtividade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRamoAtividade"),
    :schema_element => [
      ["arg0", ["RamoAtividade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarRamoAtividadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRamoAtividadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarRelacionamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRelacionamento"),
    :schema_element => [
      ["arg0", ["Relacionamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarRelacionamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarRelacionamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarSegmento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarSegmento"),
    :schema_element => [
      ["arg0", ["Segmento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarSegmentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarSegmentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AdicionarServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarServico"),
    :schema_element => [
      ["arg0", ["Servico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AdicionarServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "adicionarServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarArquivo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarArquivo"),
    :schema_element => [
      ["arg0", ["Arquivo", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarArquivoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarArquivoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarCicloFaturamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarCicloFaturamento"),
    :schema_element => [
      ["arg0", ["CicloFaturamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarCicloFaturamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarCicloFaturamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarCliente,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarCliente"),
    :schema_element => [
      ["arg0", ["Cliente", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCR", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteCRCadastro,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCRCadastro"),
    :schema_element => [
      ["arg0", ["ClienteCRCadastro", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteCRCadastroResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCRCadastroResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteNF,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNF", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteNFResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteNFResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCR", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoConsumo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumo", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoConsumoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoConsumoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoPorClienteServicoUuid,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoPorClienteServicoUuidResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarClienteServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarClienteServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarContato,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarContato"),
    :schema_element => [
      ["arg0", ["Contato", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarContatoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarContatoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarEndereco,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarEndereco"),
    :schema_element => [
      ["arg0", ["Endereco", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarEnderecoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarEnderecoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarFamilia,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarFamilia"),
    :schema_element => [
      ["arg0", ["Familia", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarFamiliaResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarFamiliaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarMotivoCancelamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["MotivoCancelamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarMotivoCancelamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoCancelamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarMotivoSuspensao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["MotivoSuspensao", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarMotivoSuspensaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarMotivoSuspensaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarPeriodicidade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarPeriodicidade"),
    :schema_element => [
      ["arg0", ["Periodicidade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarPeriodicidadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarPeriodicidadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarRamoAtividade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRamoAtividade"),
    :schema_element => [
      ["arg0", ["RamoAtividade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarRamoAtividadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRamoAtividadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarRelacionamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRelacionamento"),
    :schema_element => [
      ["arg0", ["Relacionamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarRelacionamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarRelacionamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarSegmento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarSegmento"),
    :schema_element => [
      ["arg0", ["Segmento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarSegmentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarSegmentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AlterarServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarServico"),
    :schema_element => [
      ["arg0", ["Servico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AlterarServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "alterarServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AtivarClienteServicoPorClienteServicoUuid,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "ativarClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AtivarClienteServicoPorClienteServicoUuidResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "ativarClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => AutorizarClienteCartao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "autorizarClienteCartao"),
    :schema_element => [
      ["arg0", ["ClienteCartao", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AutorizarClienteCartaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "autorizarClienteCartaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => BaixarCobranca,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "baixarCobranca"),
    :schema_element => [
      ["arg0", ["Lancamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => BaixarCobrancaResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "baixarCobrancaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => CancelarClienteCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteCR"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CancelarClienteCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => CancelarClienteServicoPorClienteServicoUuid,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CancelarClienteServicoPorClienteServicoUuidResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => CancelarNFsEmissao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CancelarNFsEmissaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "cancelarNFsEmissaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ClonarClienteServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "clonarClienteServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClonarClienteServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "clonarClienteServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ClonarServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "clonarServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ClonarServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "clonarServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ConsultarCobranca,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "consultarCobranca"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ConsultarCobrancaResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "consultarCobrancaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ConsultarNFsEmissao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "consultarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ConsultarNFsEmissaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "consultarNFsEmissaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => EmailCobranca,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "emailCobranca"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => EmailCobrancaResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "emailCobrancaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => EnviarNFsEmissao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "enviarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => EnviarNFsEmissaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "enviarNFsEmissaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => FecharClienteServicoConsumo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "fecharClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => FecharClienteServicoConsumoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "fecharClienteServicoConsumoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => GerarCobranca,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "gerarCobranca"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GerarCobrancaResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "gerarCobrancaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => GerarNFsEmissao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "gerarNFsEmissao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GerarNFsEmissaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "gerarNFsEmissaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarArquivo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarArquivo"),
    :schema_element => [
      ["arg0", ["ArquivoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarArquivoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarArquivoResponse"),
    :schema_element => [
      ["v_return", ["Arquivo[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarCicloFaturamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarCicloFaturamento"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarCicloFaturamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarCicloFaturamentoResponse"),
    :schema_element => [
      ["v_return", ["CicloFaturamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarCidade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarCidade"),
    :schema_element => [
      ["arg0", ["CidadeFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarCidadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarCidadeResponse"),
    :schema_element => [
      ["v_return", ["Cidade[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarCliente,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarCliente"),
    :schema_element => [
      ["arg0", ["ClienteFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCRFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteCR[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteNF,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNFFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteNFResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteNFResponse"),
    :schema_element => [
      ["v_return", ["ClienteNF[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteResponse"),
    :schema_element => [
      ["v_return", ["Cliente[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServicoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServicoCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCRFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServicoCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoCR[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServicoConsumo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServicoConsumoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoConsumoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoConsumo[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarClienteServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarClienteServicoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServico[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarCodigoCNAE,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarCodigoCNAE"),
    :schema_element => [
      ["arg0", ["CodigoCNAEFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarCodigoCNAEResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarCodigoCNAEResponse"),
    :schema_element => [
      ["v_return", ["CodigoCNAE[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarConfiguracao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarConfiguracao"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarConfiguracaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarConfiguracaoResponse"),
    :schema_element => [
      ["v_return", ["Configuracao[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarContato,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarContato"),
    :schema_element => [
      ["arg0", ["ContatoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarContatoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarContatoResponse"),
    :schema_element => [
      ["v_return", ["Contato[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarEndereco,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarEndereco"),
    :schema_element => [
      ["arg0", ["EnderecoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarEnderecoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarEnderecoResponse"),
    :schema_element => [
      ["v_return", ["Endereco[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarFamilia,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarFamilia"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarFamiliaResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarFamiliaResponse"),
    :schema_element => [
      ["v_return", ["Familia[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarIndiceEconomico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomico"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarIndiceEconomicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomicoResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomico[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarIndiceEconomicoValor,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomicoValor"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarIndiceEconomicoValorResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarIndiceEconomicoValorResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomicoValor[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarMeioPagamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarMeioPagamento"),
    :schema_element => [
      ["arg0", ["MeioPagamentoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarMeioPagamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarMeioPagamentoResponse"),
    :schema_element => [
      ["v_return", ["MeioPagamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarMotivoCancelamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoCancelamento"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarMotivoCancelamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoCancelamentoResponse"),
    :schema_element => [
      ["v_return", ["MotivoCancelamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarMotivoSuspensao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoSuspensao"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarMotivoSuspensaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarMotivoSuspensaoResponse"),
    :schema_element => [
      ["v_return", ["MotivoSuspensao[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarNaturezaOperacao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarNaturezaOperacao"),
    :schema_element => [
      ["arg0", ["NaturezaOperacaoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarNaturezaOperacaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarNaturezaOperacaoResponse"),
    :schema_element => [
      ["v_return", ["NaturezaOperacao[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarPeriodicidade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarPeriodicidade"),
    :schema_element => [
      ["arg0", ["PeriodicidadeFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarPeriodicidadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarPeriodicidadeResponse"),
    :schema_element => [
      ["v_return", ["Periodicidade[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarRamoAtividade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarRamoAtividade"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarRamoAtividadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarRamoAtividadeResponse"),
    :schema_element => [
      ["v_return", ["RamoAtividade[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarRegimeTributario,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegimeTributario"),
    :schema_element => [
      ["arg0", ["RegimeTributarioFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarRegimeTributarioResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegimeTributarioResponse"),
    :schema_element => [
      ["v_return", ["RegimeTributario[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarRegra,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegra"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarRegraResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarRegraResponse"),
    :schema_element => [
      ["v_return", ["Regra[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarRelacionamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarRelacionamento"),
    :schema_element => [
      ["arg0", ["RelacionamentoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarRelacionamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarRelacionamentoResponse"),
    :schema_element => [
      ["v_return", ["Relacionamento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarSegmento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarSegmento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarSegmentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarSegmentoResponse"),
    :schema_element => [
      ["v_return", ["Segmento[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarServico"),
    :schema_element => [
      ["arg0", ["ServicoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarServicoAliquotaPrestador,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarServicoAliquotaPrestador"),
    :schema_element => [
      ["arg0", ["ServicoAliquotaPrestadorFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarServicoAliquotaPrestadorResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarServicoAliquotaPrestadorResponse"),
    :schema_element => [
      ["v_return", ["ServicoAliquotaPrestador[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarServicoResponse"),
    :schema_element => [
      ["v_return", ["Servico[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarSimplesNacional,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarSimplesNacional"),
    :schema_element => [
      ["arg0", ["SimplesNacionalFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarSimplesNacionalResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarSimplesNacionalResponse"),
    :schema_element => [
      ["v_return", ["SimplesNacional[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTemplateEmail,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTemplateEmail"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ListarTemplateEmailResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTemplateEmailResponse"),
    :schema_element => [
      ["v_return", ["TemplateEmail[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalArquivo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalArquivo"),
    :schema_element => [
      ["arg0", ["ArquivoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalArquivoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalArquivoResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCRFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCRResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteCRTransacaoBoleto,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCRTransacaoBoleto"),
    :schema_element => [
      ["arg0", ["ClienteCRTransacaoBoletoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteCRTransacaoBoletoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteCRTransacaoBoletoResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteNF,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNFFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalClienteNFResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalClienteNFResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServico"),
    :schema_element => [
      ["arg0", ["ServicoFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalServicoAliquotaPrestador,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServicoAliquotaPrestador"),
    :schema_element => [
      ["arg0", ["ServicoAliquotaPrestadorFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalServicoAliquotaPrestadorResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServicoAliquotaPrestadorResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTotalServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTotalServicoResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(nil, "return")]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTributacaoMunicipio,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTributacaoMunicipio"),
    :schema_element => [
      ["arg0", ["TributacaoMunicipioFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarTributacaoMunicipioResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarTributacaoMunicipioResponse"),
    :schema_element => [
      ["v_return", ["TributacaoMunicipio[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarUf,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarUf"),
    :schema_element => [
      ["arg0", ["UfFilter", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ListarUfResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "listarUfResponse"),
    :schema_element => [
      ["v_return", ["Uf[]", XSD::QName.new(nil, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterArquivo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterArquivo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterArquivoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterArquivoResponse"),
    :schema_element => [
      ["v_return", ["Arquivo", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterCicloFaturamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterCicloFaturamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterCicloFaturamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterCicloFaturamentoResponse"),
    :schema_element => [
      ["v_return", ["CicloFaturamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterCidade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterCidade"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterCidadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterCidadeResponse"),
    :schema_element => [
      ["v_return", ["Cidade", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterCliente,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterCliente"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCR"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCRCadastro,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRCadastro"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCRCadastroResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRCadastroResponse"),
    :schema_element => [
      ["v_return", ["ClienteCRCadastro", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteCR", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCRTransacaoBoleto,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRTransacaoBoleto"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteCRTransacaoBoletoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteCRTransacaoBoletoResponse"),
    :schema_element => [
      ["v_return", ["ClienteCRTransacaoBoleto", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteNF,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteNF"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteNFResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteNFResponse"),
    :schema_element => [
      ["v_return", ["ClienteNF", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteResponse"),
    :schema_element => [
      ["v_return", ["Cliente", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteServicoCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoCR"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteServicoCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoCRResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoCR", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteServicoConsumo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteServicoConsumoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoConsumoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServicoConsumo", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterClienteServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterClienteServicoResponse"),
    :schema_element => [
      ["v_return", ["ClienteServico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterCodigoCNAE,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterCodigoCNAE"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterCodigoCNAEResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterCodigoCNAEResponse"),
    :schema_element => [
      ["v_return", ["CodigoCNAE", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterConfiguracao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterConfiguracao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterConfiguracaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterConfiguracaoResponse"),
    :schema_element => [
      ["v_return", ["Configuracao", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterContato,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterContato"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterContatoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterContatoResponse"),
    :schema_element => [
      ["v_return", ["Contato", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterEndereco,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterEndereco"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterEnderecoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterEnderecoResponse"),
    :schema_element => [
      ["v_return", ["Endereco", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterFamilia,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterFamilia"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterFamiliaResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterFamiliaResponse"),
    :schema_element => [
      ["v_return", ["Familia", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterGerente,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterGerente"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterGerenteResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterGerenteResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterIndiceEconomico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterIndiceEconomicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomicoResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterIndiceEconomicoValor,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomicoValor"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterIndiceEconomicoValorResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterIndiceEconomicoValorResponse"),
    :schema_element => [
      ["v_return", ["IndiceEconomicoValor", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMeioPagamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterMeioPagamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMeioPagamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterMeioPagamentoResponse"),
    :schema_element => [
      ["v_return", ["MeioPagamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMotivoCancelamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMotivoCancelamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoCancelamentoResponse"),
    :schema_element => [
      ["v_return", ["MotivoCancelamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMotivoSuspensao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterMotivoSuspensaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterMotivoSuspensaoResponse"),
    :schema_element => [
      ["v_return", ["MotivoSuspensao", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterNaturezaOperacao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterNaturezaOperacao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterNaturezaOperacaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterNaturezaOperacaoResponse"),
    :schema_element => [
      ["v_return", ["NaturezaOperacao", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterPadraoPreenchimento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterPadraoPreenchimento"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ObterPadraoPreenchimentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterPadraoPreenchimentoResponse"),
    :schema_element => [
      ["v_return", ["PadraoPreenchimento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterPeriodicidade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterPeriodicidade"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterPeriodicidadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterPeriodicidadeResponse"),
    :schema_element => [
      ["v_return", ["Periodicidade", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRamoAtividade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterRamoAtividade"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRamoAtividadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterRamoAtividadeResponse"),
    :schema_element => [
      ["v_return", ["RamoAtividade", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRegimeTributario,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegimeTributario"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRegimeTributarioResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegimeTributarioResponse"),
    :schema_element => [
      ["v_return", ["RegimeTributario", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRegra,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegra"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRegraResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterRegraResponse"),
    :schema_element => [
      ["v_return", ["Regra", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRelacionamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterRelacionamento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterRelacionamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterRelacionamentoResponse"),
    :schema_element => [
      ["v_return", ["Relacionamento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterSegmento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterSegmento"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterSegmentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterSegmentoResponse"),
    :schema_element => [
      ["v_return", ["Segmento", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterServico"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterServicoAliquotaPrestador,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterServicoAliquotaPrestador"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterServicoAliquotaPrestadorResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterServicoAliquotaPrestadorResponse"),
    :schema_element => [
      ["v_return", ["ServicoAliquotaPrestador", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterServicoResponse"),
    :schema_element => [
      ["v_return", ["Servico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterSimplesNacional,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterSimplesNacional"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterSimplesNacionalResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterSimplesNacionalResponse"),
    :schema_element => [
      ["v_return", ["SimplesNacional", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTemplateEmail,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmail"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTemplateEmailPorTipo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmailPorTipo"),
    :schema_element => [
      ["arg0", ["TipoTemplateEmail", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTemplateEmailPorTipoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmailPorTipoResponse"),
    :schema_element => [
      ["v_return", ["TemplateEmail", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTemplateEmailResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterTemplateEmailResponse"),
    :schema_element => [
      ["v_return", ["TemplateEmail", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTributacaoMunicipio,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterTributacaoMunicipio"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterTributacaoMunicipioResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterTributacaoMunicipioResponse"),
    :schema_element => [
      ["v_return", ["TributacaoMunicipio", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterUf,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterUf"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterUfResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterUfResponse"),
    :schema_element => [
      ["v_return", ["Uf", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterUuidUsuario,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterUuidUsuario"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ObterUuidUsuarioResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "obterUuidUsuarioResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ReabrirClienteServicoConsumo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "reabrirClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ReabrirClienteServicoConsumoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "reabrirClienteServicoConsumoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => ReativarCliente,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "reativarCliente"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPBoolean", XSD::QName.new(nil, "arg1")]]
    ]
  )

  LiteralRegistry.register(
    :class => ReativarClienteResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "reativarClienteResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RegraClienteServicoImpostos,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "regraClienteServicoImpostos"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RegraClienteServicoImpostosResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "regraClienteServicoImpostosResponse"),
    :schema_element => [
      ["v_return", ["ClienteServico", XSD::QName.new(nil, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverArquivo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerArquivo"),
    :schema_element => [
      ["arg0", ["Arquivo", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverArquivoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerArquivoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverCicloFaturamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerCicloFaturamento"),
    :schema_element => [
      ["arg0", ["CicloFaturamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverCicloFaturamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerCicloFaturamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverCliente,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerCliente"),
    :schema_element => [
      ["arg0", ["Cliente", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCR"),
    :schema_element => [
      ["arg0", ["ClienteCR", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverClienteCartao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCartao"),
    :schema_element => [
      ["arg0", ["ClienteCartao", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteCartaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteCartaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverClienteNF,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteNF"),
    :schema_element => [
      ["arg0", ["ClienteNF", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteNFResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteNFResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverClienteResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverClienteServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServico"),
    :schema_element => [
      ["arg0", ["ClienteServico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteServicoCR,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoCR"),
    :schema_element => [
      ["arg0", ["ClienteServicoCR", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteServicoCRResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoCRResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverClienteServicoConsumo,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoConsumo"),
    :schema_element => [
      ["arg0", ["ClienteServicoConsumo", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverClienteServicoConsumoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoConsumoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverClienteServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerClienteServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverContato,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerContato"),
    :schema_element => [
      ["arg0", ["Contato", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverContatoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerContatoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverEndereco,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerEndereco"),
    :schema_element => [
      ["arg0", ["Endereco", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverEnderecoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerEnderecoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverFamilia,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerFamilia"),
    :schema_element => [
      ["arg0", ["Familia", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverFamiliaResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerFamiliaResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverMotivoCancelamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoCancelamento"),
    :schema_element => [
      ["arg0", ["MotivoCancelamento", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverMotivoCancelamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoCancelamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverMotivoSuspensao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoSuspensao"),
    :schema_element => [
      ["arg0", ["MotivoSuspensao", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverMotivoSuspensaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerMotivoSuspensaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverPeriodicidade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerPeriodicidade"),
    :schema_element => [
      ["arg0", ["Periodicidade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverPeriodicidadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerPeriodicidadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverRamoAtividade,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerRamoAtividade"),
    :schema_element => [
      ["arg0", ["RamoAtividade", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverRamoAtividadeResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerRamoAtividadeResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverRelacionamento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerRelacionamento"),
    :schema_element => [
      ["arg0", ["Relacionamento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverRelacionamentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerRelacionamentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverSegmento,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerSegmento"),
    :schema_element => [
      ["arg0", ["Segmento", XSD::QName.new(nil, "arg0")], [0, 1]],
      ["arg1", ["SOAP::SOAPString", XSD::QName.new(nil, "arg1")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverSegmentoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerSegmentoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => RemoverServico,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerServico"),
    :schema_element => [
      ["arg0", ["Servico", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RemoverServicoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "removerServicoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => SolicitarClienteCartao,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "solicitarClienteCartao"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SolicitarClienteCartaoResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "solicitarClienteCartaoResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => SuspenderClienteServicoPorClienteServicoUuid,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "suspenderClienteServicoPorClienteServicoUuid"),
    :schema_element => [
      ["arg0", ["SOAP::SOAPString", XSD::QName.new(nil, "arg0")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SuspenderClienteServicoPorClienteServicoUuidResponse,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "suspenderClienteServicoPorClienteServicoUuidResponse"),
    :schema_element => []
  )

  LiteralRegistry.register(
    :class => SmartbillAPIMessage,
    :schema_name => XSD::QName.new(NsWsApiSmartbillComBr, "SmartbillAPIMessage"),
    :schema_element => [
      ["mensagem", ["SOAP::SOAPString", XSD::QName.new(nil, "mensagem")]],
      ["classe", ["SOAP::SOAPString", XSD::QName.new(nil, "classe")]],
      ["metodo", ["SOAP::SOAPString", XSD::QName.new(nil, "metodo")]],
      ["codigo", ["Status", XSD::QName.new(nil, "codigo")]],
      ["objetoRetorno", ["SmartbillObjetoRetorno", XSD::QName.new(nil, "objetoRetorno")]],
      ["objetosRetorno", ["SmartbillObjetoRetorno[]", XSD::QName.new(nil, "objetosRetorno")], [0, nil]],
      ["smartbillStackTrace", ["SOAP::SOAPString", XSD::QName.new(nil, "smartbillStackTrace")]]
    ]
  )
end
