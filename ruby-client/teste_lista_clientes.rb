#!/usr/bin/env ruby
require './SmartbillAPIServiceDriver.rb'
require './SmartbillAPIService.rb'

endpoint_url = "https://ws.api.smartbill.com.br:8443/SmartbillAPI?WSDL"
service = SmartbillAPI.new(endpoint_url)
service.options["protocol.http.basic_auth"] << [endpoint_url, "demo@smartbill.com.br", "demo"]

# run ruby with -d to see SOAP wiredumps.
service.wiredump_dev = STDERR if $DEBUG


# Smartbill Sistemas e Servicos S.A.
#
# Matheus Santos (matheus@smartbill.com.br)
#
# Created at:
# 2013-10-27 14:55:03

puts
puts
puts
puts "-------------------------------------------------------------"
puts "-- Smartbill Ruby Client                                   --"
puts "-------------------------------------------------------------"
puts
puts "Conectando no webservice e baixando WSDL.."
puts
puts

#clientes = []



puts "Chamando listarCliente()#SmartbillAPI .."

########

# Busca de clientes sem filtro
clientes = service.listarCliente(nil) # sem filtro

## OU ##

# Declarando filtro de cliente
#clienteFilter = ClienteFilter.new
#clienteFilter.cpfCnpj = "00280273000722"

# Usando filtro na consulta
#clientes = service.listarCliente({arg0: clienteFilter})

#########

puts
puts 
puts "-- Lista de clientes da conta DEMO --"
puts

clientes.each do |cliente|
  
  puts "Tipo Pessoa: #{cliente.tipoPessoa} - Nome/Razao Social: #{cliente.razaoSocial} - CPF/CNPJ: #{cliente.cnpjCpf}"
  
end

puts
puts
puts "Fim"
puts
puts
