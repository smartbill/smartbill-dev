require 'xsd/qname'

# {http://ws.api.smartbill.com.br/}reabrirClienteServicoConsumo
#   arg0 - SOAP::SOAPString
class ReabrirClienteServicoConsumo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}reabrirClienteServicoConsumoResponse
class ReabrirClienteServicoConsumoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}smartbillObjetoRetorno
#   codigo - Status
#   mensagem - SOAP::SOAPString
#   modelo - SOAP::SOAPString
#   uuid - SOAP::SOAPString
#   webservice - SOAP::SOAPString
class SmartbillObjetoRetorno
  attr_accessor :codigo
  attr_accessor :mensagem
  attr_accessor :modelo
  attr_accessor :uuid
  attr_accessor :webservice

  def initialize(codigo = nil, mensagem = nil, modelo = nil, uuid = nil, webservice = nil)
    @codigo = codigo
    @mensagem = mensagem
    @modelo = modelo
    @uuid = uuid
    @webservice = webservice
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalClienteNF
#   arg0 - ClienteNFFilter
class ListarTotalClienteNF
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}clienteNFFilter
#   clienteCRUuid - SOAP::SOAPString
#   clienteUuid - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class ClienteNFFilter
  attr_accessor :clienteCRUuid
  attr_accessor :clienteUuid
  attr_accessor :uuid

  def initialize(clienteCRUuid = nil, clienteUuid = nil, uuid = nil)
    @clienteCRUuid = clienteCRUuid
    @clienteUuid = clienteUuid
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalClienteNFResponse
#   m_return - SOAP::SOAPLong
class ListarTotalClienteNFResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}listarEndereco
#   arg0 - EnderecoFilter
class ListarEndereco
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}enderecoFilter
#   clienteUuid - SOAP::SOAPString
#   enderecoDeFaturamento - SOAP::SOAPBoolean
#   tipoEndereco - TipoEndereco
class EnderecoFilter
  attr_accessor :clienteUuid
  attr_accessor :enderecoDeFaturamento
  attr_accessor :tipoEndereco

  def initialize(clienteUuid = nil, enderecoDeFaturamento = nil, tipoEndereco = nil)
    @clienteUuid = clienteUuid
    @enderecoDeFaturamento = enderecoDeFaturamento
    @tipoEndereco = tipoEndereco
  end
end

# {http://ws.api.smartbill.com.br/}listarEnderecoResponse
class ListarEnderecoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}endereco
#   bairro - SOAP::SOAPString
#   cep - SOAP::SOAPString
#   cidade - Cidade
#   complemento - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   enderecoDeFaturamento - SOAP::SOAPBoolean
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   logradouro - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   numero - SOAP::SOAPInt
#   tipoEndereco - TipoEndereco
#   uf - Uf
#   uuid - SOAP::SOAPString
class Endereco
  attr_accessor :bairro
  attr_accessor :cep
  attr_accessor :cidade
  attr_accessor :complemento
  attr_accessor :criacao
  attr_accessor :enderecoDeFaturamento
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :logradouro
  attr_accessor :modificacao
  attr_accessor :numero
  attr_accessor :tipoEndereco
  attr_accessor :uf
  attr_accessor :uuid

  def initialize(bairro = nil, cep = nil, cidade = nil, complemento = nil, criacao = nil, enderecoDeFaturamento = nil, exclusao = nil, gerenteUuid = nil, logradouro = nil, modificacao = nil, numero = nil, tipoEndereco = nil, uf = nil, uuid = nil)
    @bairro = bairro
    @cep = cep
    @cidade = cidade
    @complemento = complemento
    @criacao = criacao
    @enderecoDeFaturamento = enderecoDeFaturamento
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @logradouro = logradouro
    @modificacao = modificacao
    @numero = numero
    @tipoEndereco = tipoEndereco
    @uf = uf
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}cidade
#   disponivelParaCadastro - SOAP::SOAPBoolean
#   ibge - SOAP::SOAPLong
#   nome - SOAP::SOAPString
#   uf - Uf
#   uuid - SOAP::SOAPString
class Cidade
  attr_accessor :disponivelParaCadastro
  attr_accessor :ibge
  attr_accessor :nome
  attr_accessor :uf
  attr_accessor :uuid

  def initialize(disponivelParaCadastro = nil, ibge = nil, nome = nil, uf = nil, uuid = nil)
    @disponivelParaCadastro = disponivelParaCadastro
    @ibge = ibge
    @nome = nome
    @uf = uf
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}uf
#   nome - SOAP::SOAPString
#   sigla - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class Uf
  attr_accessor :nome
  attr_accessor :sigla
  attr_accessor :uuid

  def initialize(nome = nil, sigla = nil, uuid = nil)
    @nome = nome
    @sigla = sigla
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}listarIndiceEconomicoValor
#   arg0 - SOAP::SOAPString
class ListarIndiceEconomicoValor
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}listarIndiceEconomicoValorResponse
class ListarIndiceEconomicoValorResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}indiceEconomicoValor
#   ano - SOAP::SOAPInt
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   mes - SOAP::SOAPInt
#   modificacao - SOAP::SOAPDateTime
#   uuid - SOAP::SOAPString
#   valorIndiceMes - SOAP::SOAPDecimal
#   valorIndiceUltimos12Meses - SOAP::SOAPDecimal
class IndiceEconomicoValor
  attr_accessor :ano
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :mes
  attr_accessor :modificacao
  attr_accessor :uuid
  attr_accessor :valorIndiceMes
  attr_accessor :valorIndiceUltimos12Meses

  def initialize(ano = nil, criacao = nil, exclusao = nil, gerenteUuid = nil, mes = nil, modificacao = nil, uuid = nil, valorIndiceMes = nil, valorIndiceUltimos12Meses = nil)
    @ano = ano
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @mes = mes
    @modificacao = modificacao
    @uuid = uuid
    @valorIndiceMes = valorIndiceMes
    @valorIndiceUltimos12Meses = valorIndiceUltimos12Meses
  end
end

# {http://ws.api.smartbill.com.br/}clonarClienteServico
#   arg0 - SOAP::SOAPString
#   arg1 - SOAP::SOAPString
class ClonarClienteServico
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}clonarClienteServicoResponse
class ClonarClienteServicoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarClienteServicoConsumo
#   arg0 - ClienteServicoConsumoFilter
class ListarClienteServicoConsumo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoConsumoFilter
#   anoReferencia - SOAP::SOAPInt
#   clienteServicoUuid - SOAP::SOAPString
#   clienteUuid - SOAP::SOAPString
#   mesReferencia - SOAP::SOAPInt
#   tipoConsumo - TipoConsumo
#   tipoConsumoEstado - TipoConsumoEstado
class ClienteServicoConsumoFilter
  attr_accessor :anoReferencia
  attr_accessor :clienteServicoUuid
  attr_accessor :clienteUuid
  attr_accessor :mesReferencia
  attr_accessor :tipoConsumo
  attr_accessor :tipoConsumoEstado

  def initialize(anoReferencia = nil, clienteServicoUuid = nil, clienteUuid = nil, mesReferencia = nil, tipoConsumo = nil, tipoConsumoEstado = nil)
    @anoReferencia = anoReferencia
    @clienteServicoUuid = clienteServicoUuid
    @clienteUuid = clienteUuid
    @mesReferencia = mesReferencia
    @tipoConsumo = tipoConsumo
    @tipoConsumoEstado = tipoConsumoEstado
  end
end

# {http://ws.api.smartbill.com.br/}listarClienteServicoConsumoResponse
class ListarClienteServicoConsumoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}clienteServicoConsumo
#   anoReferencia - SOAP::SOAPInt
#   clienteServicoUuid - SOAP::SOAPString
#   clienteUuid - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   mesReferencia - SOAP::SOAPInt
#   modificacao - SOAP::SOAPDateTime
#   quantidades - ClienteServicoConsumoQuantidade
#   tipoConsumo - TipoConsumo
#   tipoConsumoEstado - TipoConsumoEstado
#   unidadeMedida - UnidadeMedida
#   unidadeMetrica - UnidadeMetrica
#   uuid - SOAP::SOAPString
class ClienteServicoConsumo
  attr_accessor :anoReferencia
  attr_accessor :clienteServicoUuid
  attr_accessor :clienteUuid
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :mesReferencia
  attr_accessor :modificacao
  attr_accessor :quantidades
  attr_accessor :tipoConsumo
  attr_accessor :tipoConsumoEstado
  attr_accessor :unidadeMedida
  attr_accessor :unidadeMetrica
  attr_accessor :uuid

  def initialize(anoReferencia = nil, clienteServicoUuid = nil, clienteUuid = nil, criacao = nil, exclusao = nil, gerenteUuid = nil, mesReferencia = nil, modificacao = nil, quantidades = [], tipoConsumo = nil, tipoConsumoEstado = nil, unidadeMedida = nil, unidadeMetrica = nil, uuid = nil)
    @anoReferencia = anoReferencia
    @clienteServicoUuid = clienteServicoUuid
    @clienteUuid = clienteUuid
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @mesReferencia = mesReferencia
    @modificacao = modificacao
    @quantidades = quantidades
    @tipoConsumo = tipoConsumo
    @tipoConsumoEstado = tipoConsumoEstado
    @unidadeMedida = unidadeMedida
    @unidadeMetrica = unidadeMetrica
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoConsumoQuantidade
#   criacao - SOAP::SOAPDateTime
#   descricao - SOAP::SOAPString
#   exclusao - SOAP::SOAPDateTime
#   fimConsumo - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   identificadorFaturamento - SOAP::SOAPString
#   inicioConsumo - SOAP::SOAPDateTime
#   modificacao - SOAP::SOAPDateTime
#   quantidade - SOAP::SOAPDecimal
#   uuid - SOAP::SOAPString
class ClienteServicoConsumoQuantidade
  attr_accessor :criacao
  attr_accessor :descricao
  attr_accessor :exclusao
  attr_accessor :fimConsumo
  attr_accessor :gerenteUuid
  attr_accessor :identificadorFaturamento
  attr_accessor :inicioConsumo
  attr_accessor :modificacao
  attr_accessor :quantidade
  attr_accessor :uuid

  def initialize(criacao = nil, descricao = nil, exclusao = nil, fimConsumo = nil, gerenteUuid = nil, identificadorFaturamento = nil, inicioConsumo = nil, modificacao = nil, quantidade = nil, uuid = nil)
    @criacao = criacao
    @descricao = descricao
    @exclusao = exclusao
    @fimConsumo = fimConsumo
    @gerenteUuid = gerenteUuid
    @identificadorFaturamento = identificadorFaturamento
    @inicioConsumo = inicioConsumo
    @modificacao = modificacao
    @quantidade = quantidade
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}listarClienteNF
#   arg0 - ClienteNFFilter
class ListarClienteNF
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}listarClienteNFResponse
class ListarClienteNFResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}clienteNF
#   alertaRetorno - SOAP::SOAPString
#   aliquotaCOFINS - SOAP::SOAPDecimal
#   aliquotaCSLL - SOAP::SOAPDecimal
#   aliquotaINSS - SOAP::SOAPDecimal
#   aliquotaIR - SOAP::SOAPDecimal
#   aliquotaISS - SOAP::SOAPDecimal
#   aliquotaISSRetido - SOAP::SOAPDecimal
#   aliquotaPIS - SOAP::SOAPDecimal
#   codigo - SOAP::SOAPString
#   codigoCNAE - SOAP::SOAPString
#   codigoServicoNF - SOAP::SOAPString
#   codigoVerificacao - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   dataEmissao - SOAP::SOAPDateTime
#   descricaoRPS - SOAP::SOAPString
#   erroRetorno - SOAP::SOAPString
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   incentivadorCultural - SOAP::SOAPString
#   loteNF - LoteNF
#   modificacao - SOAP::SOAPDateTime
#   motivoCancelamento - SOAP::SOAPString
#   naturezaOperacao - SOAP::SOAPString
#   numeroNFS - SOAP::SOAPInteger
#   numeroRPS - SOAP::SOAPLong
#   regimeTributario - SOAP::SOAPString
#   serieRPS - SOAP::SOAPString
#   servicosCR - ClienteServicoCR
#   simplesNacional - SOAP::SOAPString
#   textoEnvio - SOAP::SOAPString
#   textoRetorno - SOAP::SOAPString
#   tipoAmbiente - TipoAmbiente
#   tipoNFCancelamento - TipoNFCancelamento
#   tipoNFEstado - TipoNFEstado
#   tributacaoMunicipio - SOAP::SOAPString
#   urlConsulta - SOAP::SOAPString
#   uuid - SOAP::SOAPString
#   valorCOFINS - SOAP::SOAPDecimal
#   valorCSLL - SOAP::SOAPDecimal
#   valorDeducao - SOAP::SOAPDecimal
#   valorINSS - SOAP::SOAPDecimal
#   valorIR - SOAP::SOAPDecimal
#   valorISS - SOAP::SOAPDecimal
#   valorISSRetido - SOAP::SOAPDecimal
#   valorPIS - SOAP::SOAPDecimal
#   valorServicos - SOAP::SOAPDecimal
class ClienteNF
  attr_accessor :alertaRetorno
  attr_accessor :aliquotaCOFINS
  attr_accessor :aliquotaCSLL
  attr_accessor :aliquotaINSS
  attr_accessor :aliquotaIR
  attr_accessor :aliquotaISS
  attr_accessor :aliquotaISSRetido
  attr_accessor :aliquotaPIS
  attr_accessor :codigo
  attr_accessor :codigoCNAE
  attr_accessor :codigoServicoNF
  attr_accessor :codigoVerificacao
  attr_accessor :criacao
  attr_accessor :dataEmissao
  attr_accessor :descricaoRPS
  attr_accessor :erroRetorno
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :incentivadorCultural
  attr_accessor :loteNF
  attr_accessor :modificacao
  attr_accessor :motivoCancelamento
  attr_accessor :naturezaOperacao
  attr_accessor :numeroNFS
  attr_accessor :numeroRPS
  attr_accessor :regimeTributario
  attr_accessor :serieRPS
  attr_accessor :servicosCR
  attr_accessor :simplesNacional
  attr_accessor :textoEnvio
  attr_accessor :textoRetorno
  attr_accessor :tipoAmbiente
  attr_accessor :tipoNFCancelamento
  attr_accessor :tipoNFEstado
  attr_accessor :tributacaoMunicipio
  attr_accessor :urlConsulta
  attr_accessor :uuid
  attr_accessor :valorCOFINS
  attr_accessor :valorCSLL
  attr_accessor :valorDeducao
  attr_accessor :valorINSS
  attr_accessor :valorIR
  attr_accessor :valorISS
  attr_accessor :valorISSRetido
  attr_accessor :valorPIS
  attr_accessor :valorServicos

  def initialize(alertaRetorno = nil, aliquotaCOFINS = nil, aliquotaCSLL = nil, aliquotaINSS = nil, aliquotaIR = nil, aliquotaISS = nil, aliquotaISSRetido = nil, aliquotaPIS = nil, codigo = nil, codigoCNAE = nil, codigoServicoNF = nil, codigoVerificacao = nil, criacao = nil, dataEmissao = nil, descricaoRPS = nil, erroRetorno = nil, exclusao = nil, gerenteUuid = nil, incentivadorCultural = nil, loteNF = nil, modificacao = nil, motivoCancelamento = nil, naturezaOperacao = nil, numeroNFS = nil, numeroRPS = nil, regimeTributario = nil, serieRPS = nil, servicosCR = [], simplesNacional = nil, textoEnvio = nil, textoRetorno = nil, tipoAmbiente = nil, tipoNFCancelamento = nil, tipoNFEstado = nil, tributacaoMunicipio = nil, urlConsulta = nil, uuid = nil, valorCOFINS = nil, valorCSLL = nil, valorDeducao = nil, valorINSS = nil, valorIR = nil, valorISS = nil, valorISSRetido = nil, valorPIS = nil, valorServicos = nil)
    @alertaRetorno = alertaRetorno
    @aliquotaCOFINS = aliquotaCOFINS
    @aliquotaCSLL = aliquotaCSLL
    @aliquotaINSS = aliquotaINSS
    @aliquotaIR = aliquotaIR
    @aliquotaISS = aliquotaISS
    @aliquotaISSRetido = aliquotaISSRetido
    @aliquotaPIS = aliquotaPIS
    @codigo = codigo
    @codigoCNAE = codigoCNAE
    @codigoServicoNF = codigoServicoNF
    @codigoVerificacao = codigoVerificacao
    @criacao = criacao
    @dataEmissao = dataEmissao
    @descricaoRPS = descricaoRPS
    @erroRetorno = erroRetorno
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @incentivadorCultural = incentivadorCultural
    @loteNF = loteNF
    @modificacao = modificacao
    @motivoCancelamento = motivoCancelamento
    @naturezaOperacao = naturezaOperacao
    @numeroNFS = numeroNFS
    @numeroRPS = numeroRPS
    @regimeTributario = regimeTributario
    @serieRPS = serieRPS
    @servicosCR = servicosCR
    @simplesNacional = simplesNacional
    @textoEnvio = textoEnvio
    @textoRetorno = textoRetorno
    @tipoAmbiente = tipoAmbiente
    @tipoNFCancelamento = tipoNFCancelamento
    @tipoNFEstado = tipoNFEstado
    @tributacaoMunicipio = tributacaoMunicipio
    @urlConsulta = urlConsulta
    @uuid = uuid
    @valorCOFINS = valorCOFINS
    @valorCSLL = valorCSLL
    @valorDeducao = valorDeducao
    @valorINSS = valorINSS
    @valorIR = valorIR
    @valorISS = valorISS
    @valorISSRetido = valorISSRetido
    @valorPIS = valorPIS
    @valorServicos = valorServicos
  end
end

# {http://ws.api.smartbill.com.br/}loteNF
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   numero - SOAP::SOAPLong
#   urlEnvio - SOAP::SOAPString
#   urlRetorno - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class LoteNF
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :numero
  attr_accessor :urlEnvio
  attr_accessor :urlRetorno
  attr_accessor :uuid

  def initialize(criacao = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, numero = nil, urlEnvio = nil, urlRetorno = nil, uuid = nil)
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @numero = numero
    @urlEnvio = urlEnvio
    @urlRetorno = urlRetorno
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoCR
#   anoReferencia - SOAP::SOAPInt
#   clienteServico - ClienteServico
#   clienteServicoConsumo - ClienteServicoConsumo
#   clienteServicoParcelaAtivacao - ClienteServicoParcelaAtivacao
#   clienteServicoParcelaServico - ClienteServicoParcelaServico
#   criacao - SOAP::SOAPDateTime
#   dataEmissao - SOAP::SOAPDateTime
#   dataVencimento - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   mesReferencia - SOAP::SOAPInt
#   modificacao - SOAP::SOAPDateTime
#   projecaoFaturamento - ProjecaoFaturamento
#   referencia - SOAP::SOAPInt
#   serie - SOAP::SOAPString
#   subSerie - SOAP::SOAPString
#   tipoCREstado - TipoCREstado
#   uuid - SOAP::SOAPString
#   valorAtivacaoTotal - SOAP::SOAPDecimal
#   valorServicoTotal - SOAP::SOAPDecimal
class ClienteServicoCR
  attr_accessor :anoReferencia
  attr_accessor :clienteServico
  attr_accessor :clienteServicoConsumo
  attr_accessor :clienteServicoParcelaAtivacao
  attr_accessor :clienteServicoParcelaServico
  attr_accessor :criacao
  attr_accessor :dataEmissao
  attr_accessor :dataVencimento
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :mesReferencia
  attr_accessor :modificacao
  attr_accessor :projecaoFaturamento
  attr_accessor :referencia
  attr_accessor :serie
  attr_accessor :subSerie
  attr_accessor :tipoCREstado
  attr_accessor :uuid
  attr_accessor :valorAtivacaoTotal
  attr_accessor :valorServicoTotal

  def initialize(anoReferencia = nil, clienteServico = nil, clienteServicoConsumo = nil, clienteServicoParcelaAtivacao = nil, clienteServicoParcelaServico = nil, criacao = nil, dataEmissao = nil, dataVencimento = nil, exclusao = nil, gerenteUuid = nil, mesReferencia = nil, modificacao = nil, projecaoFaturamento = nil, referencia = nil, serie = nil, subSerie = nil, tipoCREstado = nil, uuid = nil, valorAtivacaoTotal = nil, valorServicoTotal = nil)
    @anoReferencia = anoReferencia
    @clienteServico = clienteServico
    @clienteServicoConsumo = clienteServicoConsumo
    @clienteServicoParcelaAtivacao = clienteServicoParcelaAtivacao
    @clienteServicoParcelaServico = clienteServicoParcelaServico
    @criacao = criacao
    @dataEmissao = dataEmissao
    @dataVencimento = dataVencimento
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @mesReferencia = mesReferencia
    @modificacao = modificacao
    @projecaoFaturamento = projecaoFaturamento
    @referencia = referencia
    @serie = serie
    @subSerie = subSerie
    @tipoCREstado = tipoCREstado
    @uuid = uuid
    @valorAtivacaoTotal = valorAtivacaoTotal
    @valorServicoTotal = valorServicoTotal
  end
end

# {http://ws.api.smartbill.com.br/}clienteServico
#   acrescimos - ClienteServicoAcrescimo
#   aliquotaCOFINS - SOAP::SOAPDecimal
#   aliquotaCSLL - SOAP::SOAPDecimal
#   aliquotaINSS - SOAP::SOAPDecimal
#   aliquotaIR - SOAP::SOAPDecimal
#   aliquotaISS - SOAP::SOAPDecimal
#   aliquotaISSRetido - SOAP::SOAPDecimal
#   aliquotaPIS - SOAP::SOAPDecimal
#   codigoConsolidaFatura - SOAP::SOAPString
#   codigoContrato - SOAP::SOAPString
#   contatos - Contato
#   criacao - SOAP::SOAPDateTime
#   dataAdesao - SOAP::SOAPDateTime
#   dataCancelamento - SOAP::SOAPDateTime
#   dataPrimeiroVencimento - SOAP::SOAPDateTime
#   dataSuspensaoFim - SOAP::SOAPDateTime
#   dataSuspensaoInicio - SOAP::SOAPDateTime
#   dataVigenciaFim - SOAP::SOAPDateTime
#   dataVigenciaInicio - SOAP::SOAPDateTime
#   descontos - ClienteServicoDesconto
#   exclusao - SOAP::SOAPDateTime
#   fatorDeReajusteDaRenovacao - IndiceEconomico
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   motivoCancelamento - MotivoCancelamento
#   motivoSuspensao - MotivoSuspensao
#   municipioPrestacaoServico - Cidade
#   naturezaOperacao - SOAP::SOAPString
#   observacao - SOAP::SOAPString
#   parcelasAtivacao - ClienteServicoParcelaAtivacao
#   parcelasServico - ClienteServicoParcelaServico
#   periodicidade - Periodicidade
#   quantidade - SOAP::SOAPDecimal
#   quantidadeTipoVigencia - SOAP::SOAPInt
#   renovacaoAutomatica - SOAP::SOAPBoolean
#   servico - Servico
#   servicoEstado - ServicoEstado
#   tarifaMinima - SOAP::SOAPDecimal
#   taxaAtivacao - SOAP::SOAPDecimal
#   tipoConsumo - TipoConsumo
#   tipoFaturamento - TipoFaturamento
#   tipoVigencia - TipoVigencia
#   unidadeMedida - UnidadeMedida
#   unidadeMetrica - UnidadeMetrica
#   usarFatorDeReajusteAutomatico - SOAP::SOAPBoolean
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
#   valores - ClienteServicoValor
#   vendedor - Usuario
class ClienteServico
  attr_accessor :acrescimos
  attr_accessor :aliquotaCOFINS
  attr_accessor :aliquotaCSLL
  attr_accessor :aliquotaINSS
  attr_accessor :aliquotaIR
  attr_accessor :aliquotaISS
  attr_accessor :aliquotaISSRetido
  attr_accessor :aliquotaPIS
  attr_accessor :codigoConsolidaFatura
  attr_accessor :codigoContrato
  attr_accessor :contatos
  attr_accessor :criacao
  attr_accessor :dataAdesao
  attr_accessor :dataCancelamento
  attr_accessor :dataPrimeiroVencimento
  attr_accessor :dataSuspensaoFim
  attr_accessor :dataSuspensaoInicio
  attr_accessor :dataVigenciaFim
  attr_accessor :dataVigenciaInicio
  attr_accessor :descontos
  attr_accessor :exclusao
  attr_accessor :fatorDeReajusteDaRenovacao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :motivoCancelamento
  attr_accessor :motivoSuspensao
  attr_accessor :municipioPrestacaoServico
  attr_accessor :naturezaOperacao
  attr_accessor :observacao
  attr_accessor :parcelasAtivacao
  attr_accessor :parcelasServico
  attr_accessor :periodicidade
  attr_accessor :quantidade
  attr_accessor :quantidadeTipoVigencia
  attr_accessor :renovacaoAutomatica
  attr_accessor :servico
  attr_accessor :servicoEstado
  attr_accessor :tarifaMinima
  attr_accessor :taxaAtivacao
  attr_accessor :tipoConsumo
  attr_accessor :tipoFaturamento
  attr_accessor :tipoVigencia
  attr_accessor :unidadeMedida
  attr_accessor :unidadeMetrica
  attr_accessor :usarFatorDeReajusteAutomatico
  attr_accessor :uuid
  attr_accessor :valor
  attr_accessor :valores
  attr_accessor :vendedor

  def initialize(acrescimos = [], aliquotaCOFINS = nil, aliquotaCSLL = nil, aliquotaINSS = nil, aliquotaIR = nil, aliquotaISS = nil, aliquotaISSRetido = nil, aliquotaPIS = nil, codigoConsolidaFatura = nil, codigoContrato = nil, contatos = [], criacao = nil, dataAdesao = nil, dataCancelamento = nil, dataPrimeiroVencimento = nil, dataSuspensaoFim = nil, dataSuspensaoInicio = nil, dataVigenciaFim = nil, dataVigenciaInicio = nil, descontos = [], exclusao = nil, fatorDeReajusteDaRenovacao = nil, gerenteUuid = nil, modificacao = nil, motivoCancelamento = nil, motivoSuspensao = nil, municipioPrestacaoServico = nil, naturezaOperacao = nil, observacao = nil, parcelasAtivacao = [], parcelasServico = [], periodicidade = nil, quantidade = nil, quantidadeTipoVigencia = nil, renovacaoAutomatica = nil, servico = nil, servicoEstado = nil, tarifaMinima = nil, taxaAtivacao = nil, tipoConsumo = nil, tipoFaturamento = nil, tipoVigencia = nil, unidadeMedida = nil, unidadeMetrica = nil, usarFatorDeReajusteAutomatico = nil, uuid = nil, valor = nil, valores = [], vendedor = nil)
    @acrescimos = acrescimos
    @aliquotaCOFINS = aliquotaCOFINS
    @aliquotaCSLL = aliquotaCSLL
    @aliquotaINSS = aliquotaINSS
    @aliquotaIR = aliquotaIR
    @aliquotaISS = aliquotaISS
    @aliquotaISSRetido = aliquotaISSRetido
    @aliquotaPIS = aliquotaPIS
    @codigoConsolidaFatura = codigoConsolidaFatura
    @codigoContrato = codigoContrato
    @contatos = contatos
    @criacao = criacao
    @dataAdesao = dataAdesao
    @dataCancelamento = dataCancelamento
    @dataPrimeiroVencimento = dataPrimeiroVencimento
    @dataSuspensaoFim = dataSuspensaoFim
    @dataSuspensaoInicio = dataSuspensaoInicio
    @dataVigenciaFim = dataVigenciaFim
    @dataVigenciaInicio = dataVigenciaInicio
    @descontos = descontos
    @exclusao = exclusao
    @fatorDeReajusteDaRenovacao = fatorDeReajusteDaRenovacao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @motivoCancelamento = motivoCancelamento
    @motivoSuspensao = motivoSuspensao
    @municipioPrestacaoServico = municipioPrestacaoServico
    @naturezaOperacao = naturezaOperacao
    @observacao = observacao
    @parcelasAtivacao = parcelasAtivacao
    @parcelasServico = parcelasServico
    @periodicidade = periodicidade
    @quantidade = quantidade
    @quantidadeTipoVigencia = quantidadeTipoVigencia
    @renovacaoAutomatica = renovacaoAutomatica
    @servico = servico
    @servicoEstado = servicoEstado
    @tarifaMinima = tarifaMinima
    @taxaAtivacao = taxaAtivacao
    @tipoConsumo = tipoConsumo
    @tipoFaturamento = tipoFaturamento
    @tipoVigencia = tipoVigencia
    @unidadeMedida = unidadeMedida
    @unidadeMetrica = unidadeMetrica
    @usarFatorDeReajusteAutomatico = usarFatorDeReajusteAutomatico
    @uuid = uuid
    @valor = valor
    @valores = valores
    @vendedor = vendedor
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoAcrescimo
#   dataVigenciaFim - SOAP::SOAPDateTime
#   dataVigenciaInicio - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   tipoValor - TipoValor
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
class ClienteServicoAcrescimo
  attr_accessor :dataVigenciaFim
  attr_accessor :dataVigenciaInicio
  attr_accessor :gerenteUuid
  attr_accessor :tipoValor
  attr_accessor :uuid
  attr_accessor :valor

  def initialize(dataVigenciaFim = nil, dataVigenciaInicio = nil, gerenteUuid = nil, tipoValor = nil, uuid = nil, valor = nil)
    @dataVigenciaFim = dataVigenciaFim
    @dataVigenciaInicio = dataVigenciaInicio
    @gerenteUuid = gerenteUuid
    @tipoValor = tipoValor
    @uuid = uuid
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}contato
#   celularDdd - SOAP::SOAPInt
#   celularNumero - SOAP::SOAPString
#   cpf - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   email - SOAP::SOAPString
#   exclusao - SOAP::SOAPDateTime
#   faxDdd - SOAP::SOAPInt
#   faxNumero - SOAP::SOAPString
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   nome - SOAP::SOAPString
#   recebeCobranca - SOAP::SOAPBoolean
#   telefoneDdd - SOAP::SOAPInt
#   telefoneNumero - SOAP::SOAPString
#   tipoContato - TipoContato
#   uuid - SOAP::SOAPString
class Contato
  attr_accessor :celularDdd
  attr_accessor :celularNumero
  attr_accessor :cpf
  attr_accessor :criacao
  attr_accessor :email
  attr_accessor :exclusao
  attr_accessor :faxDdd
  attr_accessor :faxNumero
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :nome
  attr_accessor :recebeCobranca
  attr_accessor :telefoneDdd
  attr_accessor :telefoneNumero
  attr_accessor :tipoContato
  attr_accessor :uuid

  def initialize(celularDdd = nil, celularNumero = nil, cpf = nil, criacao = nil, email = nil, exclusao = nil, faxDdd = nil, faxNumero = nil, gerenteUuid = nil, modificacao = nil, nome = nil, recebeCobranca = nil, telefoneDdd = nil, telefoneNumero = nil, tipoContato = nil, uuid = nil)
    @celularDdd = celularDdd
    @celularNumero = celularNumero
    @cpf = cpf
    @criacao = criacao
    @email = email
    @exclusao = exclusao
    @faxDdd = faxDdd
    @faxNumero = faxNumero
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @nome = nome
    @recebeCobranca = recebeCobranca
    @telefoneDdd = telefoneDdd
    @telefoneNumero = telefoneNumero
    @tipoContato = tipoContato
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoDesconto
#   dataVigenciaFim - SOAP::SOAPDateTime
#   dataVigenciaInicio - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   tipoValor - TipoValor
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
class ClienteServicoDesconto
  attr_accessor :dataVigenciaFim
  attr_accessor :dataVigenciaInicio
  attr_accessor :gerenteUuid
  attr_accessor :tipoValor
  attr_accessor :uuid
  attr_accessor :valor

  def initialize(dataVigenciaFim = nil, dataVigenciaInicio = nil, gerenteUuid = nil, tipoValor = nil, uuid = nil, valor = nil)
    @dataVigenciaFim = dataVigenciaFim
    @dataVigenciaInicio = dataVigenciaInicio
    @gerenteUuid = gerenteUuid
    @tipoValor = tipoValor
    @uuid = uuid
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}indiceEconomico
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   nome - SOAP::SOAPString
#   tipoIndiceEconomico - TipoIndiceEconomico
#   uuid - SOAP::SOAPString
#   valores - IndiceEconomicoValor
class IndiceEconomico
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :nome
  attr_accessor :tipoIndiceEconomico
  attr_accessor :uuid
  attr_accessor :valores

  def initialize(criacao = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, nome = nil, tipoIndiceEconomico = nil, uuid = nil, valores = [])
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @nome = nome
    @tipoIndiceEconomico = tipoIndiceEconomico
    @uuid = uuid
    @valores = valores
  end
end

# {http://ws.api.smartbill.com.br/}motivoCancelamento
#   contabilizarNoRelatorio - SOAP::SOAPBoolean
#   criacao - SOAP::SOAPDateTime
#   descricao - SOAP::SOAPString
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   uuid - SOAP::SOAPString
class MotivoCancelamento
  attr_accessor :contabilizarNoRelatorio
  attr_accessor :criacao
  attr_accessor :descricao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :uuid

  def initialize(contabilizarNoRelatorio = nil, criacao = nil, descricao = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, uuid = nil)
    @contabilizarNoRelatorio = contabilizarNoRelatorio
    @criacao = criacao
    @descricao = descricao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}motivoSuspensao
#   contabilizarNoRelatorio - SOAP::SOAPBoolean
#   criacao - SOAP::SOAPDateTime
#   descricao - SOAP::SOAPString
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   uuid - SOAP::SOAPString
class MotivoSuspensao
  attr_accessor :contabilizarNoRelatorio
  attr_accessor :criacao
  attr_accessor :descricao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :uuid

  def initialize(contabilizarNoRelatorio = nil, criacao = nil, descricao = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, uuid = nil)
    @contabilizarNoRelatorio = contabilizarNoRelatorio
    @criacao = criacao
    @descricao = descricao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoParcela
#   dataVencimento - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   parcela - SOAP::SOAPInt
#   tipoParcela - TipoParcela
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
class ClienteServicoParcela
  attr_accessor :dataVencimento
  attr_accessor :gerenteUuid
  attr_accessor :parcela
  attr_accessor :tipoParcela
  attr_accessor :uuid
  attr_accessor :valor

  def initialize(dataVencimento = nil, gerenteUuid = nil, parcela = nil, tipoParcela = nil, uuid = nil, valor = nil)
    @dataVencimento = dataVencimento
    @gerenteUuid = gerenteUuid
    @parcela = parcela
    @tipoParcela = tipoParcela
    @uuid = uuid
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoParcelaAtivacao
#   dataVencimento - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   parcela - SOAP::SOAPInt
#   tipoParcela - TipoParcela
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
class ClienteServicoParcelaAtivacao < ClienteServicoParcela
  attr_accessor :dataVencimento
  attr_accessor :gerenteUuid
  attr_accessor :parcela
  attr_accessor :tipoParcela
  attr_accessor :uuid
  attr_accessor :valor

  def initialize(dataVencimento = nil, gerenteUuid = nil, parcela = nil, tipoParcela = nil, uuid = nil, valor = nil)
    @dataVencimento = dataVencimento
    @gerenteUuid = gerenteUuid
    @parcela = parcela
    @tipoParcela = tipoParcela
    @uuid = uuid
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoParcelaServico
#   dataVencimento - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   parcela - SOAP::SOAPInt
#   tipoParcela - TipoParcela
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
class ClienteServicoParcelaServico < ClienteServicoParcela
  attr_accessor :dataVencimento
  attr_accessor :gerenteUuid
  attr_accessor :parcela
  attr_accessor :tipoParcela
  attr_accessor :uuid
  attr_accessor :valor

  def initialize(dataVencimento = nil, gerenteUuid = nil, parcela = nil, tipoParcela = nil, uuid = nil, valor = nil)
    @dataVencimento = dataVencimento
    @gerenteUuid = gerenteUuid
    @parcela = parcela
    @tipoParcela = tipoParcela
    @uuid = uuid
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}periodicidade
#   ativo - SOAP::SOAPBoolean
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   exibirNoFormularioDeContratoExterno - SOAP::SOAPBoolean
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   percentualDesconto - SOAP::SOAPDouble
#   tipoPeriodicidade - TipoPeriodicidade
#   uuid - SOAP::SOAPString
class Periodicidade
  attr_accessor :ativo
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :exibirNoFormularioDeContratoExterno
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :percentualDesconto
  attr_accessor :tipoPeriodicidade
  attr_accessor :uuid

  def initialize(ativo = nil, criacao = nil, exclusao = nil, exibirNoFormularioDeContratoExterno = nil, gerenteUuid = nil, modificacao = nil, percentualDesconto = nil, tipoPeriodicidade = nil, uuid = nil)
    @ativo = ativo
    @criacao = criacao
    @exclusao = exclusao
    @exibirNoFormularioDeContratoExterno = exibirNoFormularioDeContratoExterno
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @percentualDesconto = percentualDesconto
    @tipoPeriodicidade = tipoPeriodicidade
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}servico
#   ativo - SOAP::SOAPBoolean
#   codigo - SOAP::SOAPString
#   codigoServicoNF - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   descricao - SOAP::SOAPString
#   descricaoNF - SOAP::SOAPString
#   exclusao - SOAP::SOAPDateTime
#   familia - Familia
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   nome - SOAP::SOAPString
#   servicos - Servico
#   tarifaMinima - SOAP::SOAPDecimal
#   taxaAtivacao - SOAP::SOAPDecimal
#   tipoConsumo - TipoConsumo
#   tipoServico - TipoServico
#   tributacaoMunicipio - SOAP::SOAPString
#   unidadeMedida - UnidadeMedida
#   unidadeMetrica - UnidadeMetrica
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
#   valores - ServicoValor
class Servico
  attr_accessor :ativo
  attr_accessor :codigo
  attr_accessor :codigoServicoNF
  attr_accessor :criacao
  attr_accessor :descricao
  attr_accessor :descricaoNF
  attr_accessor :exclusao
  attr_accessor :familia
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :nome
  attr_accessor :servicos
  attr_accessor :tarifaMinima
  attr_accessor :taxaAtivacao
  attr_accessor :tipoConsumo
  attr_accessor :tipoServico
  attr_accessor :tributacaoMunicipio
  attr_accessor :unidadeMedida
  attr_accessor :unidadeMetrica
  attr_accessor :uuid
  attr_accessor :valor
  attr_accessor :valores

  def initialize(ativo = nil, codigo = nil, codigoServicoNF = nil, criacao = nil, descricao = nil, descricaoNF = nil, exclusao = nil, familia = nil, gerenteUuid = nil, modificacao = nil, nome = nil, servicos = [], tarifaMinima = nil, taxaAtivacao = nil, tipoConsumo = nil, tipoServico = nil, tributacaoMunicipio = nil, unidadeMedida = nil, unidadeMetrica = nil, uuid = nil, valor = nil, valores = [])
    @ativo = ativo
    @codigo = codigo
    @codigoServicoNF = codigoServicoNF
    @criacao = criacao
    @descricao = descricao
    @descricaoNF = descricaoNF
    @exclusao = exclusao
    @familia = familia
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @nome = nome
    @servicos = servicos
    @tarifaMinima = tarifaMinima
    @taxaAtivacao = taxaAtivacao
    @tipoConsumo = tipoConsumo
    @tipoServico = tipoServico
    @tributacaoMunicipio = tributacaoMunicipio
    @unidadeMedida = unidadeMedida
    @unidadeMetrica = unidadeMetrica
    @uuid = uuid
    @valor = valor
    @valores = valores
  end
end

# {http://ws.api.smartbill.com.br/}familia
#   criacao - SOAP::SOAPDateTime
#   descricao - SOAP::SOAPString
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   memorialDescritivo - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   nome - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class Familia
  attr_accessor :criacao
  attr_accessor :descricao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :memorialDescritivo
  attr_accessor :modificacao
  attr_accessor :nome
  attr_accessor :uuid

  def initialize(criacao = nil, descricao = nil, exclusao = nil, gerenteUuid = nil, memorialDescritivo = nil, modificacao = nil, nome = nil, uuid = nil)
    @criacao = criacao
    @descricao = descricao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @memorialDescritivo = memorialDescritivo
    @modificacao = modificacao
    @nome = nome
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}servicoValor
#   faixaFinal - SOAP::SOAPDecimal
#   faixaInicial - SOAP::SOAPDecimal
#   gerenteUuid - SOAP::SOAPString
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
class ServicoValor
  attr_accessor :faixaFinal
  attr_accessor :faixaInicial
  attr_accessor :gerenteUuid
  attr_accessor :uuid
  attr_accessor :valor

  def initialize(faixaFinal = nil, faixaInicial = nil, gerenteUuid = nil, uuid = nil, valor = nil)
    @faixaFinal = faixaFinal
    @faixaInicial = faixaInicial
    @gerenteUuid = gerenteUuid
    @uuid = uuid
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoValor
#   faixaFinal - SOAP::SOAPDecimal
#   faixaInicial - SOAP::SOAPDecimal
#   gerenteUuid - SOAP::SOAPString
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
class ClienteServicoValor
  attr_accessor :faixaFinal
  attr_accessor :faixaInicial
  attr_accessor :gerenteUuid
  attr_accessor :uuid
  attr_accessor :valor

  def initialize(faixaFinal = nil, faixaInicial = nil, gerenteUuid = nil, uuid = nil, valor = nil)
    @faixaFinal = faixaFinal
    @faixaInicial = faixaInicial
    @gerenteUuid = gerenteUuid
    @uuid = uuid
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}usuario
#   ativo - SOAP::SOAPBoolean
#   criacao - SOAP::SOAPDateTime
#   email - SOAP::SOAPString
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   ips - SOAP::SOAPString
#   menus - Menu
#   modificacao - SOAP::SOAPDateTime
#   nome - SOAP::SOAPString
#   othersGerenteUuids - SOAP::SOAPString
#   regras - Regra
#   senha - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class Usuario
  attr_accessor :ativo
  attr_accessor :criacao
  attr_accessor :email
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :ips
  attr_accessor :menus
  attr_accessor :modificacao
  attr_accessor :nome
  attr_accessor :othersGerenteUuids
  attr_accessor :regras
  attr_accessor :senha
  attr_accessor :uuid

  def initialize(ativo = nil, criacao = nil, email = nil, exclusao = nil, gerenteUuid = nil, ips = [], menus = [], modificacao = nil, nome = nil, othersGerenteUuids = [], regras = [], senha = nil, uuid = nil)
    @ativo = ativo
    @criacao = criacao
    @email = email
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @ips = ips
    @menus = menus
    @modificacao = modificacao
    @nome = nome
    @othersGerenteUuids = othersGerenteUuids
    @regras = regras
    @senha = senha
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}menu
#   ativo - SOAP::SOAPBoolean
#   descricao - SOAP::SOAPString
#   id - SOAP::SOAPString
#   image - SOAP::SOAPString
#   label - SOAP::SOAPString
#   menuConfig - SOAP::SOAPBoolean
#   menuItem - SOAP::SOAPBoolean
#   menuPai - Menu
#   ordem - SOAP::SOAPInt
#   uuid - SOAP::SOAPString
#   value - SOAP::SOAPString
class Menu
  attr_accessor :ativo
  attr_accessor :descricao
  attr_accessor :id
  attr_accessor :image
  attr_accessor :label
  attr_accessor :menuConfig
  attr_accessor :menuItem
  attr_accessor :menuPai
  attr_accessor :ordem
  attr_accessor :uuid
  attr_accessor :value

  def initialize(ativo = nil, descricao = nil, id = nil, image = nil, label = nil, menuConfig = nil, menuItem = nil, menuPai = nil, ordem = nil, uuid = nil, value = nil)
    @ativo = ativo
    @descricao = descricao
    @id = id
    @image = image
    @label = label
    @menuConfig = menuConfig
    @menuItem = menuItem
    @menuPai = menuPai
    @ordem = ordem
    @uuid = uuid
    @value = value
  end
end

# {http://ws.api.smartbill.com.br/}regra
#   chave - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class Regra
  attr_accessor :chave
  attr_accessor :uuid

  def initialize(chave = nil, uuid = nil)
    @chave = chave
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}ativarClienteServicoPorClienteServicoUuid
#   arg0 - SOAP::SOAPString
class AtivarClienteServicoPorClienteServicoUuid
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}ativarClienteServicoPorClienteServicoUuidResponse
class AtivarClienteServicoPorClienteServicoUuidResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarMotivoSuspensao
#   arg0 - MotivoSuspensao
class AlterarMotivoSuspensao
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}alterarMotivoSuspensaoResponse
class AlterarMotivoSuspensaoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarFamilia
#   arg0 - Familia
class AlterarFamilia
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}alterarFamiliaResponse
class AlterarFamiliaResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalServico
#   arg0 - ServicoFilter
class ListarTotalServico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}servicoFilter
#   ativo - SOAP::SOAPBoolean
#   codigo - SOAP::SOAPString
#   excluindoIds - SOAP::SOAPString
#   familia - Familia
#   generalFilter - GeneralFilter
#   servicosAdicional - Servico
#   tiposServico - TipoServico
class ServicoFilter
  attr_accessor :ativo
  attr_accessor :codigo
  attr_accessor :excluindoIds
  attr_accessor :familia
  attr_accessor :generalFilter
  attr_accessor :servicosAdicional
  attr_accessor :tiposServico

  def initialize(ativo = nil, codigo = nil, excluindoIds = [], familia = nil, generalFilter = nil, servicosAdicional = nil, tiposServico = [])
    @ativo = ativo
    @codigo = codigo
    @excluindoIds = excluindoIds
    @familia = familia
    @generalFilter = generalFilter
    @servicosAdicional = servicosAdicional
    @tiposServico = tiposServico
  end
end

# {http://ws.api.smartbill.com.br/}generalFilter
#   likeCampos - SOAP::SOAPString
#   likeValor - SOAP::SOAPString
#   maxElements - SOAP::SOAPInt
#   orderBy - SOAP::SOAPString
#   page - SOAP::SOAPInt
class GeneralFilter
  attr_accessor :likeCampos
  attr_accessor :likeValor
  attr_accessor :maxElements
  attr_accessor :orderBy
  attr_accessor :page

  def initialize(likeCampos = [], likeValor = nil, maxElements = nil, orderBy = nil, page = nil)
    @likeCampos = likeCampos
    @likeValor = likeValor
    @maxElements = maxElements
    @orderBy = orderBy
    @page = page
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalServicoResponse
#   m_return - SOAP::SOAPLong
class ListarTotalServicoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}obterServico
#   arg0 - SOAP::SOAPString
class ObterServico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterServicoResponse
#   m_return - Servico
class ObterServicoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}listarRelacionamento
#   arg0 - RelacionamentoFilter
class ListarRelacionamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}relacionamentoFilter
#   clienteUuid - SOAP::SOAPString
#   tipoRelacionamento - TipoRelacionamento
class RelacionamentoFilter
  attr_accessor :clienteUuid
  attr_accessor :tipoRelacionamento

  def initialize(clienteUuid = nil, tipoRelacionamento = nil)
    @clienteUuid = clienteUuid
    @tipoRelacionamento = tipoRelacionamento
  end
end

# {http://ws.api.smartbill.com.br/}listarRelacionamentoResponse
class ListarRelacionamentoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}relacionamento
#   clienteCRTransacaoUuid - SOAP::SOAPString
#   clienteCRUuid - SOAP::SOAPString
#   clienteNFUuid - SOAP::SOAPString
#   clienteUuid - SOAP::SOAPString
#   comentario - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   emailUuid - SOAP::SOAPString
#   enviarEmail - SOAP::SOAPBoolean
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   tipoRelacionamento - TipoRelacionamento
#   uuid - SOAP::SOAPString
class Relacionamento
  attr_accessor :clienteCRTransacaoUuid
  attr_accessor :clienteCRUuid
  attr_accessor :clienteNFUuid
  attr_accessor :clienteUuid
  attr_accessor :comentario
  attr_accessor :criacao
  attr_accessor :emailUuid
  attr_accessor :enviarEmail
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :tipoRelacionamento
  attr_accessor :uuid

  def initialize(clienteCRTransacaoUuid = nil, clienteCRUuid = nil, clienteNFUuid = nil, clienteUuid = nil, comentario = nil, criacao = nil, emailUuid = nil, enviarEmail = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, tipoRelacionamento = nil, uuid = nil)
    @clienteCRTransacaoUuid = clienteCRTransacaoUuid
    @clienteCRUuid = clienteCRUuid
    @clienteNFUuid = clienteNFUuid
    @clienteUuid = clienteUuid
    @comentario = comentario
    @criacao = criacao
    @emailUuid = emailUuid
    @enviarEmail = enviarEmail
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @tipoRelacionamento = tipoRelacionamento
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}obterRelacionamento
#   arg0 - SOAP::SOAPString
class ObterRelacionamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterRelacionamentoResponse
#   m_return - Relacionamento
class ObterRelacionamentoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}alterarSegmento
#   arg0 - Segmento
#   arg1 - SOAP::SOAPString
class AlterarSegmento
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}segmento
#   criacao - SOAP::SOAPDateTime
#   criticidade - Criticidade
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   nome - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class Segmento
  attr_accessor :criacao
  attr_accessor :criticidade
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :nome
  attr_accessor :uuid

  def initialize(criacao = nil, criticidade = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, nome = nil, uuid = nil)
    @criacao = criacao
    @criticidade = criticidade
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @nome = nome
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}alterarSegmentoResponse
class AlterarSegmentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarRelacionamento
#   arg0 - Relacionamento
#   arg1 - SOAP::SOAPString
class AlterarRelacionamento
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}alterarRelacionamentoResponse
class AlterarRelacionamentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarRegra
class ListarRegra
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarRegraResponse
class ListarRegraResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}alterarContato
#   arg0 - Contato
#   arg1 - SOAP::SOAPString
class AlterarContato
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}alterarContatoResponse
class AlterarContatoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}gerarNFsEmissao
#   arg0 - SOAP::SOAPString
#   arg1 - SOAP::SOAPString
class GerarNFsEmissao
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}gerarNFsEmissaoResponse
class GerarNFsEmissaoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}adicionarMotivoSuspensao
#   arg0 - MotivoSuspensao
class AdicionarMotivoSuspensao
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}adicionarMotivoSuspensaoResponse
class AdicionarMotivoSuspensaoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterCodigoCNAE
#   arg0 - SOAP::SOAPString
class ObterCodigoCNAE
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterCodigoCNAEResponse
#   m_return - CodigoCNAE
class ObterCodigoCNAEResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}codigoCNAE
#   codigo - SOAP::SOAPString
#   descricao - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class CodigoCNAE
  attr_accessor :codigo
  attr_accessor :descricao
  attr_accessor :uuid

  def initialize(codigo = nil, descricao = nil, uuid = nil)
    @codigo = codigo
    @descricao = descricao
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteCRCadastro
#   arg0 - ClienteCRCadastro
#   arg1 - SOAP::SOAPString
class AlterarClienteCRCadastro
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}clienteCRCadastro
#   clienteCRUuid - SOAP::SOAPString
#   clientePrestador - ClientePrestador
#   clienteTomador - ClienteTomador
#   contatoPrestador - ContatoPrestador
#   contatoTomador - ContatoTomador
#   criacao - SOAP::SOAPDateTime
#   enderecoPrestador - EnderecoPrestador
#   enderecoTomador - EnderecoTomador
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   uuid - SOAP::SOAPString
class ClienteCRCadastro
  attr_accessor :clienteCRUuid
  attr_accessor :clientePrestador
  attr_accessor :clienteTomador
  attr_accessor :contatoPrestador
  attr_accessor :contatoTomador
  attr_accessor :criacao
  attr_accessor :enderecoPrestador
  attr_accessor :enderecoTomador
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :uuid

  def initialize(clienteCRUuid = nil, clientePrestador = nil, clienteTomador = nil, contatoPrestador = nil, contatoTomador = nil, criacao = nil, enderecoPrestador = nil, enderecoTomador = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, uuid = nil)
    @clienteCRUuid = clienteCRUuid
    @clientePrestador = clientePrestador
    @clienteTomador = clienteTomador
    @contatoPrestador = contatoPrestador
    @contatoTomador = contatoTomador
    @criacao = criacao
    @enderecoPrestador = enderecoPrestador
    @enderecoTomador = enderecoTomador
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}copyFields
class CopyFields
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}clientePrestador
#   cnpjCpf - SOAP::SOAPString
#   emailGeral - SOAP::SOAPString
#   ieRg - SOAP::SOAPString
#   im - SOAP::SOAPString
#   nomeFantasia - SOAP::SOAPString
#   orgaoPublico - SOAP::SOAPBoolean
#   razaoSocial - SOAP::SOAPString
#   tipoPessoa - TipoPessoa
class ClientePrestador < CopyFields
  attr_accessor :cnpjCpf
  attr_accessor :emailGeral
  attr_accessor :ieRg
  attr_accessor :im
  attr_accessor :nomeFantasia
  attr_accessor :orgaoPublico
  attr_accessor :razaoSocial
  attr_accessor :tipoPessoa

  def initialize(cnpjCpf = nil, emailGeral = nil, ieRg = nil, im = nil, nomeFantasia = nil, orgaoPublico = nil, razaoSocial = nil, tipoPessoa = nil)
    @cnpjCpf = cnpjCpf
    @emailGeral = emailGeral
    @ieRg = ieRg
    @im = im
    @nomeFantasia = nomeFantasia
    @orgaoPublico = orgaoPublico
    @razaoSocial = razaoSocial
    @tipoPessoa = tipoPessoa
  end
end

# {http://ws.api.smartbill.com.br/}clienteTomador
#   cnpjCpf - SOAP::SOAPString
#   emailGeral - SOAP::SOAPString
#   ieRg - SOAP::SOAPString
#   im - SOAP::SOAPString
#   nomeFantasia - SOAP::SOAPString
#   orgaoPublico - SOAP::SOAPBoolean
#   razaoSocial - SOAP::SOAPString
#   tipoPessoa - TipoPessoa
class ClienteTomador < CopyFields
  attr_accessor :cnpjCpf
  attr_accessor :emailGeral
  attr_accessor :ieRg
  attr_accessor :im
  attr_accessor :nomeFantasia
  attr_accessor :orgaoPublico
  attr_accessor :razaoSocial
  attr_accessor :tipoPessoa

  def initialize(cnpjCpf = nil, emailGeral = nil, ieRg = nil, im = nil, nomeFantasia = nil, orgaoPublico = nil, razaoSocial = nil, tipoPessoa = nil)
    @cnpjCpf = cnpjCpf
    @emailGeral = emailGeral
    @ieRg = ieRg
    @im = im
    @nomeFantasia = nomeFantasia
    @orgaoPublico = orgaoPublico
    @razaoSocial = razaoSocial
    @tipoPessoa = tipoPessoa
  end
end

# {http://ws.api.smartbill.com.br/}contatoPrestador
#   celularDdd - SOAP::SOAPInt
#   celularNumero - SOAP::SOAPString
#   cpf - SOAP::SOAPString
#   email - SOAP::SOAPString
#   faxDdd - SOAP::SOAPInt
#   faxNumero - SOAP::SOAPString
#   nome - SOAP::SOAPString
#   telefoneDdd - SOAP::SOAPInt
#   telefoneNumero - SOAP::SOAPString
class ContatoPrestador < CopyFields
  attr_accessor :celularDdd
  attr_accessor :celularNumero
  attr_accessor :cpf
  attr_accessor :email
  attr_accessor :faxDdd
  attr_accessor :faxNumero
  attr_accessor :nome
  attr_accessor :telefoneDdd
  attr_accessor :telefoneNumero

  def initialize(celularDdd = nil, celularNumero = nil, cpf = nil, email = nil, faxDdd = nil, faxNumero = nil, nome = nil, telefoneDdd = nil, telefoneNumero = nil)
    @celularDdd = celularDdd
    @celularNumero = celularNumero
    @cpf = cpf
    @email = email
    @faxDdd = faxDdd
    @faxNumero = faxNumero
    @nome = nome
    @telefoneDdd = telefoneDdd
    @telefoneNumero = telefoneNumero
  end
end

# {http://ws.api.smartbill.com.br/}contatoTomador
#   celularDdd - SOAP::SOAPInt
#   celularNumero - SOAP::SOAPString
#   cpf - SOAP::SOAPString
#   email - SOAP::SOAPString
#   faxDdd - SOAP::SOAPInt
#   faxNumero - SOAP::SOAPString
#   nome - SOAP::SOAPString
#   telefoneDdd - SOAP::SOAPInt
#   telefoneNumero - SOAP::SOAPString
class ContatoTomador < CopyFields
  attr_accessor :celularDdd
  attr_accessor :celularNumero
  attr_accessor :cpf
  attr_accessor :email
  attr_accessor :faxDdd
  attr_accessor :faxNumero
  attr_accessor :nome
  attr_accessor :telefoneDdd
  attr_accessor :telefoneNumero

  def initialize(celularDdd = nil, celularNumero = nil, cpf = nil, email = nil, faxDdd = nil, faxNumero = nil, nome = nil, telefoneDdd = nil, telefoneNumero = nil)
    @celularDdd = celularDdd
    @celularNumero = celularNumero
    @cpf = cpf
    @email = email
    @faxDdd = faxDdd
    @faxNumero = faxNumero
    @nome = nome
    @telefoneDdd = telefoneDdd
    @telefoneNumero = telefoneNumero
  end
end

# {http://ws.api.smartbill.com.br/}enderecoPrestador
#   bairro - SOAP::SOAPString
#   cep - SOAP::SOAPString
#   cidade - Cidade
#   complemento - SOAP::SOAPString
#   logradouro - SOAP::SOAPString
#   numero - SOAP::SOAPInt
#   uf - Uf
class EnderecoPrestador < CopyFields
  attr_accessor :bairro
  attr_accessor :cep
  attr_accessor :cidade
  attr_accessor :complemento
  attr_accessor :logradouro
  attr_accessor :numero
  attr_accessor :uf

  def initialize(bairro = nil, cep = nil, cidade = nil, complemento = nil, logradouro = nil, numero = nil, uf = nil)
    @bairro = bairro
    @cep = cep
    @cidade = cidade
    @complemento = complemento
    @logradouro = logradouro
    @numero = numero
    @uf = uf
  end
end

# {http://ws.api.smartbill.com.br/}enderecoTomador
#   bairro - SOAP::SOAPString
#   cep - SOAP::SOAPString
#   cidade - Cidade
#   complemento - SOAP::SOAPString
#   logradouro - SOAP::SOAPString
#   numero - SOAP::SOAPInt
#   uf - Uf
class EnderecoTomador < CopyFields
  attr_accessor :bairro
  attr_accessor :cep
  attr_accessor :cidade
  attr_accessor :complemento
  attr_accessor :logradouro
  attr_accessor :numero
  attr_accessor :uf

  def initialize(bairro = nil, cep = nil, cidade = nil, complemento = nil, logradouro = nil, numero = nil, uf = nil)
    @bairro = bairro
    @cep = cep
    @cidade = cidade
    @complemento = complemento
    @logradouro = logradouro
    @numero = numero
    @uf = uf
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteCRCadastroResponse
class AlterarClienteCRCadastroResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarRamoAtividade
class ListarRamoAtividade
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarRamoAtividadeResponse
class ListarRamoAtividadeResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}ramoAtividade
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   nome - SOAP::SOAPString
#   segmentos - Segmento
#   uuid - SOAP::SOAPString
class RamoAtividade
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :nome
  attr_accessor :segmentos
  attr_accessor :uuid

  def initialize(criacao = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, nome = nil, segmentos = [], uuid = nil)
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @nome = nome
    @segmentos = segmentos
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}baixarCobranca
#   arg0 - Lancamento
#   arg1 - SOAP::SOAPString
class BaixarCobranca
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}lancamento
#   clienteCRUuid - SOAP::SOAPString
#   codigo - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   data - SOAP::SOAPDateTime
#   descricao - SOAP::SOAPString
#   gerenteUuid - SOAP::SOAPString
#   tipoLancamento - TipoLancamento
#   tipoMeioPagamento - TipoMeioPagamento
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
class Lancamento
  attr_accessor :clienteCRUuid
  attr_accessor :codigo
  attr_accessor :criacao
  attr_accessor :data
  attr_accessor :descricao
  attr_accessor :gerenteUuid
  attr_accessor :tipoLancamento
  attr_accessor :tipoMeioPagamento
  attr_accessor :uuid
  attr_accessor :valor

  def initialize(clienteCRUuid = nil, codigo = nil, criacao = nil, data = nil, descricao = nil, gerenteUuid = nil, tipoLancamento = nil, tipoMeioPagamento = nil, uuid = nil, valor = nil)
    @clienteCRUuid = clienteCRUuid
    @codigo = codigo
    @criacao = criacao
    @data = data
    @descricao = descricao
    @gerenteUuid = gerenteUuid
    @tipoLancamento = tipoLancamento
    @tipoMeioPagamento = tipoMeioPagamento
    @uuid = uuid
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}baixarCobrancaResponse
class BaixarCobrancaResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteServicoCR
#   arg0 - ClienteServicoCR
class RemoverClienteServicoCR
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteServicoCRResponse
class RemoverClienteServicoCRResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalClienteCRTransacaoBoleto
#   arg0 - ClienteCRTransacaoBoletoFilter
class ListarTotalClienteCRTransacaoBoleto
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}clienteCRTransacaoBoletoFilter
#   clienteCRUuid - SOAP::SOAPString
#   clienteUuid - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class ClienteCRTransacaoBoletoFilter
  attr_accessor :clienteCRUuid
  attr_accessor :clienteUuid
  attr_accessor :uuid

  def initialize(clienteCRUuid = nil, clienteUuid = nil, uuid = nil)
    @clienteCRUuid = clienteCRUuid
    @clienteUuid = clienteUuid
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalClienteCRTransacaoBoletoResponse
#   m_return - SOAP::SOAPLong
class ListarTotalClienteCRTransacaoBoletoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}obterRegimeTributario
#   arg0 - SOAP::SOAPString
class ObterRegimeTributario
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterRegimeTributarioResponse
#   m_return - RegimeTributario
class ObterRegimeTributarioResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}regimeTributario
#   codigo - SOAP::SOAPString
#   descricao - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class RegimeTributario
  attr_accessor :codigo
  attr_accessor :descricao
  attr_accessor :uuid

  def initialize(codigo = nil, descricao = nil, uuid = nil)
    @codigo = codigo
    @descricao = descricao
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}removerServico
#   arg0 - Servico
class RemoverServico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerServicoResponse
class RemoverServicoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalArquivo
#   arg0 - ArquivoFilter
class ListarTotalArquivo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}arquivoFilter
#   clienteUuid - SOAP::SOAPString
#   generalFilter - GeneralFilter
class ArquivoFilter
  attr_accessor :clienteUuid
  attr_accessor :generalFilter

  def initialize(clienteUuid = nil, generalFilter = nil)
    @clienteUuid = clienteUuid
    @generalFilter = generalFilter
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalArquivoResponse
#   m_return - SOAP::SOAPLong
class ListarTotalArquivoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}consultarCobranca
#   arg0 - SOAP::SOAPString
#   arg1 - SOAP::SOAPString
class ConsultarCobranca
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}consultarCobrancaResponse
class ConsultarCobrancaResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterContato
#   arg0 - SOAP::SOAPString
class ObterContato
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterContatoResponse
#   m_return - Contato
class ObterContatoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteCRCadastro
#   arg0 - SOAP::SOAPString
class ObterClienteCRCadastro
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteCRCadastroResponse
#   m_return - ClienteCRCadastro
class ObterClienteCRCadastroResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteServico
#   arg0 - ClienteServico
class RemoverClienteServico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteServicoResponse
class RemoverClienteServicoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarServicoAliquotaPrestador
#   arg0 - ServicoAliquotaPrestadorFilter
class ListarServicoAliquotaPrestador
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}servicoAliquotaPrestadorFilter
#   cidadeUuid - SOAP::SOAPString
#   codigo - SOAP::SOAPString
#   generalFilter - GeneralFilter
#   gerenteUuid - SOAP::SOAPString
class ServicoAliquotaPrestadorFilter
  attr_accessor :cidadeUuid
  attr_accessor :codigo
  attr_accessor :generalFilter
  attr_accessor :gerenteUuid

  def initialize(cidadeUuid = nil, codigo = nil, generalFilter = nil, gerenteUuid = nil)
    @cidadeUuid = cidadeUuid
    @codigo = codigo
    @generalFilter = generalFilter
    @gerenteUuid = gerenteUuid
  end
end

# {http://ws.api.smartbill.com.br/}listarServicoAliquotaPrestadorResponse
class ListarServicoAliquotaPrestadorResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}servicoAliquotaPrestador
#   aliquotaINSSEX - SOAP::SOAPDecimal
#   aliquotaINSSPF - SOAP::SOAPDecimal
#   aliquotaINSSRetidoPJ - SOAP::SOAPDecimal
#   aliquotaINSSRetidoPU - SOAP::SOAPDecimal
#   aliquotaIRRetidoPJ - SOAP::SOAPDecimal
#   aliquotaIRRetidoPU - SOAP::SOAPDecimal
#   aliquotaISSEX - SOAP::SOAPDecimal
#   aliquotaISSPF - SOAP::SOAPDecimal
#   aliquotaISSPJ - SOAP::SOAPDecimal
#   aliquotaISSPU - SOAP::SOAPDecimal
#   aliquotaISSRetidoPJ - SOAP::SOAPDecimal
#   aliquotaISSRetidoPU - SOAP::SOAPDecimal
#   codigo - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   descricao - SOAP::SOAPString
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   incideINSSPJ - SOAP::SOAPBoolean
#   incideINSSPU - SOAP::SOAPBoolean
#   modificacao - SOAP::SOAPDateTime
#   retemINSSPJ - SOAP::SOAPBoolean
#   retemINSSPU - SOAP::SOAPBoolean
#   retemIRPJ - SOAP::SOAPBoolean
#   retemIRPU - SOAP::SOAPBoolean
#   retemISSPJ - SOAP::SOAPBoolean
#   retemISSPU - SOAP::SOAPBoolean
#   retemISSPrestadorCadastrado - SOAP::SOAPBoolean
#   retemISSPrestadorMunicipio - SOAP::SOAPBoolean
#   retemISSPrestadorNaoCadastrado - SOAP::SOAPBoolean
#   retemISSTomadorIgualPrestador - SOAP::SOAPBoolean
#   retemPCCPJ - SOAP::SOAPBoolean
#   retemPCCPU - SOAP::SOAPBoolean
#   servicoAliquotaTomadors - ServicoAliquotaTomador
#   tributacaoMunicipio - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class ServicoAliquotaPrestador
  attr_accessor :aliquotaINSSEX
  attr_accessor :aliquotaINSSPF
  attr_accessor :aliquotaINSSRetidoPJ
  attr_accessor :aliquotaINSSRetidoPU
  attr_accessor :aliquotaIRRetidoPJ
  attr_accessor :aliquotaIRRetidoPU
  attr_accessor :aliquotaISSEX
  attr_accessor :aliquotaISSPF
  attr_accessor :aliquotaISSPJ
  attr_accessor :aliquotaISSPU
  attr_accessor :aliquotaISSRetidoPJ
  attr_accessor :aliquotaISSRetidoPU
  attr_accessor :codigo
  attr_accessor :criacao
  attr_accessor :descricao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :incideINSSPJ
  attr_accessor :incideINSSPU
  attr_accessor :modificacao
  attr_accessor :retemINSSPJ
  attr_accessor :retemINSSPU
  attr_accessor :retemIRPJ
  attr_accessor :retemIRPU
  attr_accessor :retemISSPJ
  attr_accessor :retemISSPU
  attr_accessor :retemISSPrestadorCadastrado
  attr_accessor :retemISSPrestadorMunicipio
  attr_accessor :retemISSPrestadorNaoCadastrado
  attr_accessor :retemISSTomadorIgualPrestador
  attr_accessor :retemPCCPJ
  attr_accessor :retemPCCPU
  attr_accessor :servicoAliquotaTomadors
  attr_accessor :tributacaoMunicipio
  attr_accessor :uuid

  def initialize(aliquotaINSSEX = nil, aliquotaINSSPF = nil, aliquotaINSSRetidoPJ = nil, aliquotaINSSRetidoPU = nil, aliquotaIRRetidoPJ = nil, aliquotaIRRetidoPU = nil, aliquotaISSEX = nil, aliquotaISSPF = nil, aliquotaISSPJ = nil, aliquotaISSPU = nil, aliquotaISSRetidoPJ = nil, aliquotaISSRetidoPU = nil, codigo = nil, criacao = nil, descricao = nil, exclusao = nil, gerenteUuid = nil, incideINSSPJ = nil, incideINSSPU = nil, modificacao = nil, retemINSSPJ = nil, retemINSSPU = nil, retemIRPJ = nil, retemIRPU = nil, retemISSPJ = nil, retemISSPU = nil, retemISSPrestadorCadastrado = nil, retemISSPrestadorMunicipio = nil, retemISSPrestadorNaoCadastrado = nil, retemISSTomadorIgualPrestador = nil, retemPCCPJ = nil, retemPCCPU = nil, servicoAliquotaTomadors = [], tributacaoMunicipio = nil, uuid = nil)
    @aliquotaINSSEX = aliquotaINSSEX
    @aliquotaINSSPF = aliquotaINSSPF
    @aliquotaINSSRetidoPJ = aliquotaINSSRetidoPJ
    @aliquotaINSSRetidoPU = aliquotaINSSRetidoPU
    @aliquotaIRRetidoPJ = aliquotaIRRetidoPJ
    @aliquotaIRRetidoPU = aliquotaIRRetidoPU
    @aliquotaISSEX = aliquotaISSEX
    @aliquotaISSPF = aliquotaISSPF
    @aliquotaISSPJ = aliquotaISSPJ
    @aliquotaISSPU = aliquotaISSPU
    @aliquotaISSRetidoPJ = aliquotaISSRetidoPJ
    @aliquotaISSRetidoPU = aliquotaISSRetidoPU
    @codigo = codigo
    @criacao = criacao
    @descricao = descricao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @incideINSSPJ = incideINSSPJ
    @incideINSSPU = incideINSSPU
    @modificacao = modificacao
    @retemINSSPJ = retemINSSPJ
    @retemINSSPU = retemINSSPU
    @retemIRPJ = retemIRPJ
    @retemIRPU = retemIRPU
    @retemISSPJ = retemISSPJ
    @retemISSPU = retemISSPU
    @retemISSPrestadorCadastrado = retemISSPrestadorCadastrado
    @retemISSPrestadorMunicipio = retemISSPrestadorMunicipio
    @retemISSPrestadorNaoCadastrado = retemISSPrestadorNaoCadastrado
    @retemISSTomadorIgualPrestador = retemISSTomadorIgualPrestador
    @retemPCCPJ = retemPCCPJ
    @retemPCCPU = retemPCCPU
    @servicoAliquotaTomadors = servicoAliquotaTomadors
    @tributacaoMunicipio = tributacaoMunicipio
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}servicoAliquotaTomador
#   aliquotaISSRetido - SOAP::SOAPDecimal
#   cidade - Cidade
#   gerenteUuid - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class ServicoAliquotaTomador
  attr_accessor :aliquotaISSRetido
  attr_accessor :cidade
  attr_accessor :gerenteUuid
  attr_accessor :uuid

  def initialize(aliquotaISSRetido = nil, cidade = nil, gerenteUuid = nil, uuid = nil)
    @aliquotaISSRetido = aliquotaISSRetido
    @cidade = cidade
    @gerenteUuid = gerenteUuid
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}obterArquivo
#   arg0 - SOAP::SOAPString
class ObterArquivo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterArquivoResponse
#   m_return - Arquivo
class ObterArquivoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}arquivo
#   clienteUuid - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   descricao - SOAP::SOAPString
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   nome - SOAP::SOAPString
#   src - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class Arquivo
  attr_accessor :clienteUuid
  attr_accessor :criacao
  attr_accessor :descricao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :nome
  attr_accessor :src
  attr_accessor :uuid

  def initialize(clienteUuid = nil, criacao = nil, descricao = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, nome = nil, src = nil, uuid = nil)
    @clienteUuid = clienteUuid
    @criacao = criacao
    @descricao = descricao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @nome = nome
    @src = src
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}adicionarSegmento
#   arg0 - Segmento
#   arg1 - SOAP::SOAPString
class AdicionarSegmento
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}adicionarSegmentoResponse
class AdicionarSegmentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterFamilia
#   arg0 - SOAP::SOAPString
class ObterFamilia
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterFamiliaResponse
#   m_return - Familia
class ObterFamiliaResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}obterTemplateEmail
#   arg0 - SOAP::SOAPString
class ObterTemplateEmail
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterTemplateEmailResponse
#   m_return - TemplateEmail
class ObterTemplateEmailResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}templateEmail
#   assunto - SOAP::SOAPString
#   comCopia - SOAP::SOAPString
#   comCopiaOculta - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   mensagem - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   tipoTemplateEmail - TipoTemplateEmail
#   uuid - SOAP::SOAPString
class TemplateEmail
  attr_accessor :assunto
  attr_accessor :comCopia
  attr_accessor :comCopiaOculta
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :mensagem
  attr_accessor :modificacao
  attr_accessor :tipoTemplateEmail
  attr_accessor :uuid

  def initialize(assunto = nil, comCopia = nil, comCopiaOculta = nil, criacao = nil, exclusao = nil, gerenteUuid = nil, mensagem = nil, modificacao = nil, tipoTemplateEmail = nil, uuid = nil)
    @assunto = assunto
    @comCopia = comCopia
    @comCopiaOculta = comCopiaOculta
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @mensagem = mensagem
    @modificacao = modificacao
    @tipoTemplateEmail = tipoTemplateEmail
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}cancelarNFsEmissao
#   arg0 - SOAP::SOAPString
#   arg1 - SOAP::SOAPString
class CancelarNFsEmissao
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}cancelarNFsEmissaoResponse
class CancelarNFsEmissaoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarCliente
#   arg0 - Cliente
class AlterarCliente
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}cliente
#   bloqueioFaturamento - BloqueioFaturamento
#   cartoes - ClienteCartao
#   cicloFaturamento - CicloFaturamento
#   cnpjCpf - SOAP::SOAPString
#   codigo - SOAP::SOAPString
#   comoConheceu - SOAP::SOAPString
#   complementoDoCorpoDaNFe - SOAP::SOAPString
#   contatos - Contato
#   criacao - SOAP::SOAPDateTime
#   diaVencimento - SOAP::SOAPInt
#   documentoFiscal - DocumentoFiscal
#   domicilioFaturamento - Cidade
#   dominio - SOAP::SOAPString
#   emailGeral - SOAP::SOAPString
#   enderecos - Endereco
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   ieRg - SOAP::SOAPString
#   im - SOAP::SOAPString
#   indicadoPor - SOAP::SOAPString
#   meioPagamento - MeioPagamento
#   modificacao - SOAP::SOAPDateTime
#   nomeFantasia - SOAP::SOAPString
#   orgaoPublico - SOAP::SOAPBoolean
#   ramoAtividade - RamoAtividade
#   razaoSocial - SOAP::SOAPString
#   telefoneDdd - SOAP::SOAPInt
#   telefoneNumero - SOAP::SOAPString
#   tipoPessoa - TipoPessoa
#   urlPropria - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class Cliente
  attr_accessor :bloqueioFaturamento
  attr_accessor :cartoes
  attr_accessor :cicloFaturamento
  attr_accessor :cnpjCpf
  attr_accessor :codigo
  attr_accessor :comoConheceu
  attr_accessor :complementoDoCorpoDaNFe
  attr_accessor :contatos
  attr_accessor :criacao
  attr_accessor :diaVencimento
  attr_accessor :documentoFiscal
  attr_accessor :domicilioFaturamento
  attr_accessor :dominio
  attr_accessor :emailGeral
  attr_accessor :enderecos
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :ieRg
  attr_accessor :im
  attr_accessor :indicadoPor
  attr_accessor :meioPagamento
  attr_accessor :modificacao
  attr_accessor :nomeFantasia
  attr_accessor :orgaoPublico
  attr_accessor :ramoAtividade
  attr_accessor :razaoSocial
  attr_accessor :telefoneDdd
  attr_accessor :telefoneNumero
  attr_accessor :tipoPessoa
  attr_accessor :urlPropria
  attr_accessor :uuid

  def initialize(bloqueioFaturamento = nil, cartoes = [], cicloFaturamento = nil, cnpjCpf = nil, codigo = nil, comoConheceu = nil, complementoDoCorpoDaNFe = nil, contatos = [], criacao = nil, diaVencimento = nil, documentoFiscal = nil, domicilioFaturamento = nil, dominio = nil, emailGeral = nil, enderecos = [], exclusao = nil, gerenteUuid = nil, ieRg = nil, im = nil, indicadoPor = nil, meioPagamento = nil, modificacao = nil, nomeFantasia = nil, orgaoPublico = nil, ramoAtividade = nil, razaoSocial = nil, telefoneDdd = nil, telefoneNumero = nil, tipoPessoa = nil, urlPropria = nil, uuid = nil)
    @bloqueioFaturamento = bloqueioFaturamento
    @cartoes = cartoes
    @cicloFaturamento = cicloFaturamento
    @cnpjCpf = cnpjCpf
    @codigo = codigo
    @comoConheceu = comoConheceu
    @complementoDoCorpoDaNFe = complementoDoCorpoDaNFe
    @contatos = contatos
    @criacao = criacao
    @diaVencimento = diaVencimento
    @documentoFiscal = documentoFiscal
    @domicilioFaturamento = domicilioFaturamento
    @dominio = dominio
    @emailGeral = emailGeral
    @enderecos = enderecos
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @ieRg = ieRg
    @im = im
    @indicadoPor = indicadoPor
    @meioPagamento = meioPagamento
    @modificacao = modificacao
    @nomeFantasia = nomeFantasia
    @orgaoPublico = orgaoPublico
    @ramoAtividade = ramoAtividade
    @razaoSocial = razaoSocial
    @telefoneDdd = telefoneDdd
    @telefoneNumero = telefoneNumero
    @tipoPessoa = tipoPessoa
    @urlPropria = urlPropria
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}clienteCartao
#   ano - SOAP::SOAPInt
#   ativo - SOAP::SOAPBoolean
#   chaveCompra - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   mes - SOAP::SOAPInt
#   modificacao - SOAP::SOAPDateTime
#   numero - SOAP::SOAPString
#   principal - SOAP::SOAPBoolean
#   security - SOAP::SOAPString
#   tipoBandeira - TipoBandeira
#   titular - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class ClienteCartao
  attr_accessor :ano
  attr_accessor :ativo
  attr_accessor :chaveCompra
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :mes
  attr_accessor :modificacao
  attr_accessor :numero
  attr_accessor :principal
  attr_accessor :security
  attr_accessor :tipoBandeira
  attr_accessor :titular
  attr_accessor :uuid

  def initialize(ano = nil, ativo = nil, chaveCompra = nil, criacao = nil, exclusao = nil, gerenteUuid = nil, mes = nil, modificacao = nil, numero = nil, principal = nil, security = nil, tipoBandeira = nil, titular = nil, uuid = nil)
    @ano = ano
    @ativo = ativo
    @chaveCompra = chaveCompra
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @mes = mes
    @modificacao = modificacao
    @numero = numero
    @principal = principal
    @security = security
    @tipoBandeira = tipoBandeira
    @titular = titular
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}cicloFaturamento
#   criacao - SOAP::SOAPDateTime
#   diaEnvioBoleto - SOAP::SOAPInt
#   diaEnvioBoletoFatorMesBase - SOAP::SOAPInt
#   diaEnvioNFE - SOAP::SOAPInt
#   diaEnvioNFEFatorMesBase - SOAP::SOAPInt
#   diaFechamentoConsumo - SOAP::SOAPInt
#   diaFechamentoConsumoFatorMesBase - SOAP::SOAPInt
#   diaFechamentoFatura - SOAP::SOAPInt
#   diaFechamentoFaturaFatorMesBase - SOAP::SOAPInt
#   diaVencimento - SOAP::SOAPInt
#   diaVencimentoFatorMesBase - SOAP::SOAPInt
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   nome - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class CicloFaturamento
  attr_accessor :criacao
  attr_accessor :diaEnvioBoleto
  attr_accessor :diaEnvioBoletoFatorMesBase
  attr_accessor :diaEnvioNFE
  attr_accessor :diaEnvioNFEFatorMesBase
  attr_accessor :diaFechamentoConsumo
  attr_accessor :diaFechamentoConsumoFatorMesBase
  attr_accessor :diaFechamentoFatura
  attr_accessor :diaFechamentoFaturaFatorMesBase
  attr_accessor :diaVencimento
  attr_accessor :diaVencimentoFatorMesBase
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :nome
  attr_accessor :uuid

  def initialize(criacao = nil, diaEnvioBoleto = nil, diaEnvioBoletoFatorMesBase = nil, diaEnvioNFE = nil, diaEnvioNFEFatorMesBase = nil, diaFechamentoConsumo = nil, diaFechamentoConsumoFatorMesBase = nil, diaFechamentoFatura = nil, diaFechamentoFaturaFatorMesBase = nil, diaVencimento = nil, diaVencimentoFatorMesBase = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, nome = nil, uuid = nil)
    @criacao = criacao
    @diaEnvioBoleto = diaEnvioBoleto
    @diaEnvioBoletoFatorMesBase = diaEnvioBoletoFatorMesBase
    @diaEnvioNFE = diaEnvioNFE
    @diaEnvioNFEFatorMesBase = diaEnvioNFEFatorMesBase
    @diaFechamentoConsumo = diaFechamentoConsumo
    @diaFechamentoConsumoFatorMesBase = diaFechamentoConsumoFatorMesBase
    @diaFechamentoFatura = diaFechamentoFatura
    @diaFechamentoFaturaFatorMesBase = diaFechamentoFaturaFatorMesBase
    @diaVencimento = diaVencimento
    @diaVencimentoFatorMesBase = diaVencimentoFatorMesBase
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @nome = nome
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}meioPagamento
#   agencia - SOAP::SOAPString
#   agenciaDigito - SOAP::SOAPString
#   ativo - SOAP::SOAPBoolean
#   banco - TipoBanco
#   conta - SOAP::SOAPString
#   contaDigito - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   exibirNoFormularioDeContratoExterno - SOAP::SOAPBoolean
#   gerenteUuid - SOAP::SOAPString
#   instrucoesDeCobrancaLinha1 - SOAP::SOAPString
#   instrucoesDeCobrancaLinha2 - SOAP::SOAPString
#   instrucoesDeCobrancaLinha3 - SOAP::SOAPString
#   instrucoesDeCobrancaLinha4 - SOAP::SOAPString
#   instrucoesDeCobrancaLinha5 - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   moraMensalAposVencimento - SOAP::SOAPDecimal
#   multaAposVencimento - SOAP::SOAPDecimal
#   nome - SOAP::SOAPString
#   nossoNumeroInicial - SOAP::SOAPLong
#   numeroCarteira - SOAP::SOAPString
#   numeroConvenio - SOAP::SOAPString
#   quantidadeDiasCobrancaAposVencimento - SOAP::SOAPInt
#   quantidadeDiasConsiderarVencido - SOAP::SOAPInt
#   quantidadeDiasEnvioAntecipado - SOAP::SOAPInt
#   tarifaBoleto - SOAP::SOAPDecimal
#   tipoMeioPagamento - TipoMeioPagamento
#   uuid - SOAP::SOAPString
class MeioPagamento
  attr_accessor :agencia
  attr_accessor :agenciaDigito
  attr_accessor :ativo
  attr_accessor :banco
  attr_accessor :conta
  attr_accessor :contaDigito
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :exibirNoFormularioDeContratoExterno
  attr_accessor :gerenteUuid
  attr_accessor :instrucoesDeCobrancaLinha1
  attr_accessor :instrucoesDeCobrancaLinha2
  attr_accessor :instrucoesDeCobrancaLinha3
  attr_accessor :instrucoesDeCobrancaLinha4
  attr_accessor :instrucoesDeCobrancaLinha5
  attr_accessor :modificacao
  attr_accessor :moraMensalAposVencimento
  attr_accessor :multaAposVencimento
  attr_accessor :nome
  attr_accessor :nossoNumeroInicial
  attr_accessor :numeroCarteira
  attr_accessor :numeroConvenio
  attr_accessor :quantidadeDiasCobrancaAposVencimento
  attr_accessor :quantidadeDiasConsiderarVencido
  attr_accessor :quantidadeDiasEnvioAntecipado
  attr_accessor :tarifaBoleto
  attr_accessor :tipoMeioPagamento
  attr_accessor :uuid

  def initialize(agencia = nil, agenciaDigito = nil, ativo = nil, banco = nil, conta = nil, contaDigito = nil, criacao = nil, exclusao = nil, exibirNoFormularioDeContratoExterno = nil, gerenteUuid = nil, instrucoesDeCobrancaLinha1 = nil, instrucoesDeCobrancaLinha2 = nil, instrucoesDeCobrancaLinha3 = nil, instrucoesDeCobrancaLinha4 = nil, instrucoesDeCobrancaLinha5 = nil, modificacao = nil, moraMensalAposVencimento = nil, multaAposVencimento = nil, nome = nil, nossoNumeroInicial = nil, numeroCarteira = nil, numeroConvenio = nil, quantidadeDiasCobrancaAposVencimento = nil, quantidadeDiasConsiderarVencido = nil, quantidadeDiasEnvioAntecipado = nil, tarifaBoleto = nil, tipoMeioPagamento = nil, uuid = nil)
    @agencia = agencia
    @agenciaDigito = agenciaDigito
    @ativo = ativo
    @banco = banco
    @conta = conta
    @contaDigito = contaDigito
    @criacao = criacao
    @exclusao = exclusao
    @exibirNoFormularioDeContratoExterno = exibirNoFormularioDeContratoExterno
    @gerenteUuid = gerenteUuid
    @instrucoesDeCobrancaLinha1 = instrucoesDeCobrancaLinha1
    @instrucoesDeCobrancaLinha2 = instrucoesDeCobrancaLinha2
    @instrucoesDeCobrancaLinha3 = instrucoesDeCobrancaLinha3
    @instrucoesDeCobrancaLinha4 = instrucoesDeCobrancaLinha4
    @instrucoesDeCobrancaLinha5 = instrucoesDeCobrancaLinha5
    @modificacao = modificacao
    @moraMensalAposVencimento = moraMensalAposVencimento
    @multaAposVencimento = multaAposVencimento
    @nome = nome
    @nossoNumeroInicial = nossoNumeroInicial
    @numeroCarteira = numeroCarteira
    @numeroConvenio = numeroConvenio
    @quantidadeDiasCobrancaAposVencimento = quantidadeDiasCobrancaAposVencimento
    @quantidadeDiasConsiderarVencido = quantidadeDiasConsiderarVencido
    @quantidadeDiasEnvioAntecipado = quantidadeDiasEnvioAntecipado
    @tarifaBoleto = tarifaBoleto
    @tipoMeioPagamento = tipoMeioPagamento
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteResponse
class AlterarClienteResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}adicionarRelacionamento
#   arg0 - Relacionamento
#   arg1 - SOAP::SOAPString
class AdicionarRelacionamento
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}adicionarRelacionamentoResponse
class AdicionarRelacionamentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteServico
#   arg0 - ClienteServico
#   arg1 - SOAP::SOAPString
class AlterarClienteServico
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteServicoResponse
class AlterarClienteServicoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarCicloFaturamento
class ListarCicloFaturamento
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarCicloFaturamentoResponse
class ListarCicloFaturamentoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}obterConfiguracao
#   arg0 - SOAP::SOAPString
class ObterConfiguracao
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterConfiguracaoResponse
#   m_return - Configuracao
class ObterConfiguracaoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}configuracao
#   cadastrosPrefeitura - Cidade
#   caminhoCabecalhoEmail - SOAP::SOAPString
#   caminhoLogo - SOAP::SOAPString
#   certificadoDigitalCaminho - SOAP::SOAPString
#   certificadoDigitalSenha - SOAP::SOAPString
#   codigoCNAE - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   domicilioFaturamento - Cidade
#   emailCobranca - SOAP::SOAPString
#   emailFinanceiro - SOAP::SOAPString
#   entityIdMyFinance - SOAP::SOAPInt
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   incentivadorCultural - SOAP::SOAPString
#   merchantKeyMundipagg - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   numeroLOTE - SOAP::SOAPLong
#   numeroRPS - SOAP::SOAPLong
#   referenciaCR - SOAP::SOAPInt
#   referenciaCSCR - SOAP::SOAPInt
#   regimeTributario - SOAP::SOAPString
#   senhaMastersaf - SOAP::SOAPString
#   serieCR - SOAP::SOAPString
#   serieCSCR - SOAP::SOAPString
#   serieRPS - SOAP::SOAPString
#   seriesCliente - Serie
#   seriesContrato - Serie
#   simplesNacional - SOAP::SOAPString
#   smtpEmailDeCobranca - SOAP::SOAPString
#   subSerieCR - SOAP::SOAPString
#   subSerieCSCR - SOAP::SOAPString
#   tipoAmbiente - TipoAmbiente
#   tipoStatusEmailCobranca - TipoStatusEmailCobranca
#   tokenMyFinance - SOAP::SOAPString
#   usuarioMastersaf - SOAP::SOAPString
#   uuid - SOAP::SOAPString
#   webserviceMastersaf - SOAP::SOAPString
class Configuracao
  attr_accessor :cadastrosPrefeitura
  attr_accessor :caminhoCabecalhoEmail
  attr_accessor :caminhoLogo
  attr_accessor :certificadoDigitalCaminho
  attr_accessor :certificadoDigitalSenha
  attr_accessor :codigoCNAE
  attr_accessor :criacao
  attr_accessor :domicilioFaturamento
  attr_accessor :emailCobranca
  attr_accessor :emailFinanceiro
  attr_accessor :entityIdMyFinance
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :incentivadorCultural
  attr_accessor :merchantKeyMundipagg
  attr_accessor :modificacao
  attr_accessor :numeroLOTE
  attr_accessor :numeroRPS
  attr_accessor :referenciaCR
  attr_accessor :referenciaCSCR
  attr_accessor :regimeTributario
  attr_accessor :senhaMastersaf
  attr_accessor :serieCR
  attr_accessor :serieCSCR
  attr_accessor :serieRPS
  attr_accessor :seriesCliente
  attr_accessor :seriesContrato
  attr_accessor :simplesNacional
  attr_accessor :smtpEmailDeCobranca
  attr_accessor :subSerieCR
  attr_accessor :subSerieCSCR
  attr_accessor :tipoAmbiente
  attr_accessor :tipoStatusEmailCobranca
  attr_accessor :tokenMyFinance
  attr_accessor :usuarioMastersaf
  attr_accessor :uuid
  attr_accessor :webserviceMastersaf

  def initialize(cadastrosPrefeitura = [], caminhoCabecalhoEmail = nil, caminhoLogo = nil, certificadoDigitalCaminho = nil, certificadoDigitalSenha = nil, codigoCNAE = nil, criacao = nil, domicilioFaturamento = nil, emailCobranca = nil, emailFinanceiro = nil, entityIdMyFinance = nil, exclusao = nil, gerenteUuid = nil, incentivadorCultural = nil, merchantKeyMundipagg = nil, modificacao = nil, numeroLOTE = nil, numeroRPS = nil, referenciaCR = nil, referenciaCSCR = nil, regimeTributario = nil, senhaMastersaf = nil, serieCR = nil, serieCSCR = nil, serieRPS = nil, seriesCliente = [], seriesContrato = [], simplesNacional = nil, smtpEmailDeCobranca = nil, subSerieCR = nil, subSerieCSCR = nil, tipoAmbiente = nil, tipoStatusEmailCobranca = nil, tokenMyFinance = nil, usuarioMastersaf = nil, uuid = nil, webserviceMastersaf = nil)
    @cadastrosPrefeitura = cadastrosPrefeitura
    @caminhoCabecalhoEmail = caminhoCabecalhoEmail
    @caminhoLogo = caminhoLogo
    @certificadoDigitalCaminho = certificadoDigitalCaminho
    @certificadoDigitalSenha = certificadoDigitalSenha
    @codigoCNAE = codigoCNAE
    @criacao = criacao
    @domicilioFaturamento = domicilioFaturamento
    @emailCobranca = emailCobranca
    @emailFinanceiro = emailFinanceiro
    @entityIdMyFinance = entityIdMyFinance
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @incentivadorCultural = incentivadorCultural
    @merchantKeyMundipagg = merchantKeyMundipagg
    @modificacao = modificacao
    @numeroLOTE = numeroLOTE
    @numeroRPS = numeroRPS
    @referenciaCR = referenciaCR
    @referenciaCSCR = referenciaCSCR
    @regimeTributario = regimeTributario
    @senhaMastersaf = senhaMastersaf
    @serieCR = serieCR
    @serieCSCR = serieCSCR
    @serieRPS = serieRPS
    @seriesCliente = seriesCliente
    @seriesContrato = seriesContrato
    @simplesNacional = simplesNacional
    @smtpEmailDeCobranca = smtpEmailDeCobranca
    @subSerieCR = subSerieCR
    @subSerieCSCR = subSerieCSCR
    @tipoAmbiente = tipoAmbiente
    @tipoStatusEmailCobranca = tipoStatusEmailCobranca
    @tokenMyFinance = tokenMyFinance
    @usuarioMastersaf = usuarioMastersaf
    @uuid = uuid
    @webserviceMastersaf = webserviceMastersaf
  end
end

# {http://ws.api.smartbill.com.br/}serie
#   criacao - SOAP::SOAPDateTime
#   subSeries - SubSerie
#   tamanhoReferencia - SOAP::SOAPInt
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPString
class Serie
  attr_accessor :criacao
  attr_accessor :subSeries
  attr_accessor :tamanhoReferencia
  attr_accessor :uuid
  attr_accessor :valor

  def initialize(criacao = nil, subSeries = [], tamanhoReferencia = nil, uuid = nil, valor = nil)
    @criacao = criacao
    @subSeries = subSeries
    @tamanhoReferencia = tamanhoReferencia
    @uuid = uuid
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}subSerie
#   criacao - SOAP::SOAPDateTime
#   referencia - SOAP::SOAPInt
#   uuid - SOAP::SOAPString
#   valor - SOAP::SOAPString
class SubSerie
  attr_accessor :criacao
  attr_accessor :referencia
  attr_accessor :uuid
  attr_accessor :valor

  def initialize(criacao = nil, referencia = nil, uuid = nil, valor = nil)
    @criacao = criacao
    @referencia = referencia
    @uuid = uuid
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}removerRelacionamento
#   arg0 - Relacionamento
#   arg1 - SOAP::SOAPString
class RemoverRelacionamento
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}removerRelacionamentoResponse
class RemoverRelacionamentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}removerContato
#   arg0 - Contato
#   arg1 - SOAP::SOAPString
class RemoverContato
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}removerContatoResponse
class RemoverContatoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterSimplesNacional
#   arg0 - SOAP::SOAPString
class ObterSimplesNacional
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterSimplesNacionalResponse
#   m_return - SimplesNacional
class ObterSimplesNacionalResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}simplesNacional
#   codigo - SOAP::SOAPString
#   descricao - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class SimplesNacional
  attr_accessor :codigo
  attr_accessor :descricao
  attr_accessor :uuid

  def initialize(codigo = nil, descricao = nil, uuid = nil)
    @codigo = codigo
    @descricao = descricao
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}alterarMotivoCancelamento
#   arg0 - MotivoCancelamento
class AlterarMotivoCancelamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}alterarMotivoCancelamentoResponse
class AlterarMotivoCancelamentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteNF
#   arg0 - ClienteNF
#   arg1 - SOAP::SOAPString
class AlterarClienteNF
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteNFResponse
class AlterarClienteNFResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteCR
#   arg0 - ClienteCR
#   arg1 - SOAP::SOAPString
class AlterarClienteCR
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}clienteCR
#   anoReferencia - SOAP::SOAPInt
#   boletos - ClienteCRTransacaoBoleto
#   cartoes - ClienteCRTransacaoCartao
#   cliente - Cliente
#   clienteCRCadastroUuid - SOAP::SOAPString
#   codigo - SOAP::SOAPString
#   criacao - SOAP::SOAPDateTime
#   dataBaixa - SOAP::SOAPDateTime
#   dataCancelamento - SOAP::SOAPDateTime
#   dataEmissao - SOAP::SOAPDateTime
#   dataEnvio - SOAP::SOAPDateTime
#   dataEnvioAtraso - SOAP::SOAPDateTime
#   dataPagamento - SOAP::SOAPDateTime
#   dataVencimento - SOAP::SOAPDateTime
#   dataVencimentoOriginal - SOAP::SOAPDateTime
#   depositos - ClienteCRTransacaoDeposito
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   mesReferencia - SOAP::SOAPInt
#   modificacao - SOAP::SOAPDateTime
#   motivoCancelamento - MotivoCancelamento
#   notasFiscais - ClienteNF
#   projecaoFaturamento - ProjecaoFaturamento
#   referencia - SOAP::SOAPInt
#   serie - SOAP::SOAPString
#   servicosCR - ClienteServicoCR
#   subSerie - SOAP::SOAPString
#   tipoCREstado - TipoCREstado
#   uuid - SOAP::SOAPString
#   valorDesconto - SOAP::SOAPDecimal
#   valorIOF - SOAP::SOAPDecimal
#   valorImposto - SOAP::SOAPDecimal
#   valorJurosMora - SOAP::SOAPDecimal
#   valorMulta - SOAP::SOAPDecimal
#   valorPago - SOAP::SOAPDecimal
#   valorTarifaBoleto - SOAP::SOAPDecimal
#   valorTotal - SOAP::SOAPDecimal
#   valorTotalOriginal - SOAP::SOAPDecimal
class ClienteCR
  attr_accessor :anoReferencia
  attr_accessor :boletos
  attr_accessor :cartoes
  attr_accessor :cliente
  attr_accessor :clienteCRCadastroUuid
  attr_accessor :codigo
  attr_accessor :criacao
  attr_accessor :dataBaixa
  attr_accessor :dataCancelamento
  attr_accessor :dataEmissao
  attr_accessor :dataEnvio
  attr_accessor :dataEnvioAtraso
  attr_accessor :dataPagamento
  attr_accessor :dataVencimento
  attr_accessor :dataVencimentoOriginal
  attr_accessor :depositos
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :mesReferencia
  attr_accessor :modificacao
  attr_accessor :motivoCancelamento
  attr_accessor :notasFiscais
  attr_accessor :projecaoFaturamento
  attr_accessor :referencia
  attr_accessor :serie
  attr_accessor :servicosCR
  attr_accessor :subSerie
  attr_accessor :tipoCREstado
  attr_accessor :uuid
  attr_accessor :valorDesconto
  attr_accessor :valorIOF
  attr_accessor :valorImposto
  attr_accessor :valorJurosMora
  attr_accessor :valorMulta
  attr_accessor :valorPago
  attr_accessor :valorTarifaBoleto
  attr_accessor :valorTotal
  attr_accessor :valorTotalOriginal

  def initialize(anoReferencia = nil, boletos = [], cartoes = [], cliente = nil, clienteCRCadastroUuid = nil, codigo = nil, criacao = nil, dataBaixa = nil, dataCancelamento = nil, dataEmissao = nil, dataEnvio = nil, dataEnvioAtraso = nil, dataPagamento = nil, dataVencimento = nil, dataVencimentoOriginal = nil, depositos = [], exclusao = nil, gerenteUuid = nil, mesReferencia = nil, modificacao = nil, motivoCancelamento = nil, notasFiscais = [], projecaoFaturamento = nil, referencia = nil, serie = nil, servicosCR = [], subSerie = nil, tipoCREstado = nil, uuid = nil, valorDesconto = nil, valorIOF = nil, valorImposto = nil, valorJurosMora = nil, valorMulta = nil, valorPago = nil, valorTarifaBoleto = nil, valorTotal = nil, valorTotalOriginal = nil)
    @anoReferencia = anoReferencia
    @boletos = boletos
    @cartoes = cartoes
    @cliente = cliente
    @clienteCRCadastroUuid = clienteCRCadastroUuid
    @codigo = codigo
    @criacao = criacao
    @dataBaixa = dataBaixa
    @dataCancelamento = dataCancelamento
    @dataEmissao = dataEmissao
    @dataEnvio = dataEnvio
    @dataEnvioAtraso = dataEnvioAtraso
    @dataPagamento = dataPagamento
    @dataVencimento = dataVencimento
    @dataVencimentoOriginal = dataVencimentoOriginal
    @depositos = depositos
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @mesReferencia = mesReferencia
    @modificacao = modificacao
    @motivoCancelamento = motivoCancelamento
    @notasFiscais = notasFiscais
    @projecaoFaturamento = projecaoFaturamento
    @referencia = referencia
    @serie = serie
    @servicosCR = servicosCR
    @subSerie = subSerie
    @tipoCREstado = tipoCREstado
    @uuid = uuid
    @valorDesconto = valorDesconto
    @valorIOF = valorIOF
    @valorImposto = valorImposto
    @valorJurosMora = valorJurosMora
    @valorMulta = valorMulta
    @valorPago = valorPago
    @valorTarifaBoleto = valorTarifaBoleto
    @valorTotal = valorTotal
    @valorTotalOriginal = valorTotalOriginal
  end
end

# {http://ws.api.smartbill.com.br/}clienteCRTransacao
#   consulta - SOAP::SOAPDateTime
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   tipoClienteCRTransacao - TipoClienteCRTransacao
#   uuid - SOAP::SOAPString
class ClienteCRTransacao
  attr_accessor :consulta
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :tipoClienteCRTransacao
  attr_accessor :uuid

  def initialize(consulta = nil, criacao = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, tipoClienteCRTransacao = nil, uuid = nil)
    @consulta = consulta
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @tipoClienteCRTransacao = tipoClienteCRTransacao
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}clienteCRTransacaoBoleto
#   consulta - SOAP::SOAPDateTime
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   tipoClienteCRTransacao - TipoClienteCRTransacao
#   uuid - SOAP::SOAPString
#   boletoUrl - SOAP::SOAPString
#   chavePedido - SOAP::SOAPString
#   chaveRequisicao - SOAP::SOAPString
#   chaveTransacao - SOAP::SOAPString
#   codigoBarra - SOAP::SOAPString
#   dataVencimento - SOAP::SOAPDateTime
#   nossoNumero - SOAP::SOAPLong
#   referenciaPedido - SOAP::SOAPString
#   referenciaTransacao - SOAP::SOAPString
#   situacaoPedido - TipoStatusGateway
#   valor - SOAP::SOAPDecimal
class ClienteCRTransacaoBoleto < ClienteCRTransacao
  attr_accessor :consulta
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :tipoClienteCRTransacao
  attr_accessor :uuid
  attr_accessor :boletoUrl
  attr_accessor :chavePedido
  attr_accessor :chaveRequisicao
  attr_accessor :chaveTransacao
  attr_accessor :codigoBarra
  attr_accessor :dataVencimento
  attr_accessor :nossoNumero
  attr_accessor :referenciaPedido
  attr_accessor :referenciaTransacao
  attr_accessor :situacaoPedido
  attr_accessor :valor

  def initialize(consulta = nil, criacao = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, tipoClienteCRTransacao = nil, uuid = nil, boletoUrl = nil, chavePedido = nil, chaveRequisicao = nil, chaveTransacao = nil, codigoBarra = nil, dataVencimento = nil, nossoNumero = nil, referenciaPedido = nil, referenciaTransacao = nil, situacaoPedido = nil, valor = nil)
    @consulta = consulta
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @tipoClienteCRTransacao = tipoClienteCRTransacao
    @uuid = uuid
    @boletoUrl = boletoUrl
    @chavePedido = chavePedido
    @chaveRequisicao = chaveRequisicao
    @chaveTransacao = chaveTransacao
    @codigoBarra = codigoBarra
    @dataVencimento = dataVencimento
    @nossoNumero = nossoNumero
    @referenciaPedido = referenciaPedido
    @referenciaTransacao = referenciaTransacao
    @situacaoPedido = situacaoPedido
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}clienteCRTransacaoCartao
#   consulta - SOAP::SOAPDateTime
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   tipoClienteCRTransacao - TipoClienteCRTransacao
#   uuid - SOAP::SOAPString
#   chaveCompra - SOAP::SOAPString
#   chavePedido - SOAP::SOAPString
#   chaveRequisicao - SOAP::SOAPString
#   chaveTransacao - SOAP::SOAPString
#   codigoAdquirente - SOAP::SOAPString
#   codigoAutorizacao - SOAP::SOAPString
#   dataTransacao - SOAP::SOAPDateTime
#   identificacaoTransacao - SOAP::SOAPString
#   mensagemAdquirente - SOAP::SOAPString
#   numero - SOAP::SOAPString
#   referenciaPedido - SOAP::SOAPString
#   referenciaTransacao - SOAP::SOAPString
#   situacaoPedido - TipoStatusGateway
#   tipoBandeira - TipoBandeira
#   titular - SOAP::SOAPString
#   valor - SOAP::SOAPDecimal
class ClienteCRTransacaoCartao < ClienteCRTransacao
  attr_accessor :consulta
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :tipoClienteCRTransacao
  attr_accessor :uuid
  attr_accessor :chaveCompra
  attr_accessor :chavePedido
  attr_accessor :chaveRequisicao
  attr_accessor :chaveTransacao
  attr_accessor :codigoAdquirente
  attr_accessor :codigoAutorizacao
  attr_accessor :dataTransacao
  attr_accessor :identificacaoTransacao
  attr_accessor :mensagemAdquirente
  attr_accessor :numero
  attr_accessor :referenciaPedido
  attr_accessor :referenciaTransacao
  attr_accessor :situacaoPedido
  attr_accessor :tipoBandeira
  attr_accessor :titular
  attr_accessor :valor

  def initialize(consulta = nil, criacao = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, tipoClienteCRTransacao = nil, uuid = nil, chaveCompra = nil, chavePedido = nil, chaveRequisicao = nil, chaveTransacao = nil, codigoAdquirente = nil, codigoAutorizacao = nil, dataTransacao = nil, identificacaoTransacao = nil, mensagemAdquirente = nil, numero = nil, referenciaPedido = nil, referenciaTransacao = nil, situacaoPedido = nil, tipoBandeira = nil, titular = nil, valor = nil)
    @consulta = consulta
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @tipoClienteCRTransacao = tipoClienteCRTransacao
    @uuid = uuid
    @chaveCompra = chaveCompra
    @chavePedido = chavePedido
    @chaveRequisicao = chaveRequisicao
    @chaveTransacao = chaveTransacao
    @codigoAdquirente = codigoAdquirente
    @codigoAutorizacao = codigoAutorizacao
    @dataTransacao = dataTransacao
    @identificacaoTransacao = identificacaoTransacao
    @mensagemAdquirente = mensagemAdquirente
    @numero = numero
    @referenciaPedido = referenciaPedido
    @referenciaTransacao = referenciaTransacao
    @situacaoPedido = situacaoPedido
    @tipoBandeira = tipoBandeira
    @titular = titular
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}clienteCRTransacaoDeposito
#   consulta - SOAP::SOAPDateTime
#   criacao - SOAP::SOAPDateTime
#   exclusao - SOAP::SOAPDateTime
#   gerenteUuid - SOAP::SOAPString
#   modificacao - SOAP::SOAPDateTime
#   tipoClienteCRTransacao - TipoClienteCRTransacao
#   uuid - SOAP::SOAPString
#   dataVencimento - SOAP::SOAPDateTime
#   situacaoPedido - TipoStatusGateway
#   valor - SOAP::SOAPDecimal
class ClienteCRTransacaoDeposito < ClienteCRTransacao
  attr_accessor :consulta
  attr_accessor :criacao
  attr_accessor :exclusao
  attr_accessor :gerenteUuid
  attr_accessor :modificacao
  attr_accessor :tipoClienteCRTransacao
  attr_accessor :uuid
  attr_accessor :dataVencimento
  attr_accessor :situacaoPedido
  attr_accessor :valor

  def initialize(consulta = nil, criacao = nil, exclusao = nil, gerenteUuid = nil, modificacao = nil, tipoClienteCRTransacao = nil, uuid = nil, dataVencimento = nil, situacaoPedido = nil, valor = nil)
    @consulta = consulta
    @criacao = criacao
    @exclusao = exclusao
    @gerenteUuid = gerenteUuid
    @modificacao = modificacao
    @tipoClienteCRTransacao = tipoClienteCRTransacao
    @uuid = uuid
    @dataVencimento = dataVencimento
    @situacaoPedido = situacaoPedido
    @valor = valor
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteCRResponse
class AlterarClienteCRResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}adicionarCliente
#   arg0 - Cliente
class AdicionarCliente
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteResponse
class AdicionarClienteResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarCodigoCNAE
#   arg0 - CodigoCNAEFilter
class ListarCodigoCNAE
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}codigoCNAEFilter
class CodigoCNAEFilter
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarCodigoCNAEResponse
class ListarCodigoCNAEResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}obterNaturezaOperacao
#   arg0 - SOAP::SOAPString
class ObterNaturezaOperacao
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterNaturezaOperacaoResponse
#   m_return - NaturezaOperacao
class ObterNaturezaOperacaoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}naturezaOperacao
#   codigo - SOAP::SOAPString
#   descricao - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class NaturezaOperacao
  attr_accessor :codigo
  attr_accessor :descricao
  attr_accessor :uuid

  def initialize(codigo = nil, descricao = nil, uuid = nil)
    @codigo = codigo
    @descricao = descricao
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteServicoCR
#   arg0 - SOAP::SOAPString
class ObterClienteServicoCR
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteServicoCRResponse
#   m_return - ClienteServicoCR
class ObterClienteServicoCRResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}gerarCobranca
#   arg0 - SOAP::SOAPString
#   arg1 - SOAP::SOAPString
class GerarCobranca
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}gerarCobrancaResponse
class GerarCobrancaResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteNF
#   arg0 - SOAP::SOAPString
class ObterClienteNF
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteNFResponse
#   m_return - ClienteNF
class ObterClienteNFResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}emailCobranca
#   arg0 - SOAP::SOAPString
#   arg1 - SOAP::SOAPString
class EmailCobranca
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}emailCobrancaResponse
class EmailCobrancaResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteServico
#   arg0 - SOAP::SOAPString
class ObterClienteServico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteServicoResponse
#   m_return - ClienteServico
class ObterClienteServicoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}listarSegmento
#   arg0 - SOAP::SOAPString
class ListarSegmento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}listarSegmentoResponse
class ListarSegmentoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}adicionarFamilia
#   arg0 - Familia
class AdicionarFamilia
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}adicionarFamiliaResponse
class AdicionarFamiliaResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarMeioPagamento
#   arg0 - MeioPagamentoFilter
class ListarMeioPagamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}meioPagamentoFilter
#   ativo - SOAP::SOAPBoolean
#   tiposMeioPagamento - TipoMeioPagamento
class MeioPagamentoFilter
  attr_accessor :ativo
  attr_accessor :tiposMeioPagamento

  def initialize(ativo = nil, tiposMeioPagamento = [])
    @ativo = ativo
    @tiposMeioPagamento = tiposMeioPagamento
  end
end

# {http://ws.api.smartbill.com.br/}listarMeioPagamentoResponse
class ListarMeioPagamentoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}removerClienteCR
#   arg0 - ClienteCR
#   arg1 - SOAP::SOAPString
class RemoverClienteCR
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteCRResponse
class RemoverClienteCRResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteCR
#   arg0 - SOAP::SOAPString
class ObterClienteCR
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteCRResponse
#   m_return - ClienteCR
class ObterClienteCRResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}alterarRamoAtividade
#   arg0 - RamoAtividade
class AlterarRamoAtividade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}alterarRamoAtividadeResponse
class AlterarRamoAtividadeResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterUuidUsuario
#   arg0 - SOAP::SOAPString
class ObterUuidUsuario
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterUuidUsuarioResponse
#   m_return - SOAP::SOAPString
class ObterUuidUsuarioResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}obterMeioPagamento
#   arg0 - SOAP::SOAPString
class ObterMeioPagamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterMeioPagamentoResponse
#   m_return - MeioPagamento
class ObterMeioPagamentoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}obterEndereco
#   arg0 - SOAP::SOAPString
class ObterEndereco
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterEnderecoResponse
#   m_return - Endereco
class ObterEnderecoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}solicitarClienteCartao
#   arg0 - SOAP::SOAPString
class SolicitarClienteCartao
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}solicitarClienteCartaoResponse
class SolicitarClienteCartaoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}cancelarClienteServicoPorClienteServicoUuid
#   arg0 - SOAP::SOAPString
class CancelarClienteServicoPorClienteServicoUuid
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}cancelarClienteServicoPorClienteServicoUuidResponse
class CancelarClienteServicoPorClienteServicoUuidResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarEndereco
#   arg0 - Endereco
#   arg1 - SOAP::SOAPString
class AlterarEndereco
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}alterarEnderecoResponse
class AlterarEnderecoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}regraClienteServicoImpostos
#   arg0 - ClienteServico
#   arg1 - SOAP::SOAPString
class RegraClienteServicoImpostos
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}regraClienteServicoImpostosResponse
#   m_return - ClienteServico
class RegraClienteServicoImpostosResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}obterGerente
#   arg0 - SOAP::SOAPString
class ObterGerente
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterGerenteResponse
#   m_return - SOAP::SOAPString
class ObterGerenteResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalClienteCR
#   arg0 - ClienteCRFilter
class ListarTotalClienteCR
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}clienteCRFilter
#   anoReferencia - SOAP::SOAPInt
#   boleto - SOAP::SOAPLong
#   cicloFaturamento - CicloFaturamento
#   clienteServicoCRUuid - SOAP::SOAPString
#   clienteServicoUuid - SOAP::SOAPString
#   clienteUuid - SOAP::SOAPString
#   enviado - SOAP::SOAPBoolean
#   generalFilter - GeneralFilter
#   mesReferencia - SOAP::SOAPInt
#   notaFiscal - SOAP::SOAPInteger
#   pago - SOAP::SOAPBoolean
#   possuiNFEmitido - SOAP::SOAPBoolean
#   projecaoFaturamento - ProjecaoFaturamento
#   rangeDataPagamento - SOAP::SOAPDateTime
#   rangeDataVencimento - SOAP::SOAPDateTime
#   rangeDataVencimentoOriginal - SOAP::SOAPDateTime
#   tipoCREstado - TipoCREstado
#   vencido - SOAP::SOAPBoolean
class ClienteCRFilter
  attr_accessor :anoReferencia
  attr_accessor :boleto
  attr_accessor :cicloFaturamento
  attr_accessor :clienteServicoCRUuid
  attr_accessor :clienteServicoUuid
  attr_accessor :clienteUuid
  attr_accessor :enviado
  attr_accessor :generalFilter
  attr_accessor :mesReferencia
  attr_accessor :notaFiscal
  attr_accessor :pago
  attr_accessor :possuiNFEmitido
  attr_accessor :projecaoFaturamento
  attr_accessor :rangeDataPagamento
  attr_accessor :rangeDataVencimento
  attr_accessor :rangeDataVencimentoOriginal
  attr_accessor :tipoCREstado
  attr_accessor :vencido

  def initialize(anoReferencia = nil, boleto = nil, cicloFaturamento = nil, clienteServicoCRUuid = nil, clienteServicoUuid = nil, clienteUuid = nil, enviado = nil, generalFilter = nil, mesReferencia = nil, notaFiscal = nil, pago = nil, possuiNFEmitido = nil, projecaoFaturamento = [], rangeDataPagamento = [], rangeDataVencimento = [], rangeDataVencimentoOriginal = [], tipoCREstado = [], vencido = nil)
    @anoReferencia = anoReferencia
    @boleto = boleto
    @cicloFaturamento = cicloFaturamento
    @clienteServicoCRUuid = clienteServicoCRUuid
    @clienteServicoUuid = clienteServicoUuid
    @clienteUuid = clienteUuid
    @enviado = enviado
    @generalFilter = generalFilter
    @mesReferencia = mesReferencia
    @notaFiscal = notaFiscal
    @pago = pago
    @possuiNFEmitido = possuiNFEmitido
    @projecaoFaturamento = projecaoFaturamento
    @rangeDataPagamento = rangeDataPagamento
    @rangeDataVencimento = rangeDataVencimento
    @rangeDataVencimentoOriginal = rangeDataVencimentoOriginal
    @tipoCREstado = tipoCREstado
    @vencido = vencido
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalClienteCRResponse
#   m_return - SOAP::SOAPLong
class ListarTotalClienteCRResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}suspenderClienteServicoPorClienteServicoUuid
#   arg0 - SOAP::SOAPString
class SuspenderClienteServicoPorClienteServicoUuid
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}suspenderClienteServicoPorClienteServicoUuidResponse
class SuspenderClienteServicoPorClienteServicoUuidResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}adicionarEndereco
#   arg0 - Endereco
#   arg1 - SOAP::SOAPString
class AdicionarEndereco
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}adicionarEnderecoResponse
class AdicionarEnderecoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}removerCliente
#   arg0 - Cliente
class RemoverCliente
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteResponse
class RemoverClienteResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterPadraoPreenchimento
class ObterPadraoPreenchimento
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterPadraoPreenchimentoResponse
#   m_return - PadraoPreenchimento
class ObterPadraoPreenchimentoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}padraoPreenchimento
#   ativo - SOAP::SOAPBoolean
#   bloqueioFaturamento - BloqueioFaturamento
#   cicloFaturamento - CicloFaturamento
#   cidade - Cidade
#   criacao - SOAP::SOAPDateTime
#   documentoFiscal - DocumentoFiscal
#   exclusao - SOAP::SOAPDateTime
#   familia - Familia
#   fatorDeReajusteDaRenovacao - IndiceEconomico
#   gerenteUuid - SOAP::SOAPString
#   meioPagamento - MeioPagamento
#   modificacao - SOAP::SOAPDateTime
#   naturezaOperacao - SOAP::SOAPString
#   orgaoPublico - SOAP::SOAPBoolean
#   periodicidade - Periodicidade
#   quantidadeTipoVigencia - SOAP::SOAPInt
#   ramoAtividade - RamoAtividade
#   recebeCobranca - SOAP::SOAPBoolean
#   renovacaoAutomatica - SOAP::SOAPBoolean
#   tipoContato - TipoContato
#   tipoEndereco - TipoEndereco
#   tipoFaturamento - TipoFaturamento
#   tipoPessoa - TipoPessoa
#   tipoServico - TipoServico
#   tipoVigencia - TipoVigencia
#   usarFatorDeReajusteAutomatico - SOAP::SOAPBoolean
#   uuid - SOAP::SOAPString
class PadraoPreenchimento
  attr_accessor :ativo
  attr_accessor :bloqueioFaturamento
  attr_accessor :cicloFaturamento
  attr_accessor :cidade
  attr_accessor :criacao
  attr_accessor :documentoFiscal
  attr_accessor :exclusao
  attr_accessor :familia
  attr_accessor :fatorDeReajusteDaRenovacao
  attr_accessor :gerenteUuid
  attr_accessor :meioPagamento
  attr_accessor :modificacao
  attr_accessor :naturezaOperacao
  attr_accessor :orgaoPublico
  attr_accessor :periodicidade
  attr_accessor :quantidadeTipoVigencia
  attr_accessor :ramoAtividade
  attr_accessor :recebeCobranca
  attr_accessor :renovacaoAutomatica
  attr_accessor :tipoContato
  attr_accessor :tipoEndereco
  attr_accessor :tipoFaturamento
  attr_accessor :tipoPessoa
  attr_accessor :tipoServico
  attr_accessor :tipoVigencia
  attr_accessor :usarFatorDeReajusteAutomatico
  attr_accessor :uuid

  def initialize(ativo = nil, bloqueioFaturamento = nil, cicloFaturamento = nil, cidade = nil, criacao = nil, documentoFiscal = nil, exclusao = nil, familia = nil, fatorDeReajusteDaRenovacao = nil, gerenteUuid = nil, meioPagamento = nil, modificacao = nil, naturezaOperacao = nil, orgaoPublico = nil, periodicidade = nil, quantidadeTipoVigencia = nil, ramoAtividade = nil, recebeCobranca = nil, renovacaoAutomatica = nil, tipoContato = nil, tipoEndereco = nil, tipoFaturamento = nil, tipoPessoa = nil, tipoServico = nil, tipoVigencia = nil, usarFatorDeReajusteAutomatico = nil, uuid = nil)
    @ativo = ativo
    @bloqueioFaturamento = bloqueioFaturamento
    @cicloFaturamento = cicloFaturamento
    @cidade = cidade
    @criacao = criacao
    @documentoFiscal = documentoFiscal
    @exclusao = exclusao
    @familia = familia
    @fatorDeReajusteDaRenovacao = fatorDeReajusteDaRenovacao
    @gerenteUuid = gerenteUuid
    @meioPagamento = meioPagamento
    @modificacao = modificacao
    @naturezaOperacao = naturezaOperacao
    @orgaoPublico = orgaoPublico
    @periodicidade = periodicidade
    @quantidadeTipoVigencia = quantidadeTipoVigencia
    @ramoAtividade = ramoAtividade
    @recebeCobranca = recebeCobranca
    @renovacaoAutomatica = renovacaoAutomatica
    @tipoContato = tipoContato
    @tipoEndereco = tipoEndereco
    @tipoFaturamento = tipoFaturamento
    @tipoPessoa = tipoPessoa
    @tipoServico = tipoServico
    @tipoVigencia = tipoVigencia
    @usarFatorDeReajusteAutomatico = usarFatorDeReajusteAutomatico
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}removerMotivoSuspensao
#   arg0 - MotivoSuspensao
class RemoverMotivoSuspensao
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerMotivoSuspensaoResponse
class RemoverMotivoSuspensaoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarArquivo
#   arg0 - Arquivo
class AlterarArquivo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}alterarArquivoResponse
class AlterarArquivoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterServicoAliquotaPrestador
#   arg0 - SOAP::SOAPString
class ObterServicoAliquotaPrestador
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterServicoAliquotaPrestadorResponse
#   m_return - ServicoAliquotaPrestador
class ObterServicoAliquotaPrestadorResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}listarTemplateEmail
class ListarTemplateEmail
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarTemplateEmailResponse
class ListarTemplateEmailResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}listarIndiceEconomico
class ListarIndiceEconomico
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarIndiceEconomicoResponse
class ListarIndiceEconomicoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}obterTributacaoMunicipio
#   arg0 - SOAP::SOAPString
class ObterTributacaoMunicipio
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterTributacaoMunicipioResponse
#   m_return - TributacaoMunicipio
class ObterTributacaoMunicipioResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}tributacaoMunicipio
#   codigo - SOAP::SOAPString
#   descricao - SOAP::SOAPString
#   uuid - SOAP::SOAPString
class TributacaoMunicipio
  attr_accessor :codigo
  attr_accessor :descricao
  attr_accessor :uuid

  def initialize(codigo = nil, descricao = nil, uuid = nil)
    @codigo = codigo
    @descricao = descricao
    @uuid = uuid
  end
end

# {http://ws.api.smartbill.com.br/}listarFamilia
class ListarFamilia
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarFamiliaResponse
class ListarFamiliaResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}listarPeriodicidade
#   arg0 - PeriodicidadeFilter
class ListarPeriodicidade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}periodicidadeFilter
#   ativo - SOAP::SOAPBoolean
class PeriodicidadeFilter
  attr_accessor :ativo

  def initialize(ativo = nil)
    @ativo = ativo
  end
end

# {http://ws.api.smartbill.com.br/}listarPeriodicidadeResponse
class ListarPeriodicidadeResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}listarConfiguracao
class ListarConfiguracao
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarConfiguracaoResponse
class ListarConfiguracaoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}listarClienteServico
#   arg0 - ClienteServicoFilter
class ListarClienteServico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoFilter
#   clienteUuid - SOAP::SOAPString
#   codigoContrato - SOAP::SOAPString
#   excluindoUnidadesMetricas - UnidadeMetrica
#   servicoEstado - ServicoEstado
#   servicoUuid - SOAP::SOAPString
#   tipoConsumoEstado - TipoConsumoEstado
#   tipoServico - TipoServico
class ClienteServicoFilter
  attr_accessor :clienteUuid
  attr_accessor :codigoContrato
  attr_accessor :excluindoUnidadesMetricas
  attr_accessor :servicoEstado
  attr_accessor :servicoUuid
  attr_accessor :tipoConsumoEstado
  attr_accessor :tipoServico

  def initialize(clienteUuid = nil, codigoContrato = nil, excluindoUnidadesMetricas = [], servicoEstado = nil, servicoUuid = nil, tipoConsumoEstado = nil, tipoServico = nil)
    @clienteUuid = clienteUuid
    @codigoContrato = codigoContrato
    @excluindoUnidadesMetricas = excluindoUnidadesMetricas
    @servicoEstado = servicoEstado
    @servicoUuid = servicoUuid
    @tipoConsumoEstado = tipoConsumoEstado
    @tipoServico = tipoServico
  end
end

# {http://ws.api.smartbill.com.br/}listarClienteServicoResponse
class ListarClienteServicoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}adicionarRamoAtividade
#   arg0 - RamoAtividade
class AdicionarRamoAtividade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}adicionarRamoAtividadeResponse
class AdicionarRamoAtividadeResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarUf
#   arg0 - UfFilter
class ListarUf
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}ufFilter
#   sigla - SOAP::SOAPString
class UfFilter
  attr_accessor :sigla

  def initialize(sigla = nil)
    @sigla = sigla
  end
end

# {http://ws.api.smartbill.com.br/}listarUfResponse
class ListarUfResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}removerRamoAtividade
#   arg0 - RamoAtividade
class RemoverRamoAtividade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerRamoAtividadeResponse
class RemoverRamoAtividadeResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarPeriodicidade
#   arg0 - Periodicidade
class AlterarPeriodicidade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}alterarPeriodicidadeResponse
class AlterarPeriodicidadeResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteNF
#   arg0 - ClienteNF
#   arg1 - SOAP::SOAPString
class RemoverClienteNF
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteNFResponse
class RemoverClienteNFResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}reativarCliente
#   arg0 - SOAP::SOAPString
#   arg1 - SOAP::SOAPBoolean
class ReativarCliente
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}reativarClienteResponse
class ReativarClienteResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteServicoCR
#   arg0 - ClienteServicoCR
class AdicionarClienteServicoCR
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteServicoCRResponse
class AdicionarClienteServicoCRResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterCicloFaturamento
#   arg0 - SOAP::SOAPString
class ObterCicloFaturamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterCicloFaturamentoResponse
#   m_return - CicloFaturamento
class ObterCicloFaturamentoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteServicoConsumo
#   arg0 - ClienteServicoConsumo
#   arg1 - SOAP::SOAPString
class AlterarClienteServicoConsumo
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteServicoConsumoResponse
class AlterarClienteServicoConsumoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarServico
#   arg0 - Servico
class AlterarServico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}alterarServicoResponse
class AlterarServicoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterUf
#   arg0 - SOAP::SOAPString
class ObterUf
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterUfResponse
#   m_return - Uf
class ObterUfResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalServicoAliquotaPrestador
#   arg0 - ServicoAliquotaPrestadorFilter
class ListarTotalServicoAliquotaPrestador
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}listarTotalServicoAliquotaPrestadorResponse
#   m_return - SOAP::SOAPLong
class ListarTotalServicoAliquotaPrestadorResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}removerFamilia
#   arg0 - Familia
class RemoverFamilia
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerFamiliaResponse
class RemoverFamiliaResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarContato
#   arg0 - ContatoFilter
class ListarContato
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}contatoFilter
#   clienteUuid - SOAP::SOAPString
class ContatoFilter
  attr_accessor :clienteUuid

  def initialize(clienteUuid = nil)
    @clienteUuid = clienteUuid
  end
end

# {http://ws.api.smartbill.com.br/}listarContatoResponse
class ListarContatoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}enviarNFsEmissao
#   arg0 - SOAP::SOAPString
#   arg1 - SOAP::SOAPString
class EnviarNFsEmissao
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}enviarNFsEmissaoResponse
class EnviarNFsEmissaoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterSegmento
#   arg0 - SOAP::SOAPString
class ObterSegmento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterSegmentoResponse
#   m_return - Segmento
class ObterSegmentoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}obterMotivoSuspensao
#   arg0 - SOAP::SOAPString
class ObterMotivoSuspensao
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterMotivoSuspensaoResponse
#   m_return - MotivoSuspensao
class ObterMotivoSuspensaoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteNF
#   arg0 - ClienteNF
#   arg1 - SOAP::SOAPString
class AdicionarClienteNF
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteNFResponse
class AdicionarClienteNFResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}removerMotivoCancelamento
#   arg0 - MotivoCancelamento
class RemoverMotivoCancelamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerMotivoCancelamentoResponse
class RemoverMotivoCancelamentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}removerSegmento
#   arg0 - Segmento
#   arg1 - SOAP::SOAPString
class RemoverSegmento
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}removerSegmentoResponse
class RemoverSegmentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterRegra
#   arg0 - SOAP::SOAPString
class ObterRegra
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterRegraResponse
#   m_return - Regra
class ObterRegraResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}obterIndiceEconomico
#   arg0 - SOAP::SOAPString
class ObterIndiceEconomico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterIndiceEconomicoResponse
#   m_return - IndiceEconomico
class ObterIndiceEconomicoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}adicionarArquivo
#   arg0 - Arquivo
class AdicionarArquivo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}adicionarArquivoResponse
class AdicionarArquivoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarCicloFaturamento
#   arg0 - CicloFaturamento
class AlterarCicloFaturamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}alterarCicloFaturamentoResponse
class AlterarCicloFaturamentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteCR
#   arg0 - ClienteCR
#   arg1 - SOAP::SOAPString
class AdicionarClienteCR
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteCRResponse
class AdicionarClienteCRResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteServicoConsumo
#   arg0 - ClienteServicoConsumo
#   arg1 - SOAP::SOAPString
class RemoverClienteServicoConsumo
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteServicoConsumoResponse
class RemoverClienteServicoConsumoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarTributacaoMunicipio
#   arg0 - TributacaoMunicipioFilter
class ListarTributacaoMunicipio
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}tributacaoMunicipioFilter
#   cidadeUuid - SOAP::SOAPString
class TributacaoMunicipioFilter
  attr_accessor :cidadeUuid

  def initialize(cidadeUuid = nil)
    @cidadeUuid = cidadeUuid
  end
end

# {http://ws.api.smartbill.com.br/}listarTributacaoMunicipioResponse
class ListarTributacaoMunicipioResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}listarSimplesNacional
#   arg0 - SimplesNacionalFilter
class ListarSimplesNacional
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}simplesNacionalFilter
#   cidadeUuid - SOAP::SOAPString
class SimplesNacionalFilter
  attr_accessor :cidadeUuid

  def initialize(cidadeUuid = nil)
    @cidadeUuid = cidadeUuid
  end
end

# {http://ws.api.smartbill.com.br/}listarSimplesNacionalResponse
class ListarSimplesNacionalResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}adicionarPeriodicidade
#   arg0 - Periodicidade
class AdicionarPeriodicidade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}adicionarPeriodicidadeResponse
class AdicionarPeriodicidadeResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteServicoConsumo
#   arg0 - ClienteServicoConsumo
#   arg1 - SOAP::SOAPString
class AdicionarClienteServicoConsumo
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteServicoConsumoResponse
class AdicionarClienteServicoConsumoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterIndiceEconomicoValor
#   arg0 - SOAP::SOAPString
class ObterIndiceEconomicoValor
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterIndiceEconomicoValorResponse
#   m_return - IndiceEconomicoValor
class ObterIndiceEconomicoValorResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}listarArquivo
#   arg0 - ArquivoFilter
class ListarArquivo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}listarArquivoResponse
class ListarArquivoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}fecharClienteServicoConsumo
#   arg0 - SOAP::SOAPString
class FecharClienteServicoConsumo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}fecharClienteServicoConsumoResponse
class FecharClienteServicoConsumoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterRamoAtividade
#   arg0 - SOAP::SOAPString
class ObterRamoAtividade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterRamoAtividadeResponse
#   m_return - RamoAtividade
class ObterRamoAtividadeResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}removerPeriodicidade
#   arg0 - Periodicidade
class RemoverPeriodicidade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerPeriodicidadeResponse
class RemoverPeriodicidadeResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarClienteCR
#   arg0 - ClienteCRFilter
class ListarClienteCR
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}listarClienteCRResponse
class ListarClienteCRResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}listarCidade
#   arg0 - CidadeFilter
class ListarCidade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}cidadeFilter
#   disponivelParaCadastro - SOAP::SOAPBoolean
#   nome - SOAP::SOAPString
#   uf - Uf
class CidadeFilter
  attr_accessor :disponivelParaCadastro
  attr_accessor :nome
  attr_accessor :uf

  def initialize(disponivelParaCadastro = nil, nome = nil, uf = nil)
    @disponivelParaCadastro = disponivelParaCadastro
    @nome = nome
    @uf = uf
  end
end

# {http://ws.api.smartbill.com.br/}listarCidadeResponse
class ListarCidadeResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}removerEndereco
#   arg0 - Endereco
#   arg1 - SOAP::SOAPString
class RemoverEndereco
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}removerEnderecoResponse
class RemoverEnderecoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}autorizarClienteCartao
#   arg0 - ClienteCartao
#   arg1 - SOAP::SOAPString
class AutorizarClienteCartao
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}autorizarClienteCartaoResponse
class AutorizarClienteCartaoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteServico
#   arg0 - ClienteServico
#   arg1 - SOAP::SOAPString
class AdicionarClienteServico
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}adicionarClienteServicoResponse
class AdicionarClienteServicoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarNaturezaOperacao
#   arg0 - NaturezaOperacaoFilter
class ListarNaturezaOperacao
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}naturezaOperacaoFilter
#   cidadeUuid - SOAP::SOAPString
class NaturezaOperacaoFilter
  attr_accessor :cidadeUuid

  def initialize(cidadeUuid = nil)
    @cidadeUuid = cidadeUuid
  end
end

# {http://ws.api.smartbill.com.br/}listarNaturezaOperacaoResponse
class ListarNaturezaOperacaoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}obterTemplateEmailPorTipo
#   arg0 - TipoTemplateEmail
class ObterTemplateEmailPorTipo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterTemplateEmailPorTipoResponse
#   m_return - TemplateEmail
class ObterTemplateEmailPorTipoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}listarClienteServicoCR
#   arg0 - ClienteServicoCRFilter
class ListarClienteServicoCR
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}clienteServicoCRFilter
#   uuidCliente - SOAP::SOAPString
#   uuidClienteCR - SOAP::SOAPString
#   uuidClienteServico - SOAP::SOAPString
#   uuidClienteServicoConsumo - SOAP::SOAPString
class ClienteServicoCRFilter
  attr_accessor :uuidCliente
  attr_accessor :uuidClienteCR
  attr_accessor :uuidClienteServico
  attr_accessor :uuidClienteServicoConsumo

  def initialize(uuidCliente = nil, uuidClienteCR = nil, uuidClienteServico = nil, uuidClienteServicoConsumo = nil)
    @uuidCliente = uuidCliente
    @uuidClienteCR = uuidClienteCR
    @uuidClienteServico = uuidClienteServico
    @uuidClienteServicoConsumo = uuidClienteServicoConsumo
  end
end

# {http://ws.api.smartbill.com.br/}listarClienteServicoCRResponse
class ListarClienteServicoCRResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}removerCicloFaturamento
#   arg0 - CicloFaturamento
class RemoverCicloFaturamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerCicloFaturamentoResponse
class RemoverCicloFaturamentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteServicoConsumo
#   arg0 - SOAP::SOAPString
class ObterClienteServicoConsumo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteServicoConsumoResponse
#   m_return - ClienteServicoConsumo
class ObterClienteServicoConsumoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteServicoCR
#   arg0 - ClienteServicoCR
class AlterarClienteServicoCR
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteServicoCRResponse
class AlterarClienteServicoCRResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarMotivoCancelamento
class ListarMotivoCancelamento
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarMotivoCancelamentoResponse
class ListarMotivoCancelamentoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}adicionarMotivoCancelamento
#   arg0 - MotivoCancelamento
class AdicionarMotivoCancelamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}adicionarMotivoCancelamentoResponse
class AdicionarMotivoCancelamentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterMotivoCancelamento
#   arg0 - SOAP::SOAPString
class ObterMotivoCancelamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterMotivoCancelamentoResponse
#   m_return - MotivoCancelamento
class ObterMotivoCancelamentoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}adicionarCicloFaturamento
#   arg0 - CicloFaturamento
class AdicionarCicloFaturamento
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}adicionarCicloFaturamentoResponse
class AdicionarCicloFaturamentoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}adicionarContato
#   arg0 - Contato
#   arg1 - SOAP::SOAPString
class AdicionarContato
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}adicionarContatoResponse
class AdicionarContatoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterCliente
#   arg0 - SOAP::SOAPString
class ObterCliente
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteResponse
#   m_return - Cliente
class ObterClienteResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}cancelarClienteCR
#   arg0 - SOAP::SOAPString
#   arg1 - SOAP::SOAPString
class CancelarClienteCR
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}cancelarClienteCRResponse
class CancelarClienteCRResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteServicoPorClienteServicoUuid
#   arg0 - SOAP::SOAPString
class AlterarClienteServicoPorClienteServicoUuid
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}alterarClienteServicoPorClienteServicoUuidResponse
class AlterarClienteServicoPorClienteServicoUuidResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarRegimeTributario
#   arg0 - RegimeTributarioFilter
class ListarRegimeTributario
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}regimeTributarioFilter
#   cidadeUuid - SOAP::SOAPString
class RegimeTributarioFilter
  attr_accessor :cidadeUuid

  def initialize(cidadeUuid = nil)
    @cidadeUuid = cidadeUuid
  end
end

# {http://ws.api.smartbill.com.br/}listarRegimeTributarioResponse
class ListarRegimeTributarioResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}obterClienteCRTransacaoBoleto
#   arg0 - SOAP::SOAPString
class ObterClienteCRTransacaoBoleto
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterClienteCRTransacaoBoletoResponse
#   m_return - ClienteCRTransacaoBoleto
class ObterClienteCRTransacaoBoletoResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteCartao
#   arg0 - ClienteCartao
#   arg1 - SOAP::SOAPString
class RemoverClienteCartao
  attr_accessor :arg0
  attr_accessor :arg1

  def initialize(arg0 = nil, arg1 = nil)
    @arg0 = arg0
    @arg1 = arg1
  end
end

# {http://ws.api.smartbill.com.br/}removerClienteCartaoResponse
class RemoverClienteCartaoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarCliente
#   arg0 - ClienteFilter
class ListarCliente
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}clienteFilter
#   cpfCnpj - SOAP::SOAPString
#   generalFilter - GeneralFilter
class ClienteFilter
  attr_accessor :cpfCnpj
  attr_accessor :generalFilter

  def initialize(cpfCnpj = nil, generalFilter = nil)
    @cpfCnpj = cpfCnpj
    @generalFilter = generalFilter
  end
end

# {http://ws.api.smartbill.com.br/}listarClienteResponse
class ListarClienteResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}removerArquivo
#   arg0 - Arquivo
class RemoverArquivo
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}removerArquivoResponse
class RemoverArquivoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterCidade
#   arg0 - SOAP::SOAPString
class ObterCidade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterCidadeResponse
#   m_return - Cidade
class ObterCidadeResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}clonarServico
#   arg0 - SOAP::SOAPString
class ClonarServico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}clonarServicoResponse
class ClonarServicoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}obterPeriodicidade
#   arg0 - SOAP::SOAPString
class ObterPeriodicidade
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}obterPeriodicidadeResponse
#   m_return - Periodicidade
class ObterPeriodicidadeResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://ws.api.smartbill.com.br/}listarServico
#   arg0 - ServicoFilter
class ListarServico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}listarServicoResponse
class ListarServicoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}listarMotivoSuspensao
class ListarMotivoSuspensao
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}listarMotivoSuspensaoResponse
class ListarMotivoSuspensaoResponse < ::Array
end

# {http://ws.api.smartbill.com.br/}adicionarServico
#   arg0 - Servico
class AdicionarServico
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}adicionarServicoResponse
class AdicionarServicoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}consultarNFsEmissao
#   arg0 - SOAP::SOAPString
class ConsultarNFsEmissao
  attr_accessor :arg0

  def initialize(arg0 = nil)
    @arg0 = arg0
  end
end

# {http://ws.api.smartbill.com.br/}consultarNFsEmissaoResponse
class ConsultarNFsEmissaoResponse
  def initialize
  end
end

# {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#   mensagem - SOAP::SOAPString
#   classe - SOAP::SOAPString
#   metodo - SOAP::SOAPString
#   codigo - Status
#   objetoRetorno - SmartbillObjetoRetorno
#   objetosRetorno - SmartbillObjetoRetorno
#   smartbillStackTrace - SOAP::SOAPString
class SmartbillAPIMessage < ::StandardError
  attr_accessor :mensagem
  attr_accessor :classe
  attr_accessor :metodo
  attr_accessor :codigo
  attr_accessor :objetoRetorno
  attr_accessor :objetosRetorno
  attr_accessor :smartbillStackTrace

  def initialize(mensagem = nil, classe = nil, metodo = nil, codigo = nil, objetoRetorno = nil, objetosRetorno = [], smartbillStackTrace = nil)
    @mensagem = mensagem
    @classe = classe
    @metodo = metodo
    @codigo = codigo
    @objetoRetorno = objetoRetorno
    @objetosRetorno = objetosRetorno
    @smartbillStackTrace = smartbillStackTrace
  end
end

# {http://ws.api.smartbill.com.br/}status
class Status < ::String
  ACCEPTED = new("ACCEPTED")
  BAD_REQUEST = new("BAD_REQUEST")
  CONFLICT = new("CONFLICT")
  CREATED = new("CREATED")
  FORBIDDEN = new("FORBIDDEN")
  GONE = new("GONE")
  INTERNAL_SERVER_ERROR = new("INTERNAL_SERVER_ERROR")
  MOVED_PERMANENTLY = new("MOVED_PERMANENTLY")
  NOT_ACCEPTABLE = new("NOT_ACCEPTABLE")
  NOT_FOUND = new("NOT_FOUND")
  NOT_MODIFIED = new("NOT_MODIFIED")
  NO_CONTENT = new("NO_CONTENT")
  OK = new("OK")
  PRECONDITION_FAILED = new("PRECONDITION_FAILED")
  SEE_OTHER = new("SEE_OTHER")
  SERVICE_UNAVAILABLE = new("SERVICE_UNAVAILABLE")
  TEMPORARY_REDIRECT = new("TEMPORARY_REDIRECT")
  UNAUTHORIZED = new("UNAUTHORIZED")
  UNSUPPORTED_MEDIA_TYPE = new("UNSUPPORTED_MEDIA_TYPE")
end

# {http://ws.api.smartbill.com.br/}tipoEndereco
class TipoEndereco < ::String
  COBRANCA = new("COBRANCA")
  COMERCIAL = new("COMERCIAL")
  RESIDENCIAL = new("RESIDENCIAL")
end

# {http://ws.api.smartbill.com.br/}tipoConsumo
class TipoConsumo < ::String
  DIA = new("DIA")
  HORA = new("HORA")
  MES = new("MES")
  MINUTO = new("MINUTO")
  SEGUNDO = new("SEGUNDO")
end

# {http://ws.api.smartbill.com.br/}tipoConsumoEstado
class TipoConsumoEstado < ::String
  EM_EDICAO = new("EM_EDICAO")
  FECHADO = new("FECHADO")
end

# {http://ws.api.smartbill.com.br/}unidadeMedida
class UnidadeMedida < ::String
  CLIENTE = new("CLIENTE")
  FATURAMENTO = new("FATURAMENTO")
  LICENCA = new("LICENCA")
  MB = new("MB")
  MOEDA = new("MOEDA")
  PAGEVIEW = new("PAGEVIEW")
  TEMPO = new("TEMPO")
  UNIDADE = new("UNIDADE")
  USUARIO = new("USUARIO")
end

# {http://ws.api.smartbill.com.br/}unidadeMetrica
class UnidadeMetrica < ::String
  PRECO_POR_ETAPA = new("PRECO_POR_ETAPA")
  PRECO_POR_VOLUME = new("PRECO_POR_VOLUME")
  PRECO_POR_VOLUME_ACUMULATIVO = new("PRECO_POR_VOLUME_ACUMULATIVO")
  VALOR_FIXO = new("VALOR_FIXO")
  VALOR_INFORMADO = new("VALOR_INFORMADO")
end

# {http://ws.api.smartbill.com.br/}tipoValor
class TipoValor < ::String
  MOEDA = new("MOEDA")
  PORCENTAGEM = new("PORCENTAGEM")
end

# {http://ws.api.smartbill.com.br/}tipoContato
class TipoContato < ::String
  COBRANCA = new("COBRANCA")
  COMPRAS = new("COMPRAS")
  CONTABIL = new("CONTABIL")
  FINANCEIRO = new("FINANCEIRO")
  JURIDICO = new("JURIDICO")
  OUTROS = new("OUTROS")
  RESPONSAVEL_LEGAL = new("RESPONSAVEL_LEGAL")
  RH = new("RH")
  TECNICO = new("TECNICO")
end

# {http://ws.api.smartbill.com.br/}tipoIndiceEconomico
class TipoIndiceEconomico < ::String
  IGP = new("IGP")
  IGPM = new("IGPM")
  IPCA = new("IPCA")
  OUTROS = new("OUTROS")
end

# {http://ws.api.smartbill.com.br/}tipoParcela
class TipoParcela < ::String
  ATIVACAO = new("ATIVACAO")
  SERVICO = new("SERVICO")
end

# {http://ws.api.smartbill.com.br/}tipoPeriodicidade
class TipoPeriodicidade < ::String
  ANUAL = new("ANUAL")
  BIMESTRAL = new("BIMESTRAL")
  MENSAL = new("MENSAL")
  PONTUAL = new("PONTUAL")
  SEMESTRAL = new("SEMESTRAL")
  TRIMESTRAL = new("TRIMESTRAL")
end

# {http://ws.api.smartbill.com.br/}tipoServico
class TipoServico < ::String
  ADICIONAL = new("ADICIONAL")
  PRINCIPAL = new("PRINCIPAL")
end

# {http://ws.api.smartbill.com.br/}servicoEstado
class ServicoEstado < ::String
  ATIVO = new("ATIVO")
  CANCELADO = new("CANCELADO")
  EM_EDICAO = new("EM_EDICAO")
  SUSPENSO = new("SUSPENSO")
  TRIAL = new("TRIAL")
end

# {http://ws.api.smartbill.com.br/}tipoFaturamento
class TipoFaturamento < ::String
  POS_PAGO = new("POS_PAGO")
  PRE_PAGO = new("PRE_PAGO")
end

# {http://ws.api.smartbill.com.br/}tipoVigencia
class TipoVigencia < ::String
  ANO = new("ANO")
  BIMESTRE = new("BIMESTRE")
  MES = new("MES")
  SEMESTRE = new("SEMESTRE")
  TRIMESTRE = new("TRIMESTRE")
end

# {http://ws.api.smartbill.com.br/}projecaoFaturamento
class ProjecaoFaturamento < ::String
  PREVISTO = new("PREVISTO")
  REAL = new("REAL")
end

# {http://ws.api.smartbill.com.br/}tipoCREstado
class TipoCREstado < ::String
  ATIVO = new("ATIVO")
  BAIXADO = new("BAIXADO")
  CANCELADO = new("CANCELADO")
  EM_EDICAO = new("EM_EDICAO")
  PROCESSANDO = new("PROCESSANDO")
  SUSPENSO = new("SUSPENSO")
end

# {http://ws.api.smartbill.com.br/}tipoAmbiente
class TipoAmbiente < ::String
  HOMOLOGACAO = new("HOMOLOGACAO")
  PRODUCAO = new("PRODUCAO")
end

# {http://ws.api.smartbill.com.br/}tipoNFCancelamento
class TipoNFCancelamento < ::String
  DUPLICIDADE_NA_NOTA = new("DUPLICIDADE_NA_NOTA")
  ERRO_NA_EMISSAO = new("ERRO_NA_EMISSAO")
  OUTROS = new("OUTROS")
  SERVICO_NAO_PRESTADO = new("SERVICO_NAO_PRESTADO")
end

# {http://ws.api.smartbill.com.br/}tipoNFEstado
class TipoNFEstado < ::String
  APROVADA = new("APROVADA")
  CANCELADA = new("CANCELADA")
  DUPLICIDADE = new("DUPLICIDADE")
  ENVIADA = new("ENVIADA")
  ERRO_GERAL = new("ERRO_GERAL")
  GERADA = new("GERADA")
  MANUAL = new("MANUAL")
  NAO_EMITIDA = new("NAO_EMITIDA")
  REJEITADA = new("REJEITADA")
  SOLICITADA = new("SOLICITADA")
end

# {http://ws.api.smartbill.com.br/}tipoRelacionamento
class TipoRelacionamento < ::String
  ANOTACAO = new("ANOTACAO")
  AUMENTO_DE_PLANO = new("AUMENTO_DE_PLANO")
  BOLETO = new("BOLETO")
  CANCELAMENTO_DE_CONTRATO = new("CANCELAMENTO_DE_CONTRATO")
  CARTAO_CREDITO = new("CARTAO_CREDITO")
  COBRANCA = new("COBRANCA")
  NOTA_FISCAL = new("NOTA_FISCAL")
  REAJUSTE_DE_CONTRATO = new("REAJUSTE_DE_CONTRATO")
  RENOVACAO_DE_CONTRATO = new("RENOVACAO_DE_CONTRATO")
  VENDA_DE_NOVOS_MODULOS_ADDONS = new("VENDA_DE_NOVOS_MODULOS_ADDONS")
end

# {http://ws.api.smartbill.com.br/}criticidade
class Criticidade < ::String
  ALTA = new("ALTA")
  BAIXA = new("BAIXA")
  NAO_CRITICO = new("NAO_CRITICO")
end

# {http://ws.api.smartbill.com.br/}tipoPessoa
class TipoPessoa < ::String
  FISICA = new("FISICA")
  INTERNACIONAL = new("INTERNACIONAL")
  JURIDICA = new("JURIDICA")
end

# {http://ws.api.smartbill.com.br/}tipoLancamento
class TipoLancamento < ::String
  C = new("C")
  D = new("D")
end

# {http://ws.api.smartbill.com.br/}tipoMeioPagamento
class TipoMeioPagamento < ::String
  BOLETO = new("BOLETO")
  CARTAO_CREDITO = new("CARTAO_CREDITO")
  CHEQUE = new("CHEQUE")
  DEPOSITO = new("DEPOSITO")
  DINHEIRO = new("DINHEIRO")
end

# {http://ws.api.smartbill.com.br/}tipoTemplateEmail
class TipoTemplateEmail < ::String
  AGRADECIMENTO_DE_PAGAMENTO_BOLETO = new("AGRADECIMENTO_DE_PAGAMENTO_BOLETO")
  AGRADECIMENTO_DE_PAGAMENTO_CARTAO_DE_CREDITO = new("AGRADECIMENTO_DE_PAGAMENTO_CARTAO_DE_CREDITO")
  AVISO_CANCELAMENTO_DE_CONTRATO = new("AVISO_CANCELAMENTO_DE_CONTRATO")
  AVISO_DE_CANCELAMENTO_DE_CONTRATO = new("AVISO_DE_CANCELAMENTO_DE_CONTRATO")
  AVISO_DE_COBRANCA_EM_ATRASO = new("AVISO_DE_COBRANCA_EM_ATRASO")
  AVISO_DE_COBRANCA_EM_ATRASO_SUSPENSAO_EM_X_DIAS = new("AVISO_DE_COBRANCA_EM_ATRASO_SUSPENSAO_EM_X_DIAS")
  AVISO_PAGAMENTO_DE_CR_A_MAIOR = new("AVISO_PAGAMENTO_DE_CR_A_MAIOR")
  AVISO_PAGAMENTO_DE_CR_A_MENOR = new("AVISO_PAGAMENTO_DE_CR_A_MENOR")
  AVISO_SUSPENSAO_DE_CONTRATO = new("AVISO_SUSPENSAO_DE_CONTRATO")
  CONFIRMACAO_DE_RECEBIMENTO_DO_BOLETO = new("CONFIRMACAO_DE_RECEBIMENTO_DO_BOLETO")
  ENVIO_DA_COBRANCA_BOLETO = new("ENVIO_DA_COBRANCA_BOLETO")
  ENVIO_DA_COBRANCA_CARTAO_DE_CREDITO = new("ENVIO_DA_COBRANCA_CARTAO_DE_CREDITO")
  ENVIO_DA_COBRANCA_DEPOSITO = new("ENVIO_DA_COBRANCA_DEPOSITO")
  ENVIO_DE_RELATORIO = new("ENVIO_DE_RELATORIO")
  NOVO_CLIENTE_ATIVADO = new("NOVO_CLIENTE_ATIVADO")
  NOVO_USUARIO = new("NOVO_USUARIO")
  RESET_DE_SENHA = new("RESET_DE_SENHA")
  SOLICITACAO_DE_CARTAO = new("SOLICITACAO_DE_CARTAO")
end

# {http://ws.api.smartbill.com.br/}bloqueioFaturamento
class BloqueioFaturamento < ::String
  DOIS_CR = new("DOIS_CR")
  DOZE_CR = new("DOZE_CR")
  SEIS_CR = new("SEIS_CR")
  TRES_CR = new("TRES_CR")
  UM_CR = new("UM_CR")
end

# {http://ws.api.smartbill.com.br/}tipoBandeira
class TipoBandeira < ::String
  AMEX = new("AMEX")
  DINERS = new("DINERS")
  ELO = new("ELO")
  HIPERCARD = new("HIPERCARD")
  MASTERCARD = new("MASTERCARD")
  VISA = new("VISA")
end

# {http://ws.api.smartbill.com.br/}documentoFiscal
class DocumentoFiscal < ::String
  INVOICE = new("INVOICE")
  NAO_SE_APLICA = new("NAO_SE_APLICA")
  NFE = new("NFE")
  RECIBO = new("RECIBO")
end

# {http://ws.api.smartbill.com.br/}tipoBanco
class TipoBanco < ::String
  BANCO_DO_BRASIL = new("BANCO_DO_BRASIL")
  BRADESCO = new("BRADESCO")
  HSBC = new("HSBC")
  ITAU = new("ITAU")
  SANTANDER = new("SANTANDER")
end

# {http://ws.api.smartbill.com.br/}tipoStatusEmailCobranca
class TipoStatusEmailCobranca < ::String
  APROVADO = new("APROVADO")
  ETAPA_DOIS = new("ETAPA_DOIS")
  ETAPA_UM = new("ETAPA_UM")
  NAO_CONFIGURADO = new("NAO_CONFIGURADO")
end

# {http://ws.api.smartbill.com.br/}tipoStatusGateway
class TipoStatusGateway < ::String
  ABERTO = new("ABERTO")
  CANCELADO = new("CANCELADO")
  ERRO_GERAL = new("ERRO_GERAL")
  FECHADO = new("FECHADO")
  PAGO = new("PAGO")
  PAGO_A_MAIS = new("PAGO_A_MAIS")
  PAGO_A_MENOS = new("PAGO_A_MENOS")
  PENDENTE = new("PENDENTE")
end

# {http://ws.api.smartbill.com.br/}tipoClienteCRTransacao
class TipoClienteCRTransacao < ::String
  BOLETO = new("BOLETO")
  CARTAO = new("CARTAO")
  DEPOSITO = new("DEPOSITO")
end
