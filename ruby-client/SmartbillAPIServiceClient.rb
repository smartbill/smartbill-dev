#!/usr/bin/env ruby
require './SmartbillAPIServiceDriver.rb'

endpoint_url = ARGV.shift
obj = SmartbillAPI.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   listarEndereco(parameters)
#
# ARGS
#   parameters      ListarEndereco - {http://ws.api.smartbill.com.br/}listarEndereco
#
# RETURNS
#   parameters      ListarEnderecoResponse - {http://ws.api.smartbill.com.br/}listarEnderecoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarEndereco(parameters)

# SYNOPSIS
#   listarTotalClienteNF(parameters)
#
# ARGS
#   parameters      ListarTotalClienteNF - {http://ws.api.smartbill.com.br/}listarTotalClienteNF
#
# RETURNS
#   parameters      ListarTotalClienteNFResponse - {http://ws.api.smartbill.com.br/}listarTotalClienteNFResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarTotalClienteNF(parameters)

# SYNOPSIS
#   reabrirClienteServicoConsumo(parameters)
#
# ARGS
#   parameters      ReabrirClienteServicoConsumo - {http://ws.api.smartbill.com.br/}reabrirClienteServicoConsumo
#
# RETURNS
#   parameters      ReabrirClienteServicoConsumoResponse - {http://ws.api.smartbill.com.br/}reabrirClienteServicoConsumoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.reabrirClienteServicoConsumo(parameters)

# SYNOPSIS
#   clonarClienteServico(parameters)
#
# ARGS
#   parameters      ClonarClienteServico - {http://ws.api.smartbill.com.br/}clonarClienteServico
#
# RETURNS
#   parameters      ClonarClienteServicoResponse - {http://ws.api.smartbill.com.br/}clonarClienteServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.clonarClienteServico(parameters)

# SYNOPSIS
#   listarIndiceEconomicoValor(parameters)
#
# ARGS
#   parameters      ListarIndiceEconomicoValor - {http://ws.api.smartbill.com.br/}listarIndiceEconomicoValor
#
# RETURNS
#   parameters      ListarIndiceEconomicoValorResponse - {http://ws.api.smartbill.com.br/}listarIndiceEconomicoValorResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarIndiceEconomicoValor(parameters)

# SYNOPSIS
#   listarClienteServicoConsumo(parameters)
#
# ARGS
#   parameters      ListarClienteServicoConsumo - {http://ws.api.smartbill.com.br/}listarClienteServicoConsumo
#
# RETURNS
#   parameters      ListarClienteServicoConsumoResponse - {http://ws.api.smartbill.com.br/}listarClienteServicoConsumoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarClienteServicoConsumo(parameters)

# SYNOPSIS
#   listarClienteNF(parameters)
#
# ARGS
#   parameters      ListarClienteNF - {http://ws.api.smartbill.com.br/}listarClienteNF
#
# RETURNS
#   parameters      ListarClienteNFResponse - {http://ws.api.smartbill.com.br/}listarClienteNFResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarClienteNF(parameters)

# SYNOPSIS
#   ativarClienteServicoPorClienteServicoUuid(parameters)
#
# ARGS
#   parameters      AtivarClienteServicoPorClienteServicoUuid - {http://ws.api.smartbill.com.br/}ativarClienteServicoPorClienteServicoUuid
#
# RETURNS
#   parameters      AtivarClienteServicoPorClienteServicoUuidResponse - {http://ws.api.smartbill.com.br/}ativarClienteServicoPorClienteServicoUuidResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.ativarClienteServicoPorClienteServicoUuid(parameters)

# SYNOPSIS
#   alterarMotivoSuspensao(parameters)
#
# ARGS
#   parameters      AlterarMotivoSuspensao - {http://ws.api.smartbill.com.br/}alterarMotivoSuspensao
#
# RETURNS
#   parameters      AlterarMotivoSuspensaoResponse - {http://ws.api.smartbill.com.br/}alterarMotivoSuspensaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarMotivoSuspensao(parameters)

# SYNOPSIS
#   alterarFamilia(parameters)
#
# ARGS
#   parameters      AlterarFamilia - {http://ws.api.smartbill.com.br/}alterarFamilia
#
# RETURNS
#   parameters      AlterarFamiliaResponse - {http://ws.api.smartbill.com.br/}alterarFamiliaResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarFamilia(parameters)

# SYNOPSIS
#   listarTotalServico(parameters)
#
# ARGS
#   parameters      ListarTotalServico - {http://ws.api.smartbill.com.br/}listarTotalServico
#
# RETURNS
#   parameters      ListarTotalServicoResponse - {http://ws.api.smartbill.com.br/}listarTotalServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarTotalServico(parameters)

# SYNOPSIS
#   obterServico(parameters)
#
# ARGS
#   parameters      ObterServico - {http://ws.api.smartbill.com.br/}obterServico
#
# RETURNS
#   parameters      ObterServicoResponse - {http://ws.api.smartbill.com.br/}obterServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterServico(parameters)

# SYNOPSIS
#   listarRelacionamento(parameters)
#
# ARGS
#   parameters      ListarRelacionamento - {http://ws.api.smartbill.com.br/}listarRelacionamento
#
# RETURNS
#   parameters      ListarRelacionamentoResponse - {http://ws.api.smartbill.com.br/}listarRelacionamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarRelacionamento(parameters)

# SYNOPSIS
#   obterRelacionamento(parameters)
#
# ARGS
#   parameters      ObterRelacionamento - {http://ws.api.smartbill.com.br/}obterRelacionamento
#
# RETURNS
#   parameters      ObterRelacionamentoResponse - {http://ws.api.smartbill.com.br/}obterRelacionamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterRelacionamento(parameters)

# SYNOPSIS
#   alterarSegmento(parameters)
#
# ARGS
#   parameters      AlterarSegmento - {http://ws.api.smartbill.com.br/}alterarSegmento
#
# RETURNS
#   parameters      AlterarSegmentoResponse - {http://ws.api.smartbill.com.br/}alterarSegmentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarSegmento(parameters)

# SYNOPSIS
#   alterarRelacionamento(parameters)
#
# ARGS
#   parameters      AlterarRelacionamento - {http://ws.api.smartbill.com.br/}alterarRelacionamento
#
# RETURNS
#   parameters      AlterarRelacionamentoResponse - {http://ws.api.smartbill.com.br/}alterarRelacionamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarRelacionamento(parameters)

# SYNOPSIS
#   listarRegra(parameters)
#
# ARGS
#   parameters      ListarRegra - {http://ws.api.smartbill.com.br/}listarRegra
#
# RETURNS
#   parameters      ListarRegraResponse - {http://ws.api.smartbill.com.br/}listarRegraResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarRegra(parameters)

# SYNOPSIS
#   alterarContato(parameters)
#
# ARGS
#   parameters      AlterarContato - {http://ws.api.smartbill.com.br/}alterarContato
#
# RETURNS
#   parameters      AlterarContatoResponse - {http://ws.api.smartbill.com.br/}alterarContatoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarContato(parameters)

# SYNOPSIS
#   gerarNFsEmissao(parameters)
#
# ARGS
#   parameters      GerarNFsEmissao - {http://ws.api.smartbill.com.br/}gerarNFsEmissao
#
# RETURNS
#   parameters      GerarNFsEmissaoResponse - {http://ws.api.smartbill.com.br/}gerarNFsEmissaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.gerarNFsEmissao(parameters)

# SYNOPSIS
#   adicionarMotivoSuspensao(parameters)
#
# ARGS
#   parameters      AdicionarMotivoSuspensao - {http://ws.api.smartbill.com.br/}adicionarMotivoSuspensao
#
# RETURNS
#   parameters      AdicionarMotivoSuspensaoResponse - {http://ws.api.smartbill.com.br/}adicionarMotivoSuspensaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarMotivoSuspensao(parameters)

# SYNOPSIS
#   obterCodigoCNAE(parameters)
#
# ARGS
#   parameters      ObterCodigoCNAE - {http://ws.api.smartbill.com.br/}obterCodigoCNAE
#
# RETURNS
#   parameters      ObterCodigoCNAEResponse - {http://ws.api.smartbill.com.br/}obterCodigoCNAEResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterCodigoCNAE(parameters)

# SYNOPSIS
#   alterarClienteCRCadastro(parameters)
#
# ARGS
#   parameters      AlterarClienteCRCadastro - {http://ws.api.smartbill.com.br/}alterarClienteCRCadastro
#
# RETURNS
#   parameters      AlterarClienteCRCadastroResponse - {http://ws.api.smartbill.com.br/}alterarClienteCRCadastroResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarClienteCRCadastro(parameters)

# SYNOPSIS
#   listarRamoAtividade(parameters)
#
# ARGS
#   parameters      ListarRamoAtividade - {http://ws.api.smartbill.com.br/}listarRamoAtividade
#
# RETURNS
#   parameters      ListarRamoAtividadeResponse - {http://ws.api.smartbill.com.br/}listarRamoAtividadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarRamoAtividade(parameters)

# SYNOPSIS
#   baixarCobranca(parameters)
#
# ARGS
#   parameters      BaixarCobranca - {http://ws.api.smartbill.com.br/}baixarCobranca
#
# RETURNS
#   parameters      BaixarCobrancaResponse - {http://ws.api.smartbill.com.br/}baixarCobrancaResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.baixarCobranca(parameters)

# SYNOPSIS
#   listarTotalClienteCRTransacaoBoleto(parameters)
#
# ARGS
#   parameters      ListarTotalClienteCRTransacaoBoleto - {http://ws.api.smartbill.com.br/}listarTotalClienteCRTransacaoBoleto
#
# RETURNS
#   parameters      ListarTotalClienteCRTransacaoBoletoResponse - {http://ws.api.smartbill.com.br/}listarTotalClienteCRTransacaoBoletoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarTotalClienteCRTransacaoBoleto(parameters)

# SYNOPSIS
#   removerClienteServicoCR(parameters)
#
# ARGS
#   parameters      RemoverClienteServicoCR - {http://ws.api.smartbill.com.br/}removerClienteServicoCR
#
# RETURNS
#   parameters      RemoverClienteServicoCRResponse - {http://ws.api.smartbill.com.br/}removerClienteServicoCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerClienteServicoCR(parameters)

# SYNOPSIS
#   obterRegimeTributario(parameters)
#
# ARGS
#   parameters      ObterRegimeTributario - {http://ws.api.smartbill.com.br/}obterRegimeTributario
#
# RETURNS
#   parameters      ObterRegimeTributarioResponse - {http://ws.api.smartbill.com.br/}obterRegimeTributarioResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterRegimeTributario(parameters)

# SYNOPSIS
#   removerServico(parameters)
#
# ARGS
#   parameters      RemoverServico - {http://ws.api.smartbill.com.br/}removerServico
#
# RETURNS
#   parameters      RemoverServicoResponse - {http://ws.api.smartbill.com.br/}removerServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerServico(parameters)

# SYNOPSIS
#   consultarCobranca(parameters)
#
# ARGS
#   parameters      ConsultarCobranca - {http://ws.api.smartbill.com.br/}consultarCobranca
#
# RETURNS
#   parameters      ConsultarCobrancaResponse - {http://ws.api.smartbill.com.br/}consultarCobrancaResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.consultarCobranca(parameters)

# SYNOPSIS
#   listarTotalArquivo(parameters)
#
# ARGS
#   parameters      ListarTotalArquivo - {http://ws.api.smartbill.com.br/}listarTotalArquivo
#
# RETURNS
#   parameters      ListarTotalArquivoResponse - {http://ws.api.smartbill.com.br/}listarTotalArquivoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarTotalArquivo(parameters)

# SYNOPSIS
#   obterContato(parameters)
#
# ARGS
#   parameters      ObterContato - {http://ws.api.smartbill.com.br/}obterContato
#
# RETURNS
#   parameters      ObterContatoResponse - {http://ws.api.smartbill.com.br/}obterContatoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterContato(parameters)

# SYNOPSIS
#   obterClienteCRCadastro(parameters)
#
# ARGS
#   parameters      ObterClienteCRCadastro - {http://ws.api.smartbill.com.br/}obterClienteCRCadastro
#
# RETURNS
#   parameters      ObterClienteCRCadastroResponse - {http://ws.api.smartbill.com.br/}obterClienteCRCadastroResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterClienteCRCadastro(parameters)

# SYNOPSIS
#   removerClienteServico(parameters)
#
# ARGS
#   parameters      RemoverClienteServico - {http://ws.api.smartbill.com.br/}removerClienteServico
#
# RETURNS
#   parameters      RemoverClienteServicoResponse - {http://ws.api.smartbill.com.br/}removerClienteServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerClienteServico(parameters)

# SYNOPSIS
#   listarServicoAliquotaPrestador(parameters)
#
# ARGS
#   parameters      ListarServicoAliquotaPrestador - {http://ws.api.smartbill.com.br/}listarServicoAliquotaPrestador
#
# RETURNS
#   parameters      ListarServicoAliquotaPrestadorResponse - {http://ws.api.smartbill.com.br/}listarServicoAliquotaPrestadorResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarServicoAliquotaPrestador(parameters)

# SYNOPSIS
#   adicionarSegmento(parameters)
#
# ARGS
#   parameters      AdicionarSegmento - {http://ws.api.smartbill.com.br/}adicionarSegmento
#
# RETURNS
#   parameters      AdicionarSegmentoResponse - {http://ws.api.smartbill.com.br/}adicionarSegmentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarSegmento(parameters)

# SYNOPSIS
#   obterArquivo(parameters)
#
# ARGS
#   parameters      ObterArquivo - {http://ws.api.smartbill.com.br/}obterArquivo
#
# RETURNS
#   parameters      ObterArquivoResponse - {http://ws.api.smartbill.com.br/}obterArquivoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterArquivo(parameters)

# SYNOPSIS
#   obterFamilia(parameters)
#
# ARGS
#   parameters      ObterFamilia - {http://ws.api.smartbill.com.br/}obterFamilia
#
# RETURNS
#   parameters      ObterFamiliaResponse - {http://ws.api.smartbill.com.br/}obterFamiliaResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterFamilia(parameters)

# SYNOPSIS
#   obterTemplateEmail(parameters)
#
# ARGS
#   parameters      ObterTemplateEmail - {http://ws.api.smartbill.com.br/}obterTemplateEmail
#
# RETURNS
#   parameters      ObterTemplateEmailResponse - {http://ws.api.smartbill.com.br/}obterTemplateEmailResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterTemplateEmail(parameters)

# SYNOPSIS
#   cancelarNFsEmissao(parameters)
#
# ARGS
#   parameters      CancelarNFsEmissao - {http://ws.api.smartbill.com.br/}cancelarNFsEmissao
#
# RETURNS
#   parameters      CancelarNFsEmissaoResponse - {http://ws.api.smartbill.com.br/}cancelarNFsEmissaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.cancelarNFsEmissao(parameters)

# SYNOPSIS
#   adicionarRelacionamento(parameters)
#
# ARGS
#   parameters      AdicionarRelacionamento - {http://ws.api.smartbill.com.br/}adicionarRelacionamento
#
# RETURNS
#   parameters      AdicionarRelacionamentoResponse - {http://ws.api.smartbill.com.br/}adicionarRelacionamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarRelacionamento(parameters)

# SYNOPSIS
#   alterarCliente(parameters)
#
# ARGS
#   parameters      AlterarCliente - {http://ws.api.smartbill.com.br/}alterarCliente
#
# RETURNS
#   parameters      AlterarClienteResponse - {http://ws.api.smartbill.com.br/}alterarClienteResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarCliente(parameters)

# SYNOPSIS
#   alterarClienteServico(parameters)
#
# ARGS
#   parameters      AlterarClienteServico - {http://ws.api.smartbill.com.br/}alterarClienteServico
#
# RETURNS
#   parameters      AlterarClienteServicoResponse - {http://ws.api.smartbill.com.br/}alterarClienteServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarClienteServico(parameters)

# SYNOPSIS
#   listarCicloFaturamento(parameters)
#
# ARGS
#   parameters      ListarCicloFaturamento - {http://ws.api.smartbill.com.br/}listarCicloFaturamento
#
# RETURNS
#   parameters      ListarCicloFaturamentoResponse - {http://ws.api.smartbill.com.br/}listarCicloFaturamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarCicloFaturamento(parameters)

# SYNOPSIS
#   obterConfiguracao(parameters)
#
# ARGS
#   parameters      ObterConfiguracao - {http://ws.api.smartbill.com.br/}obterConfiguracao
#
# RETURNS
#   parameters      ObterConfiguracaoResponse - {http://ws.api.smartbill.com.br/}obterConfiguracaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterConfiguracao(parameters)

# SYNOPSIS
#   obterSimplesNacional(parameters)
#
# ARGS
#   parameters      ObterSimplesNacional - {http://ws.api.smartbill.com.br/}obterSimplesNacional
#
# RETURNS
#   parameters      ObterSimplesNacionalResponse - {http://ws.api.smartbill.com.br/}obterSimplesNacionalResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterSimplesNacional(parameters)

# SYNOPSIS
#   removerContato(parameters)
#
# ARGS
#   parameters      RemoverContato - {http://ws.api.smartbill.com.br/}removerContato
#
# RETURNS
#   parameters      RemoverContatoResponse - {http://ws.api.smartbill.com.br/}removerContatoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerContato(parameters)

# SYNOPSIS
#   removerRelacionamento(parameters)
#
# ARGS
#   parameters      RemoverRelacionamento - {http://ws.api.smartbill.com.br/}removerRelacionamento
#
# RETURNS
#   parameters      RemoverRelacionamentoResponse - {http://ws.api.smartbill.com.br/}removerRelacionamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerRelacionamento(parameters)

# SYNOPSIS
#   alterarClienteNF(parameters)
#
# ARGS
#   parameters      AlterarClienteNF - {http://ws.api.smartbill.com.br/}alterarClienteNF
#
# RETURNS
#   parameters      AlterarClienteNFResponse - {http://ws.api.smartbill.com.br/}alterarClienteNFResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarClienteNF(parameters)

# SYNOPSIS
#   alterarMotivoCancelamento(parameters)
#
# ARGS
#   parameters      AlterarMotivoCancelamento - {http://ws.api.smartbill.com.br/}alterarMotivoCancelamento
#
# RETURNS
#   parameters      AlterarMotivoCancelamentoResponse - {http://ws.api.smartbill.com.br/}alterarMotivoCancelamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarMotivoCancelamento(parameters)

# SYNOPSIS
#   alterarClienteCR(parameters)
#
# ARGS
#   parameters      AlterarClienteCR - {http://ws.api.smartbill.com.br/}alterarClienteCR
#
# RETURNS
#   parameters      AlterarClienteCRResponse - {http://ws.api.smartbill.com.br/}alterarClienteCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarClienteCR(parameters)

# SYNOPSIS
#   adicionarCliente(parameters)
#
# ARGS
#   parameters      AdicionarCliente - {http://ws.api.smartbill.com.br/}adicionarCliente
#
# RETURNS
#   parameters      AdicionarClienteResponse - {http://ws.api.smartbill.com.br/}adicionarClienteResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarCliente(parameters)

# SYNOPSIS
#   listarCodigoCNAE(parameters)
#
# ARGS
#   parameters      ListarCodigoCNAE - {http://ws.api.smartbill.com.br/}listarCodigoCNAE
#
# RETURNS
#   parameters      ListarCodigoCNAEResponse - {http://ws.api.smartbill.com.br/}listarCodigoCNAEResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarCodigoCNAE(parameters)

# SYNOPSIS
#   obterClienteServicoCR(parameters)
#
# ARGS
#   parameters      ObterClienteServicoCR - {http://ws.api.smartbill.com.br/}obterClienteServicoCR
#
# RETURNS
#   parameters      ObterClienteServicoCRResponse - {http://ws.api.smartbill.com.br/}obterClienteServicoCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterClienteServicoCR(parameters)

# SYNOPSIS
#   obterNaturezaOperacao(parameters)
#
# ARGS
#   parameters      ObterNaturezaOperacao - {http://ws.api.smartbill.com.br/}obterNaturezaOperacao
#
# RETURNS
#   parameters      ObterNaturezaOperacaoResponse - {http://ws.api.smartbill.com.br/}obterNaturezaOperacaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterNaturezaOperacao(parameters)

# SYNOPSIS
#   gerarCobranca(parameters)
#
# ARGS
#   parameters      GerarCobranca - {http://ws.api.smartbill.com.br/}gerarCobranca
#
# RETURNS
#   parameters      GerarCobrancaResponse - {http://ws.api.smartbill.com.br/}gerarCobrancaResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.gerarCobranca(parameters)

# SYNOPSIS
#   obterClienteNF(parameters)
#
# ARGS
#   parameters      ObterClienteNF - {http://ws.api.smartbill.com.br/}obterClienteNF
#
# RETURNS
#   parameters      ObterClienteNFResponse - {http://ws.api.smartbill.com.br/}obterClienteNFResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterClienteNF(parameters)

# SYNOPSIS
#   emailCobranca(parameters)
#
# ARGS
#   parameters      EmailCobranca - {http://ws.api.smartbill.com.br/}emailCobranca
#
# RETURNS
#   parameters      EmailCobrancaResponse - {http://ws.api.smartbill.com.br/}emailCobrancaResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.emailCobranca(parameters)

# SYNOPSIS
#   adicionarFamilia(parameters)
#
# ARGS
#   parameters      AdicionarFamilia - {http://ws.api.smartbill.com.br/}adicionarFamilia
#
# RETURNS
#   parameters      AdicionarFamiliaResponse - {http://ws.api.smartbill.com.br/}adicionarFamiliaResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarFamilia(parameters)

# SYNOPSIS
#   listarSegmento(parameters)
#
# ARGS
#   parameters      ListarSegmento - {http://ws.api.smartbill.com.br/}listarSegmento
#
# RETURNS
#   parameters      ListarSegmentoResponse - {http://ws.api.smartbill.com.br/}listarSegmentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarSegmento(parameters)

# SYNOPSIS
#   obterClienteServico(parameters)
#
# ARGS
#   parameters      ObterClienteServico - {http://ws.api.smartbill.com.br/}obterClienteServico
#
# RETURNS
#   parameters      ObterClienteServicoResponse - {http://ws.api.smartbill.com.br/}obterClienteServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterClienteServico(parameters)

# SYNOPSIS
#   listarMeioPagamento(parameters)
#
# ARGS
#   parameters      ListarMeioPagamento - {http://ws.api.smartbill.com.br/}listarMeioPagamento
#
# RETURNS
#   parameters      ListarMeioPagamentoResponse - {http://ws.api.smartbill.com.br/}listarMeioPagamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarMeioPagamento(parameters)

# SYNOPSIS
#   alterarRamoAtividade(parameters)
#
# ARGS
#   parameters      AlterarRamoAtividade - {http://ws.api.smartbill.com.br/}alterarRamoAtividade
#
# RETURNS
#   parameters      AlterarRamoAtividadeResponse - {http://ws.api.smartbill.com.br/}alterarRamoAtividadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarRamoAtividade(parameters)

# SYNOPSIS
#   obterClienteCR(parameters)
#
# ARGS
#   parameters      ObterClienteCR - {http://ws.api.smartbill.com.br/}obterClienteCR
#
# RETURNS
#   parameters      ObterClienteCRResponse - {http://ws.api.smartbill.com.br/}obterClienteCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterClienteCR(parameters)

# SYNOPSIS
#   removerClienteCR(parameters)
#
# ARGS
#   parameters      RemoverClienteCR - {http://ws.api.smartbill.com.br/}removerClienteCR
#
# RETURNS
#   parameters      RemoverClienteCRResponse - {http://ws.api.smartbill.com.br/}removerClienteCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerClienteCR(parameters)

# SYNOPSIS
#   obterUuidUsuario(parameters)
#
# ARGS
#   parameters      ObterUuidUsuario - {http://ws.api.smartbill.com.br/}obterUuidUsuario
#
# RETURNS
#   parameters      ObterUuidUsuarioResponse - {http://ws.api.smartbill.com.br/}obterUuidUsuarioResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterUuidUsuario(parameters)

# SYNOPSIS
#   obterMeioPagamento(parameters)
#
# ARGS
#   parameters      ObterMeioPagamento - {http://ws.api.smartbill.com.br/}obterMeioPagamento
#
# RETURNS
#   parameters      ObterMeioPagamentoResponse - {http://ws.api.smartbill.com.br/}obterMeioPagamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterMeioPagamento(parameters)

# SYNOPSIS
#   obterEndereco(parameters)
#
# ARGS
#   parameters      ObterEndereco - {http://ws.api.smartbill.com.br/}obterEndereco
#
# RETURNS
#   parameters      ObterEnderecoResponse - {http://ws.api.smartbill.com.br/}obterEnderecoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterEndereco(parameters)

# SYNOPSIS
#   solicitarClienteCartao(parameters)
#
# ARGS
#   parameters      SolicitarClienteCartao - {http://ws.api.smartbill.com.br/}solicitarClienteCartao
#
# RETURNS
#   parameters      SolicitarClienteCartaoResponse - {http://ws.api.smartbill.com.br/}solicitarClienteCartaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.solicitarClienteCartao(parameters)

# SYNOPSIS
#   cancelarClienteServicoPorClienteServicoUuid(parameters)
#
# ARGS
#   parameters      CancelarClienteServicoPorClienteServicoUuid - {http://ws.api.smartbill.com.br/}cancelarClienteServicoPorClienteServicoUuid
#
# RETURNS
#   parameters      CancelarClienteServicoPorClienteServicoUuidResponse - {http://ws.api.smartbill.com.br/}cancelarClienteServicoPorClienteServicoUuidResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.cancelarClienteServicoPorClienteServicoUuid(parameters)

# SYNOPSIS
#   alterarEndereco(parameters)
#
# ARGS
#   parameters      AlterarEndereco - {http://ws.api.smartbill.com.br/}alterarEndereco
#
# RETURNS
#   parameters      AlterarEnderecoResponse - {http://ws.api.smartbill.com.br/}alterarEnderecoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarEndereco(parameters)

# SYNOPSIS
#   regraClienteServicoImpostos(parameters)
#
# ARGS
#   parameters      RegraClienteServicoImpostos - {http://ws.api.smartbill.com.br/}regraClienteServicoImpostos
#
# RETURNS
#   parameters      RegraClienteServicoImpostosResponse - {http://ws.api.smartbill.com.br/}regraClienteServicoImpostosResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.regraClienteServicoImpostos(parameters)

# SYNOPSIS
#   listarTotalClienteCR(parameters)
#
# ARGS
#   parameters      ListarTotalClienteCR - {http://ws.api.smartbill.com.br/}listarTotalClienteCR
#
# RETURNS
#   parameters      ListarTotalClienteCRResponse - {http://ws.api.smartbill.com.br/}listarTotalClienteCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarTotalClienteCR(parameters)

# SYNOPSIS
#   obterGerente(parameters)
#
# ARGS
#   parameters      ObterGerente - {http://ws.api.smartbill.com.br/}obterGerente
#
# RETURNS
#   parameters      ObterGerenteResponse - {http://ws.api.smartbill.com.br/}obterGerenteResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterGerente(parameters)

# SYNOPSIS
#   suspenderClienteServicoPorClienteServicoUuid(parameters)
#
# ARGS
#   parameters      SuspenderClienteServicoPorClienteServicoUuid - {http://ws.api.smartbill.com.br/}suspenderClienteServicoPorClienteServicoUuid
#
# RETURNS
#   parameters      SuspenderClienteServicoPorClienteServicoUuidResponse - {http://ws.api.smartbill.com.br/}suspenderClienteServicoPorClienteServicoUuidResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.suspenderClienteServicoPorClienteServicoUuid(parameters)

# SYNOPSIS
#   adicionarEndereco(parameters)
#
# ARGS
#   parameters      AdicionarEndereco - {http://ws.api.smartbill.com.br/}adicionarEndereco
#
# RETURNS
#   parameters      AdicionarEnderecoResponse - {http://ws.api.smartbill.com.br/}adicionarEnderecoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarEndereco(parameters)

# SYNOPSIS
#   obterPadraoPreenchimento(parameters)
#
# ARGS
#   parameters      ObterPadraoPreenchimento - {http://ws.api.smartbill.com.br/}obterPadraoPreenchimento
#
# RETURNS
#   parameters      ObterPadraoPreenchimentoResponse - {http://ws.api.smartbill.com.br/}obterPadraoPreenchimentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterPadraoPreenchimento(parameters)

# SYNOPSIS
#   removerCliente(parameters)
#
# ARGS
#   parameters      RemoverCliente - {http://ws.api.smartbill.com.br/}removerCliente
#
# RETURNS
#   parameters      RemoverClienteResponse - {http://ws.api.smartbill.com.br/}removerClienteResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerCliente(parameters)

# SYNOPSIS
#   removerMotivoSuspensao(parameters)
#
# ARGS
#   parameters      RemoverMotivoSuspensao - {http://ws.api.smartbill.com.br/}removerMotivoSuspensao
#
# RETURNS
#   parameters      RemoverMotivoSuspensaoResponse - {http://ws.api.smartbill.com.br/}removerMotivoSuspensaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerMotivoSuspensao(parameters)

# SYNOPSIS
#   alterarArquivo(parameters)
#
# ARGS
#   parameters      AlterarArquivo - {http://ws.api.smartbill.com.br/}alterarArquivo
#
# RETURNS
#   parameters      AlterarArquivoResponse - {http://ws.api.smartbill.com.br/}alterarArquivoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarArquivo(parameters)

# SYNOPSIS
#   obterServicoAliquotaPrestador(parameters)
#
# ARGS
#   parameters      ObterServicoAliquotaPrestador - {http://ws.api.smartbill.com.br/}obterServicoAliquotaPrestador
#
# RETURNS
#   parameters      ObterServicoAliquotaPrestadorResponse - {http://ws.api.smartbill.com.br/}obterServicoAliquotaPrestadorResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterServicoAliquotaPrestador(parameters)

# SYNOPSIS
#   listarTemplateEmail(parameters)
#
# ARGS
#   parameters      ListarTemplateEmail - {http://ws.api.smartbill.com.br/}listarTemplateEmail
#
# RETURNS
#   parameters      ListarTemplateEmailResponse - {http://ws.api.smartbill.com.br/}listarTemplateEmailResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarTemplateEmail(parameters)

# SYNOPSIS
#   listarIndiceEconomico(parameters)
#
# ARGS
#   parameters      ListarIndiceEconomico - {http://ws.api.smartbill.com.br/}listarIndiceEconomico
#
# RETURNS
#   parameters      ListarIndiceEconomicoResponse - {http://ws.api.smartbill.com.br/}listarIndiceEconomicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarIndiceEconomico(parameters)

# SYNOPSIS
#   obterTributacaoMunicipio(parameters)
#
# ARGS
#   parameters      ObterTributacaoMunicipio - {http://ws.api.smartbill.com.br/}obterTributacaoMunicipio
#
# RETURNS
#   parameters      ObterTributacaoMunicipioResponse - {http://ws.api.smartbill.com.br/}obterTributacaoMunicipioResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterTributacaoMunicipio(parameters)

# SYNOPSIS
#   listarFamilia(parameters)
#
# ARGS
#   parameters      ListarFamilia - {http://ws.api.smartbill.com.br/}listarFamilia
#
# RETURNS
#   parameters      ListarFamiliaResponse - {http://ws.api.smartbill.com.br/}listarFamiliaResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarFamilia(parameters)

# SYNOPSIS
#   listarPeriodicidade(parameters)
#
# ARGS
#   parameters      ListarPeriodicidade - {http://ws.api.smartbill.com.br/}listarPeriodicidade
#
# RETURNS
#   parameters      ListarPeriodicidadeResponse - {http://ws.api.smartbill.com.br/}listarPeriodicidadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarPeriodicidade(parameters)

# SYNOPSIS
#   listarConfiguracao(parameters)
#
# ARGS
#   parameters      ListarConfiguracao - {http://ws.api.smartbill.com.br/}listarConfiguracao
#
# RETURNS
#   parameters      ListarConfiguracaoResponse - {http://ws.api.smartbill.com.br/}listarConfiguracaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarConfiguracao(parameters)

# SYNOPSIS
#   listarClienteServico(parameters)
#
# ARGS
#   parameters      ListarClienteServico - {http://ws.api.smartbill.com.br/}listarClienteServico
#
# RETURNS
#   parameters      ListarClienteServicoResponse - {http://ws.api.smartbill.com.br/}listarClienteServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarClienteServico(parameters)

# SYNOPSIS
#   adicionarRamoAtividade(parameters)
#
# ARGS
#   parameters      AdicionarRamoAtividade - {http://ws.api.smartbill.com.br/}adicionarRamoAtividade
#
# RETURNS
#   parameters      AdicionarRamoAtividadeResponse - {http://ws.api.smartbill.com.br/}adicionarRamoAtividadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarRamoAtividade(parameters)

# SYNOPSIS
#   listarUf(parameters)
#
# ARGS
#   parameters      ListarUf - {http://ws.api.smartbill.com.br/}listarUf
#
# RETURNS
#   parameters      ListarUfResponse - {http://ws.api.smartbill.com.br/}listarUfResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarUf(parameters)

# SYNOPSIS
#   removerRamoAtividade(parameters)
#
# ARGS
#   parameters      RemoverRamoAtividade - {http://ws.api.smartbill.com.br/}removerRamoAtividade
#
# RETURNS
#   parameters      RemoverRamoAtividadeResponse - {http://ws.api.smartbill.com.br/}removerRamoAtividadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerRamoAtividade(parameters)

# SYNOPSIS
#   alterarPeriodicidade(parameters)
#
# ARGS
#   parameters      AlterarPeriodicidade - {http://ws.api.smartbill.com.br/}alterarPeriodicidade
#
# RETURNS
#   parameters      AlterarPeriodicidadeResponse - {http://ws.api.smartbill.com.br/}alterarPeriodicidadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarPeriodicidade(parameters)

# SYNOPSIS
#   removerClienteNF(parameters)
#
# ARGS
#   parameters      RemoverClienteNF - {http://ws.api.smartbill.com.br/}removerClienteNF
#
# RETURNS
#   parameters      RemoverClienteNFResponse - {http://ws.api.smartbill.com.br/}removerClienteNFResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerClienteNF(parameters)

# SYNOPSIS
#   reativarCliente(parameters)
#
# ARGS
#   parameters      ReativarCliente - {http://ws.api.smartbill.com.br/}reativarCliente
#
# RETURNS
#   parameters      ReativarClienteResponse - {http://ws.api.smartbill.com.br/}reativarClienteResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.reativarCliente(parameters)

# SYNOPSIS
#   adicionarClienteServicoCR(parameters)
#
# ARGS
#   parameters      AdicionarClienteServicoCR - {http://ws.api.smartbill.com.br/}adicionarClienteServicoCR
#
# RETURNS
#   parameters      AdicionarClienteServicoCRResponse - {http://ws.api.smartbill.com.br/}adicionarClienteServicoCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarClienteServicoCR(parameters)

# SYNOPSIS
#   alterarServico(parameters)
#
# ARGS
#   parameters      AlterarServico - {http://ws.api.smartbill.com.br/}alterarServico
#
# RETURNS
#   parameters      AlterarServicoResponse - {http://ws.api.smartbill.com.br/}alterarServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarServico(parameters)

# SYNOPSIS
#   alterarClienteServicoConsumo(parameters)
#
# ARGS
#   parameters      AlterarClienteServicoConsumo - {http://ws.api.smartbill.com.br/}alterarClienteServicoConsumo
#
# RETURNS
#   parameters      AlterarClienteServicoConsumoResponse - {http://ws.api.smartbill.com.br/}alterarClienteServicoConsumoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarClienteServicoConsumo(parameters)

# SYNOPSIS
#   obterCicloFaturamento(parameters)
#
# ARGS
#   parameters      ObterCicloFaturamento - {http://ws.api.smartbill.com.br/}obterCicloFaturamento
#
# RETURNS
#   parameters      ObterCicloFaturamentoResponse - {http://ws.api.smartbill.com.br/}obterCicloFaturamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterCicloFaturamento(parameters)

# SYNOPSIS
#   listarTotalServicoAliquotaPrestador(parameters)
#
# ARGS
#   parameters      ListarTotalServicoAliquotaPrestador - {http://ws.api.smartbill.com.br/}listarTotalServicoAliquotaPrestador
#
# RETURNS
#   parameters      ListarTotalServicoAliquotaPrestadorResponse - {http://ws.api.smartbill.com.br/}listarTotalServicoAliquotaPrestadorResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarTotalServicoAliquotaPrestador(parameters)

# SYNOPSIS
#   obterUf(parameters)
#
# ARGS
#   parameters      ObterUf - {http://ws.api.smartbill.com.br/}obterUf
#
# RETURNS
#   parameters      ObterUfResponse - {http://ws.api.smartbill.com.br/}obterUfResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterUf(parameters)

# SYNOPSIS
#   removerFamilia(parameters)
#
# ARGS
#   parameters      RemoverFamilia - {http://ws.api.smartbill.com.br/}removerFamilia
#
# RETURNS
#   parameters      RemoverFamiliaResponse - {http://ws.api.smartbill.com.br/}removerFamiliaResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerFamilia(parameters)

# SYNOPSIS
#   enviarNFsEmissao(parameters)
#
# ARGS
#   parameters      EnviarNFsEmissao - {http://ws.api.smartbill.com.br/}enviarNFsEmissao
#
# RETURNS
#   parameters      EnviarNFsEmissaoResponse - {http://ws.api.smartbill.com.br/}enviarNFsEmissaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.enviarNFsEmissao(parameters)

# SYNOPSIS
#   listarContato(parameters)
#
# ARGS
#   parameters      ListarContato - {http://ws.api.smartbill.com.br/}listarContato
#
# RETURNS
#   parameters      ListarContatoResponse - {http://ws.api.smartbill.com.br/}listarContatoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarContato(parameters)

# SYNOPSIS
#   obterMotivoSuspensao(parameters)
#
# ARGS
#   parameters      ObterMotivoSuspensao - {http://ws.api.smartbill.com.br/}obterMotivoSuspensao
#
# RETURNS
#   parameters      ObterMotivoSuspensaoResponse - {http://ws.api.smartbill.com.br/}obterMotivoSuspensaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterMotivoSuspensao(parameters)

# SYNOPSIS
#   obterSegmento(parameters)
#
# ARGS
#   parameters      ObterSegmento - {http://ws.api.smartbill.com.br/}obterSegmento
#
# RETURNS
#   parameters      ObterSegmentoResponse - {http://ws.api.smartbill.com.br/}obterSegmentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterSegmento(parameters)

# SYNOPSIS
#   adicionarClienteNF(parameters)
#
# ARGS
#   parameters      AdicionarClienteNF - {http://ws.api.smartbill.com.br/}adicionarClienteNF
#
# RETURNS
#   parameters      AdicionarClienteNFResponse - {http://ws.api.smartbill.com.br/}adicionarClienteNFResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarClienteNF(parameters)

# SYNOPSIS
#   removerMotivoCancelamento(parameters)
#
# ARGS
#   parameters      RemoverMotivoCancelamento - {http://ws.api.smartbill.com.br/}removerMotivoCancelamento
#
# RETURNS
#   parameters      RemoverMotivoCancelamentoResponse - {http://ws.api.smartbill.com.br/}removerMotivoCancelamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerMotivoCancelamento(parameters)

# SYNOPSIS
#   obterIndiceEconomico(parameters)
#
# ARGS
#   parameters      ObterIndiceEconomico - {http://ws.api.smartbill.com.br/}obterIndiceEconomico
#
# RETURNS
#   parameters      ObterIndiceEconomicoResponse - {http://ws.api.smartbill.com.br/}obterIndiceEconomicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterIndiceEconomico(parameters)

# SYNOPSIS
#   obterRegra(parameters)
#
# ARGS
#   parameters      ObterRegra - {http://ws.api.smartbill.com.br/}obterRegra
#
# RETURNS
#   parameters      ObterRegraResponse - {http://ws.api.smartbill.com.br/}obterRegraResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterRegra(parameters)

# SYNOPSIS
#   removerSegmento(parameters)
#
# ARGS
#   parameters      RemoverSegmento - {http://ws.api.smartbill.com.br/}removerSegmento
#
# RETURNS
#   parameters      RemoverSegmentoResponse - {http://ws.api.smartbill.com.br/}removerSegmentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerSegmento(parameters)

# SYNOPSIS
#   adicionarArquivo(parameters)
#
# ARGS
#   parameters      AdicionarArquivo - {http://ws.api.smartbill.com.br/}adicionarArquivo
#
# RETURNS
#   parameters      AdicionarArquivoResponse - {http://ws.api.smartbill.com.br/}adicionarArquivoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarArquivo(parameters)

# SYNOPSIS
#   alterarCicloFaturamento(parameters)
#
# ARGS
#   parameters      AlterarCicloFaturamento - {http://ws.api.smartbill.com.br/}alterarCicloFaturamento
#
# RETURNS
#   parameters      AlterarCicloFaturamentoResponse - {http://ws.api.smartbill.com.br/}alterarCicloFaturamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarCicloFaturamento(parameters)

# SYNOPSIS
#   adicionarClienteCR(parameters)
#
# ARGS
#   parameters      AdicionarClienteCR - {http://ws.api.smartbill.com.br/}adicionarClienteCR
#
# RETURNS
#   parameters      AdicionarClienteCRResponse - {http://ws.api.smartbill.com.br/}adicionarClienteCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarClienteCR(parameters)

# SYNOPSIS
#   removerClienteServicoConsumo(parameters)
#
# ARGS
#   parameters      RemoverClienteServicoConsumo - {http://ws.api.smartbill.com.br/}removerClienteServicoConsumo
#
# RETURNS
#   parameters      RemoverClienteServicoConsumoResponse - {http://ws.api.smartbill.com.br/}removerClienteServicoConsumoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerClienteServicoConsumo(parameters)

# SYNOPSIS
#   adicionarPeriodicidade(parameters)
#
# ARGS
#   parameters      AdicionarPeriodicidade - {http://ws.api.smartbill.com.br/}adicionarPeriodicidade
#
# RETURNS
#   parameters      AdicionarPeriodicidadeResponse - {http://ws.api.smartbill.com.br/}adicionarPeriodicidadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarPeriodicidade(parameters)

# SYNOPSIS
#   listarSimplesNacional(parameters)
#
# ARGS
#   parameters      ListarSimplesNacional - {http://ws.api.smartbill.com.br/}listarSimplesNacional
#
# RETURNS
#   parameters      ListarSimplesNacionalResponse - {http://ws.api.smartbill.com.br/}listarSimplesNacionalResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarSimplesNacional(parameters)

# SYNOPSIS
#   listarTributacaoMunicipio(parameters)
#
# ARGS
#   parameters      ListarTributacaoMunicipio - {http://ws.api.smartbill.com.br/}listarTributacaoMunicipio
#
# RETURNS
#   parameters      ListarTributacaoMunicipioResponse - {http://ws.api.smartbill.com.br/}listarTributacaoMunicipioResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarTributacaoMunicipio(parameters)

# SYNOPSIS
#   adicionarClienteServicoConsumo(parameters)
#
# ARGS
#   parameters      AdicionarClienteServicoConsumo - {http://ws.api.smartbill.com.br/}adicionarClienteServicoConsumo
#
# RETURNS
#   parameters      AdicionarClienteServicoConsumoResponse - {http://ws.api.smartbill.com.br/}adicionarClienteServicoConsumoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarClienteServicoConsumo(parameters)

# SYNOPSIS
#   listarArquivo(parameters)
#
# ARGS
#   parameters      ListarArquivo - {http://ws.api.smartbill.com.br/}listarArquivo
#
# RETURNS
#   parameters      ListarArquivoResponse - {http://ws.api.smartbill.com.br/}listarArquivoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarArquivo(parameters)

# SYNOPSIS
#   obterIndiceEconomicoValor(parameters)
#
# ARGS
#   parameters      ObterIndiceEconomicoValor - {http://ws.api.smartbill.com.br/}obterIndiceEconomicoValor
#
# RETURNS
#   parameters      ObterIndiceEconomicoValorResponse - {http://ws.api.smartbill.com.br/}obterIndiceEconomicoValorResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterIndiceEconomicoValor(parameters)

# SYNOPSIS
#   fecharClienteServicoConsumo(parameters)
#
# ARGS
#   parameters      FecharClienteServicoConsumo - {http://ws.api.smartbill.com.br/}fecharClienteServicoConsumo
#
# RETURNS
#   parameters      FecharClienteServicoConsumoResponse - {http://ws.api.smartbill.com.br/}fecharClienteServicoConsumoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.fecharClienteServicoConsumo(parameters)

# SYNOPSIS
#   obterRamoAtividade(parameters)
#
# ARGS
#   parameters      ObterRamoAtividade - {http://ws.api.smartbill.com.br/}obterRamoAtividade
#
# RETURNS
#   parameters      ObterRamoAtividadeResponse - {http://ws.api.smartbill.com.br/}obterRamoAtividadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterRamoAtividade(parameters)

# SYNOPSIS
#   removerPeriodicidade(parameters)
#
# ARGS
#   parameters      RemoverPeriodicidade - {http://ws.api.smartbill.com.br/}removerPeriodicidade
#
# RETURNS
#   parameters      RemoverPeriodicidadeResponse - {http://ws.api.smartbill.com.br/}removerPeriodicidadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerPeriodicidade(parameters)

# SYNOPSIS
#   listarCidade(parameters)
#
# ARGS
#   parameters      ListarCidade - {http://ws.api.smartbill.com.br/}listarCidade
#
# RETURNS
#   parameters      ListarCidadeResponse - {http://ws.api.smartbill.com.br/}listarCidadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarCidade(parameters)

# SYNOPSIS
#   listarClienteCR(parameters)
#
# ARGS
#   parameters      ListarClienteCR - {http://ws.api.smartbill.com.br/}listarClienteCR
#
# RETURNS
#   parameters      ListarClienteCRResponse - {http://ws.api.smartbill.com.br/}listarClienteCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarClienteCR(parameters)

# SYNOPSIS
#   adicionarClienteServico(parameters)
#
# ARGS
#   parameters      AdicionarClienteServico - {http://ws.api.smartbill.com.br/}adicionarClienteServico
#
# RETURNS
#   parameters      AdicionarClienteServicoResponse - {http://ws.api.smartbill.com.br/}adicionarClienteServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarClienteServico(parameters)

# SYNOPSIS
#   autorizarClienteCartao(parameters)
#
# ARGS
#   parameters      AutorizarClienteCartao - {http://ws.api.smartbill.com.br/}autorizarClienteCartao
#
# RETURNS
#   parameters      AutorizarClienteCartaoResponse - {http://ws.api.smartbill.com.br/}autorizarClienteCartaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.autorizarClienteCartao(parameters)

# SYNOPSIS
#   removerEndereco(parameters)
#
# ARGS
#   parameters      RemoverEndereco - {http://ws.api.smartbill.com.br/}removerEndereco
#
# RETURNS
#   parameters      RemoverEnderecoResponse - {http://ws.api.smartbill.com.br/}removerEnderecoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerEndereco(parameters)

# SYNOPSIS
#   listarNaturezaOperacao(parameters)
#
# ARGS
#   parameters      ListarNaturezaOperacao - {http://ws.api.smartbill.com.br/}listarNaturezaOperacao
#
# RETURNS
#   parameters      ListarNaturezaOperacaoResponse - {http://ws.api.smartbill.com.br/}listarNaturezaOperacaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarNaturezaOperacao(parameters)

# SYNOPSIS
#   obterTemplateEmailPorTipo(parameters)
#
# ARGS
#   parameters      ObterTemplateEmailPorTipo - {http://ws.api.smartbill.com.br/}obterTemplateEmailPorTipo
#
# RETURNS
#   parameters      ObterTemplateEmailPorTipoResponse - {http://ws.api.smartbill.com.br/}obterTemplateEmailPorTipoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterTemplateEmailPorTipo(parameters)

# SYNOPSIS
#   listarClienteServicoCR(parameters)
#
# ARGS
#   parameters      ListarClienteServicoCR - {http://ws.api.smartbill.com.br/}listarClienteServicoCR
#
# RETURNS
#   parameters      ListarClienteServicoCRResponse - {http://ws.api.smartbill.com.br/}listarClienteServicoCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarClienteServicoCR(parameters)

# SYNOPSIS
#   removerCicloFaturamento(parameters)
#
# ARGS
#   parameters      RemoverCicloFaturamento - {http://ws.api.smartbill.com.br/}removerCicloFaturamento
#
# RETURNS
#   parameters      RemoverCicloFaturamentoResponse - {http://ws.api.smartbill.com.br/}removerCicloFaturamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerCicloFaturamento(parameters)

# SYNOPSIS
#   alterarClienteServicoCR(parameters)
#
# ARGS
#   parameters      AlterarClienteServicoCR - {http://ws.api.smartbill.com.br/}alterarClienteServicoCR
#
# RETURNS
#   parameters      AlterarClienteServicoCRResponse - {http://ws.api.smartbill.com.br/}alterarClienteServicoCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarClienteServicoCR(parameters)

# SYNOPSIS
#   obterClienteServicoConsumo(parameters)
#
# ARGS
#   parameters      ObterClienteServicoConsumo - {http://ws.api.smartbill.com.br/}obterClienteServicoConsumo
#
# RETURNS
#   parameters      ObterClienteServicoConsumoResponse - {http://ws.api.smartbill.com.br/}obterClienteServicoConsumoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterClienteServicoConsumo(parameters)

# SYNOPSIS
#   listarMotivoCancelamento(parameters)
#
# ARGS
#   parameters      ListarMotivoCancelamento - {http://ws.api.smartbill.com.br/}listarMotivoCancelamento
#
# RETURNS
#   parameters      ListarMotivoCancelamentoResponse - {http://ws.api.smartbill.com.br/}listarMotivoCancelamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarMotivoCancelamento(parameters)

# SYNOPSIS
#   adicionarMotivoCancelamento(parameters)
#
# ARGS
#   parameters      AdicionarMotivoCancelamento - {http://ws.api.smartbill.com.br/}adicionarMotivoCancelamento
#
# RETURNS
#   parameters      AdicionarMotivoCancelamentoResponse - {http://ws.api.smartbill.com.br/}adicionarMotivoCancelamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarMotivoCancelamento(parameters)

# SYNOPSIS
#   obterMotivoCancelamento(parameters)
#
# ARGS
#   parameters      ObterMotivoCancelamento - {http://ws.api.smartbill.com.br/}obterMotivoCancelamento
#
# RETURNS
#   parameters      ObterMotivoCancelamentoResponse - {http://ws.api.smartbill.com.br/}obterMotivoCancelamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterMotivoCancelamento(parameters)

# SYNOPSIS
#   adicionarCicloFaturamento(parameters)
#
# ARGS
#   parameters      AdicionarCicloFaturamento - {http://ws.api.smartbill.com.br/}adicionarCicloFaturamento
#
# RETURNS
#   parameters      AdicionarCicloFaturamentoResponse - {http://ws.api.smartbill.com.br/}adicionarCicloFaturamentoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarCicloFaturamento(parameters)

# SYNOPSIS
#   adicionarContato(parameters)
#
# ARGS
#   parameters      AdicionarContato - {http://ws.api.smartbill.com.br/}adicionarContato
#
# RETURNS
#   parameters      AdicionarContatoResponse - {http://ws.api.smartbill.com.br/}adicionarContatoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarContato(parameters)

# SYNOPSIS
#   obterCliente(parameters)
#
# ARGS
#   parameters      ObterCliente - {http://ws.api.smartbill.com.br/}obterCliente
#
# RETURNS
#   parameters      ObterClienteResponse - {http://ws.api.smartbill.com.br/}obterClienteResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterCliente(parameters)

# SYNOPSIS
#   cancelarClienteCR(parameters)
#
# ARGS
#   parameters      CancelarClienteCR - {http://ws.api.smartbill.com.br/}cancelarClienteCR
#
# RETURNS
#   parameters      CancelarClienteCRResponse - {http://ws.api.smartbill.com.br/}cancelarClienteCRResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.cancelarClienteCR(parameters)

# SYNOPSIS
#   alterarClienteServicoPorClienteServicoUuid(parameters)
#
# ARGS
#   parameters      AlterarClienteServicoPorClienteServicoUuid - {http://ws.api.smartbill.com.br/}alterarClienteServicoPorClienteServicoUuid
#
# RETURNS
#   parameters      AlterarClienteServicoPorClienteServicoUuidResponse - {http://ws.api.smartbill.com.br/}alterarClienteServicoPorClienteServicoUuidResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.alterarClienteServicoPorClienteServicoUuid(parameters)

# SYNOPSIS
#   listarRegimeTributario(parameters)
#
# ARGS
#   parameters      ListarRegimeTributario - {http://ws.api.smartbill.com.br/}listarRegimeTributario
#
# RETURNS
#   parameters      ListarRegimeTributarioResponse - {http://ws.api.smartbill.com.br/}listarRegimeTributarioResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarRegimeTributario(parameters)

# SYNOPSIS
#   obterClienteCRTransacaoBoleto(parameters)
#
# ARGS
#   parameters      ObterClienteCRTransacaoBoleto - {http://ws.api.smartbill.com.br/}obterClienteCRTransacaoBoleto
#
# RETURNS
#   parameters      ObterClienteCRTransacaoBoletoResponse - {http://ws.api.smartbill.com.br/}obterClienteCRTransacaoBoletoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterClienteCRTransacaoBoleto(parameters)

# SYNOPSIS
#   removerClienteCartao(parameters)
#
# ARGS
#   parameters      RemoverClienteCartao - {http://ws.api.smartbill.com.br/}removerClienteCartao
#
# RETURNS
#   parameters      RemoverClienteCartaoResponse - {http://ws.api.smartbill.com.br/}removerClienteCartaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerClienteCartao(parameters)

# SYNOPSIS
#   listarCliente(parameters)
#
# ARGS
#   parameters      ListarCliente - {http://ws.api.smartbill.com.br/}listarCliente
#
# RETURNS
#   parameters      ListarClienteResponse - {http://ws.api.smartbill.com.br/}listarClienteResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarCliente(parameters)

# SYNOPSIS
#   removerArquivo(parameters)
#
# ARGS
#   parameters      RemoverArquivo - {http://ws.api.smartbill.com.br/}removerArquivo
#
# RETURNS
#   parameters      RemoverArquivoResponse - {http://ws.api.smartbill.com.br/}removerArquivoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.removerArquivo(parameters)

# SYNOPSIS
#   clonarServico(parameters)
#
# ARGS
#   parameters      ClonarServico - {http://ws.api.smartbill.com.br/}clonarServico
#
# RETURNS
#   parameters      ClonarServicoResponse - {http://ws.api.smartbill.com.br/}clonarServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.clonarServico(parameters)

# SYNOPSIS
#   obterCidade(parameters)
#
# ARGS
#   parameters      ObterCidade - {http://ws.api.smartbill.com.br/}obterCidade
#
# RETURNS
#   parameters      ObterCidadeResponse - {http://ws.api.smartbill.com.br/}obterCidadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterCidade(parameters)

# SYNOPSIS
#   listarMotivoSuspensao(parameters)
#
# ARGS
#   parameters      ListarMotivoSuspensao - {http://ws.api.smartbill.com.br/}listarMotivoSuspensao
#
# RETURNS
#   parameters      ListarMotivoSuspensaoResponse - {http://ws.api.smartbill.com.br/}listarMotivoSuspensaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarMotivoSuspensao(parameters)

# SYNOPSIS
#   listarServico(parameters)
#
# ARGS
#   parameters      ListarServico - {http://ws.api.smartbill.com.br/}listarServico
#
# RETURNS
#   parameters      ListarServicoResponse - {http://ws.api.smartbill.com.br/}listarServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.listarServico(parameters)

# SYNOPSIS
#   obterPeriodicidade(parameters)
#
# ARGS
#   parameters      ObterPeriodicidade - {http://ws.api.smartbill.com.br/}obterPeriodicidade
#
# RETURNS
#   parameters      ObterPeriodicidadeResponse - {http://ws.api.smartbill.com.br/}obterPeriodicidadeResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.obterPeriodicidade(parameters)

# SYNOPSIS
#   adicionarServico(parameters)
#
# ARGS
#   parameters      AdicionarServico - {http://ws.api.smartbill.com.br/}adicionarServico
#
# RETURNS
#   parameters      AdicionarServicoResponse - {http://ws.api.smartbill.com.br/}adicionarServicoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.adicionarServico(parameters)

# SYNOPSIS
#   consultarNFsEmissao(parameters)
#
# ARGS
#   parameters      ConsultarNFsEmissao - {http://ws.api.smartbill.com.br/}consultarNFsEmissao
#
# RETURNS
#   parameters      ConsultarNFsEmissaoResponse - {http://ws.api.smartbill.com.br/}consultarNFsEmissaoResponse
#
# RAISES
#   smartbillAPIMessage SmartbillAPIMessage - {http://ws.api.smartbill.com.br/}SmartbillAPIMessage
#
parameters = nil
puts obj.consultarNFsEmissao(parameters)


