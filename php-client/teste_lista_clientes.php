<?php

	include("smartbill/SmartbillWebService.php");
	
	$smartbill = new SmartbillWebService('demo@smartbill.com.br', 'demo');
	
	$conn = $smartbill->connSmartbillAPIWS();

	try {
		$clientes = $conn->listarCliente();
	} catch(SoapFault $sf) {
		echo $sf;
	}

	$total = count($clientes->return);
	
	echo "<p>Clientes: {$total}</p>";
	echo "<p>";
	
	foreach ($clientes->return as $cliente) {
		$razaoSocial = $cliente->razaoSocial;
		$uuid = $cliente->uuid;
		echo "<strong>Razão Social:</strong> {$razaoSocial} [{$uuid}]<br>";
	}

	echo "</p>";
	
	echo "<h2>Obter Cliente</h2>";
	echo "<p>";

	try {
		$obterCliente = $conn->obterCliente(array('arg0' => "51d626af-1f45-429e-bff9-8d8e517a2a2e"));
	} catch(SoapFault $sf) {
		echo $sf;
	}

	echo "</p>";

	var_dump($obterCliente);



    class generalFilter {

		public $likeCampos;

		public $likeValor;

		public $maxElements;

		public $orderBy;

		public $page;

		public $desc;

	}

	class clienteCRFilter {

		public $generalFilter;

		public $anoReferencia;

		public $boleto;

		public $cicloFaturamento;

		public $clienteServicoCRUuid;

		public $clienteServicoUuid;

		public $clienteUuid;

		public $enviado;

		public $fakeRangeDataPagamento;

		public $fakeRangeDataVencimento;

		public $fakeRangeDataVencimentoOriginal;

		public $mesReferencia;

		public $notaFiscal;

		public $pago;

		public $possuiNFEmitido;

		public $projecaoFaturamento = [];

		public $rangeDataPagamento = [];

		public $rangeDataVencimento = [];

		public $rangeDataVencimentoOriginal = [];

		public $tipoCREstado = [];

		public $vencido;

	}


	$general = new generalFilter;

	$general->maxElements = 10000;

	$filter = new clienteCRFilter;

	$filter->generalFilter = $general;

	//$filter->clienteUuid = "0e83c997-0e20-4c75-b831-0dccb14e26a2";


	echo "<h2>Listar CR</h2>";
	echo "<p>";

	try {
		$listarClienteCR = $conn->listarClienteCR(array('arg0' => $filter));
	} catch(SoapFault $sf) {
		echo $sf;
	}

	echo "</p>";

	var_dump($listarClienteCR);

?>