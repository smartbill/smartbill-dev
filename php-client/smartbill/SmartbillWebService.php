<?php

	class SmartbillWebService {
		
		private $login;
		private $pass;
		
		function SmartbillWebService( $l, $p ) {
			$this->login = $l;
			$this->pass = $p;
		}
		
		function connSmartbillAPIWS() {
			$return = new SoapClient("https://ws.api.smartbill.com.br:8443/api/SmartbillAPI?WSDL", 
				array(
				  "location" => 'https://ws.api.smartbill.com.br:8443/api/SmartbillAPI?WSDL',
				  "login"=> $this->login, 
				  "password"=> $this->pass		
				)
			);
			
			#var_dump($client->__getFunctions());
			return $return;
		}
		
	}

?>
