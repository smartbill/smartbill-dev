/**
 * 
 */
package br.com.smartbill.api.test;

import br.com.smartbill.api.ws.Cidade;
import br.com.smartbill.api.ws.CidadeFilter;
import br.com.smartbill.api.ws.Cliente;
import br.com.smartbill.api.ws.Contato;
import br.com.smartbill.api.ws.DocumentoFiscal;
import br.com.smartbill.api.ws.Endereco;
import br.com.smartbill.api.ws.MeioPagamentoFilter;
import br.com.smartbill.api.ws.SmartbillAPI;
import br.com.smartbill.api.ws.SmartbillAPIMessage_Exception;
import br.com.smartbill.api.ws.TipoContato;
import br.com.smartbill.api.ws.TipoEndereco;
import br.com.smartbill.api.ws.TipoMeioPagamento;
import br.com.smartbill.api.ws.TipoPessoa;
import br.com.smartbill.api.ws.Uf;

/**
 * @author Daniel
 *
 */
public class AddClienteTest {

	final static SmartbillAPI service = new SmartbillApiAuth().getSmartbillAPI();
	
	public static void main(String[] args) throws SmartbillAPIMessage_Exception {

		Cliente cliente = new Cliente();
		
		cliente.setRazaoSocial("Demonstração Razão Social");		
		cliente.setTipoPessoa(TipoPessoa.FISICA);
		cliente.setCnpjCpf("395.743.998-18");
		cliente.setDocumentoFiscal(DocumentoFiscal.NAO_SE_APLICA);

		MeioPagamentoFilter meioPagamentoFilter = new MeioPagamentoFilter();
		meioPagamentoFilter.getTiposMeioPagamento().add(TipoMeioPagamento.BOLETO);		
		cliente.setMeioPagamento(service.listarMeioPagamento(meioPagamentoFilter).get(0));
		
		cliente.setDiaVencimento(10);
		
		// ou 
		
		/*for(CicloFaturamento cicloFaturamento : service.listarCicloFaturamento()) {
			if(cicloFaturamento.getNome().equals("Demonstração Ciclo Faturamento")) {
				cliente.setCicloFaturamento(cicloFaturamento);
				break;
			}
		}*/
		
		Contato contato = new Contato();		
		contato.setTipoContato(TipoContato.CONTABIL);
		contato.setNome("Demonstração Nome de Contato");
		contato.setEmail("demo@smartbill.com.br");
		cliente.getContatos().add(contato);	
		
		Endereco endereco = new Endereco();		
		endereco.setEnderecoDeFaturamento(true);
		endereco.setCep("14020-620");
		endereco.setTipoEndereco(TipoEndereco.COMERCIAL);
		endereco.setLogradouro("Rua Galileu Galilei, 1685 - Sala 801");
		
		for(Uf uf: service.listarUf(null)) {
			if(uf.getSigla().equals("SP")) {
				endereco.setUf(uf);
				
				CidadeFilter cidadeFilter = new CidadeFilter();
				cidadeFilter.setUf(uf);
				
				for(Cidade cidade: service.listarCidade(cidadeFilter)) {
					endereco.setCidade(cidade);
				}
				
			}
		}
		
		cliente.getEnderecos().add(endereco);
		
		try {
			service.adicionarCliente(cliente);
		} catch (SmartbillAPIMessage_Exception e) {
			System.out.println(e.getFaultInfo().getMensagem());
		}

	}
	
}
