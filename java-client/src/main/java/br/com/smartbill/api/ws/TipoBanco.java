
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoBanco.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoBanco">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BRADESCO"/>
 *     &lt;enumeration value="HSBC"/>
 *     &lt;enumeration value="ITAU"/>
 *     &lt;enumeration value="SANTANDER"/>
 *     &lt;enumeration value="BANCO_DO_BRASIL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoBanco")
@XmlEnum
public enum TipoBanco {

    BRADESCO,
    HSBC,
    ITAU,
    SANTANDER,
    BANCO_DO_BRASIL;

    public String value() {
        return name();
    }

    public static TipoBanco fromValue(String v) {
        return valueOf(v);
    }

}
