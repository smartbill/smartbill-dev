
package br.com.smartbill.api.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SmartbillAPIMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SmartbillAPIMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mensagem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="classe" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="metodo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigo" type="{http://ws.api.smartbill.com.br/}status"/>
 *         &lt;element name="objetoRetorno" type="{http://ws.api.smartbill.com.br/}smartbillObjetoRetorno"/>
 *         &lt;element name="objetosRetorno" type="{http://ws.api.smartbill.com.br/}smartbillObjetoRetorno" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="smartbillStackTrace" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SmartbillAPIMessage", propOrder = {
    "mensagem",
    "classe",
    "metodo",
    "codigo",
    "objetoRetorno",
    "objetosRetorno",
    "smartbillStackTrace"
})
public class SmartbillAPIMessage {

    @XmlElement(required = true, nillable = true)
    protected String mensagem;
    @XmlElement(required = true, nillable = true)
    protected String classe;
    @XmlElement(required = true, nillable = true)
    protected String metodo;
    @XmlElement(required = true, nillable = true)
    protected Status codigo;
    @XmlElement(required = true, nillable = true)
    protected SmartbillObjetoRetorno objetoRetorno;
    protected List<SmartbillObjetoRetorno> objetosRetorno;
    @XmlElement(required = true, nillable = true)
    protected String smartbillStackTrace;

    /**
     * Gets the value of the mensagem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Sets the value of the mensagem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagem(String value) {
        this.mensagem = value;
    }

    /**
     * Gets the value of the classe property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClasse() {
        return classe;
    }

    /**
     * Sets the value of the classe property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClasse(String value) {
        this.classe = value;
    }

    /**
     * Gets the value of the metodo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetodo() {
        return metodo;
    }

    /**
     * Sets the value of the metodo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetodo(String value) {
        this.metodo = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setCodigo(Status value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the objetoRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link SmartbillObjetoRetorno }
     *     
     */
    public SmartbillObjetoRetorno getObjetoRetorno() {
        return objetoRetorno;
    }

    /**
     * Sets the value of the objetoRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link SmartbillObjetoRetorno }
     *     
     */
    public void setObjetoRetorno(SmartbillObjetoRetorno value) {
        this.objetoRetorno = value;
    }

    /**
     * Gets the value of the objetosRetorno property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objetosRetorno property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjetosRetorno().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SmartbillObjetoRetorno }
     * 
     * 
     */
    public List<SmartbillObjetoRetorno> getObjetosRetorno() {
        if (objetosRetorno == null) {
            objetosRetorno = new ArrayList<SmartbillObjetoRetorno>();
        }
        return this.objetosRetorno;
    }

    /**
     * Gets the value of the smartbillStackTrace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmartbillStackTrace() {
        return smartbillStackTrace;
    }

    /**
     * Sets the value of the smartbillStackTrace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmartbillStackTrace(String value) {
        this.smartbillStackTrace = value;
    }

}
