
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for unidadeMetrica.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="unidadeMetrica">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="VALOR_FIXO"/>
 *     &lt;enumeration value="VALOR_INFORMADO"/>
 *     &lt;enumeration value="PRECO_POR_VOLUME"/>
 *     &lt;enumeration value="PRECO_POR_VOLUME_ACUMULATIVO"/>
 *     &lt;enumeration value="PRECO_POR_ETAPA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "unidadeMetrica")
@XmlEnum
public enum UnidadeMetrica {

    VALOR_FIXO,
    VALOR_INFORMADO,
    PRECO_POR_VOLUME,
    PRECO_POR_VOLUME_ACUMULATIVO,
    PRECO_POR_ETAPA;

    public String value() {
        return name();
    }

    public static UnidadeMetrica fromValue(String v) {
        return valueOf(v);
    }

}
