
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for servicoAliquotaTomador complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="servicoAliquotaTomador">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aliquotaISSRetido" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cidade" type="{http://ws.api.smartbill.com.br/}cidade" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "servicoAliquotaTomador", propOrder = {
    "aliquotaISSRetido",
    "cidade",
    "gerenteUuid",
    "uuid"
})
public class ServicoAliquotaTomador {

    protected BigDecimal aliquotaISSRetido;
    protected Cidade cidade;
    protected String gerenteUuid;
    protected String uuid;

    /**
     * Gets the value of the aliquotaISSRetido property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISSRetido() {
        return aliquotaISSRetido;
    }

    /**
     * Sets the value of the aliquotaISSRetido property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISSRetido(BigDecimal value) {
        this.aliquotaISSRetido = value;
    }

    /**
     * Gets the value of the cidade property.
     * 
     * @return
     *     possible object is
     *     {@link Cidade }
     *     
     */
    public Cidade getCidade() {
        return cidade;
    }

    /**
     * Sets the value of the cidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cidade }
     *     
     */
    public void setCidade(Cidade value) {
        this.cidade = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
