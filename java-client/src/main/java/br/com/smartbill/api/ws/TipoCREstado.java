
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoCREstado.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoCREstado">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EM_EDICAO"/>
 *     &lt;enumeration value="ATIVO"/>
 *     &lt;enumeration value="BAIXADO"/>
 *     &lt;enumeration value="BAIXADO_MANUAL"/>
 *     &lt;enumeration value="SUSPENSO"/>
 *     &lt;enumeration value="CANCELADO"/>
 *     &lt;enumeration value="PROCESSANDO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoCREstado")
@XmlEnum
public enum TipoCREstado {

    EM_EDICAO,
    ATIVO,
    BAIXADO,
    BAIXADO_MANUAL,
    SUSPENSO,
    CANCELADO,
    PROCESSANDO;

    public String value() {
        return name();
    }

    public static TipoCREstado fromValue(String v) {
        return valueOf(v);
    }

}
