
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoTemplateEmail.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoTemplateEmail">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CONFIRMACAO_DE_RECEBIMENTO_DO_BOLETO"/>
 *     &lt;enumeration value="SOLICITACAO_DE_CARTAO"/>
 *     &lt;enumeration value="ENVIO_DA_COBRANCA_DEPOSITO"/>
 *     &lt;enumeration value="ENVIO_DA_COBRANCA_BOLETO"/>
 *     &lt;enumeration value="ENVIO_DA_COBRANCA_CARTAO_DE_CREDITO"/>
 *     &lt;enumeration value="AVISO_DE_COBRANCA_EM_ATRASO"/>
 *     &lt;enumeration value="AVISO_DE_COBRANCA_EM_ATRASO_SUSPENSAO_EM_X_DIAS"/>
 *     &lt;enumeration value="AVISO_SUSPENSAO_DE_CONTRATO"/>
 *     &lt;enumeration value="AVISO_CANCELAMENTO_DE_CONTRATO"/>
 *     &lt;enumeration value="AVISO_VENCIMENTO_DE_CONTRATO"/>
 *     &lt;enumeration value="AGRADECIMENTO_DE_PAGAMENTO_BOLETO"/>
 *     &lt;enumeration value="AGRADECIMENTO_DE_PAGAMENTO_CARTAO_DE_CREDITO"/>
 *     &lt;enumeration value="NOVO_USUARIO"/>
 *     &lt;enumeration value="RESET_DE_SENHA"/>
 *     &lt;enumeration value="NOVO_CLIENTE_ATIVADO"/>
 *     &lt;enumeration value="AVISO_PAGAMENTO_DE_CR_A_MENOR"/>
 *     &lt;enumeration value="AVISO_PAGAMENTO_DE_CR_A_MAIOR"/>
 *     &lt;enumeration value="AVISO_DE_CANCELAMENTO_DE_CONTRATO"/>
 *     &lt;enumeration value="ENVIO_DE_RELATORIO"/>
 *     &lt;enumeration value="AVISO_DE_INADIMPLENCIA_MODELO_1"/>
 *     &lt;enumeration value="AVISO_DE_INADIMPLENCIA_MODELO_2"/>
 *     &lt;enumeration value="AVISO_DE_INADIMPLENCIA_MODELO_3"/>
 *     &lt;enumeration value="AVISO_DE_INADIMPLENCIA_MODELO_4"/>
 *     &lt;enumeration value="AVISO_DE_INADIMPLENCIA_MODELO_5"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoTemplateEmail")
@XmlEnum
public enum TipoTemplateEmail {

    CONFIRMACAO_DE_RECEBIMENTO_DO_BOLETO,
    SOLICITACAO_DE_CARTAO,
    ENVIO_DA_COBRANCA_DEPOSITO,
    ENVIO_DA_COBRANCA_BOLETO,
    ENVIO_DA_COBRANCA_CARTAO_DE_CREDITO,
    AVISO_DE_COBRANCA_EM_ATRASO,
    AVISO_DE_COBRANCA_EM_ATRASO_SUSPENSAO_EM_X_DIAS,
    AVISO_SUSPENSAO_DE_CONTRATO,
    AVISO_CANCELAMENTO_DE_CONTRATO,
    AVISO_VENCIMENTO_DE_CONTRATO,
    AGRADECIMENTO_DE_PAGAMENTO_BOLETO,
    AGRADECIMENTO_DE_PAGAMENTO_CARTAO_DE_CREDITO,
    NOVO_USUARIO,
    RESET_DE_SENHA,
    NOVO_CLIENTE_ATIVADO,
    AVISO_PAGAMENTO_DE_CR_A_MENOR,
    AVISO_PAGAMENTO_DE_CR_A_MAIOR,
    AVISO_DE_CANCELAMENTO_DE_CONTRATO,
    ENVIO_DE_RELATORIO,
    AVISO_DE_INADIMPLENCIA_MODELO_1,
    AVISO_DE_INADIMPLENCIA_MODELO_2,
    AVISO_DE_INADIMPLENCIA_MODELO_3,
    AVISO_DE_INADIMPLENCIA_MODELO_4,
    AVISO_DE_INADIMPLENCIA_MODELO_5;

    public String value() {
        return name();
    }

    public static TipoTemplateEmail fromValue(String v) {
        return valueOf(v);
    }

}
