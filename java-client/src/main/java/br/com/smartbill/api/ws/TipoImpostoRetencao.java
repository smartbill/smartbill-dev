
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoImpostoRetencao.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoImpostoRetencao">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PIS"/>
 *     &lt;enumeration value="COFINS"/>
 *     &lt;enumeration value="CSLL"/>
 *     &lt;enumeration value="IR"/>
 *     &lt;enumeration value="INSS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoImpostoRetencao")
@XmlEnum
public enum TipoImpostoRetencao {

    PIS,
    COFINS,
    CSLL,
    IR,
    INSS;

    public String value() {
        return name();
    }

    public static TipoImpostoRetencao fromValue(String v) {
        return valueOf(v);
    }

}
