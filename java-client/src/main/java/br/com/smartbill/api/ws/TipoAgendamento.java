
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoAgendamento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoAgendamento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PENDENTE"/>
 *     &lt;enumeration value="PROCESSANDO"/>
 *     &lt;enumeration value="CONCLUIDO"/>
 *     &lt;enumeration value="ERRO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoAgendamento")
@XmlEnum
public enum TipoAgendamento {

    PENDENTE,
    PROCESSANDO,
    CONCLUIDO,
    ERRO;

    public String value() {
        return name();
    }

    public static TipoAgendamento fromValue(String v) {
        return valueOf(v);
    }

}
