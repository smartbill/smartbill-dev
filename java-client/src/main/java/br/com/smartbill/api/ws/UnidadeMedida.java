
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for unidadeMedida.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="unidadeMedida">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNIDADE"/>
 *     &lt;enumeration value="MOEDA"/>
 *     &lt;enumeration value="TEMPO"/>
 *     &lt;enumeration value="PAGEVIEW"/>
 *     &lt;enumeration value="MB"/>
 *     &lt;enumeration value="CLIENTE"/>
 *     &lt;enumeration value="USUARIO"/>
 *     &lt;enumeration value="FATURAMENTO"/>
 *     &lt;enumeration value="LICENCA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "unidadeMedida")
@XmlEnum
public enum UnidadeMedida {

    UNIDADE,
    MOEDA,
    TEMPO,
    PAGEVIEW,
    MB,
    CLIENTE,
    USUARIO,
    FATURAMENTO,
    LICENCA;

    public String value() {
        return name();
    }

    public static UnidadeMedida fromValue(String v) {
        return valueOf(v);
    }

}
