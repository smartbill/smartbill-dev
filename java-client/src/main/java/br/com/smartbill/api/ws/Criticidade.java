
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for criticidade.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="criticidade">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ALTA"/>
 *     &lt;enumeration value="BAIXA"/>
 *     &lt;enumeration value="NAO_CRITICO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "criticidade")
@XmlEnum
public enum Criticidade {

    ALTA,
    BAIXA,
    NAO_CRITICO;

    public String value() {
        return name();
    }

    public static Criticidade fromValue(String v) {
        return valueOf(v);
    }

}
