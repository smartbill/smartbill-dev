
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for clienteFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cpfCnpj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="generalFilter" type="{http://ws.api.smartbill.com.br/}generalFilter" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteFilter", propOrder = {
    "cpfCnpj",
    "generalFilter"
})
public class ClienteFilter {

    protected String cpfCnpj;
    protected GeneralFilter generalFilter;

    /**
     * Gets the value of the cpfCnpj property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Sets the value of the cpfCnpj property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfCnpj(String value) {
        this.cpfCnpj = value;
    }

    /**
     * Gets the value of the generalFilter property.
     * 
     * @return
     *     possible object is
     *     {@link GeneralFilter }
     *     
     */
    public GeneralFilter getGeneralFilter() {
        return generalFilter;
    }

    /**
     * Sets the value of the generalFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralFilter }
     *     
     */
    public void setGeneralFilter(GeneralFilter value) {
        this.generalFilter = value;
    }

}
