
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteServicoCR complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteServicoCR">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anoReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="clienteServico" type="{http://ws.api.smartbill.com.br/}clienteServico" minOccurs="0"/>
 *         &lt;element name="clienteServicoConsumo" type="{http://ws.api.smartbill.com.br/}clienteServicoConsumo" minOccurs="0"/>
 *         &lt;element name="clienteServicoParcelaAtivacao" type="{http://ws.api.smartbill.com.br/}clienteServicoParcelaAtivacao" minOccurs="0"/>
 *         &lt;element name="clienteServicoParcelaServico" type="{http://ws.api.smartbill.com.br/}clienteServicoParcelaServico" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataEmissao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataVencimento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mesReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="projecaoFaturamento" type="{http://ws.api.smartbill.com.br/}projecaoFaturamento" minOccurs="0"/>
 *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="serie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subSerie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoCREstado" type="{http://ws.api.smartbill.com.br/}tipoCREstado" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valorAtivacaoTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorServicoTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteServicoCR", propOrder = {
    "anoReferencia",
    "clienteServico",
    "clienteServicoConsumo",
    "clienteServicoParcelaAtivacao",
    "clienteServicoParcelaServico",
    "criacao",
    "dataEmissao",
    "dataVencimento",
    "exclusao",
    "gerenteUuid",
    "mesReferencia",
    "modificacao",
    "projecaoFaturamento",
    "referencia",
    "serie",
    "subSerie",
    "tipoCREstado",
    "uuid",
    "valorAtivacaoTotal",
    "valorServicoTotal"
})
public class ClienteServicoCR {

    protected Integer anoReferencia;
    protected ClienteServico clienteServico;
    protected ClienteServicoConsumo clienteServicoConsumo;
    protected ClienteServicoParcelaAtivacao clienteServicoParcelaAtivacao;
    protected ClienteServicoParcelaServico clienteServicoParcelaServico;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEmissao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataVencimento;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    protected Integer mesReferencia;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected ProjecaoFaturamento projecaoFaturamento;
    protected Integer referencia;
    protected String serie;
    protected String subSerie;
    protected TipoCREstado tipoCREstado;
    protected String uuid;
    protected BigDecimal valorAtivacaoTotal;
    protected BigDecimal valorServicoTotal;

    /**
     * Gets the value of the anoReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnoReferencia() {
        return anoReferencia;
    }

    /**
     * Sets the value of the anoReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnoReferencia(Integer value) {
        this.anoReferencia = value;
    }

    /**
     * Gets the value of the clienteServico property.
     * 
     * @return
     *     possible object is
     *     {@link ClienteServico }
     *     
     */
    public ClienteServico getClienteServico() {
        return clienteServico;
    }

    /**
     * Sets the value of the clienteServico property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClienteServico }
     *     
     */
    public void setClienteServico(ClienteServico value) {
        this.clienteServico = value;
    }

    /**
     * Gets the value of the clienteServicoConsumo property.
     * 
     * @return
     *     possible object is
     *     {@link ClienteServicoConsumo }
     *     
     */
    public ClienteServicoConsumo getClienteServicoConsumo() {
        return clienteServicoConsumo;
    }

    /**
     * Sets the value of the clienteServicoConsumo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClienteServicoConsumo }
     *     
     */
    public void setClienteServicoConsumo(ClienteServicoConsumo value) {
        this.clienteServicoConsumo = value;
    }

    /**
     * Gets the value of the clienteServicoParcelaAtivacao property.
     * 
     * @return
     *     possible object is
     *     {@link ClienteServicoParcelaAtivacao }
     *     
     */
    public ClienteServicoParcelaAtivacao getClienteServicoParcelaAtivacao() {
        return clienteServicoParcelaAtivacao;
    }

    /**
     * Sets the value of the clienteServicoParcelaAtivacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClienteServicoParcelaAtivacao }
     *     
     */
    public void setClienteServicoParcelaAtivacao(ClienteServicoParcelaAtivacao value) {
        this.clienteServicoParcelaAtivacao = value;
    }

    /**
     * Gets the value of the clienteServicoParcelaServico property.
     * 
     * @return
     *     possible object is
     *     {@link ClienteServicoParcelaServico }
     *     
     */
    public ClienteServicoParcelaServico getClienteServicoParcelaServico() {
        return clienteServicoParcelaServico;
    }

    /**
     * Sets the value of the clienteServicoParcelaServico property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClienteServicoParcelaServico }
     *     
     */
    public void setClienteServicoParcelaServico(ClienteServicoParcelaServico value) {
        this.clienteServicoParcelaServico = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the dataEmissao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEmissao() {
        return dataEmissao;
    }

    /**
     * Sets the value of the dataEmissao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEmissao(XMLGregorianCalendar value) {
        this.dataEmissao = value;
    }

    /**
     * Gets the value of the dataVencimento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataVencimento() {
        return dataVencimento;
    }

    /**
     * Sets the value of the dataVencimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataVencimento(XMLGregorianCalendar value) {
        this.dataVencimento = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the mesReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMesReferencia() {
        return mesReferencia;
    }

    /**
     * Sets the value of the mesReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMesReferencia(Integer value) {
        this.mesReferencia = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the projecaoFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link ProjecaoFaturamento }
     *     
     */
    public ProjecaoFaturamento getProjecaoFaturamento() {
        return projecaoFaturamento;
    }

    /**
     * Sets the value of the projecaoFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjecaoFaturamento }
     *     
     */
    public void setProjecaoFaturamento(ProjecaoFaturamento value) {
        this.projecaoFaturamento = value;
    }

    /**
     * Gets the value of the referencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReferencia() {
        return referencia;
    }

    /**
     * Sets the value of the referencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReferencia(Integer value) {
        this.referencia = value;
    }

    /**
     * Gets the value of the serie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerie() {
        return serie;
    }

    /**
     * Sets the value of the serie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerie(String value) {
        this.serie = value;
    }

    /**
     * Gets the value of the subSerie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubSerie() {
        return subSerie;
    }

    /**
     * Sets the value of the subSerie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubSerie(String value) {
        this.subSerie = value;
    }

    /**
     * Gets the value of the tipoCREstado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCREstado }
     *     
     */
    public TipoCREstado getTipoCREstado() {
        return tipoCREstado;
    }

    /**
     * Sets the value of the tipoCREstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCREstado }
     *     
     */
    public void setTipoCREstado(TipoCREstado value) {
        this.tipoCREstado = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the valorAtivacaoTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorAtivacaoTotal() {
        return valorAtivacaoTotal;
    }

    /**
     * Sets the value of the valorAtivacaoTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorAtivacaoTotal(BigDecimal value) {
        this.valorAtivacaoTotal = value;
    }

    /**
     * Gets the value of the valorServicoTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorServicoTotal() {
        return valorServicoTotal;
    }

    /**
     * Sets the value of the valorServicoTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorServicoTotal(BigDecimal value) {
        this.valorServicoTotal = value;
    }

}
