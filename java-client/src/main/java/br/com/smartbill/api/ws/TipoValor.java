
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoValor.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoValor">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MOEDA"/>
 *     &lt;enumeration value="PORCENTAGEM"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoValor")
@XmlEnum
public enum TipoValor {

    MOEDA,
    PORCENTAGEM;

    public String value() {
        return name();
    }

    public static TipoValor fromValue(String v) {
        return valueOf(v);
    }

}
