
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoConsumoEstado.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoConsumoEstado">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EM_EDICAO"/>
 *     &lt;enumeration value="FECHADO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoConsumoEstado")
@XmlEnum
public enum TipoConsumoEstado {

    EM_EDICAO,
    FECHADO;

    public String value() {
        return name();
    }

    public static TipoConsumoEstado fromValue(String v) {
        return valueOf(v);
    }

}
