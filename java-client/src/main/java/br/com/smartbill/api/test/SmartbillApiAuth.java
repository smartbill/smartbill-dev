package br.com.smartbill.api.test;

import javax.xml.ws.BindingProvider;

import br.com.smartbill.api.ws.SmartbillAPI;
import br.com.smartbill.api.ws.SmartbillAPIService;

public class SmartbillApiAuth {

	private static final String USERNAME = "demo@smartbill.com.br";
	private static final String PASSWORD = "demo";

	private static final String wsdlURL = "https://ws.api.smartbill.com.br:8443";

	private SmartbillAPI service = new SmartbillAPIService().getSmartbillAPIPort();
	
	static {
		System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
	}

	private void setEndPoint(Object service, String className) {

		((BindingProvider) service).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdlURL + "/" + className + "?WSDL");
		((BindingProvider) service).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, USERNAME);
		((BindingProvider) service).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, PASSWORD);

	}

	public SmartbillAPI getSmartbillAPI() {

		this.setEndPoint(service, "SmartbillAPI");
		return service;

	}

}
