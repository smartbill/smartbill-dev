
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for relacionamentoFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="relacionamentoFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clienteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoRelacionamento" type="{http://ws.api.smartbill.com.br/}tipoRelacionamento" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "relacionamentoFilter", propOrder = {
    "clienteUuid",
    "tipoRelacionamento"
})
public class RelacionamentoFilter {

    protected String clienteUuid;
    protected TipoRelacionamento tipoRelacionamento;

    /**
     * Gets the value of the clienteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteUuid() {
        return clienteUuid;
    }

    /**
     * Sets the value of the clienteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteUuid(String value) {
        this.clienteUuid = value;
    }

    /**
     * Gets the value of the tipoRelacionamento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoRelacionamento }
     *     
     */
    public TipoRelacionamento getTipoRelacionamento() {
        return tipoRelacionamento;
    }

    /**
     * Sets the value of the tipoRelacionamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoRelacionamento }
     *     
     */
    public void setTipoRelacionamento(TipoRelacionamento value) {
        this.tipoRelacionamento = value;
    }

}
