/**
 * 
 */
package br.com.smartbill.api.test;

import br.com.smartbill.api.ws.Familia;
import br.com.smartbill.api.ws.Servico;
import br.com.smartbill.api.ws.SmartbillAPI;
import br.com.smartbill.api.ws.SmartbillAPIMessage_Exception;
import br.com.smartbill.api.ws.TipoServico;
import br.com.smartbill.api.ws.UnidadeMetrica;

/**
 * @author Daniel
 *
 */
public class AddServicoTest {

final static SmartbillAPI service = new SmartbillApiAuth().getSmartbillAPI();
	
	public static void main(String[] args) throws SmartbillAPIMessage_Exception {

		Servico servico = new Servico();		
		servico.setNome("Demostração Serviço");
		servico.setTipoServico(TipoServico.PRINCIPAL);
		servico.setUnidadeMetrica(UnidadeMetrica.VALOR_FIXO);
		
		for(Familia familia : service.listarFamilia()) {
			if(familia.getNome().equals("Gateway")) {
				servico.setFamilia(familia);
				break;
			}
		}
		
		try {
			service.adicionarServico(servico);
		} catch (SmartbillAPIMessage_Exception e) {
			System.out.println(e.getFaultInfo().getMensagem());
		}

	}
	
}
