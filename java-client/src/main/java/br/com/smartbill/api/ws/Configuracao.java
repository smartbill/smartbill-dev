
package br.com.smartbill.api.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for configuracao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="configuracao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cadastrosPrefeitura" type="{http://ws.api.smartbill.com.br/}cidade" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="caminhoCabecalhoEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="caminhoLogo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="certificadoDigitalCaminho" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="certificadoDigitalSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCNAE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="domicilioFaturamento" type="{http://ws.api.smartbill.com.br/}cidade" minOccurs="0"/>
 *         &lt;element name="emailCobranca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emailFinanceiro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="entityIdMyFinance" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="incentivadorCultural" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantKeyMundipagg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="numeroLOTE" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="numeroRPS" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="referenciaCR" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="referenciaCSCR" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="regimeTributario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senhaMastersaf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serieCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serieCSCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serieRPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seriesCliente" type="{http://ws.api.smartbill.com.br/}serie" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="seriesContrato" type="{http://ws.api.smartbill.com.br/}serie" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="simplesNacional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="smtpEmailDeCobranca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subSerieCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subSerieCSCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoAmbiente" type="{http://ws.api.smartbill.com.br/}tipoAmbiente" minOccurs="0"/>
 *         &lt;element name="tipoStatusEmailCobranca" type="{http://ws.api.smartbill.com.br/}tipoStatusEmailCobranca" minOccurs="0"/>
 *         &lt;element name="tokenMyFinance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usuarioMastersaf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="webserviceMastersaf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "configuracao", propOrder = {
    "cadastrosPrefeitura",
    "caminhoCabecalhoEmail",
    "caminhoLogo",
    "certificadoDigitalCaminho",
    "certificadoDigitalSenha",
    "codigoCNAE",
    "criacao",
    "domicilioFaturamento",
    "emailCobranca",
    "emailFinanceiro",
    "entityIdMyFinance",
    "exclusao",
    "gerenteUuid",
    "incentivadorCultural",
    "merchantKeyMundipagg",
    "modificacao",
    "numeroLOTE",
    "numeroRPS",
    "referenciaCR",
    "referenciaCSCR",
    "regimeTributario",
    "senhaMastersaf",
    "serieCR",
    "serieCSCR",
    "serieRPS",
    "seriesCliente",
    "seriesContrato",
    "simplesNacional",
    "smtpEmailDeCobranca",
    "subSerieCR",
    "subSerieCSCR",
    "tipoAmbiente",
    "tipoStatusEmailCobranca",
    "tokenMyFinance",
    "usuarioMastersaf",
    "uuid",
    "webserviceMastersaf"
})
public class Configuracao {

    @XmlElement(nillable = true)
    protected List<Cidade> cadastrosPrefeitura;
    protected String caminhoCabecalhoEmail;
    protected String caminhoLogo;
    protected String certificadoDigitalCaminho;
    protected String certificadoDigitalSenha;
    protected String codigoCNAE;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    protected Cidade domicilioFaturamento;
    protected String emailCobranca;
    protected String emailFinanceiro;
    protected Integer entityIdMyFinance;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    protected String incentivadorCultural;
    protected String merchantKeyMundipagg;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected Long numeroLOTE;
    protected Long numeroRPS;
    protected Integer referenciaCR;
    protected Integer referenciaCSCR;
    protected String regimeTributario;
    protected String senhaMastersaf;
    protected String serieCR;
    protected String serieCSCR;
    protected String serieRPS;
    @XmlElement(nillable = true)
    protected List<Serie> seriesCliente;
    @XmlElement(nillable = true)
    protected List<Serie> seriesContrato;
    protected String simplesNacional;
    protected String smtpEmailDeCobranca;
    protected String subSerieCR;
    protected String subSerieCSCR;
    protected TipoAmbiente tipoAmbiente;
    protected TipoStatusEmailCobranca tipoStatusEmailCobranca;
    protected String tokenMyFinance;
    protected String usuarioMastersaf;
    protected String uuid;
    protected String webserviceMastersaf;

    /**
     * Gets the value of the cadastrosPrefeitura property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cadastrosPrefeitura property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCadastrosPrefeitura().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cidade }
     * 
     * 
     */
    public List<Cidade> getCadastrosPrefeitura() {
        if (cadastrosPrefeitura == null) {
            cadastrosPrefeitura = new ArrayList<Cidade>();
        }
        return this.cadastrosPrefeitura;
    }

    /**
     * Gets the value of the caminhoCabecalhoEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaminhoCabecalhoEmail() {
        return caminhoCabecalhoEmail;
    }

    /**
     * Sets the value of the caminhoCabecalhoEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaminhoCabecalhoEmail(String value) {
        this.caminhoCabecalhoEmail = value;
    }

    /**
     * Gets the value of the caminhoLogo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaminhoLogo() {
        return caminhoLogo;
    }

    /**
     * Sets the value of the caminhoLogo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaminhoLogo(String value) {
        this.caminhoLogo = value;
    }

    /**
     * Gets the value of the certificadoDigitalCaminho property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificadoDigitalCaminho() {
        return certificadoDigitalCaminho;
    }

    /**
     * Sets the value of the certificadoDigitalCaminho property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificadoDigitalCaminho(String value) {
        this.certificadoDigitalCaminho = value;
    }

    /**
     * Gets the value of the certificadoDigitalSenha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificadoDigitalSenha() {
        return certificadoDigitalSenha;
    }

    /**
     * Sets the value of the certificadoDigitalSenha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificadoDigitalSenha(String value) {
        this.certificadoDigitalSenha = value;
    }

    /**
     * Gets the value of the codigoCNAE property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCNAE() {
        return codigoCNAE;
    }

    /**
     * Sets the value of the codigoCNAE property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCNAE(String value) {
        this.codigoCNAE = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the domicilioFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link Cidade }
     *     
     */
    public Cidade getDomicilioFaturamento() {
        return domicilioFaturamento;
    }

    /**
     * Sets the value of the domicilioFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cidade }
     *     
     */
    public void setDomicilioFaturamento(Cidade value) {
        this.domicilioFaturamento = value;
    }

    /**
     * Gets the value of the emailCobranca property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailCobranca() {
        return emailCobranca;
    }

    /**
     * Sets the value of the emailCobranca property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailCobranca(String value) {
        this.emailCobranca = value;
    }

    /**
     * Gets the value of the emailFinanceiro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailFinanceiro() {
        return emailFinanceiro;
    }

    /**
     * Sets the value of the emailFinanceiro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailFinanceiro(String value) {
        this.emailFinanceiro = value;
    }

    /**
     * Gets the value of the entityIdMyFinance property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEntityIdMyFinance() {
        return entityIdMyFinance;
    }

    /**
     * Sets the value of the entityIdMyFinance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEntityIdMyFinance(Integer value) {
        this.entityIdMyFinance = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the incentivadorCultural property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncentivadorCultural() {
        return incentivadorCultural;
    }

    /**
     * Sets the value of the incentivadorCultural property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncentivadorCultural(String value) {
        this.incentivadorCultural = value;
    }

    /**
     * Gets the value of the merchantKeyMundipagg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantKeyMundipagg() {
        return merchantKeyMundipagg;
    }

    /**
     * Sets the value of the merchantKeyMundipagg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantKeyMundipagg(String value) {
        this.merchantKeyMundipagg = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the numeroLOTE property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroLOTE() {
        return numeroLOTE;
    }

    /**
     * Sets the value of the numeroLOTE property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroLOTE(Long value) {
        this.numeroLOTE = value;
    }

    /**
     * Gets the value of the numeroRPS property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroRPS() {
        return numeroRPS;
    }

    /**
     * Sets the value of the numeroRPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroRPS(Long value) {
        this.numeroRPS = value;
    }

    /**
     * Gets the value of the referenciaCR property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReferenciaCR() {
        return referenciaCR;
    }

    /**
     * Sets the value of the referenciaCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReferenciaCR(Integer value) {
        this.referenciaCR = value;
    }

    /**
     * Gets the value of the referenciaCSCR property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReferenciaCSCR() {
        return referenciaCSCR;
    }

    /**
     * Sets the value of the referenciaCSCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReferenciaCSCR(Integer value) {
        this.referenciaCSCR = value;
    }

    /**
     * Gets the value of the regimeTributario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegimeTributario() {
        return regimeTributario;
    }

    /**
     * Sets the value of the regimeTributario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegimeTributario(String value) {
        this.regimeTributario = value;
    }

    /**
     * Gets the value of the senhaMastersaf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenhaMastersaf() {
        return senhaMastersaf;
    }

    /**
     * Sets the value of the senhaMastersaf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenhaMastersaf(String value) {
        this.senhaMastersaf = value;
    }

    /**
     * Gets the value of the serieCR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerieCR() {
        return serieCR;
    }

    /**
     * Sets the value of the serieCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerieCR(String value) {
        this.serieCR = value;
    }

    /**
     * Gets the value of the serieCSCR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerieCSCR() {
        return serieCSCR;
    }

    /**
     * Sets the value of the serieCSCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerieCSCR(String value) {
        this.serieCSCR = value;
    }

    /**
     * Gets the value of the serieRPS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerieRPS() {
        return serieRPS;
    }

    /**
     * Sets the value of the serieRPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerieRPS(String value) {
        this.serieRPS = value;
    }

    /**
     * Gets the value of the seriesCliente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the seriesCliente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeriesCliente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Serie }
     * 
     * 
     */
    public List<Serie> getSeriesCliente() {
        if (seriesCliente == null) {
            seriesCliente = new ArrayList<Serie>();
        }
        return this.seriesCliente;
    }

    /**
     * Gets the value of the seriesContrato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the seriesContrato property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeriesContrato().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Serie }
     * 
     * 
     */
    public List<Serie> getSeriesContrato() {
        if (seriesContrato == null) {
            seriesContrato = new ArrayList<Serie>();
        }
        return this.seriesContrato;
    }

    /**
     * Gets the value of the simplesNacional property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSimplesNacional() {
        return simplesNacional;
    }

    /**
     * Sets the value of the simplesNacional property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSimplesNacional(String value) {
        this.simplesNacional = value;
    }

    /**
     * Gets the value of the smtpEmailDeCobranca property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmtpEmailDeCobranca() {
        return smtpEmailDeCobranca;
    }

    /**
     * Sets the value of the smtpEmailDeCobranca property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmtpEmailDeCobranca(String value) {
        this.smtpEmailDeCobranca = value;
    }

    /**
     * Gets the value of the subSerieCR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubSerieCR() {
        return subSerieCR;
    }

    /**
     * Sets the value of the subSerieCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubSerieCR(String value) {
        this.subSerieCR = value;
    }

    /**
     * Gets the value of the subSerieCSCR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubSerieCSCR() {
        return subSerieCSCR;
    }

    /**
     * Sets the value of the subSerieCSCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubSerieCSCR(String value) {
        this.subSerieCSCR = value;
    }

    /**
     * Gets the value of the tipoAmbiente property.
     * 
     * @return
     *     possible object is
     *     {@link TipoAmbiente }
     *     
     */
    public TipoAmbiente getTipoAmbiente() {
        return tipoAmbiente;
    }

    /**
     * Sets the value of the tipoAmbiente property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoAmbiente }
     *     
     */
    public void setTipoAmbiente(TipoAmbiente value) {
        this.tipoAmbiente = value;
    }

    /**
     * Gets the value of the tipoStatusEmailCobranca property.
     * 
     * @return
     *     possible object is
     *     {@link TipoStatusEmailCobranca }
     *     
     */
    public TipoStatusEmailCobranca getTipoStatusEmailCobranca() {
        return tipoStatusEmailCobranca;
    }

    /**
     * Sets the value of the tipoStatusEmailCobranca property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoStatusEmailCobranca }
     *     
     */
    public void setTipoStatusEmailCobranca(TipoStatusEmailCobranca value) {
        this.tipoStatusEmailCobranca = value;
    }

    /**
     * Gets the value of the tokenMyFinance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTokenMyFinance() {
        return tokenMyFinance;
    }

    /**
     * Sets the value of the tokenMyFinance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTokenMyFinance(String value) {
        this.tokenMyFinance = value;
    }

    /**
     * Gets the value of the usuarioMastersaf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioMastersaf() {
        return usuarioMastersaf;
    }

    /**
     * Sets the value of the usuarioMastersaf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioMastersaf(String value) {
        this.usuarioMastersaf = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the webserviceMastersaf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebserviceMastersaf() {
        return webserviceMastersaf;
    }

    /**
     * Sets the value of the webserviceMastersaf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebserviceMastersaf(String value) {
        this.webserviceMastersaf = value;
    }

}
