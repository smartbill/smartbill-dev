
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoCampoEmail.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoCampoEmail">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TEXTO"/>
 *     &lt;enumeration value="IMAGEM"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoCampoEmail")
@XmlEnum
public enum TipoCampoEmail {

    TEXTO,
    IMAGEM;

    public String value() {
        return name();
    }

    public static TipoCampoEmail fromValue(String v) {
        return valueOf(v);
    }

}
