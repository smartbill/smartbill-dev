
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for clienteServicoCRDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteServicoCRDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anoReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="consumo" type="{http://ws.api.smartbill.com.br/}tipoConsumoEstado" minOccurs="0"/>
 *         &lt;element name="mesReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroNFS" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="projecaoFaturamento" type="{http://ws.api.smartbill.com.br/}projecaoFaturamento" minOccurs="0"/>
 *         &lt;element name="tipoCREstado" type="{http://ws.api.smartbill.com.br/}tipoCREstado" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuidCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuidNF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valorAtivacaoTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorServicoTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteServicoCRDTO", propOrder = {
    "anoReferencia",
    "codigo",
    "codigoCR",
    "codigoContrato",
    "consumo",
    "mesReferencia",
    "numeroNFS",
    "projecaoFaturamento",
    "tipoCREstado",
    "uuid",
    "uuidCR",
    "uuidNF",
    "valorAtivacaoTotal",
    "valorServicoTotal"
})
public class ClienteServicoCRDTO {

    protected Integer anoReferencia;
    protected String codigo;
    protected String codigoCR;
    protected String codigoContrato;
    protected TipoConsumoEstado consumo;
    protected Integer mesReferencia;
    protected BigInteger numeroNFS;
    protected ProjecaoFaturamento projecaoFaturamento;
    protected TipoCREstado tipoCREstado;
    protected String uuid;
    protected String uuidCR;
    protected String uuidNF;
    protected BigDecimal valorAtivacaoTotal;
    protected BigDecimal valorServicoTotal;

    /**
     * Gets the value of the anoReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnoReferencia() {
        return anoReferencia;
    }

    /**
     * Sets the value of the anoReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnoReferencia(Integer value) {
        this.anoReferencia = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the codigoCR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCR() {
        return codigoCR;
    }

    /**
     * Sets the value of the codigoCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCR(String value) {
        this.codigoCR = value;
    }

    /**
     * Gets the value of the codigoContrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoContrato() {
        return codigoContrato;
    }

    /**
     * Sets the value of the codigoContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoContrato(String value) {
        this.codigoContrato = value;
    }

    /**
     * Gets the value of the consumo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoConsumoEstado }
     *     
     */
    public TipoConsumoEstado getConsumo() {
        return consumo;
    }

    /**
     * Sets the value of the consumo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConsumoEstado }
     *     
     */
    public void setConsumo(TipoConsumoEstado value) {
        this.consumo = value;
    }

    /**
     * Gets the value of the mesReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMesReferencia() {
        return mesReferencia;
    }

    /**
     * Sets the value of the mesReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMesReferencia(Integer value) {
        this.mesReferencia = value;
    }

    /**
     * Gets the value of the numeroNFS property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumeroNFS() {
        return numeroNFS;
    }

    /**
     * Sets the value of the numeroNFS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumeroNFS(BigInteger value) {
        this.numeroNFS = value;
    }

    /**
     * Gets the value of the projecaoFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link ProjecaoFaturamento }
     *     
     */
    public ProjecaoFaturamento getProjecaoFaturamento() {
        return projecaoFaturamento;
    }

    /**
     * Sets the value of the projecaoFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjecaoFaturamento }
     *     
     */
    public void setProjecaoFaturamento(ProjecaoFaturamento value) {
        this.projecaoFaturamento = value;
    }

    /**
     * Gets the value of the tipoCREstado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCREstado }
     *     
     */
    public TipoCREstado getTipoCREstado() {
        return tipoCREstado;
    }

    /**
     * Sets the value of the tipoCREstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCREstado }
     *     
     */
    public void setTipoCREstado(TipoCREstado value) {
        this.tipoCREstado = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the uuidCR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidCR() {
        return uuidCR;
    }

    /**
     * Sets the value of the uuidCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidCR(String value) {
        this.uuidCR = value;
    }

    /**
     * Gets the value of the uuidNF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidNF() {
        return uuidNF;
    }

    /**
     * Sets the value of the uuidNF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidNF(String value) {
        this.uuidNF = value;
    }

    /**
     * Gets the value of the valorAtivacaoTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorAtivacaoTotal() {
        return valorAtivacaoTotal;
    }

    /**
     * Sets the value of the valorAtivacaoTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorAtivacaoTotal(BigDecimal value) {
        this.valorAtivacaoTotal = value;
    }

    /**
     * Gets the value of the valorServicoTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorServicoTotal() {
        return valorServicoTotal;
    }

    /**
     * Sets the value of the valorServicoTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorServicoTotal(BigDecimal value) {
        this.valorServicoTotal = value;
    }

}
