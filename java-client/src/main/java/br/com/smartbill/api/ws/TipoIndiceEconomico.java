
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoIndiceEconomico.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoIndiceEconomico">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="IGPM"/>
 *     &lt;enumeration value="IPCA"/>
 *     &lt;enumeration value="IGP"/>
 *     &lt;enumeration value="OUTROS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoIndiceEconomico")
@XmlEnum
public enum TipoIndiceEconomico {

    IGPM,
    IPCA,
    IGP,
    OUTROS;

    public String value() {
        return name();
    }

    public static TipoIndiceEconomico fromValue(String v) {
        return valueOf(v);
    }

}
