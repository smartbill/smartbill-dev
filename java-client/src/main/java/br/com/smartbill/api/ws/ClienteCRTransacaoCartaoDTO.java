
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteCRTransacaoCartaoDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteCRTransacaoCartaoDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anoReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cicloFaturamento" type="{http://ws.api.smartbill.com.br/}cicloFaturamento" minOccurs="0"/>
 *         &lt;element name="clienteCRUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clienteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cnpjCpfTomador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoAdquirente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataTransacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="mensagemAdquirente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mesReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="permissao" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="razaoSocialTomador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="situacaoPedido" type="{http://ws.api.smartbill.com.br/}tipoStatusGateway" minOccurs="0"/>
 *         &lt;element name="tipoBandeira" type="{http://ws.api.smartbill.com.br/}tipoBandeira" minOccurs="0"/>
 *         &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteCRTransacaoCartaoDTO", propOrder = {
    "anoReferencia",
    "cicloFaturamento",
    "clienteCRUuid",
    "clienteUuid",
    "cnpjCpfTomador",
    "codigoAdquirente",
    "codigoCR",
    "codigoContrato",
    "dataTransacao",
    "mensagemAdquirente",
    "mesReferencia",
    "numero",
    "permissao",
    "razaoSocialTomador",
    "situacaoPedido",
    "tipoBandeira",
    "titular",
    "uuid",
    "valor"
})
public class ClienteCRTransacaoCartaoDTO {

    protected Integer anoReferencia;
    protected CicloFaturamento cicloFaturamento;
    protected String clienteCRUuid;
    protected String clienteUuid;
    protected String cnpjCpfTomador;
    protected String codigoAdquirente;
    protected String codigoCR;
    protected String codigoContrato;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataTransacao;
    protected String mensagemAdquirente;
    protected Integer mesReferencia;
    protected String numero;
    @XmlElement(nillable = true)
    protected List<Integer> permissao;
    protected String razaoSocialTomador;
    protected TipoStatusGateway situacaoPedido;
    protected TipoBandeira tipoBandeira;
    protected String titular;
    protected String uuid;
    protected BigDecimal valor;

    /**
     * Gets the value of the anoReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnoReferencia() {
        return anoReferencia;
    }

    /**
     * Sets the value of the anoReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnoReferencia(Integer value) {
        this.anoReferencia = value;
    }

    /**
     * Gets the value of the cicloFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link CicloFaturamento }
     *     
     */
    public CicloFaturamento getCicloFaturamento() {
        return cicloFaturamento;
    }

    /**
     * Sets the value of the cicloFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link CicloFaturamento }
     *     
     */
    public void setCicloFaturamento(CicloFaturamento value) {
        this.cicloFaturamento = value;
    }

    /**
     * Gets the value of the clienteCRUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteCRUuid() {
        return clienteCRUuid;
    }

    /**
     * Sets the value of the clienteCRUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteCRUuid(String value) {
        this.clienteCRUuid = value;
    }

    /**
     * Gets the value of the clienteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteUuid() {
        return clienteUuid;
    }

    /**
     * Sets the value of the clienteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteUuid(String value) {
        this.clienteUuid = value;
    }

    /**
     * Gets the value of the cnpjCpfTomador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnpjCpfTomador() {
        return cnpjCpfTomador;
    }

    /**
     * Sets the value of the cnpjCpfTomador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnpjCpfTomador(String value) {
        this.cnpjCpfTomador = value;
    }

    /**
     * Gets the value of the codigoAdquirente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAdquirente() {
        return codigoAdquirente;
    }

    /**
     * Sets the value of the codigoAdquirente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAdquirente(String value) {
        this.codigoAdquirente = value;
    }

    /**
     * Gets the value of the codigoCR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCR() {
        return codigoCR;
    }

    /**
     * Sets the value of the codigoCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCR(String value) {
        this.codigoCR = value;
    }

    /**
     * Gets the value of the codigoContrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoContrato() {
        return codigoContrato;
    }

    /**
     * Sets the value of the codigoContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoContrato(String value) {
        this.codigoContrato = value;
    }

    /**
     * Gets the value of the dataTransacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataTransacao() {
        return dataTransacao;
    }

    /**
     * Sets the value of the dataTransacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataTransacao(XMLGregorianCalendar value) {
        this.dataTransacao = value;
    }

    /**
     * Gets the value of the mensagemAdquirente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagemAdquirente() {
        return mensagemAdquirente;
    }

    /**
     * Sets the value of the mensagemAdquirente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagemAdquirente(String value) {
        this.mensagemAdquirente = value;
    }

    /**
     * Gets the value of the mesReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMesReferencia() {
        return mesReferencia;
    }

    /**
     * Sets the value of the mesReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMesReferencia(Integer value) {
        this.mesReferencia = value;
    }

    /**
     * Gets the value of the numero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the permissao property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the permissao property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPermissao().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getPermissao() {
        if (permissao == null) {
            permissao = new ArrayList<Integer>();
        }
        return this.permissao;
    }

    /**
     * Gets the value of the razaoSocialTomador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazaoSocialTomador() {
        return razaoSocialTomador;
    }

    /**
     * Sets the value of the razaoSocialTomador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazaoSocialTomador(String value) {
        this.razaoSocialTomador = value;
    }

    /**
     * Gets the value of the situacaoPedido property.
     * 
     * @return
     *     possible object is
     *     {@link TipoStatusGateway }
     *     
     */
    public TipoStatusGateway getSituacaoPedido() {
        return situacaoPedido;
    }

    /**
     * Sets the value of the situacaoPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoStatusGateway }
     *     
     */
    public void setSituacaoPedido(TipoStatusGateway value) {
        this.situacaoPedido = value;
    }

    /**
     * Gets the value of the tipoBandeira property.
     * 
     * @return
     *     possible object is
     *     {@link TipoBandeira }
     *     
     */
    public TipoBandeira getTipoBandeira() {
        return tipoBandeira;
    }

    /**
     * Sets the value of the tipoBandeira property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoBandeira }
     *     
     */
    public void setTipoBandeira(TipoBandeira value) {
        this.tipoBandeira = value;
    }

    /**
     * Gets the value of the titular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitular() {
        return titular;
    }

    /**
     * Sets the value of the titular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitular(String value) {
        this.titular = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValor(BigDecimal value) {
        this.valor = value;
    }

}
