package br.com.smartbill.api.ws;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.4.4
 * 2013-11-25T14:18:51.911-02:00
 * Generated source version: 2.4.4
 * 
 */
@WebServiceClient(name = "ParametroWSService", 
                  wsdlLocation = "file:/C:/Users/Daniel/smartbill/workspace/smartbill-app/smartbill-app-web/target/wsprovide/resources/ParametroWSService.wsdl",
                  targetNamespace = "http://ws.api.smartbill.com.br/") 
public class ParametroWSService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://ws.api.smartbill.com.br/", "ParametroWSService");
    public final static QName ParametroWSPort = new QName("http://ws.api.smartbill.com.br/", "ParametroWSPort");
    static {
        URL url = null;
        try {
            url = new URL("file:/C:/Users/Daniel/smartbill/workspace/smartbill-app/smartbill-app-web/target/wsprovide/resources/ParametroWSService.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(ParametroWSService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:/C:/Users/Daniel/smartbill/workspace/smartbill-app/smartbill-app-web/target/wsprovide/resources/ParametroWSService.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public ParametroWSService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public ParametroWSService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ParametroWSService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public ParametroWSService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public ParametroWSService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public ParametroWSService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns ParametroWS
     */
    @WebEndpoint(name = "ParametroWSPort")
    public ParametroWS getParametroWSPort() {
        return super.getPort(ParametroWSPort, ParametroWS.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ParametroWS
     */
    @WebEndpoint(name = "ParametroWSPort")
    public ParametroWS getParametroWSPort(WebServiceFeature... features) {
        return super.getPort(ParametroWSPort, ParametroWS.class, features);
    }

}
