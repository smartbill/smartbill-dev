
package br.com.smartbill.api.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.smartbill.api.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObterServicoAliquotaPrestador_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterServicoAliquotaPrestador");
    private final static QName _AdicionarServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarServico");
    private final static QName _RemoverArquivoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerArquivoResponse");
    private final static QName _ObterConfiguracaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterConfiguracaoResponse");
    private final static QName _ListarTotalArquivoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalArquivoResponse");
    private final static QName _AdicionarCliente_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarCliente");
    private final static QName _RemoverContatoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerContatoResponse");
    private final static QName _ConsultarNFsEmissao_QNAME = new QName("http://ws.api.smartbill.com.br/", "consultarNFsEmissao");
    private final static QName _RemoverMotivoCancelamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerMotivoCancelamento");
    private final static QName _ClonarServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "clonarServico");
    private final static QName _ListarRelacionamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarRelacionamento");
    private final static QName _AlterarCliente_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarCliente");
    private final static QName _ListarFamiliaResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarFamiliaResponse");
    private final static QName _ObterRelacionamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterRelacionamento");
    private final static QName _GerarCobrancaResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "gerarCobrancaResponse");
    private final static QName _AlterarPeriodicidadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarPeriodicidadeResponse");
    private final static QName _ListarRelacionamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarRelacionamentoResponse");
    private final static QName _ObterIndiceEconomico_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterIndiceEconomico");
    private final static QName _ListarArquivoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarArquivoResponse");
    private final static QName _ObterUf_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterUf");
    private final static QName _ListarTotalClienteNFResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalClienteNFResponse");
    private final static QName _AlterarContatoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarContatoResponse");
    private final static QName _ListarCidadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarCidadeResponse");
    private final static QName _AlterarRamoAtividadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarRamoAtividadeResponse");
    private final static QName _ListarRegra_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarRegra");
    private final static QName _ListarMotivoSuspensao_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarMotivoSuspensao");
    private final static QName _CancelarClienteCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "cancelarClienteCR");
    private final static QName _ObterClienteServicoCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteServicoCRResponse");
    private final static QName _SolicitarClienteCartao_QNAME = new QName("http://ws.api.smartbill.com.br/", "solicitarClienteCartao");
    private final static QName _ObterPadraoPreenchimentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterPadraoPreenchimentoResponse");
    private final static QName _ListarArquivo_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarArquivo");
    private final static QName _CancelarClienteServicoPorClienteServicoUuid_QNAME = new QName("http://ws.api.smartbill.com.br/", "cancelarClienteServicoPorClienteServicoUuid");
    private final static QName _AdicionarFamilia_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarFamilia");
    private final static QName _ListarRegraResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarRegraResponse");
    private final static QName _ListarEndereco_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarEndereco");
    private final static QName _ListarClienteServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteServicoResponse");
    private final static QName _ObterServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterServicoResponse");
    private final static QName _ListarSimplesNacional_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarSimplesNacional");
    private final static QName _AdicionarClienteServicoCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteServicoCR");
    private final static QName _ListarClienteNF_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteNF");
    private final static QName _ListarCidade_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarCidade");
    private final static QName _AlterarClienteCRCadastro_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteCRCadastro");
    private final static QName _AdicionarClienteServicoConsumoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteServicoConsumoResponse");
    private final static QName _ListarTotalClienteCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalClienteCR");
    private final static QName _AlterarSegmentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarSegmentoResponse");
    private final static QName _ListarClienteResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteResponse");
    private final static QName _ListarTemplateEmailResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTemplateEmailResponse");
    private final static QName _ObterCliente_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterCliente");
    private final static QName _AlterarSegmento_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarSegmento");
    private final static QName _RemoverContato_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerContato");
    private final static QName _RemoverClienteServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteServico");
    private final static QName _ObterServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterServico");
    private final static QName _AlterarEndereco_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarEndereco");
    private final static QName _AutorizarClienteCartaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "autorizarClienteCartaoResponse");
    private final static QName _RemoverSegmentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerSegmentoResponse");
    private final static QName _ConsultarCobranca_QNAME = new QName("http://ws.api.smartbill.com.br/", "consultarCobranca");
    private final static QName _CancelarClienteServicoPorClienteServicoUuidResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "cancelarClienteServicoPorClienteServicoUuidResponse");
    private final static QName _ObterFamiliaResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterFamiliaResponse");
    private final static QName _ListarClienteServicoConsumo_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteServicoConsumo");
    private final static QName _ListarUfResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarUfResponse");
    private final static QName _ListarClienteServicoCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteServicoCRResponse");
    private final static QName _ObterTributacaoMunicipio_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterTributacaoMunicipio");
    private final static QName _ListarFamilia_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarFamilia");
    private final static QName _SuspenderClienteServicoPorClienteServicoUuidResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "suspenderClienteServicoPorClienteServicoUuidResponse");
    private final static QName _ObterSegmentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterSegmentoResponse");
    private final static QName _ClonarClienteServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "clonarClienteServicoResponse");
    private final static QName _ListarServicoAliquotaPrestador_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarServicoAliquotaPrestador");
    private final static QName _ObterPadraoPreenchimento_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterPadraoPreenchimento");
    private final static QName _ObterRamoAtividade_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterRamoAtividade");
    private final static QName _AdicionarSegmentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarSegmentoResponse");
    private final static QName _AdicionarEndereco_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarEndereco");
    private final static QName _AdicionarClienteServicoCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteServicoCRResponse");
    private final static QName _RemoverClienteServicoCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteServicoCRResponse");
    private final static QName _FecharClienteServicoConsumo_QNAME = new QName("http://ws.api.smartbill.com.br/", "fecharClienteServicoConsumo");
    private final static QName _AlterarServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarServico");
    private final static QName _ObterArquivo_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterArquivo");
    private final static QName _RemoverRelacionamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerRelacionamento");
    private final static QName _ListarServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarServico");
    private final static QName _AlterarArquivo_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarArquivo");
    private final static QName _ListarTotalServicoAliquotaPrestadorResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalServicoAliquotaPrestadorResponse");
    private final static QName _ObterIndiceEconomicoValor_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterIndiceEconomicoValor");
    private final static QName _ObterIndiceEconomicoValorResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterIndiceEconomicoValorResponse");
    private final static QName _RemoverRamoAtividade_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerRamoAtividade");
    private final static QName _ObterRelacionamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterRelacionamentoResponse");
    private final static QName _ClonarServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "clonarServicoResponse");
    private final static QName _ObterCodigoCNAEResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterCodigoCNAEResponse");
    private final static QName _ObterArquivoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterArquivoResponse");
    private final static QName _AdicionarClienteCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteCR");
    private final static QName _ObterMotivoCancelamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterMotivoCancelamento");
    private final static QName _AlterarArquivoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarArquivoResponse");
    private final static QName _AlterarClienteNFResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteNFResponse");
    private final static QName _RemoverClienteNF_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteNF");
    private final static QName _AdicionarContato_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarContato");
    private final static QName _ListarRamoAtividade_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarRamoAtividade");
    private final static QName _ReabrirClienteServicoConsumoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "reabrirClienteServicoConsumoResponse");
    private final static QName _AlterarEnderecoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarEnderecoResponse");
    private final static QName _RegraClienteServicoImpostos_QNAME = new QName("http://ws.api.smartbill.com.br/", "regraClienteServicoImpostos");
    private final static QName _ObterContatoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterContatoResponse");
    private final static QName _AlterarClienteCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteCRResponse");
    private final static QName _ReabrirClienteServicoConsumo_QNAME = new QName("http://ws.api.smartbill.com.br/", "reabrirClienteServicoConsumo");
    private final static QName _AlterarClienteServicoPorClienteServicoUuid_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteServicoPorClienteServicoUuid");
    private final static QName _ObterSimplesNacionalResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterSimplesNacionalResponse");
    private final static QName _RemoverFamiliaResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerFamiliaResponse");
    private final static QName _ListarRamoAtividadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarRamoAtividadeResponse");
    private final static QName _ObterTemplateEmailPorTipo_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterTemplateEmailPorTipo");
    private final static QName _ObterSimplesNacional_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterSimplesNacional");
    private final static QName _ObterRegra_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterRegra");
    private final static QName _ListarClienteServicoCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteServicoCR");
    private final static QName _AtivarClienteServicoPorClienteServicoUuid_QNAME = new QName("http://ws.api.smartbill.com.br/", "ativarClienteServicoPorClienteServicoUuid");
    private final static QName _ObterTributacaoMunicipioResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterTributacaoMunicipioResponse");
    private final static QName _AlterarFamiliaResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarFamiliaResponse");
    private final static QName _ListarClienteNFResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteNFResponse");
    private final static QName _ObterCidade_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterCidade");
    private final static QName _AlterarCicloFaturamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarCicloFaturamento");
    private final static QName _ObterClienteCRTransacaoBoletoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteCRTransacaoBoletoResponse");
    private final static QName _ListarClienteCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteCRResponse");
    private final static QName _RemoverCicloFaturamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerCicloFaturamento");
    private final static QName _ListarCicloFaturamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarCicloFaturamentoResponse");
    private final static QName _ObterClienteServicoConsumoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteServicoConsumoResponse");
    private final static QName _ObterServicoAliquotaPrestadorResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterServicoAliquotaPrestadorResponse");
    private final static QName _RemoverEndereco_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerEndereco");
    private final static QName _ObterClienteNF_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteNF");
    private final static QName _ListarTemplateEmail_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTemplateEmail");
    private final static QName _AdicionarPeriodicidade_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarPeriodicidade");
    private final static QName _AdicionarRamoAtividade_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarRamoAtividade");
    private final static QName _AdicionarServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarServicoResponse");
    private final static QName _RemoverClienteServicoConsumoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteServicoConsumoResponse");
    private final static QName _SmartbillAPIMessage_QNAME = new QName("http://ws.api.smartbill.com.br/", "SmartbillAPIMessage");
    private final static QName _ListarIndiceEconomicoValorResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarIndiceEconomicoValorResponse");
    private final static QName _ListarUf_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarUf");
    private final static QName _AlterarClienteServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteServico");
    private final static QName _AdicionarClienteResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteResponse");
    private final static QName _AlterarClienteServicoCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteServicoCR");
    private final static QName _ListarMeioPagamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarMeioPagamento");
    private final static QName _ObterNaturezaOperacaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterNaturezaOperacaoResponse");
    private final static QName _AtivarClienteServicoPorClienteServicoUuidResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "ativarClienteServicoPorClienteServicoUuidResponse");
    private final static QName _AlterarClienteServicoCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteServicoCRResponse");
    private final static QName _ObterTemplateEmailPorTipoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterTemplateEmailPorTipoResponse");
    private final static QName _AlterarMotivoCancelamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarMotivoCancelamento");
    private final static QName _ListarRegimeTributario_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarRegimeTributario");
    private final static QName _ListarMotivoCancelamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarMotivoCancelamento");
    private final static QName _ObterTemplateEmailResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterTemplateEmailResponse");
    private final static QName _AdicionarRelacionamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarRelacionamentoResponse");
    private final static QName _AlterarRelacionamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarRelacionamento");
    private final static QName _ConsultarNFsEmissaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "consultarNFsEmissaoResponse");
    private final static QName _RemoverPeriodicidadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerPeriodicidadeResponse");
    private final static QName _ListarPeriodicidade_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarPeriodicidade");
    private final static QName _CancelarNFsEmissaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "cancelarNFsEmissaoResponse");
    private final static QName _RemoverServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerServico");
    private final static QName _ListarServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarServicoResponse");
    private final static QName _AlterarRelacionamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarRelacionamentoResponse");
    private final static QName _ListarTotalClienteCRTransacaoBoleto_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalClienteCRTransacaoBoleto");
    private final static QName _AdicionarClienteCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteCRResponse");
    private final static QName _AlterarClienteServicoConsumo_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteServicoConsumo");
    private final static QName _BaixarCobrancaResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "baixarCobrancaResponse");
    private final static QName _ObterClienteCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteCRResponse");
    private final static QName _ListarServicoAliquotaPrestadorResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarServicoAliquotaPrestadorResponse");
    private final static QName _GerarNFsEmissaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "gerarNFsEmissaoResponse");
    private final static QName _ObterCicloFaturamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterCicloFaturamento");
    private final static QName _ObterMotivoSuspensaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterMotivoSuspensaoResponse");
    private final static QName _ObterRegimeTributario_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterRegimeTributario");
    private final static QName _CancelarNFsEmissao_QNAME = new QName("http://ws.api.smartbill.com.br/", "cancelarNFsEmissao");
    private final static QName _ListarTotalClienteNF_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalClienteNF");
    private final static QName _AdicionarFamiliaResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarFamiliaResponse");
    private final static QName _SolicitarClienteCartaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "solicitarClienteCartaoResponse");
    private final static QName _EnviarNFsEmissaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "enviarNFsEmissaoResponse");
    private final static QName _ObterClienteServicoCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteServicoCR");
    private final static QName _ObterIndiceEconomicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterIndiceEconomicoResponse");
    private final static QName _AdicionarMotivoCancelamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarMotivoCancelamento");
    private final static QName _ListarTributacaoMunicipioResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTributacaoMunicipioResponse");
    private final static QName _RemoverMotivoSuspensaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerMotivoSuspensaoResponse");
    private final static QName _AdicionarEnderecoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarEnderecoResponse");
    private final static QName _RemoverClienteServicoConsumo_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteServicoConsumo");
    private final static QName _ListarClienteServicoConsumoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteServicoConsumoResponse");
    private final static QName _AlterarFamilia_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarFamilia");
    private final static QName _RemoverFamilia_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerFamilia");
    private final static QName _AlterarRamoAtividade_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarRamoAtividade");
    private final static QName _AdicionarClienteNF_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteNF");
    private final static QName _ListarNaturezaOperacaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarNaturezaOperacaoResponse");
    private final static QName _ObterUfResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterUfResponse");
    private final static QName _AlterarClienteServicoConsumoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteServicoConsumoResponse");
    private final static QName _AutorizarClienteCartao_QNAME = new QName("http://ws.api.smartbill.com.br/", "autorizarClienteCartao");
    private final static QName _ObterCicloFaturamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterCicloFaturamentoResponse");
    private final static QName _ConsultarCobrancaResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "consultarCobrancaResponse");
    private final static QName _ListarCicloFaturamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarCicloFaturamento");
    private final static QName _RemoverClienteCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteCRResponse");
    private final static QName _AlterarClienteNF_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteNF");
    private final static QName _ObterPeriodicidade_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterPeriodicidade");
    private final static QName _RemoverEnderecoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerEnderecoResponse");
    private final static QName _EnviarNFsEmissao_QNAME = new QName("http://ws.api.smartbill.com.br/", "enviarNFsEmissao");
    private final static QName _AdicionarCicloFaturamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarCicloFaturamentoResponse");
    private final static QName _BaixarCobranca_QNAME = new QName("http://ws.api.smartbill.com.br/", "baixarCobranca");
    private final static QName _AlterarPeriodicidade_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarPeriodicidade");
    private final static QName _ObterMeioPagamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterMeioPagamento");
    private final static QName _ListarTotalServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalServico");
    private final static QName _ObterCidadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterCidadeResponse");
    private final static QName _ObterEndereco_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterEndereco");
    private final static QName _ListarSegmento_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarSegmento");
    private final static QName _ObterEnderecoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterEnderecoResponse");
    private final static QName _ListarTotalServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalServicoResponse");
    private final static QName _ObterSegmento_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterSegmento");
    private final static QName _ClonarClienteServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "clonarClienteServico");
    private final static QName _ObterGerenteResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterGerenteResponse");
    private final static QName _AdicionarCicloFaturamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarCicloFaturamento");
    private final static QName _ListarCodigoCNAE_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarCodigoCNAE");
    private final static QName _ListarIndiceEconomicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarIndiceEconomicoResponse");
    private final static QName _AlterarMotivoCancelamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarMotivoCancelamentoResponse");
    private final static QName _ObterMotivoSuspensao_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterMotivoSuspensao");
    private final static QName _RemoverRelacionamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerRelacionamentoResponse");
    private final static QName _AlterarClienteServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteServicoResponse");
    private final static QName _ListarSimplesNacionalResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarSimplesNacionalResponse");
    private final static QName _ObterRegraResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterRegraResponse");
    private final static QName _AlterarContato_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarContato");
    private final static QName _AlterarClienteResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteResponse");
    private final static QName _ListarTotalClienteCRTransacaoBoletoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalClienteCRTransacaoBoletoResponse");
    private final static QName _RegraClienteServicoImpostosResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "regraClienteServicoImpostosResponse");
    private final static QName _ObterClienteServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteServicoResponse");
    private final static QName _AlterarMotivoSuspensaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarMotivoSuspensaoResponse");
    private final static QName _EmailCobrancaResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "emailCobrancaResponse");
    private final static QName _ObterNaturezaOperacao_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterNaturezaOperacao");
    private final static QName _AlterarCicloFaturamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarCicloFaturamentoResponse");
    private final static QName _RemoverCliente_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerCliente");
    private final static QName _SuspenderClienteServicoPorClienteServicoUuid_QNAME = new QName("http://ws.api.smartbill.com.br/", "suspenderClienteServicoPorClienteServicoUuid");
    private final static QName _ObterClienteServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteServico");
    private final static QName _ListarIndiceEconomico_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarIndiceEconomico");
    private final static QName _CancelarClienteCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "cancelarClienteCRResponse");
    private final static QName _ReativarCliente_QNAME = new QName("http://ws.api.smartbill.com.br/", "reativarCliente");
    private final static QName _RemoverClienteServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteServicoResponse");
    private final static QName _RemoverServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerServicoResponse");
    private final static QName _ObterTemplateEmail_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterTemplateEmail");
    private final static QName _RemoverClienteCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteCR");
    private final static QName _ReativarClienteResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "reativarClienteResponse");
    private final static QName _FecharClienteServicoConsumoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "fecharClienteServicoConsumoResponse");
    private final static QName _EmailCobranca_QNAME = new QName("http://ws.api.smartbill.com.br/", "emailCobranca");
    private final static QName _ListarSegmentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarSegmentoResponse");
    private final static QName _AdicionarClienteServicoConsumo_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteServicoConsumo");
    private final static QName _ObterClienteCRTransacaoBoleto_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteCRTransacaoBoleto");
    private final static QName _ObterRegimeTributarioResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterRegimeTributarioResponse");
    private final static QName _AdicionarMotivoSuspensao_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarMotivoSuspensao");
    private final static QName _AdicionarArquivoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarArquivoResponse");
    private final static QName _ObterGerente_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterGerente");
    private final static QName _RemoverClienteServicoCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteServicoCR");
    private final static QName _AlterarMotivoSuspensao_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarMotivoSuspensao");
    private final static QName _ListarMotivoSuspensaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarMotivoSuspensaoResponse");
    private final static QName _GerarCobranca_QNAME = new QName("http://ws.api.smartbill.com.br/", "gerarCobranca");
    private final static QName _ObterContato_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterContato");
    private final static QName _AdicionarClienteServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteServico");
    private final static QName _RemoverMotivoSuspensao_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerMotivoSuspensao");
    private final static QName _AdicionarClienteServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteServicoResponse");
    private final static QName _ObterClienteCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteCR");
    private final static QName _RemoverClienteCartaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteCartaoResponse");
    private final static QName _RemoverPeriodicidade_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerPeriodicidade");
    private final static QName _AlterarClienteCRCadastroResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteCRCadastroResponse");
    private final static QName _ListarTributacaoMunicipio_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTributacaoMunicipio");
    private final static QName _ListarCliente_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarCliente");
    private final static QName _RemoverCicloFaturamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerCicloFaturamentoResponse");
    private final static QName _ObterClienteCRCadastro_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteCRCadastro");
    private final static QName _AdicionarRamoAtividadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarRamoAtividadeResponse");
    private final static QName _AdicionarClienteNFResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarClienteNFResponse");
    private final static QName _ListarTotalArquivo_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalArquivo");
    private final static QName _AdicionarRelacionamento_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarRelacionamento");
    private final static QName _ObterConfiguracao_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterConfiguracao");
    private final static QName _ObterPeriodicidadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterPeriodicidadeResponse");
    private final static QName _ListarClienteServico_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteServico");
    private final static QName _ObterUuidUsuario_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterUuidUsuario");
    private final static QName _ObterFamilia_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterFamilia");
    private final static QName _RemoverArquivo_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerArquivo");
    private final static QName _ListarMotivoCancelamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarMotivoCancelamentoResponse");
    private final static QName _AlterarClienteServicoPorClienteServicoUuidResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteServicoPorClienteServicoUuidResponse");
    private final static QName _ObterClienteResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteResponse");
    private final static QName _AdicionarContatoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarContatoResponse");
    private final static QName _AdicionarMotivoCancelamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarMotivoCancelamentoResponse");
    private final static QName _AdicionarPeriodicidadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarPeriodicidadeResponse");
    private final static QName _ListarTotalServicoAliquotaPrestador_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalServicoAliquotaPrestador");
    private final static QName _ListarNaturezaOperacao_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarNaturezaOperacao");
    private final static QName _RemoverClienteResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteResponse");
    private final static QName _ObterRamoAtividadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterRamoAtividadeResponse");
    private final static QName _AdicionarSegmento_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarSegmento");
    private final static QName _ListarContatoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarContatoResponse");
    private final static QName _ListarClienteCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarClienteCR");
    private final static QName _AdicionarArquivo_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarArquivo");
    private final static QName _AlterarServicoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarServicoResponse");
    private final static QName _ListarPeriodicidadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarPeriodicidadeResponse");
    private final static QName _ListarCodigoCNAEResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarCodigoCNAEResponse");
    private final static QName _ListarEnderecoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarEnderecoResponse");
    private final static QName _GerarNFsEmissao_QNAME = new QName("http://ws.api.smartbill.com.br/", "gerarNFsEmissao");
    private final static QName _ObterClienteNFResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteNFResponse");
    private final static QName _ListarRegimeTributarioResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarRegimeTributarioResponse");
    private final static QName _ObterMotivoCancelamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterMotivoCancelamentoResponse");
    private final static QName _AlterarClienteCR_QNAME = new QName("http://ws.api.smartbill.com.br/", "alterarClienteCR");
    private final static QName _AdicionarMotivoSuspensaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "adicionarMotivoSuspensaoResponse");
    private final static QName _RemoverClienteCartao_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteCartao");
    private final static QName _RemoverMotivoCancelamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerMotivoCancelamentoResponse");
    private final static QName _ObterUuidUsuarioResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterUuidUsuarioResponse");
    private final static QName _ListarConfiguracaoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarConfiguracaoResponse");
    private final static QName _RemoverSegmento_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerSegmento");
    private final static QName _ObterClienteServicoConsumo_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteServicoConsumo");
    private final static QName _ListarTotalClienteCRResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarTotalClienteCRResponse");
    private final static QName _RemoverRamoAtividadeResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerRamoAtividadeResponse");
    private final static QName _ObterCodigoCNAE_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterCodigoCNAE");
    private final static QName _RemoverClienteNFResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "removerClienteNFResponse");
    private final static QName _ListarContato_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarContato");
    private final static QName _ListarConfiguracao_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarConfiguracao");
    private final static QName _ListarMeioPagamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarMeioPagamentoResponse");
    private final static QName _ObterClienteCRCadastroResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterClienteCRCadastroResponse");
    private final static QName _ListarIndiceEconomicoValor_QNAME = new QName("http://ws.api.smartbill.com.br/", "listarIndiceEconomicoValor");
    private final static QName _ObterMeioPagamentoResponse_QNAME = new QName("http://ws.api.smartbill.com.br/", "obterMeioPagamentoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.smartbill.api.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AdicionarEndereco }
     * 
     */
    public AdicionarEndereco createAdicionarEndereco() {
        return new AdicionarEndereco();
    }

    /**
     * Create an instance of {@link AdicionarClienteServicoCRResponse }
     * 
     */
    public AdicionarClienteServicoCRResponse createAdicionarClienteServicoCRResponse() {
        return new AdicionarClienteServicoCRResponse();
    }

    /**
     * Create an instance of {@link RemoverClienteServicoCRResponse }
     * 
     */
    public RemoverClienteServicoCRResponse createRemoverClienteServicoCRResponse() {
        return new RemoverClienteServicoCRResponse();
    }

    /**
     * Create an instance of {@link FecharClienteServicoConsumo }
     * 
     */
    public FecharClienteServicoConsumo createFecharClienteServicoConsumo() {
        return new FecharClienteServicoConsumo();
    }

    /**
     * Create an instance of {@link AlterarServico }
     * 
     */
    public AlterarServico createAlterarServico() {
        return new AlterarServico();
    }

    /**
     * Create an instance of {@link ObterArquivo }
     * 
     */
    public ObterArquivo createObterArquivo() {
        return new ObterArquivo();
    }

    /**
     * Create an instance of {@link RemoverRelacionamento }
     * 
     */
    public RemoverRelacionamento createRemoverRelacionamento() {
        return new RemoverRelacionamento();
    }

    /**
     * Create an instance of {@link ListarServico }
     * 
     */
    public ListarServico createListarServico() {
        return new ListarServico();
    }

    /**
     * Create an instance of {@link AlterarArquivo }
     * 
     */
    public AlterarArquivo createAlterarArquivo() {
        return new AlterarArquivo();
    }

    /**
     * Create an instance of {@link ListarTotalServicoAliquotaPrestadorResponse }
     * 
     */
    public ListarTotalServicoAliquotaPrestadorResponse createListarTotalServicoAliquotaPrestadorResponse() {
        return new ListarTotalServicoAliquotaPrestadorResponse();
    }

    /**
     * Create an instance of {@link ObterIndiceEconomicoValor }
     * 
     */
    public ObterIndiceEconomicoValor createObterIndiceEconomicoValor() {
        return new ObterIndiceEconomicoValor();
    }

    /**
     * Create an instance of {@link SuspenderClienteServicoPorClienteServicoUuidResponse }
     * 
     */
    public SuspenderClienteServicoPorClienteServicoUuidResponse createSuspenderClienteServicoPorClienteServicoUuidResponse() {
        return new SuspenderClienteServicoPorClienteServicoUuidResponse();
    }

    /**
     * Create an instance of {@link ListarFamilia }
     * 
     */
    public ListarFamilia createListarFamilia() {
        return new ListarFamilia();
    }

    /**
     * Create an instance of {@link ObterSegmentoResponse }
     * 
     */
    public ObterSegmentoResponse createObterSegmentoResponse() {
        return new ObterSegmentoResponse();
    }

    /**
     * Create an instance of {@link ClonarClienteServicoResponse }
     * 
     */
    public ClonarClienteServicoResponse createClonarClienteServicoResponse() {
        return new ClonarClienteServicoResponse();
    }

    /**
     * Create an instance of {@link ListarServicoAliquotaPrestador }
     * 
     */
    public ListarServicoAliquotaPrestador createListarServicoAliquotaPrestador() {
        return new ListarServicoAliquotaPrestador();
    }

    /**
     * Create an instance of {@link ObterPadraoPreenchimento }
     * 
     */
    public ObterPadraoPreenchimento createObterPadraoPreenchimento() {
        return new ObterPadraoPreenchimento();
    }

    /**
     * Create an instance of {@link ObterRamoAtividade }
     * 
     */
    public ObterRamoAtividade createObterRamoAtividade() {
        return new ObterRamoAtividade();
    }

    /**
     * Create an instance of {@link AdicionarSegmentoResponse }
     * 
     */
    public AdicionarSegmentoResponse createAdicionarSegmentoResponse() {
        return new AdicionarSegmentoResponse();
    }

    /**
     * Create an instance of {@link RemoverSegmentoResponse }
     * 
     */
    public RemoverSegmentoResponse createRemoverSegmentoResponse() {
        return new RemoverSegmentoResponse();
    }

    /**
     * Create an instance of {@link AutorizarClienteCartaoResponse }
     * 
     */
    public AutorizarClienteCartaoResponse createAutorizarClienteCartaoResponse() {
        return new AutorizarClienteCartaoResponse();
    }

    /**
     * Create an instance of {@link ConsultarCobranca }
     * 
     */
    public ConsultarCobranca createConsultarCobranca() {
        return new ConsultarCobranca();
    }

    /**
     * Create an instance of {@link CancelarClienteServicoPorClienteServicoUuidResponse }
     * 
     */
    public CancelarClienteServicoPorClienteServicoUuidResponse createCancelarClienteServicoPorClienteServicoUuidResponse() {
        return new CancelarClienteServicoPorClienteServicoUuidResponse();
    }

    /**
     * Create an instance of {@link ObterFamiliaResponse }
     * 
     */
    public ObterFamiliaResponse createObterFamiliaResponse() {
        return new ObterFamiliaResponse();
    }

    /**
     * Create an instance of {@link ListarClienteServicoConsumo }
     * 
     */
    public ListarClienteServicoConsumo createListarClienteServicoConsumo() {
        return new ListarClienteServicoConsumo();
    }

    /**
     * Create an instance of {@link ListarUfResponse }
     * 
     */
    public ListarUfResponse createListarUfResponse() {
        return new ListarUfResponse();
    }

    /**
     * Create an instance of {@link ListarClienteServicoCRResponse }
     * 
     */
    public ListarClienteServicoCRResponse createListarClienteServicoCRResponse() {
        return new ListarClienteServicoCRResponse();
    }

    /**
     * Create an instance of {@link ObterTributacaoMunicipio }
     * 
     */
    public ObterTributacaoMunicipio createObterTributacaoMunicipio() {
        return new ObterTributacaoMunicipio();
    }

    /**
     * Create an instance of {@link AdicionarClienteServicoCR }
     * 
     */
    public AdicionarClienteServicoCR createAdicionarClienteServicoCR() {
        return new AdicionarClienteServicoCR();
    }

    /**
     * Create an instance of {@link ListarClienteNF }
     * 
     */
    public ListarClienteNF createListarClienteNF() {
        return new ListarClienteNF();
    }

    /**
     * Create an instance of {@link ListarCidade }
     * 
     */
    public ListarCidade createListarCidade() {
        return new ListarCidade();
    }

    /**
     * Create an instance of {@link AlterarClienteCRCadastro }
     * 
     */
    public AlterarClienteCRCadastro createAlterarClienteCRCadastro() {
        return new AlterarClienteCRCadastro();
    }

    /**
     * Create an instance of {@link AdicionarClienteServicoConsumoResponse }
     * 
     */
    public AdicionarClienteServicoConsumoResponse createAdicionarClienteServicoConsumoResponse() {
        return new AdicionarClienteServicoConsumoResponse();
    }

    /**
     * Create an instance of {@link ListarTotalClienteCR }
     * 
     */
    public ListarTotalClienteCR createListarTotalClienteCR() {
        return new ListarTotalClienteCR();
    }

    /**
     * Create an instance of {@link AlterarSegmentoResponse }
     * 
     */
    public AlterarSegmentoResponse createAlterarSegmentoResponse() {
        return new AlterarSegmentoResponse();
    }

    /**
     * Create an instance of {@link ListarClienteResponse }
     * 
     */
    public ListarClienteResponse createListarClienteResponse() {
        return new ListarClienteResponse();
    }

    /**
     * Create an instance of {@link ListarTemplateEmailResponse }
     * 
     */
    public ListarTemplateEmailResponse createListarTemplateEmailResponse() {
        return new ListarTemplateEmailResponse();
    }

    /**
     * Create an instance of {@link ObterCliente }
     * 
     */
    public ObterCliente createObterCliente() {
        return new ObterCliente();
    }

    /**
     * Create an instance of {@link AlterarSegmento }
     * 
     */
    public AlterarSegmento createAlterarSegmento() {
        return new AlterarSegmento();
    }

    /**
     * Create an instance of {@link RemoverContato }
     * 
     */
    public RemoverContato createRemoverContato() {
        return new RemoverContato();
    }

    /**
     * Create an instance of {@link RemoverClienteServico }
     * 
     */
    public RemoverClienteServico createRemoverClienteServico() {
        return new RemoverClienteServico();
    }

    /**
     * Create an instance of {@link ObterServico }
     * 
     */
    public ObterServico createObterServico() {
        return new ObterServico();
    }

    /**
     * Create an instance of {@link AlterarEndereco }
     * 
     */
    public AlterarEndereco createAlterarEndereco() {
        return new AlterarEndereco();
    }

    /**
     * Create an instance of {@link ListarRegraResponse }
     * 
     */
    public ListarRegraResponse createListarRegraResponse() {
        return new ListarRegraResponse();
    }

    /**
     * Create an instance of {@link ListarEndereco }
     * 
     */
    public ListarEndereco createListarEndereco() {
        return new ListarEndereco();
    }

    /**
     * Create an instance of {@link ListarClienteServicoResponse }
     * 
     */
    public ListarClienteServicoResponse createListarClienteServicoResponse() {
        return new ListarClienteServicoResponse();
    }

    /**
     * Create an instance of {@link ObterServicoResponse }
     * 
     */
    public ObterServicoResponse createObterServicoResponse() {
        return new ObterServicoResponse();
    }

    /**
     * Create an instance of {@link ListarSimplesNacional }
     * 
     */
    public ListarSimplesNacional createListarSimplesNacional() {
        return new ListarSimplesNacional();
    }

    /**
     * Create an instance of {@link ObterIndiceEconomico }
     * 
     */
    public ObterIndiceEconomico createObterIndiceEconomico() {
        return new ObterIndiceEconomico();
    }

    /**
     * Create an instance of {@link ObterUf }
     * 
     */
    public ObterUf createObterUf() {
        return new ObterUf();
    }

    /**
     * Create an instance of {@link ListarArquivoResponse }
     * 
     */
    public ListarArquivoResponse createListarArquivoResponse() {
        return new ListarArquivoResponse();
    }

    /**
     * Create an instance of {@link AlterarContatoResponse }
     * 
     */
    public AlterarContatoResponse createAlterarContatoResponse() {
        return new AlterarContatoResponse();
    }

    /**
     * Create an instance of {@link ListarTotalClienteNFResponse }
     * 
     */
    public ListarTotalClienteNFResponse createListarTotalClienteNFResponse() {
        return new ListarTotalClienteNFResponse();
    }

    /**
     * Create an instance of {@link AlterarRamoAtividadeResponse }
     * 
     */
    public AlterarRamoAtividadeResponse createAlterarRamoAtividadeResponse() {
        return new AlterarRamoAtividadeResponse();
    }

    /**
     * Create an instance of {@link ListarCidadeResponse }
     * 
     */
    public ListarCidadeResponse createListarCidadeResponse() {
        return new ListarCidadeResponse();
    }

    /**
     * Create an instance of {@link ListarMotivoSuspensao }
     * 
     */
    public ListarMotivoSuspensao createListarMotivoSuspensao() {
        return new ListarMotivoSuspensao();
    }

    /**
     * Create an instance of {@link ListarRegra }
     * 
     */
    public ListarRegra createListarRegra() {
        return new ListarRegra();
    }

    /**
     * Create an instance of {@link SolicitarClienteCartao }
     * 
     */
    public SolicitarClienteCartao createSolicitarClienteCartao() {
        return new SolicitarClienteCartao();
    }

    /**
     * Create an instance of {@link ObterClienteServicoCRResponse }
     * 
     */
    public ObterClienteServicoCRResponse createObterClienteServicoCRResponse() {
        return new ObterClienteServicoCRResponse();
    }

    /**
     * Create an instance of {@link CancelarClienteCR }
     * 
     */
    public CancelarClienteCR createCancelarClienteCR() {
        return new CancelarClienteCR();
    }

    /**
     * Create an instance of {@link ObterPadraoPreenchimentoResponse }
     * 
     */
    public ObterPadraoPreenchimentoResponse createObterPadraoPreenchimentoResponse() {
        return new ObterPadraoPreenchimentoResponse();
    }

    /**
     * Create an instance of {@link ListarArquivo }
     * 
     */
    public ListarArquivo createListarArquivo() {
        return new ListarArquivo();
    }

    /**
     * Create an instance of {@link CancelarClienteServicoPorClienteServicoUuid }
     * 
     */
    public CancelarClienteServicoPorClienteServicoUuid createCancelarClienteServicoPorClienteServicoUuid() {
        return new CancelarClienteServicoPorClienteServicoUuid();
    }

    /**
     * Create an instance of {@link AdicionarFamilia }
     * 
     */
    public AdicionarFamilia createAdicionarFamilia() {
        return new AdicionarFamilia();
    }

    /**
     * Create an instance of {@link RemoverMotivoCancelamento }
     * 
     */
    public RemoverMotivoCancelamento createRemoverMotivoCancelamento() {
        return new RemoverMotivoCancelamento();
    }

    /**
     * Create an instance of {@link ClonarServico }
     * 
     */
    public ClonarServico createClonarServico() {
        return new ClonarServico();
    }

    /**
     * Create an instance of {@link ListarRelacionamento }
     * 
     */
    public ListarRelacionamento createListarRelacionamento() {
        return new ListarRelacionamento();
    }

    /**
     * Create an instance of {@link AlterarCliente }
     * 
     */
    public AlterarCliente createAlterarCliente() {
        return new AlterarCliente();
    }

    /**
     * Create an instance of {@link ObterRelacionamento }
     * 
     */
    public ObterRelacionamento createObterRelacionamento() {
        return new ObterRelacionamento();
    }

    /**
     * Create an instance of {@link ListarFamiliaResponse }
     * 
     */
    public ListarFamiliaResponse createListarFamiliaResponse() {
        return new ListarFamiliaResponse();
    }

    /**
     * Create an instance of {@link AlterarPeriodicidadeResponse }
     * 
     */
    public AlterarPeriodicidadeResponse createAlterarPeriodicidadeResponse() {
        return new AlterarPeriodicidadeResponse();
    }

    /**
     * Create an instance of {@link GerarCobrancaResponse }
     * 
     */
    public GerarCobrancaResponse createGerarCobrancaResponse() {
        return new GerarCobrancaResponse();
    }

    /**
     * Create an instance of {@link ListarRelacionamentoResponse }
     * 
     */
    public ListarRelacionamentoResponse createListarRelacionamentoResponse() {
        return new ListarRelacionamentoResponse();
    }

    /**
     * Create an instance of {@link ObterServicoAliquotaPrestador }
     * 
     */
    public ObterServicoAliquotaPrestador createObterServicoAliquotaPrestador() {
        return new ObterServicoAliquotaPrestador();
    }

    /**
     * Create an instance of {@link RemoverArquivoResponse }
     * 
     */
    public RemoverArquivoResponse createRemoverArquivoResponse() {
        return new RemoverArquivoResponse();
    }

    /**
     * Create an instance of {@link AdicionarServico }
     * 
     */
    public AdicionarServico createAdicionarServico() {
        return new AdicionarServico();
    }

    /**
     * Create an instance of {@link ObterConfiguracaoResponse }
     * 
     */
    public ObterConfiguracaoResponse createObterConfiguracaoResponse() {
        return new ObterConfiguracaoResponse();
    }

    /**
     * Create an instance of {@link ListarTotalArquivoResponse }
     * 
     */
    public ListarTotalArquivoResponse createListarTotalArquivoResponse() {
        return new ListarTotalArquivoResponse();
    }

    /**
     * Create an instance of {@link AdicionarCliente }
     * 
     */
    public AdicionarCliente createAdicionarCliente() {
        return new AdicionarCliente();
    }

    /**
     * Create an instance of {@link RemoverContatoResponse }
     * 
     */
    public RemoverContatoResponse createRemoverContatoResponse() {
        return new RemoverContatoResponse();
    }

    /**
     * Create an instance of {@link ConsultarNFsEmissao }
     * 
     */
    public ConsultarNFsEmissao createConsultarNFsEmissao() {
        return new ConsultarNFsEmissao();
    }

    /**
     * Create an instance of {@link ListarMotivoCancelamento }
     * 
     */
    public ListarMotivoCancelamento createListarMotivoCancelamento() {
        return new ListarMotivoCancelamento();
    }

    /**
     * Create an instance of {@link ObterTemplateEmailResponse }
     * 
     */
    public ObterTemplateEmailResponse createObterTemplateEmailResponse() {
        return new ObterTemplateEmailResponse();
    }

    /**
     * Create an instance of {@link ConsultarNFsEmissaoResponse }
     * 
     */
    public ConsultarNFsEmissaoResponse createConsultarNFsEmissaoResponse() {
        return new ConsultarNFsEmissaoResponse();
    }

    /**
     * Create an instance of {@link AlterarRelacionamento }
     * 
     */
    public AlterarRelacionamento createAlterarRelacionamento() {
        return new AlterarRelacionamento();
    }

    /**
     * Create an instance of {@link AdicionarRelacionamentoResponse }
     * 
     */
    public AdicionarRelacionamentoResponse createAdicionarRelacionamentoResponse() {
        return new AdicionarRelacionamentoResponse();
    }

    /**
     * Create an instance of {@link RemoverPeriodicidadeResponse }
     * 
     */
    public RemoverPeriodicidadeResponse createRemoverPeriodicidadeResponse() {
        return new RemoverPeriodicidadeResponse();
    }

    /**
     * Create an instance of {@link RemoverServico }
     * 
     */
    public RemoverServico createRemoverServico() {
        return new RemoverServico();
    }

    /**
     * Create an instance of {@link CancelarNFsEmissaoResponse }
     * 
     */
    public CancelarNFsEmissaoResponse createCancelarNFsEmissaoResponse() {
        return new CancelarNFsEmissaoResponse();
    }

    /**
     * Create an instance of {@link ListarPeriodicidade }
     * 
     */
    public ListarPeriodicidade createListarPeriodicidade() {
        return new ListarPeriodicidade();
    }

    /**
     * Create an instance of {@link ListarServicoResponse }
     * 
     */
    public ListarServicoResponse createListarServicoResponse() {
        return new ListarServicoResponse();
    }

    /**
     * Create an instance of {@link AlterarRelacionamentoResponse }
     * 
     */
    public AlterarRelacionamentoResponse createAlterarRelacionamentoResponse() {
        return new AlterarRelacionamentoResponse();
    }

    /**
     * Create an instance of {@link AdicionarClienteCRResponse }
     * 
     */
    public AdicionarClienteCRResponse createAdicionarClienteCRResponse() {
        return new AdicionarClienteCRResponse();
    }

    /**
     * Create an instance of {@link ListarTotalClienteCRTransacaoBoleto }
     * 
     */
    public ListarTotalClienteCRTransacaoBoleto createListarTotalClienteCRTransacaoBoleto() {
        return new ListarTotalClienteCRTransacaoBoleto();
    }

    /**
     * Create an instance of {@link AlterarClienteServicoConsumo }
     * 
     */
    public AlterarClienteServicoConsumo createAlterarClienteServicoConsumo() {
        return new AlterarClienteServicoConsumo();
    }

    /**
     * Create an instance of {@link ListarIndiceEconomicoValorResponse }
     * 
     */
    public ListarIndiceEconomicoValorResponse createListarIndiceEconomicoValorResponse() {
        return new ListarIndiceEconomicoValorResponse();
    }

    /**
     * Create an instance of {@link ListarUf }
     * 
     */
    public ListarUf createListarUf() {
        return new ListarUf();
    }

    /**
     * Create an instance of {@link AlterarClienteServico }
     * 
     */
    public AlterarClienteServico createAlterarClienteServico() {
        return new AlterarClienteServico();
    }

    /**
     * Create an instance of {@link AdicionarClienteResponse }
     * 
     */
    public AdicionarClienteResponse createAdicionarClienteResponse() {
        return new AdicionarClienteResponse();
    }

    /**
     * Create an instance of {@link AlterarClienteServicoCR }
     * 
     */
    public AlterarClienteServicoCR createAlterarClienteServicoCR() {
        return new AlterarClienteServicoCR();
    }

    /**
     * Create an instance of {@link ListarMeioPagamento }
     * 
     */
    public ListarMeioPagamento createListarMeioPagamento() {
        return new ListarMeioPagamento();
    }

    /**
     * Create an instance of {@link ObterNaturezaOperacaoResponse }
     * 
     */
    public ObterNaturezaOperacaoResponse createObterNaturezaOperacaoResponse() {
        return new ObterNaturezaOperacaoResponse();
    }

    /**
     * Create an instance of {@link AtivarClienteServicoPorClienteServicoUuidResponse }
     * 
     */
    public AtivarClienteServicoPorClienteServicoUuidResponse createAtivarClienteServicoPorClienteServicoUuidResponse() {
        return new AtivarClienteServicoPorClienteServicoUuidResponse();
    }

    /**
     * Create an instance of {@link AlterarClienteServicoCRResponse }
     * 
     */
    public AlterarClienteServicoCRResponse createAlterarClienteServicoCRResponse() {
        return new AlterarClienteServicoCRResponse();
    }

    /**
     * Create an instance of {@link ObterTemplateEmailPorTipoResponse }
     * 
     */
    public ObterTemplateEmailPorTipoResponse createObterTemplateEmailPorTipoResponse() {
        return new ObterTemplateEmailPorTipoResponse();
    }

    /**
     * Create an instance of {@link AlterarMotivoCancelamento }
     * 
     */
    public AlterarMotivoCancelamento createAlterarMotivoCancelamento() {
        return new AlterarMotivoCancelamento();
    }

    /**
     * Create an instance of {@link ListarRegimeTributario }
     * 
     */
    public ListarRegimeTributario createListarRegimeTributario() {
        return new ListarRegimeTributario();
    }

    /**
     * Create an instance of {@link AdicionarRamoAtividade }
     * 
     */
    public AdicionarRamoAtividade createAdicionarRamoAtividade() {
        return new AdicionarRamoAtividade();
    }

    /**
     * Create an instance of {@link AdicionarServicoResponse }
     * 
     */
    public AdicionarServicoResponse createAdicionarServicoResponse() {
        return new AdicionarServicoResponse();
    }

    /**
     * Create an instance of {@link RemoverClienteServicoConsumoResponse }
     * 
     */
    public RemoverClienteServicoConsumoResponse createRemoverClienteServicoConsumoResponse() {
        return new RemoverClienteServicoConsumoResponse();
    }

    /**
     * Create an instance of {@link SmartbillAPIMessage }
     * 
     */
    public SmartbillAPIMessage createSmartbillAPIMessage() {
        return new SmartbillAPIMessage();
    }

    /**
     * Create an instance of {@link ObterClienteServicoConsumoResponse }
     * 
     */
    public ObterClienteServicoConsumoResponse createObterClienteServicoConsumoResponse() {
        return new ObterClienteServicoConsumoResponse();
    }

    /**
     * Create an instance of {@link ObterServicoAliquotaPrestadorResponse }
     * 
     */
    public ObterServicoAliquotaPrestadorResponse createObterServicoAliquotaPrestadorResponse() {
        return new ObterServicoAliquotaPrestadorResponse();
    }

    /**
     * Create an instance of {@link RemoverEndereco }
     * 
     */
    public RemoverEndereco createRemoverEndereco() {
        return new RemoverEndereco();
    }

    /**
     * Create an instance of {@link ObterClienteNF }
     * 
     */
    public ObterClienteNF createObterClienteNF() {
        return new ObterClienteNF();
    }

    /**
     * Create an instance of {@link ListarTemplateEmail }
     * 
     */
    public ListarTemplateEmail createListarTemplateEmail() {
        return new ListarTemplateEmail();
    }

    /**
     * Create an instance of {@link AdicionarPeriodicidade }
     * 
     */
    public AdicionarPeriodicidade createAdicionarPeriodicidade() {
        return new AdicionarPeriodicidade();
    }

    /**
     * Create an instance of {@link ListarClienteServicoCR }
     * 
     */
    public ListarClienteServicoCR createListarClienteServicoCR() {
        return new ListarClienteServicoCR();
    }

    /**
     * Create an instance of {@link AtivarClienteServicoPorClienteServicoUuid }
     * 
     */
    public AtivarClienteServicoPorClienteServicoUuid createAtivarClienteServicoPorClienteServicoUuid() {
        return new AtivarClienteServicoPorClienteServicoUuid();
    }

    /**
     * Create an instance of {@link ObterTributacaoMunicipioResponse }
     * 
     */
    public ObterTributacaoMunicipioResponse createObterTributacaoMunicipioResponse() {
        return new ObterTributacaoMunicipioResponse();
    }

    /**
     * Create an instance of {@link AlterarFamiliaResponse }
     * 
     */
    public AlterarFamiliaResponse createAlterarFamiliaResponse() {
        return new AlterarFamiliaResponse();
    }

    /**
     * Create an instance of {@link ListarClienteNFResponse }
     * 
     */
    public ListarClienteNFResponse createListarClienteNFResponse() {
        return new ListarClienteNFResponse();
    }

    /**
     * Create an instance of {@link ObterCidade }
     * 
     */
    public ObterCidade createObterCidade() {
        return new ObterCidade();
    }

    /**
     * Create an instance of {@link AlterarCicloFaturamento }
     * 
     */
    public AlterarCicloFaturamento createAlterarCicloFaturamento() {
        return new AlterarCicloFaturamento();
    }

    /**
     * Create an instance of {@link ObterClienteCRTransacaoBoletoResponse }
     * 
     */
    public ObterClienteCRTransacaoBoletoResponse createObterClienteCRTransacaoBoletoResponse() {
        return new ObterClienteCRTransacaoBoletoResponse();
    }

    /**
     * Create an instance of {@link ListarClienteCRResponse }
     * 
     */
    public ListarClienteCRResponse createListarClienteCRResponse() {
        return new ListarClienteCRResponse();
    }

    /**
     * Create an instance of {@link RemoverCicloFaturamento }
     * 
     */
    public RemoverCicloFaturamento createRemoverCicloFaturamento() {
        return new RemoverCicloFaturamento();
    }

    /**
     * Create an instance of {@link ListarCicloFaturamentoResponse }
     * 
     */
    public ListarCicloFaturamentoResponse createListarCicloFaturamentoResponse() {
        return new ListarCicloFaturamentoResponse();
    }

    /**
     * Create an instance of {@link AlterarClienteCRResponse }
     * 
     */
    public AlterarClienteCRResponse createAlterarClienteCRResponse() {
        return new AlterarClienteCRResponse();
    }

    /**
     * Create an instance of {@link ReabrirClienteServicoConsumo }
     * 
     */
    public ReabrirClienteServicoConsumo createReabrirClienteServicoConsumo() {
        return new ReabrirClienteServicoConsumo();
    }

    /**
     * Create an instance of {@link AlterarClienteServicoPorClienteServicoUuid }
     * 
     */
    public AlterarClienteServicoPorClienteServicoUuid createAlterarClienteServicoPorClienteServicoUuid() {
        return new AlterarClienteServicoPorClienteServicoUuid();
    }

    /**
     * Create an instance of {@link ObterSimplesNacionalResponse }
     * 
     */
    public ObterSimplesNacionalResponse createObterSimplesNacionalResponse() {
        return new ObterSimplesNacionalResponse();
    }

    /**
     * Create an instance of {@link RemoverFamiliaResponse }
     * 
     */
    public RemoverFamiliaResponse createRemoverFamiliaResponse() {
        return new RemoverFamiliaResponse();
    }

    /**
     * Create an instance of {@link ListarRamoAtividadeResponse }
     * 
     */
    public ListarRamoAtividadeResponse createListarRamoAtividadeResponse() {
        return new ListarRamoAtividadeResponse();
    }

    /**
     * Create an instance of {@link ObterTemplateEmailPorTipo }
     * 
     */
    public ObterTemplateEmailPorTipo createObterTemplateEmailPorTipo() {
        return new ObterTemplateEmailPorTipo();
    }

    /**
     * Create an instance of {@link ObterSimplesNacional }
     * 
     */
    public ObterSimplesNacional createObterSimplesNacional() {
        return new ObterSimplesNacional();
    }

    /**
     * Create an instance of {@link ObterRegra }
     * 
     */
    public ObterRegra createObterRegra() {
        return new ObterRegra();
    }

    /**
     * Create an instance of {@link RemoverClienteNF }
     * 
     */
    public RemoverClienteNF createRemoverClienteNF() {
        return new RemoverClienteNF();
    }

    /**
     * Create an instance of {@link AdicionarContato }
     * 
     */
    public AdicionarContato createAdicionarContato() {
        return new AdicionarContato();
    }

    /**
     * Create an instance of {@link ListarRamoAtividade }
     * 
     */
    public ListarRamoAtividade createListarRamoAtividade() {
        return new ListarRamoAtividade();
    }

    /**
     * Create an instance of {@link ReabrirClienteServicoConsumoResponse }
     * 
     */
    public ReabrirClienteServicoConsumoResponse createReabrirClienteServicoConsumoResponse() {
        return new ReabrirClienteServicoConsumoResponse();
    }

    /**
     * Create an instance of {@link AlterarEnderecoResponse }
     * 
     */
    public AlterarEnderecoResponse createAlterarEnderecoResponse() {
        return new AlterarEnderecoResponse();
    }

    /**
     * Create an instance of {@link RegraClienteServicoImpostos }
     * 
     */
    public RegraClienteServicoImpostos createRegraClienteServicoImpostos() {
        return new RegraClienteServicoImpostos();
    }

    /**
     * Create an instance of {@link ObterContatoResponse }
     * 
     */
    public ObterContatoResponse createObterContatoResponse() {
        return new ObterContatoResponse();
    }

    /**
     * Create an instance of {@link ObterIndiceEconomicoValorResponse }
     * 
     */
    public ObterIndiceEconomicoValorResponse createObterIndiceEconomicoValorResponse() {
        return new ObterIndiceEconomicoValorResponse();
    }

    /**
     * Create an instance of {@link RemoverRamoAtividade }
     * 
     */
    public RemoverRamoAtividade createRemoverRamoAtividade() {
        return new RemoverRamoAtividade();
    }

    /**
     * Create an instance of {@link ObterRelacionamentoResponse }
     * 
     */
    public ObterRelacionamentoResponse createObterRelacionamentoResponse() {
        return new ObterRelacionamentoResponse();
    }

    /**
     * Create an instance of {@link ClonarServicoResponse }
     * 
     */
    public ClonarServicoResponse createClonarServicoResponse() {
        return new ClonarServicoResponse();
    }

    /**
     * Create an instance of {@link ObterCodigoCNAEResponse }
     * 
     */
    public ObterCodigoCNAEResponse createObterCodigoCNAEResponse() {
        return new ObterCodigoCNAEResponse();
    }

    /**
     * Create an instance of {@link ObterArquivoResponse }
     * 
     */
    public ObterArquivoResponse createObterArquivoResponse() {
        return new ObterArquivoResponse();
    }

    /**
     * Create an instance of {@link AdicionarClienteCR }
     * 
     */
    public AdicionarClienteCR createAdicionarClienteCR() {
        return new AdicionarClienteCR();
    }

    /**
     * Create an instance of {@link ObterMotivoCancelamento }
     * 
     */
    public ObterMotivoCancelamento createObterMotivoCancelamento() {
        return new ObterMotivoCancelamento();
    }

    /**
     * Create an instance of {@link AlterarArquivoResponse }
     * 
     */
    public AlterarArquivoResponse createAlterarArquivoResponse() {
        return new AlterarArquivoResponse();
    }

    /**
     * Create an instance of {@link AlterarClienteNFResponse }
     * 
     */
    public AlterarClienteNFResponse createAlterarClienteNFResponse() {
        return new AlterarClienteNFResponse();
    }

    /**
     * Create an instance of {@link RemoverRelacionamentoResponse }
     * 
     */
    public RemoverRelacionamentoResponse createRemoverRelacionamentoResponse() {
        return new RemoverRelacionamentoResponse();
    }

    /**
     * Create an instance of {@link AlterarClienteServicoResponse }
     * 
     */
    public AlterarClienteServicoResponse createAlterarClienteServicoResponse() {
        return new AlterarClienteServicoResponse();
    }

    /**
     * Create an instance of {@link ListarSimplesNacionalResponse }
     * 
     */
    public ListarSimplesNacionalResponse createListarSimplesNacionalResponse() {
        return new ListarSimplesNacionalResponse();
    }

    /**
     * Create an instance of {@link ObterRegraResponse }
     * 
     */
    public ObterRegraResponse createObterRegraResponse() {
        return new ObterRegraResponse();
    }

    /**
     * Create an instance of {@link AlterarContato }
     * 
     */
    public AlterarContato createAlterarContato() {
        return new AlterarContato();
    }

    /**
     * Create an instance of {@link AlterarClienteResponse }
     * 
     */
    public AlterarClienteResponse createAlterarClienteResponse() {
        return new AlterarClienteResponse();
    }

    /**
     * Create an instance of {@link ListarTotalClienteCRTransacaoBoletoResponse }
     * 
     */
    public ListarTotalClienteCRTransacaoBoletoResponse createListarTotalClienteCRTransacaoBoletoResponse() {
        return new ListarTotalClienteCRTransacaoBoletoResponse();
    }

    /**
     * Create an instance of {@link RegraClienteServicoImpostosResponse }
     * 
     */
    public RegraClienteServicoImpostosResponse createRegraClienteServicoImpostosResponse() {
        return new RegraClienteServicoImpostosResponse();
    }

    /**
     * Create an instance of {@link ObterClienteServicoResponse }
     * 
     */
    public ObterClienteServicoResponse createObterClienteServicoResponse() {
        return new ObterClienteServicoResponse();
    }

    /**
     * Create an instance of {@link AlterarMotivoSuspensaoResponse }
     * 
     */
    public AlterarMotivoSuspensaoResponse createAlterarMotivoSuspensaoResponse() {
        return new AlterarMotivoSuspensaoResponse();
    }

    /**
     * Create an instance of {@link ListarCodigoCNAE }
     * 
     */
    public ListarCodigoCNAE createListarCodigoCNAE() {
        return new ListarCodigoCNAE();
    }

    /**
     * Create an instance of {@link ListarIndiceEconomicoResponse }
     * 
     */
    public ListarIndiceEconomicoResponse createListarIndiceEconomicoResponse() {
        return new ListarIndiceEconomicoResponse();
    }

    /**
     * Create an instance of {@link AlterarMotivoCancelamentoResponse }
     * 
     */
    public AlterarMotivoCancelamentoResponse createAlterarMotivoCancelamentoResponse() {
        return new AlterarMotivoCancelamentoResponse();
    }

    /**
     * Create an instance of {@link ObterMotivoSuspensao }
     * 
     */
    public ObterMotivoSuspensao createObterMotivoSuspensao() {
        return new ObterMotivoSuspensao();
    }

    /**
     * Create an instance of {@link ObterSegmento }
     * 
     */
    public ObterSegmento createObterSegmento() {
        return new ObterSegmento();
    }

    /**
     * Create an instance of {@link ClonarClienteServico }
     * 
     */
    public ClonarClienteServico createClonarClienteServico() {
        return new ClonarClienteServico();
    }

    /**
     * Create an instance of {@link ObterGerenteResponse }
     * 
     */
    public ObterGerenteResponse createObterGerenteResponse() {
        return new ObterGerenteResponse();
    }

    /**
     * Create an instance of {@link AdicionarCicloFaturamento }
     * 
     */
    public AdicionarCicloFaturamento createAdicionarCicloFaturamento() {
        return new AdicionarCicloFaturamento();
    }

    /**
     * Create an instance of {@link AlterarPeriodicidade }
     * 
     */
    public AlterarPeriodicidade createAlterarPeriodicidade() {
        return new AlterarPeriodicidade();
    }

    /**
     * Create an instance of {@link ObterMeioPagamento }
     * 
     */
    public ObterMeioPagamento createObterMeioPagamento() {
        return new ObterMeioPagamento();
    }

    /**
     * Create an instance of {@link ListarTotalServico }
     * 
     */
    public ListarTotalServico createListarTotalServico() {
        return new ListarTotalServico();
    }

    /**
     * Create an instance of {@link ObterCidadeResponse }
     * 
     */
    public ObterCidadeResponse createObterCidadeResponse() {
        return new ObterCidadeResponse();
    }

    /**
     * Create an instance of {@link ObterEndereco }
     * 
     */
    public ObterEndereco createObterEndereco() {
        return new ObterEndereco();
    }

    /**
     * Create an instance of {@link ListarSegmento }
     * 
     */
    public ListarSegmento createListarSegmento() {
        return new ListarSegmento();
    }

    /**
     * Create an instance of {@link ObterEnderecoResponse }
     * 
     */
    public ObterEnderecoResponse createObterEnderecoResponse() {
        return new ObterEnderecoResponse();
    }

    /**
     * Create an instance of {@link ListarTotalServicoResponse }
     * 
     */
    public ListarTotalServicoResponse createListarTotalServicoResponse() {
        return new ListarTotalServicoResponse();
    }

    /**
     * Create an instance of {@link ObterCicloFaturamentoResponse }
     * 
     */
    public ObterCicloFaturamentoResponse createObterCicloFaturamentoResponse() {
        return new ObterCicloFaturamentoResponse();
    }

    /**
     * Create an instance of {@link ConsultarCobrancaResponse }
     * 
     */
    public ConsultarCobrancaResponse createConsultarCobrancaResponse() {
        return new ConsultarCobrancaResponse();
    }

    /**
     * Create an instance of {@link ListarCicloFaturamento }
     * 
     */
    public ListarCicloFaturamento createListarCicloFaturamento() {
        return new ListarCicloFaturamento();
    }

    /**
     * Create an instance of {@link RemoverClienteCRResponse }
     * 
     */
    public RemoverClienteCRResponse createRemoverClienteCRResponse() {
        return new RemoverClienteCRResponse();
    }

    /**
     * Create an instance of {@link AlterarClienteNF }
     * 
     */
    public AlterarClienteNF createAlterarClienteNF() {
        return new AlterarClienteNF();
    }

    /**
     * Create an instance of {@link ObterPeriodicidade }
     * 
     */
    public ObterPeriodicidade createObterPeriodicidade() {
        return new ObterPeriodicidade();
    }

    /**
     * Create an instance of {@link RemoverEnderecoResponse }
     * 
     */
    public RemoverEnderecoResponse createRemoverEnderecoResponse() {
        return new RemoverEnderecoResponse();
    }

    /**
     * Create an instance of {@link EnviarNFsEmissao }
     * 
     */
    public EnviarNFsEmissao createEnviarNFsEmissao() {
        return new EnviarNFsEmissao();
    }

    /**
     * Create an instance of {@link AdicionarCicloFaturamentoResponse }
     * 
     */
    public AdicionarCicloFaturamentoResponse createAdicionarCicloFaturamentoResponse() {
        return new AdicionarCicloFaturamentoResponse();
    }

    /**
     * Create an instance of {@link BaixarCobranca }
     * 
     */
    public BaixarCobranca createBaixarCobranca() {
        return new BaixarCobranca();
    }

    /**
     * Create an instance of {@link RemoverFamilia }
     * 
     */
    public RemoverFamilia createRemoverFamilia() {
        return new RemoverFamilia();
    }

    /**
     * Create an instance of {@link AlterarRamoAtividade }
     * 
     */
    public AlterarRamoAtividade createAlterarRamoAtividade() {
        return new AlterarRamoAtividade();
    }

    /**
     * Create an instance of {@link AlterarFamilia }
     * 
     */
    public AlterarFamilia createAlterarFamilia() {
        return new AlterarFamilia();
    }

    /**
     * Create an instance of {@link AdicionarClienteNF }
     * 
     */
    public AdicionarClienteNF createAdicionarClienteNF() {
        return new AdicionarClienteNF();
    }

    /**
     * Create an instance of {@link ListarNaturezaOperacaoResponse }
     * 
     */
    public ListarNaturezaOperacaoResponse createListarNaturezaOperacaoResponse() {
        return new ListarNaturezaOperacaoResponse();
    }

    /**
     * Create an instance of {@link ObterUfResponse }
     * 
     */
    public ObterUfResponse createObterUfResponse() {
        return new ObterUfResponse();
    }

    /**
     * Create an instance of {@link AlterarClienteServicoConsumoResponse }
     * 
     */
    public AlterarClienteServicoConsumoResponse createAlterarClienteServicoConsumoResponse() {
        return new AlterarClienteServicoConsumoResponse();
    }

    /**
     * Create an instance of {@link AutorizarClienteCartao }
     * 
     */
    public AutorizarClienteCartao createAutorizarClienteCartao() {
        return new AutorizarClienteCartao();
    }

    /**
     * Create an instance of {@link ObterMotivoSuspensaoResponse }
     * 
     */
    public ObterMotivoSuspensaoResponse createObterMotivoSuspensaoResponse() {
        return new ObterMotivoSuspensaoResponse();
    }

    /**
     * Create an instance of {@link ObterRegimeTributario }
     * 
     */
    public ObterRegimeTributario createObterRegimeTributario() {
        return new ObterRegimeTributario();
    }

    /**
     * Create an instance of {@link CancelarNFsEmissao }
     * 
     */
    public CancelarNFsEmissao createCancelarNFsEmissao() {
        return new CancelarNFsEmissao();
    }

    /**
     * Create an instance of {@link ListarTotalClienteNF }
     * 
     */
    public ListarTotalClienteNF createListarTotalClienteNF() {
        return new ListarTotalClienteNF();
    }

    /**
     * Create an instance of {@link SolicitarClienteCartaoResponse }
     * 
     */
    public SolicitarClienteCartaoResponse createSolicitarClienteCartaoResponse() {
        return new SolicitarClienteCartaoResponse();
    }

    /**
     * Create an instance of {@link AdicionarFamiliaResponse }
     * 
     */
    public AdicionarFamiliaResponse createAdicionarFamiliaResponse() {
        return new AdicionarFamiliaResponse();
    }

    /**
     * Create an instance of {@link ObterIndiceEconomicoResponse }
     * 
     */
    public ObterIndiceEconomicoResponse createObterIndiceEconomicoResponse() {
        return new ObterIndiceEconomicoResponse();
    }

    /**
     * Create an instance of {@link ObterClienteServicoCR }
     * 
     */
    public ObterClienteServicoCR createObterClienteServicoCR() {
        return new ObterClienteServicoCR();
    }

    /**
     * Create an instance of {@link EnviarNFsEmissaoResponse }
     * 
     */
    public EnviarNFsEmissaoResponse createEnviarNFsEmissaoResponse() {
        return new EnviarNFsEmissaoResponse();
    }

    /**
     * Create an instance of {@link AdicionarMotivoCancelamento }
     * 
     */
    public AdicionarMotivoCancelamento createAdicionarMotivoCancelamento() {
        return new AdicionarMotivoCancelamento();
    }

    /**
     * Create an instance of {@link ListarTributacaoMunicipioResponse }
     * 
     */
    public ListarTributacaoMunicipioResponse createListarTributacaoMunicipioResponse() {
        return new ListarTributacaoMunicipioResponse();
    }

    /**
     * Create an instance of {@link RemoverMotivoSuspensaoResponse }
     * 
     */
    public RemoverMotivoSuspensaoResponse createRemoverMotivoSuspensaoResponse() {
        return new RemoverMotivoSuspensaoResponse();
    }

    /**
     * Create an instance of {@link RemoverClienteServicoConsumo }
     * 
     */
    public RemoverClienteServicoConsumo createRemoverClienteServicoConsumo() {
        return new RemoverClienteServicoConsumo();
    }

    /**
     * Create an instance of {@link AdicionarEnderecoResponse }
     * 
     */
    public AdicionarEnderecoResponse createAdicionarEnderecoResponse() {
        return new AdicionarEnderecoResponse();
    }

    /**
     * Create an instance of {@link ListarClienteServicoConsumoResponse }
     * 
     */
    public ListarClienteServicoConsumoResponse createListarClienteServicoConsumoResponse() {
        return new ListarClienteServicoConsumoResponse();
    }

    /**
     * Create an instance of {@link ObterClienteCRResponse }
     * 
     */
    public ObterClienteCRResponse createObterClienteCRResponse() {
        return new ObterClienteCRResponse();
    }

    /**
     * Create an instance of {@link BaixarCobrancaResponse }
     * 
     */
    public BaixarCobrancaResponse createBaixarCobrancaResponse() {
        return new BaixarCobrancaResponse();
    }

    /**
     * Create an instance of {@link ListarServicoAliquotaPrestadorResponse }
     * 
     */
    public ListarServicoAliquotaPrestadorResponse createListarServicoAliquotaPrestadorResponse() {
        return new ListarServicoAliquotaPrestadorResponse();
    }

    /**
     * Create an instance of {@link GerarNFsEmissaoResponse }
     * 
     */
    public GerarNFsEmissaoResponse createGerarNFsEmissaoResponse() {
        return new GerarNFsEmissaoResponse();
    }

    /**
     * Create an instance of {@link ObterCicloFaturamento }
     * 
     */
    public ObterCicloFaturamento createObterCicloFaturamento() {
        return new ObterCicloFaturamento();
    }

    /**
     * Create an instance of {@link RemoverRamoAtividadeResponse }
     * 
     */
    public RemoverRamoAtividadeResponse createRemoverRamoAtividadeResponse() {
        return new RemoverRamoAtividadeResponse();
    }

    /**
     * Create an instance of {@link ListarTotalClienteCRResponse }
     * 
     */
    public ListarTotalClienteCRResponse createListarTotalClienteCRResponse() {
        return new ListarTotalClienteCRResponse();
    }

    /**
     * Create an instance of {@link RemoverClienteNFResponse }
     * 
     */
    public RemoverClienteNFResponse createRemoverClienteNFResponse() {
        return new RemoverClienteNFResponse();
    }

    /**
     * Create an instance of {@link ObterCodigoCNAE }
     * 
     */
    public ObterCodigoCNAE createObterCodigoCNAE() {
        return new ObterCodigoCNAE();
    }

    /**
     * Create an instance of {@link ListarContato }
     * 
     */
    public ListarContato createListarContato() {
        return new ListarContato();
    }

    /**
     * Create an instance of {@link ListarConfiguracao }
     * 
     */
    public ListarConfiguracao createListarConfiguracao() {
        return new ListarConfiguracao();
    }

    /**
     * Create an instance of {@link ListarMeioPagamentoResponse }
     * 
     */
    public ListarMeioPagamentoResponse createListarMeioPagamentoResponse() {
        return new ListarMeioPagamentoResponse();
    }

    /**
     * Create an instance of {@link ListarIndiceEconomicoValor }
     * 
     */
    public ListarIndiceEconomicoValor createListarIndiceEconomicoValor() {
        return new ListarIndiceEconomicoValor();
    }

    /**
     * Create an instance of {@link ObterClienteCRCadastroResponse }
     * 
     */
    public ObterClienteCRCadastroResponse createObterClienteCRCadastroResponse() {
        return new ObterClienteCRCadastroResponse();
    }

    /**
     * Create an instance of {@link ObterMeioPagamentoResponse }
     * 
     */
    public ObterMeioPagamentoResponse createObterMeioPagamentoResponse() {
        return new ObterMeioPagamentoResponse();
    }

    /**
     * Create an instance of {@link ObterUuidUsuarioResponse }
     * 
     */
    public ObterUuidUsuarioResponse createObterUuidUsuarioResponse() {
        return new ObterUuidUsuarioResponse();
    }

    /**
     * Create an instance of {@link ListarConfiguracaoResponse }
     * 
     */
    public ListarConfiguracaoResponse createListarConfiguracaoResponse() {
        return new ListarConfiguracaoResponse();
    }

    /**
     * Create an instance of {@link RemoverSegmento }
     * 
     */
    public RemoverSegmento createRemoverSegmento() {
        return new RemoverSegmento();
    }

    /**
     * Create an instance of {@link ObterClienteServicoConsumo }
     * 
     */
    public ObterClienteServicoConsumo createObterClienteServicoConsumo() {
        return new ObterClienteServicoConsumo();
    }

    /**
     * Create an instance of {@link ListarCodigoCNAEResponse }
     * 
     */
    public ListarCodigoCNAEResponse createListarCodigoCNAEResponse() {
        return new ListarCodigoCNAEResponse();
    }

    /**
     * Create an instance of {@link ListarPeriodicidadeResponse }
     * 
     */
    public ListarPeriodicidadeResponse createListarPeriodicidadeResponse() {
        return new ListarPeriodicidadeResponse();
    }

    /**
     * Create an instance of {@link ListarEnderecoResponse }
     * 
     */
    public ListarEnderecoResponse createListarEnderecoResponse() {
        return new ListarEnderecoResponse();
    }

    /**
     * Create an instance of {@link GerarNFsEmissao }
     * 
     */
    public GerarNFsEmissao createGerarNFsEmissao() {
        return new GerarNFsEmissao();
    }

    /**
     * Create an instance of {@link ObterClienteNFResponse }
     * 
     */
    public ObterClienteNFResponse createObterClienteNFResponse() {
        return new ObterClienteNFResponse();
    }

    /**
     * Create an instance of {@link ObterMotivoCancelamentoResponse }
     * 
     */
    public ObterMotivoCancelamentoResponse createObterMotivoCancelamentoResponse() {
        return new ObterMotivoCancelamentoResponse();
    }

    /**
     * Create an instance of {@link ListarRegimeTributarioResponse }
     * 
     */
    public ListarRegimeTributarioResponse createListarRegimeTributarioResponse() {
        return new ListarRegimeTributarioResponse();
    }

    /**
     * Create an instance of {@link RemoverClienteCartao }
     * 
     */
    public RemoverClienteCartao createRemoverClienteCartao() {
        return new RemoverClienteCartao();
    }

    /**
     * Create an instance of {@link AdicionarMotivoSuspensaoResponse }
     * 
     */
    public AdicionarMotivoSuspensaoResponse createAdicionarMotivoSuspensaoResponse() {
        return new AdicionarMotivoSuspensaoResponse();
    }

    /**
     * Create an instance of {@link AlterarClienteCR }
     * 
     */
    public AlterarClienteCR createAlterarClienteCR() {
        return new AlterarClienteCR();
    }

    /**
     * Create an instance of {@link RemoverMotivoCancelamentoResponse }
     * 
     */
    public RemoverMotivoCancelamentoResponse createRemoverMotivoCancelamentoResponse() {
        return new RemoverMotivoCancelamentoResponse();
    }

    /**
     * Create an instance of {@link AdicionarMotivoCancelamentoResponse }
     * 
     */
    public AdicionarMotivoCancelamentoResponse createAdicionarMotivoCancelamentoResponse() {
        return new AdicionarMotivoCancelamentoResponse();
    }

    /**
     * Create an instance of {@link AdicionarPeriodicidadeResponse }
     * 
     */
    public AdicionarPeriodicidadeResponse createAdicionarPeriodicidadeResponse() {
        return new AdicionarPeriodicidadeResponse();
    }

    /**
     * Create an instance of {@link ListarTotalServicoAliquotaPrestador }
     * 
     */
    public ListarTotalServicoAliquotaPrestador createListarTotalServicoAliquotaPrestador() {
        return new ListarTotalServicoAliquotaPrestador();
    }

    /**
     * Create an instance of {@link ListarNaturezaOperacao }
     * 
     */
    public ListarNaturezaOperacao createListarNaturezaOperacao() {
        return new ListarNaturezaOperacao();
    }

    /**
     * Create an instance of {@link RemoverClienteResponse }
     * 
     */
    public RemoverClienteResponse createRemoverClienteResponse() {
        return new RemoverClienteResponse();
    }

    /**
     * Create an instance of {@link ObterRamoAtividadeResponse }
     * 
     */
    public ObterRamoAtividadeResponse createObterRamoAtividadeResponse() {
        return new ObterRamoAtividadeResponse();
    }

    /**
     * Create an instance of {@link AdicionarSegmento }
     * 
     */
    public AdicionarSegmento createAdicionarSegmento() {
        return new AdicionarSegmento();
    }

    /**
     * Create an instance of {@link ListarContatoResponse }
     * 
     */
    public ListarContatoResponse createListarContatoResponse() {
        return new ListarContatoResponse();
    }

    /**
     * Create an instance of {@link ListarClienteCR }
     * 
     */
    public ListarClienteCR createListarClienteCR() {
        return new ListarClienteCR();
    }

    /**
     * Create an instance of {@link AlterarServicoResponse }
     * 
     */
    public AlterarServicoResponse createAlterarServicoResponse() {
        return new AlterarServicoResponse();
    }

    /**
     * Create an instance of {@link AdicionarArquivo }
     * 
     */
    public AdicionarArquivo createAdicionarArquivo() {
        return new AdicionarArquivo();
    }

    /**
     * Create an instance of {@link ListarCliente }
     * 
     */
    public ListarCliente createListarCliente() {
        return new ListarCliente();
    }

    /**
     * Create an instance of {@link ListarTributacaoMunicipio }
     * 
     */
    public ListarTributacaoMunicipio createListarTributacaoMunicipio() {
        return new ListarTributacaoMunicipio();
    }

    /**
     * Create an instance of {@link RemoverCicloFaturamentoResponse }
     * 
     */
    public RemoverCicloFaturamentoResponse createRemoverCicloFaturamentoResponse() {
        return new RemoverCicloFaturamentoResponse();
    }

    /**
     * Create an instance of {@link AdicionarClienteNFResponse }
     * 
     */
    public AdicionarClienteNFResponse createAdicionarClienteNFResponse() {
        return new AdicionarClienteNFResponse();
    }

    /**
     * Create an instance of {@link AdicionarRamoAtividadeResponse }
     * 
     */
    public AdicionarRamoAtividadeResponse createAdicionarRamoAtividadeResponse() {
        return new AdicionarRamoAtividadeResponse();
    }

    /**
     * Create an instance of {@link ObterClienteCRCadastro }
     * 
     */
    public ObterClienteCRCadastro createObterClienteCRCadastro() {
        return new ObterClienteCRCadastro();
    }

    /**
     * Create an instance of {@link ListarTotalArquivo }
     * 
     */
    public ListarTotalArquivo createListarTotalArquivo() {
        return new ListarTotalArquivo();
    }

    /**
     * Create an instance of {@link ObterPeriodicidadeResponse }
     * 
     */
    public ObterPeriodicidadeResponse createObterPeriodicidadeResponse() {
        return new ObterPeriodicidadeResponse();
    }

    /**
     * Create an instance of {@link ObterConfiguracao }
     * 
     */
    public ObterConfiguracao createObterConfiguracao() {
        return new ObterConfiguracao();
    }

    /**
     * Create an instance of {@link AdicionarRelacionamento }
     * 
     */
    public AdicionarRelacionamento createAdicionarRelacionamento() {
        return new AdicionarRelacionamento();
    }

    /**
     * Create an instance of {@link ListarClienteServico }
     * 
     */
    public ListarClienteServico createListarClienteServico() {
        return new ListarClienteServico();
    }

    /**
     * Create an instance of {@link ObterUuidUsuario }
     * 
     */
    public ObterUuidUsuario createObterUuidUsuario() {
        return new ObterUuidUsuario();
    }

    /**
     * Create an instance of {@link RemoverArquivo }
     * 
     */
    public RemoverArquivo createRemoverArquivo() {
        return new RemoverArquivo();
    }

    /**
     * Create an instance of {@link ObterFamilia }
     * 
     */
    public ObterFamilia createObterFamilia() {
        return new ObterFamilia();
    }

    /**
     * Create an instance of {@link ObterClienteResponse }
     * 
     */
    public ObterClienteResponse createObterClienteResponse() {
        return new ObterClienteResponse();
    }

    /**
     * Create an instance of {@link AlterarClienteServicoPorClienteServicoUuidResponse }
     * 
     */
    public AlterarClienteServicoPorClienteServicoUuidResponse createAlterarClienteServicoPorClienteServicoUuidResponse() {
        return new AlterarClienteServicoPorClienteServicoUuidResponse();
    }

    /**
     * Create an instance of {@link ListarMotivoCancelamentoResponse }
     * 
     */
    public ListarMotivoCancelamentoResponse createListarMotivoCancelamentoResponse() {
        return new ListarMotivoCancelamentoResponse();
    }

    /**
     * Create an instance of {@link AdicionarContatoResponse }
     * 
     */
    public AdicionarContatoResponse createAdicionarContatoResponse() {
        return new AdicionarContatoResponse();
    }

    /**
     * Create an instance of {@link AlterarMotivoSuspensao }
     * 
     */
    public AlterarMotivoSuspensao createAlterarMotivoSuspensao() {
        return new AlterarMotivoSuspensao();
    }

    /**
     * Create an instance of {@link ListarMotivoSuspensaoResponse }
     * 
     */
    public ListarMotivoSuspensaoResponse createListarMotivoSuspensaoResponse() {
        return new ListarMotivoSuspensaoResponse();
    }

    /**
     * Create an instance of {@link GerarCobranca }
     * 
     */
    public GerarCobranca createGerarCobranca() {
        return new GerarCobranca();
    }

    /**
     * Create an instance of {@link ObterContato }
     * 
     */
    public ObterContato createObterContato() {
        return new ObterContato();
    }

    /**
     * Create an instance of {@link AdicionarClienteServico }
     * 
     */
    public AdicionarClienteServico createAdicionarClienteServico() {
        return new AdicionarClienteServico();
    }

    /**
     * Create an instance of {@link RemoverMotivoSuspensao }
     * 
     */
    public RemoverMotivoSuspensao createRemoverMotivoSuspensao() {
        return new RemoverMotivoSuspensao();
    }

    /**
     * Create an instance of {@link AdicionarClienteServicoResponse }
     * 
     */
    public AdicionarClienteServicoResponse createAdicionarClienteServicoResponse() {
        return new AdicionarClienteServicoResponse();
    }

    /**
     * Create an instance of {@link RemoverClienteCartaoResponse }
     * 
     */
    public RemoverClienteCartaoResponse createRemoverClienteCartaoResponse() {
        return new RemoverClienteCartaoResponse();
    }

    /**
     * Create an instance of {@link ObterClienteCR }
     * 
     */
    public ObterClienteCR createObterClienteCR() {
        return new ObterClienteCR();
    }

    /**
     * Create an instance of {@link RemoverPeriodicidade }
     * 
     */
    public RemoverPeriodicidade createRemoverPeriodicidade() {
        return new RemoverPeriodicidade();
    }

    /**
     * Create an instance of {@link AlterarClienteCRCadastroResponse }
     * 
     */
    public AlterarClienteCRCadastroResponse createAlterarClienteCRCadastroResponse() {
        return new AlterarClienteCRCadastroResponse();
    }

    /**
     * Create an instance of {@link RemoverClienteCR }
     * 
     */
    public RemoverClienteCR createRemoverClienteCR() {
        return new RemoverClienteCR();
    }

    /**
     * Create an instance of {@link ReativarClienteResponse }
     * 
     */
    public ReativarClienteResponse createReativarClienteResponse() {
        return new ReativarClienteResponse();
    }

    /**
     * Create an instance of {@link EmailCobranca }
     * 
     */
    public EmailCobranca createEmailCobranca() {
        return new EmailCobranca();
    }

    /**
     * Create an instance of {@link FecharClienteServicoConsumoResponse }
     * 
     */
    public FecharClienteServicoConsumoResponse createFecharClienteServicoConsumoResponse() {
        return new FecharClienteServicoConsumoResponse();
    }

    /**
     * Create an instance of {@link ListarSegmentoResponse }
     * 
     */
    public ListarSegmentoResponse createListarSegmentoResponse() {
        return new ListarSegmentoResponse();
    }

    /**
     * Create an instance of {@link AdicionarClienteServicoConsumo }
     * 
     */
    public AdicionarClienteServicoConsumo createAdicionarClienteServicoConsumo() {
        return new AdicionarClienteServicoConsumo();
    }

    /**
     * Create an instance of {@link ObterRegimeTributarioResponse }
     * 
     */
    public ObterRegimeTributarioResponse createObterRegimeTributarioResponse() {
        return new ObterRegimeTributarioResponse();
    }

    /**
     * Create an instance of {@link ObterClienteCRTransacaoBoleto }
     * 
     */
    public ObterClienteCRTransacaoBoleto createObterClienteCRTransacaoBoleto() {
        return new ObterClienteCRTransacaoBoleto();
    }

    /**
     * Create an instance of {@link ObterGerente }
     * 
     */
    public ObterGerente createObterGerente() {
        return new ObterGerente();
    }

    /**
     * Create an instance of {@link AdicionarArquivoResponse }
     * 
     */
    public AdicionarArquivoResponse createAdicionarArquivoResponse() {
        return new AdicionarArquivoResponse();
    }

    /**
     * Create an instance of {@link AdicionarMotivoSuspensao }
     * 
     */
    public AdicionarMotivoSuspensao createAdicionarMotivoSuspensao() {
        return new AdicionarMotivoSuspensao();
    }

    /**
     * Create an instance of {@link RemoverClienteServicoCR }
     * 
     */
    public RemoverClienteServicoCR createRemoverClienteServicoCR() {
        return new RemoverClienteServicoCR();
    }

    /**
     * Create an instance of {@link EmailCobrancaResponse }
     * 
     */
    public EmailCobrancaResponse createEmailCobrancaResponse() {
        return new EmailCobrancaResponse();
    }

    /**
     * Create an instance of {@link ObterNaturezaOperacao }
     * 
     */
    public ObterNaturezaOperacao createObterNaturezaOperacao() {
        return new ObterNaturezaOperacao();
    }

    /**
     * Create an instance of {@link SuspenderClienteServicoPorClienteServicoUuid }
     * 
     */
    public SuspenderClienteServicoPorClienteServicoUuid createSuspenderClienteServicoPorClienteServicoUuid() {
        return new SuspenderClienteServicoPorClienteServicoUuid();
    }

    /**
     * Create an instance of {@link RemoverCliente }
     * 
     */
    public RemoverCliente createRemoverCliente() {
        return new RemoverCliente();
    }

    /**
     * Create an instance of {@link AlterarCicloFaturamentoResponse }
     * 
     */
    public AlterarCicloFaturamentoResponse createAlterarCicloFaturamentoResponse() {
        return new AlterarCicloFaturamentoResponse();
    }

    /**
     * Create an instance of {@link ObterClienteServico }
     * 
     */
    public ObterClienteServico createObterClienteServico() {
        return new ObterClienteServico();
    }

    /**
     * Create an instance of {@link CancelarClienteCRResponse }
     * 
     */
    public CancelarClienteCRResponse createCancelarClienteCRResponse() {
        return new CancelarClienteCRResponse();
    }

    /**
     * Create an instance of {@link ListarIndiceEconomico }
     * 
     */
    public ListarIndiceEconomico createListarIndiceEconomico() {
        return new ListarIndiceEconomico();
    }

    /**
     * Create an instance of {@link RemoverServicoResponse }
     * 
     */
    public RemoverServicoResponse createRemoverServicoResponse() {
        return new RemoverServicoResponse();
    }

    /**
     * Create an instance of {@link RemoverClienteServicoResponse }
     * 
     */
    public RemoverClienteServicoResponse createRemoverClienteServicoResponse() {
        return new RemoverClienteServicoResponse();
    }

    /**
     * Create an instance of {@link ReativarCliente }
     * 
     */
    public ReativarCliente createReativarCliente() {
        return new ReativarCliente();
    }

    /**
     * Create an instance of {@link ObterTemplateEmail }
     * 
     */
    public ObterTemplateEmail createObterTemplateEmail() {
        return new ObterTemplateEmail();
    }

    /**
     * Create an instance of {@link NaturezaOperacao }
     * 
     */
    public NaturezaOperacao createNaturezaOperacao() {
        return new NaturezaOperacao();
    }

    /**
     * Create an instance of {@link ClientePrestador }
     * 
     */
    public ClientePrestador createClientePrestador() {
        return new ClientePrestador();
    }

    /**
     * Create an instance of {@link SimplesNacional }
     * 
     */
    public SimplesNacional createSimplesNacional() {
        return new SimplesNacional();
    }

    /**
     * Create an instance of {@link ContatoFilter }
     * 
     */
    public ContatoFilter createContatoFilter() {
        return new ContatoFilter();
    }

    /**
     * Create an instance of {@link TributacaoMunicipio }
     * 
     */
    public TributacaoMunicipio createTributacaoMunicipio() {
        return new TributacaoMunicipio();
    }

    /**
     * Create an instance of {@link Cliente }
     * 
     */
    public Cliente createCliente() {
        return new Cliente();
    }

    /**
     * Create an instance of {@link ClienteCR }
     * 
     */
    public ClienteCR createClienteCR() {
        return new ClienteCR();
    }

    /**
     * Create an instance of {@link ClienteServicoValor }
     * 
     */
    public ClienteServicoValor createClienteServicoValor() {
        return new ClienteServicoValor();
    }

    /**
     * Create an instance of {@link RegimeTributarioFilter }
     * 
     */
    public RegimeTributarioFilter createRegimeTributarioFilter() {
        return new RegimeTributarioFilter();
    }

    /**
     * Create an instance of {@link ClienteCRTransacaoCartao }
     * 
     */
    public ClienteCRTransacaoCartao createClienteCRTransacaoCartao() {
        return new ClienteCRTransacaoCartao();
    }

    /**
     * Create an instance of {@link RelacionamentoFilter }
     * 
     */
    public RelacionamentoFilter createRelacionamentoFilter() {
        return new RelacionamentoFilter();
    }

    /**
     * Create an instance of {@link ServicoValor }
     * 
     */
    public ServicoValor createServicoValor() {
        return new ServicoValor();
    }

    /**
     * Create an instance of {@link Serie }
     * 
     */
    public Serie createSerie() {
        return new Serie();
    }

    /**
     * Create an instance of {@link Endereco }
     * 
     */
    public Endereco createEndereco() {
        return new Endereco();
    }

    /**
     * Create an instance of {@link ArquivoFilter }
     * 
     */
    public ArquivoFilter createArquivoFilter() {
        return new ArquivoFilter();
    }

    /**
     * Create an instance of {@link ClienteTomador }
     * 
     */
    public ClienteTomador createClienteTomador() {
        return new ClienteTomador();
    }

    /**
     * Create an instance of {@link ContatoPrestador }
     * 
     */
    public ContatoPrestador createContatoPrestador() {
        return new ContatoPrestador();
    }

    /**
     * Create an instance of {@link EnderecoFilter }
     * 
     */
    public EnderecoFilter createEnderecoFilter() {
        return new EnderecoFilter();
    }

    /**
     * Create an instance of {@link Regra }
     * 
     */
    public Regra createRegra() {
        return new Regra();
    }

    /**
     * Create an instance of {@link Usuario }
     * 
     */
    public Usuario createUsuario() {
        return new Usuario();
    }

    /**
     * Create an instance of {@link Relacionamento }
     * 
     */
    public Relacionamento createRelacionamento() {
        return new Relacionamento();
    }

    /**
     * Create an instance of {@link ClienteCRTransacaoBoleto }
     * 
     */
    public ClienteCRTransacaoBoleto createClienteCRTransacaoBoleto() {
        return new ClienteCRTransacaoBoleto();
    }

    /**
     * Create an instance of {@link ClienteFilter }
     * 
     */
    public ClienteFilter createClienteFilter() {
        return new ClienteFilter();
    }

    /**
     * Create an instance of {@link ClienteServicoDesconto }
     * 
     */
    public ClienteServicoDesconto createClienteServicoDesconto() {
        return new ClienteServicoDesconto();
    }

    /**
     * Create an instance of {@link MeioPagamentoFilter }
     * 
     */
    public MeioPagamentoFilter createMeioPagamentoFilter() {
        return new MeioPagamentoFilter();
    }

    /**
     * Create an instance of {@link Menu }
     * 
     */
    public Menu createMenu() {
        return new Menu();
    }

    /**
     * Create an instance of {@link MeioPagamento }
     * 
     */
    public MeioPagamento createMeioPagamento() {
        return new MeioPagamento();
    }

    /**
     * Create an instance of {@link PadraoPreenchimento }
     * 
     */
    public PadraoPreenchimento createPadraoPreenchimento() {
        return new PadraoPreenchimento();
    }

    /**
     * Create an instance of {@link ClienteServicoParcelaAtivacao }
     * 
     */
    public ClienteServicoParcelaAtivacao createClienteServicoParcelaAtivacao() {
        return new ClienteServicoParcelaAtivacao();
    }

    /**
     * Create an instance of {@link ClienteServicoParcela }
     * 
     */
    public ClienteServicoParcela createClienteServicoParcela() {
        return new ClienteServicoParcela();
    }

    /**
     * Create an instance of {@link Contato }
     * 
     */
    public Contato createContato() {
        return new Contato();
    }

    /**
     * Create an instance of {@link Segmento }
     * 
     */
    public Segmento createSegmento() {
        return new Segmento();
    }

    /**
     * Create an instance of {@link EnderecoPrestador }
     * 
     */
    public EnderecoPrestador createEnderecoPrestador() {
        return new EnderecoPrestador();
    }

    /**
     * Create an instance of {@link ClienteCRCadastro }
     * 
     */
    public ClienteCRCadastro createClienteCRCadastro() {
        return new ClienteCRCadastro();
    }

    /**
     * Create an instance of {@link ClienteServico }
     * 
     */
    public ClienteServico createClienteServico() {
        return new ClienteServico();
    }

    /**
     * Create an instance of {@link Familia }
     * 
     */
    public Familia createFamilia() {
        return new Familia();
    }

    /**
     * Create an instance of {@link ServicoFilter }
     * 
     */
    public ServicoFilter createServicoFilter() {
        return new ServicoFilter();
    }

    /**
     * Create an instance of {@link EnderecoTomador }
     * 
     */
    public EnderecoTomador createEnderecoTomador() {
        return new EnderecoTomador();
    }

    /**
     * Create an instance of {@link ClienteCRTransacao }
     * 
     */
    public ClienteCRTransacao createClienteCRTransacao() {
        return new ClienteCRTransacao();
    }

    /**
     * Create an instance of {@link ClienteServicoParcelaServico }
     * 
     */
    public ClienteServicoParcelaServico createClienteServicoParcelaServico() {
        return new ClienteServicoParcelaServico();
    }

    /**
     * Create an instance of {@link ClienteServicoConsumoQuantidade }
     * 
     */
    public ClienteServicoConsumoQuantidade createClienteServicoConsumoQuantidade() {
        return new ClienteServicoConsumoQuantidade();
    }

    /**
     * Create an instance of {@link ClienteCartao }
     * 
     */
    public ClienteCartao createClienteCartao() {
        return new ClienteCartao();
    }

    /**
     * Create an instance of {@link Cidade }
     * 
     */
    public Cidade createCidade() {
        return new Cidade();
    }

    /**
     * Create an instance of {@link SimplesNacionalFilter }
     * 
     */
    public SimplesNacionalFilter createSimplesNacionalFilter() {
        return new SimplesNacionalFilter();
    }

    /**
     * Create an instance of {@link ServicoAliquotaPrestador }
     * 
     */
    public ServicoAliquotaPrestador createServicoAliquotaPrestador() {
        return new ServicoAliquotaPrestador();
    }

    /**
     * Create an instance of {@link CodigoCNAEFilter }
     * 
     */
    public CodigoCNAEFilter createCodigoCNAEFilter() {
        return new CodigoCNAEFilter();
    }

    /**
     * Create an instance of {@link MotivoCancelamento }
     * 
     */
    public MotivoCancelamento createMotivoCancelamento() {
        return new MotivoCancelamento();
    }

    /**
     * Create an instance of {@link Configuracao }
     * 
     */
    public Configuracao createConfiguracao() {
        return new Configuracao();
    }

    /**
     * Create an instance of {@link ClienteServicoConsumoFilter }
     * 
     */
    public ClienteServicoConsumoFilter createClienteServicoConsumoFilter() {
        return new ClienteServicoConsumoFilter();
    }

    /**
     * Create an instance of {@link ClienteServicoConsumo }
     * 
     */
    public ClienteServicoConsumo createClienteServicoConsumo() {
        return new ClienteServicoConsumo();
    }

    /**
     * Create an instance of {@link TemplateEmail }
     * 
     */
    public TemplateEmail createTemplateEmail() {
        return new TemplateEmail();
    }

    /**
     * Create an instance of {@link IndiceEconomico }
     * 
     */
    public IndiceEconomico createIndiceEconomico() {
        return new IndiceEconomico();
    }

    /**
     * Create an instance of {@link ClienteNFFilter }
     * 
     */
    public ClienteNFFilter createClienteNFFilter() {
        return new ClienteNFFilter();
    }

    /**
     * Create an instance of {@link RegimeTributario }
     * 
     */
    public RegimeTributario createRegimeTributario() {
        return new RegimeTributario();
    }

    /**
     * Create an instance of {@link MotivoSuspensao }
     * 
     */
    public MotivoSuspensao createMotivoSuspensao() {
        return new MotivoSuspensao();
    }

    /**
     * Create an instance of {@link Uf }
     * 
     */
    public Uf createUf() {
        return new Uf();
    }

    /**
     * Create an instance of {@link ClienteNF }
     * 
     */
    public ClienteNF createClienteNF() {
        return new ClienteNF();
    }

    /**
     * Create an instance of {@link UfFilter }
     * 
     */
    public UfFilter createUfFilter() {
        return new UfFilter();
    }

    /**
     * Create an instance of {@link ClienteCRTransacaoBoletoFilter }
     * 
     */
    public ClienteCRTransacaoBoletoFilter createClienteCRTransacaoBoletoFilter() {
        return new ClienteCRTransacaoBoletoFilter();
    }

    /**
     * Create an instance of {@link SmartbillObjetoRetorno }
     * 
     */
    public SmartbillObjetoRetorno createSmartbillObjetoRetorno() {
        return new SmartbillObjetoRetorno();
    }

    /**
     * Create an instance of {@link NaturezaOperacaoFilter }
     * 
     */
    public NaturezaOperacaoFilter createNaturezaOperacaoFilter() {
        return new NaturezaOperacaoFilter();
    }

    /**
     * Create an instance of {@link TributacaoMunicipioFilter }
     * 
     */
    public TributacaoMunicipioFilter createTributacaoMunicipioFilter() {
        return new TributacaoMunicipioFilter();
    }

    /**
     * Create an instance of {@link ClienteServicoFilter }
     * 
     */
    public ClienteServicoFilter createClienteServicoFilter() {
        return new ClienteServicoFilter();
    }

    /**
     * Create an instance of {@link IndiceEconomicoValor }
     * 
     */
    public IndiceEconomicoValor createIndiceEconomicoValor() {
        return new IndiceEconomicoValor();
    }

    /**
     * Create an instance of {@link GeneralFilter }
     * 
     */
    public GeneralFilter createGeneralFilter() {
        return new GeneralFilter();
    }

    /**
     * Create an instance of {@link Lancamento }
     * 
     */
    public Lancamento createLancamento() {
        return new Lancamento();
    }

    /**
     * Create an instance of {@link LoteNF }
     * 
     */
    public LoteNF createLoteNF() {
        return new LoteNF();
    }

    /**
     * Create an instance of {@link ClienteServicoAcrescimo }
     * 
     */
    public ClienteServicoAcrescimo createClienteServicoAcrescimo() {
        return new ClienteServicoAcrescimo();
    }

    /**
     * Create an instance of {@link Servico }
     * 
     */
    public Servico createServico() {
        return new Servico();
    }

    /**
     * Create an instance of {@link ContatoTomador }
     * 
     */
    public ContatoTomador createContatoTomador() {
        return new ContatoTomador();
    }

    /**
     * Create an instance of {@link SubSerie }
     * 
     */
    public SubSerie createSubSerie() {
        return new SubSerie();
    }

    /**
     * Create an instance of {@link ClienteServicoCR }
     * 
     */
    public ClienteServicoCR createClienteServicoCR() {
        return new ClienteServicoCR();
    }

    /**
     * Create an instance of {@link Periodicidade }
     * 
     */
    public Periodicidade createPeriodicidade() {
        return new Periodicidade();
    }

    /**
     * Create an instance of {@link ServicoAliquotaTomador }
     * 
     */
    public ServicoAliquotaTomador createServicoAliquotaTomador() {
        return new ServicoAliquotaTomador();
    }

    /**
     * Create an instance of {@link ClienteServicoCRFilter }
     * 
     */
    public ClienteServicoCRFilter createClienteServicoCRFilter() {
        return new ClienteServicoCRFilter();
    }

    /**
     * Create an instance of {@link ClienteCRFilter }
     * 
     */
    public ClienteCRFilter createClienteCRFilter() {
        return new ClienteCRFilter();
    }

    /**
     * Create an instance of {@link CopyFields }
     * 
     */
    public CopyFields createCopyFields() {
        return new CopyFields();
    }

    /**
     * Create an instance of {@link CicloFaturamento }
     * 
     */
    public CicloFaturamento createCicloFaturamento() {
        return new CicloFaturamento();
    }

    /**
     * Create an instance of {@link PeriodicidadeFilter }
     * 
     */
    public PeriodicidadeFilter createPeriodicidadeFilter() {
        return new PeriodicidadeFilter();
    }

    /**
     * Create an instance of {@link CidadeFilter }
     * 
     */
    public CidadeFilter createCidadeFilter() {
        return new CidadeFilter();
    }

    /**
     * Create an instance of {@link RamoAtividade }
     * 
     */
    public RamoAtividade createRamoAtividade() {
        return new RamoAtividade();
    }

    /**
     * Create an instance of {@link Arquivo }
     * 
     */
    public Arquivo createArquivo() {
        return new Arquivo();
    }

    /**
     * Create an instance of {@link CodigoCNAE }
     * 
     */
    public CodigoCNAE createCodigoCNAE() {
        return new CodigoCNAE();
    }

    /**
     * Create an instance of {@link ServicoAliquotaPrestadorFilter }
     * 
     */
    public ServicoAliquotaPrestadorFilter createServicoAliquotaPrestadorFilter() {
        return new ServicoAliquotaPrestadorFilter();
    }

    /**
     * Create an instance of {@link ClienteCRTransacaoDeposito }
     * 
     */
    public ClienteCRTransacaoDeposito createClienteCRTransacaoDeposito() {
        return new ClienteCRTransacaoDeposito();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterServicoAliquotaPrestador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterServicoAliquotaPrestador")
    public JAXBElement<ObterServicoAliquotaPrestador> createObterServicoAliquotaPrestador(ObterServicoAliquotaPrestador value) {
        return new JAXBElement<ObterServicoAliquotaPrestador>(_ObterServicoAliquotaPrestador_QNAME, ObterServicoAliquotaPrestador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarServico")
    public JAXBElement<AdicionarServico> createAdicionarServico(AdicionarServico value) {
        return new JAXBElement<AdicionarServico>(_AdicionarServico_QNAME, AdicionarServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverArquivoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerArquivoResponse")
    public JAXBElement<RemoverArquivoResponse> createRemoverArquivoResponse(RemoverArquivoResponse value) {
        return new JAXBElement<RemoverArquivoResponse>(_RemoverArquivoResponse_QNAME, RemoverArquivoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterConfiguracaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterConfiguracaoResponse")
    public JAXBElement<ObterConfiguracaoResponse> createObterConfiguracaoResponse(ObterConfiguracaoResponse value) {
        return new JAXBElement<ObterConfiguracaoResponse>(_ObterConfiguracaoResponse_QNAME, ObterConfiguracaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalArquivoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalArquivoResponse")
    public JAXBElement<ListarTotalArquivoResponse> createListarTotalArquivoResponse(ListarTotalArquivoResponse value) {
        return new JAXBElement<ListarTotalArquivoResponse>(_ListarTotalArquivoResponse_QNAME, ListarTotalArquivoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarCliente")
    public JAXBElement<AdicionarCliente> createAdicionarCliente(AdicionarCliente value) {
        return new JAXBElement<AdicionarCliente>(_AdicionarCliente_QNAME, AdicionarCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverContatoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerContatoResponse")
    public JAXBElement<RemoverContatoResponse> createRemoverContatoResponse(RemoverContatoResponse value) {
        return new JAXBElement<RemoverContatoResponse>(_RemoverContatoResponse_QNAME, RemoverContatoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarNFsEmissao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "consultarNFsEmissao")
    public JAXBElement<ConsultarNFsEmissao> createConsultarNFsEmissao(ConsultarNFsEmissao value) {
        return new JAXBElement<ConsultarNFsEmissao>(_ConsultarNFsEmissao_QNAME, ConsultarNFsEmissao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverMotivoCancelamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerMotivoCancelamento")
    public JAXBElement<RemoverMotivoCancelamento> createRemoverMotivoCancelamento(RemoverMotivoCancelamento value) {
        return new JAXBElement<RemoverMotivoCancelamento>(_RemoverMotivoCancelamento_QNAME, RemoverMotivoCancelamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClonarServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "clonarServico")
    public JAXBElement<ClonarServico> createClonarServico(ClonarServico value) {
        return new JAXBElement<ClonarServico>(_ClonarServico_QNAME, ClonarServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRelacionamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarRelacionamento")
    public JAXBElement<ListarRelacionamento> createListarRelacionamento(ListarRelacionamento value) {
        return new JAXBElement<ListarRelacionamento>(_ListarRelacionamento_QNAME, ListarRelacionamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarCliente")
    public JAXBElement<AlterarCliente> createAlterarCliente(AlterarCliente value) {
        return new JAXBElement<AlterarCliente>(_AlterarCliente_QNAME, AlterarCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarFamiliaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarFamiliaResponse")
    public JAXBElement<ListarFamiliaResponse> createListarFamiliaResponse(ListarFamiliaResponse value) {
        return new JAXBElement<ListarFamiliaResponse>(_ListarFamiliaResponse_QNAME, ListarFamiliaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterRelacionamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterRelacionamento")
    public JAXBElement<ObterRelacionamento> createObterRelacionamento(ObterRelacionamento value) {
        return new JAXBElement<ObterRelacionamento>(_ObterRelacionamento_QNAME, ObterRelacionamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GerarCobrancaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "gerarCobrancaResponse")
    public JAXBElement<GerarCobrancaResponse> createGerarCobrancaResponse(GerarCobrancaResponse value) {
        return new JAXBElement<GerarCobrancaResponse>(_GerarCobrancaResponse_QNAME, GerarCobrancaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarPeriodicidadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarPeriodicidadeResponse")
    public JAXBElement<AlterarPeriodicidadeResponse> createAlterarPeriodicidadeResponse(AlterarPeriodicidadeResponse value) {
        return new JAXBElement<AlterarPeriodicidadeResponse>(_AlterarPeriodicidadeResponse_QNAME, AlterarPeriodicidadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRelacionamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarRelacionamentoResponse")
    public JAXBElement<ListarRelacionamentoResponse> createListarRelacionamentoResponse(ListarRelacionamentoResponse value) {
        return new JAXBElement<ListarRelacionamentoResponse>(_ListarRelacionamentoResponse_QNAME, ListarRelacionamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterIndiceEconomico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterIndiceEconomico")
    public JAXBElement<ObterIndiceEconomico> createObterIndiceEconomico(ObterIndiceEconomico value) {
        return new JAXBElement<ObterIndiceEconomico>(_ObterIndiceEconomico_QNAME, ObterIndiceEconomico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarArquivoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarArquivoResponse")
    public JAXBElement<ListarArquivoResponse> createListarArquivoResponse(ListarArquivoResponse value) {
        return new JAXBElement<ListarArquivoResponse>(_ListarArquivoResponse_QNAME, ListarArquivoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterUf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterUf")
    public JAXBElement<ObterUf> createObterUf(ObterUf value) {
        return new JAXBElement<ObterUf>(_ObterUf_QNAME, ObterUf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalClienteNFResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalClienteNFResponse")
    public JAXBElement<ListarTotalClienteNFResponse> createListarTotalClienteNFResponse(ListarTotalClienteNFResponse value) {
        return new JAXBElement<ListarTotalClienteNFResponse>(_ListarTotalClienteNFResponse_QNAME, ListarTotalClienteNFResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarContatoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarContatoResponse")
    public JAXBElement<AlterarContatoResponse> createAlterarContatoResponse(AlterarContatoResponse value) {
        return new JAXBElement<AlterarContatoResponse>(_AlterarContatoResponse_QNAME, AlterarContatoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarCidadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarCidadeResponse")
    public JAXBElement<ListarCidadeResponse> createListarCidadeResponse(ListarCidadeResponse value) {
        return new JAXBElement<ListarCidadeResponse>(_ListarCidadeResponse_QNAME, ListarCidadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarRamoAtividadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarRamoAtividadeResponse")
    public JAXBElement<AlterarRamoAtividadeResponse> createAlterarRamoAtividadeResponse(AlterarRamoAtividadeResponse value) {
        return new JAXBElement<AlterarRamoAtividadeResponse>(_AlterarRamoAtividadeResponse_QNAME, AlterarRamoAtividadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRegra }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarRegra")
    public JAXBElement<ListarRegra> createListarRegra(ListarRegra value) {
        return new JAXBElement<ListarRegra>(_ListarRegra_QNAME, ListarRegra.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarMotivoSuspensao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarMotivoSuspensao")
    public JAXBElement<ListarMotivoSuspensao> createListarMotivoSuspensao(ListarMotivoSuspensao value) {
        return new JAXBElement<ListarMotivoSuspensao>(_ListarMotivoSuspensao_QNAME, ListarMotivoSuspensao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarClienteCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "cancelarClienteCR")
    public JAXBElement<CancelarClienteCR> createCancelarClienteCR(CancelarClienteCR value) {
        return new JAXBElement<CancelarClienteCR>(_CancelarClienteCR_QNAME, CancelarClienteCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteServicoCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteServicoCRResponse")
    public JAXBElement<ObterClienteServicoCRResponse> createObterClienteServicoCRResponse(ObterClienteServicoCRResponse value) {
        return new JAXBElement<ObterClienteServicoCRResponse>(_ObterClienteServicoCRResponse_QNAME, ObterClienteServicoCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SolicitarClienteCartao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "solicitarClienteCartao")
    public JAXBElement<SolicitarClienteCartao> createSolicitarClienteCartao(SolicitarClienteCartao value) {
        return new JAXBElement<SolicitarClienteCartao>(_SolicitarClienteCartao_QNAME, SolicitarClienteCartao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterPadraoPreenchimentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterPadraoPreenchimentoResponse")
    public JAXBElement<ObterPadraoPreenchimentoResponse> createObterPadraoPreenchimentoResponse(ObterPadraoPreenchimentoResponse value) {
        return new JAXBElement<ObterPadraoPreenchimentoResponse>(_ObterPadraoPreenchimentoResponse_QNAME, ObterPadraoPreenchimentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarArquivo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarArquivo")
    public JAXBElement<ListarArquivo> createListarArquivo(ListarArquivo value) {
        return new JAXBElement<ListarArquivo>(_ListarArquivo_QNAME, ListarArquivo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarClienteServicoPorClienteServicoUuid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "cancelarClienteServicoPorClienteServicoUuid")
    public JAXBElement<CancelarClienteServicoPorClienteServicoUuid> createCancelarClienteServicoPorClienteServicoUuid(CancelarClienteServicoPorClienteServicoUuid value) {
        return new JAXBElement<CancelarClienteServicoPorClienteServicoUuid>(_CancelarClienteServicoPorClienteServicoUuid_QNAME, CancelarClienteServicoPorClienteServicoUuid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarFamilia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarFamilia")
    public JAXBElement<AdicionarFamilia> createAdicionarFamilia(AdicionarFamilia value) {
        return new JAXBElement<AdicionarFamilia>(_AdicionarFamilia_QNAME, AdicionarFamilia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRegraResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarRegraResponse")
    public JAXBElement<ListarRegraResponse> createListarRegraResponse(ListarRegraResponse value) {
        return new JAXBElement<ListarRegraResponse>(_ListarRegraResponse_QNAME, ListarRegraResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarEndereco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarEndereco")
    public JAXBElement<ListarEndereco> createListarEndereco(ListarEndereco value) {
        return new JAXBElement<ListarEndereco>(_ListarEndereco_QNAME, ListarEndereco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteServicoResponse")
    public JAXBElement<ListarClienteServicoResponse> createListarClienteServicoResponse(ListarClienteServicoResponse value) {
        return new JAXBElement<ListarClienteServicoResponse>(_ListarClienteServicoResponse_QNAME, ListarClienteServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterServicoResponse")
    public JAXBElement<ObterServicoResponse> createObterServicoResponse(ObterServicoResponse value) {
        return new JAXBElement<ObterServicoResponse>(_ObterServicoResponse_QNAME, ObterServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarSimplesNacional }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarSimplesNacional")
    public JAXBElement<ListarSimplesNacional> createListarSimplesNacional(ListarSimplesNacional value) {
        return new JAXBElement<ListarSimplesNacional>(_ListarSimplesNacional_QNAME, ListarSimplesNacional.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteServicoCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteServicoCR")
    public JAXBElement<AdicionarClienteServicoCR> createAdicionarClienteServicoCR(AdicionarClienteServicoCR value) {
        return new JAXBElement<AdicionarClienteServicoCR>(_AdicionarClienteServicoCR_QNAME, AdicionarClienteServicoCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteNF }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteNF")
    public JAXBElement<ListarClienteNF> createListarClienteNF(ListarClienteNF value) {
        return new JAXBElement<ListarClienteNF>(_ListarClienteNF_QNAME, ListarClienteNF.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarCidade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarCidade")
    public JAXBElement<ListarCidade> createListarCidade(ListarCidade value) {
        return new JAXBElement<ListarCidade>(_ListarCidade_QNAME, ListarCidade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteCRCadastro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteCRCadastro")
    public JAXBElement<AlterarClienteCRCadastro> createAlterarClienteCRCadastro(AlterarClienteCRCadastro value) {
        return new JAXBElement<AlterarClienteCRCadastro>(_AlterarClienteCRCadastro_QNAME, AlterarClienteCRCadastro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteServicoConsumoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteServicoConsumoResponse")
    public JAXBElement<AdicionarClienteServicoConsumoResponse> createAdicionarClienteServicoConsumoResponse(AdicionarClienteServicoConsumoResponse value) {
        return new JAXBElement<AdicionarClienteServicoConsumoResponse>(_AdicionarClienteServicoConsumoResponse_QNAME, AdicionarClienteServicoConsumoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalClienteCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalClienteCR")
    public JAXBElement<ListarTotalClienteCR> createListarTotalClienteCR(ListarTotalClienteCR value) {
        return new JAXBElement<ListarTotalClienteCR>(_ListarTotalClienteCR_QNAME, ListarTotalClienteCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarSegmentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarSegmentoResponse")
    public JAXBElement<AlterarSegmentoResponse> createAlterarSegmentoResponse(AlterarSegmentoResponse value) {
        return new JAXBElement<AlterarSegmentoResponse>(_AlterarSegmentoResponse_QNAME, AlterarSegmentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteResponse")
    public JAXBElement<ListarClienteResponse> createListarClienteResponse(ListarClienteResponse value) {
        return new JAXBElement<ListarClienteResponse>(_ListarClienteResponse_QNAME, ListarClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTemplateEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTemplateEmailResponse")
    public JAXBElement<ListarTemplateEmailResponse> createListarTemplateEmailResponse(ListarTemplateEmailResponse value) {
        return new JAXBElement<ListarTemplateEmailResponse>(_ListarTemplateEmailResponse_QNAME, ListarTemplateEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterCliente")
    public JAXBElement<ObterCliente> createObterCliente(ObterCliente value) {
        return new JAXBElement<ObterCliente>(_ObterCliente_QNAME, ObterCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarSegmento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarSegmento")
    public JAXBElement<AlterarSegmento> createAlterarSegmento(AlterarSegmento value) {
        return new JAXBElement<AlterarSegmento>(_AlterarSegmento_QNAME, AlterarSegmento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverContato }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerContato")
    public JAXBElement<RemoverContato> createRemoverContato(RemoverContato value) {
        return new JAXBElement<RemoverContato>(_RemoverContato_QNAME, RemoverContato.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteServico")
    public JAXBElement<RemoverClienteServico> createRemoverClienteServico(RemoverClienteServico value) {
        return new JAXBElement<RemoverClienteServico>(_RemoverClienteServico_QNAME, RemoverClienteServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterServico")
    public JAXBElement<ObterServico> createObterServico(ObterServico value) {
        return new JAXBElement<ObterServico>(_ObterServico_QNAME, ObterServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarEndereco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarEndereco")
    public JAXBElement<AlterarEndereco> createAlterarEndereco(AlterarEndereco value) {
        return new JAXBElement<AlterarEndereco>(_AlterarEndereco_QNAME, AlterarEndereco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AutorizarClienteCartaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "autorizarClienteCartaoResponse")
    public JAXBElement<AutorizarClienteCartaoResponse> createAutorizarClienteCartaoResponse(AutorizarClienteCartaoResponse value) {
        return new JAXBElement<AutorizarClienteCartaoResponse>(_AutorizarClienteCartaoResponse_QNAME, AutorizarClienteCartaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverSegmentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerSegmentoResponse")
    public JAXBElement<RemoverSegmentoResponse> createRemoverSegmentoResponse(RemoverSegmentoResponse value) {
        return new JAXBElement<RemoverSegmentoResponse>(_RemoverSegmentoResponse_QNAME, RemoverSegmentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarCobranca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "consultarCobranca")
    public JAXBElement<ConsultarCobranca> createConsultarCobranca(ConsultarCobranca value) {
        return new JAXBElement<ConsultarCobranca>(_ConsultarCobranca_QNAME, ConsultarCobranca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarClienteServicoPorClienteServicoUuidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "cancelarClienteServicoPorClienteServicoUuidResponse")
    public JAXBElement<CancelarClienteServicoPorClienteServicoUuidResponse> createCancelarClienteServicoPorClienteServicoUuidResponse(CancelarClienteServicoPorClienteServicoUuidResponse value) {
        return new JAXBElement<CancelarClienteServicoPorClienteServicoUuidResponse>(_CancelarClienteServicoPorClienteServicoUuidResponse_QNAME, CancelarClienteServicoPorClienteServicoUuidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterFamiliaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterFamiliaResponse")
    public JAXBElement<ObterFamiliaResponse> createObterFamiliaResponse(ObterFamiliaResponse value) {
        return new JAXBElement<ObterFamiliaResponse>(_ObterFamiliaResponse_QNAME, ObterFamiliaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteServicoConsumo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteServicoConsumo")
    public JAXBElement<ListarClienteServicoConsumo> createListarClienteServicoConsumo(ListarClienteServicoConsumo value) {
        return new JAXBElement<ListarClienteServicoConsumo>(_ListarClienteServicoConsumo_QNAME, ListarClienteServicoConsumo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarUfResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarUfResponse")
    public JAXBElement<ListarUfResponse> createListarUfResponse(ListarUfResponse value) {
        return new JAXBElement<ListarUfResponse>(_ListarUfResponse_QNAME, ListarUfResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteServicoCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteServicoCRResponse")
    public JAXBElement<ListarClienteServicoCRResponse> createListarClienteServicoCRResponse(ListarClienteServicoCRResponse value) {
        return new JAXBElement<ListarClienteServicoCRResponse>(_ListarClienteServicoCRResponse_QNAME, ListarClienteServicoCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterTributacaoMunicipio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterTributacaoMunicipio")
    public JAXBElement<ObterTributacaoMunicipio> createObterTributacaoMunicipio(ObterTributacaoMunicipio value) {
        return new JAXBElement<ObterTributacaoMunicipio>(_ObterTributacaoMunicipio_QNAME, ObterTributacaoMunicipio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarFamilia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarFamilia")
    public JAXBElement<ListarFamilia> createListarFamilia(ListarFamilia value) {
        return new JAXBElement<ListarFamilia>(_ListarFamilia_QNAME, ListarFamilia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuspenderClienteServicoPorClienteServicoUuidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "suspenderClienteServicoPorClienteServicoUuidResponse")
    public JAXBElement<SuspenderClienteServicoPorClienteServicoUuidResponse> createSuspenderClienteServicoPorClienteServicoUuidResponse(SuspenderClienteServicoPorClienteServicoUuidResponse value) {
        return new JAXBElement<SuspenderClienteServicoPorClienteServicoUuidResponse>(_SuspenderClienteServicoPorClienteServicoUuidResponse_QNAME, SuspenderClienteServicoPorClienteServicoUuidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterSegmentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterSegmentoResponse")
    public JAXBElement<ObterSegmentoResponse> createObterSegmentoResponse(ObterSegmentoResponse value) {
        return new JAXBElement<ObterSegmentoResponse>(_ObterSegmentoResponse_QNAME, ObterSegmentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClonarClienteServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "clonarClienteServicoResponse")
    public JAXBElement<ClonarClienteServicoResponse> createClonarClienteServicoResponse(ClonarClienteServicoResponse value) {
        return new JAXBElement<ClonarClienteServicoResponse>(_ClonarClienteServicoResponse_QNAME, ClonarClienteServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarServicoAliquotaPrestador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarServicoAliquotaPrestador")
    public JAXBElement<ListarServicoAliquotaPrestador> createListarServicoAliquotaPrestador(ListarServicoAliquotaPrestador value) {
        return new JAXBElement<ListarServicoAliquotaPrestador>(_ListarServicoAliquotaPrestador_QNAME, ListarServicoAliquotaPrestador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterPadraoPreenchimento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterPadraoPreenchimento")
    public JAXBElement<ObterPadraoPreenchimento> createObterPadraoPreenchimento(ObterPadraoPreenchimento value) {
        return new JAXBElement<ObterPadraoPreenchimento>(_ObterPadraoPreenchimento_QNAME, ObterPadraoPreenchimento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterRamoAtividade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterRamoAtividade")
    public JAXBElement<ObterRamoAtividade> createObterRamoAtividade(ObterRamoAtividade value) {
        return new JAXBElement<ObterRamoAtividade>(_ObterRamoAtividade_QNAME, ObterRamoAtividade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarSegmentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarSegmentoResponse")
    public JAXBElement<AdicionarSegmentoResponse> createAdicionarSegmentoResponse(AdicionarSegmentoResponse value) {
        return new JAXBElement<AdicionarSegmentoResponse>(_AdicionarSegmentoResponse_QNAME, AdicionarSegmentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarEndereco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarEndereco")
    public JAXBElement<AdicionarEndereco> createAdicionarEndereco(AdicionarEndereco value) {
        return new JAXBElement<AdicionarEndereco>(_AdicionarEndereco_QNAME, AdicionarEndereco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteServicoCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteServicoCRResponse")
    public JAXBElement<AdicionarClienteServicoCRResponse> createAdicionarClienteServicoCRResponse(AdicionarClienteServicoCRResponse value) {
        return new JAXBElement<AdicionarClienteServicoCRResponse>(_AdicionarClienteServicoCRResponse_QNAME, AdicionarClienteServicoCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteServicoCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteServicoCRResponse")
    public JAXBElement<RemoverClienteServicoCRResponse> createRemoverClienteServicoCRResponse(RemoverClienteServicoCRResponse value) {
        return new JAXBElement<RemoverClienteServicoCRResponse>(_RemoverClienteServicoCRResponse_QNAME, RemoverClienteServicoCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FecharClienteServicoConsumo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "fecharClienteServicoConsumo")
    public JAXBElement<FecharClienteServicoConsumo> createFecharClienteServicoConsumo(FecharClienteServicoConsumo value) {
        return new JAXBElement<FecharClienteServicoConsumo>(_FecharClienteServicoConsumo_QNAME, FecharClienteServicoConsumo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarServico")
    public JAXBElement<AlterarServico> createAlterarServico(AlterarServico value) {
        return new JAXBElement<AlterarServico>(_AlterarServico_QNAME, AlterarServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterArquivo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterArquivo")
    public JAXBElement<ObterArquivo> createObterArquivo(ObterArquivo value) {
        return new JAXBElement<ObterArquivo>(_ObterArquivo_QNAME, ObterArquivo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverRelacionamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerRelacionamento")
    public JAXBElement<RemoverRelacionamento> createRemoverRelacionamento(RemoverRelacionamento value) {
        return new JAXBElement<RemoverRelacionamento>(_RemoverRelacionamento_QNAME, RemoverRelacionamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarServico")
    public JAXBElement<ListarServico> createListarServico(ListarServico value) {
        return new JAXBElement<ListarServico>(_ListarServico_QNAME, ListarServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarArquivo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarArquivo")
    public JAXBElement<AlterarArquivo> createAlterarArquivo(AlterarArquivo value) {
        return new JAXBElement<AlterarArquivo>(_AlterarArquivo_QNAME, AlterarArquivo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalServicoAliquotaPrestadorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalServicoAliquotaPrestadorResponse")
    public JAXBElement<ListarTotalServicoAliquotaPrestadorResponse> createListarTotalServicoAliquotaPrestadorResponse(ListarTotalServicoAliquotaPrestadorResponse value) {
        return new JAXBElement<ListarTotalServicoAliquotaPrestadorResponse>(_ListarTotalServicoAliquotaPrestadorResponse_QNAME, ListarTotalServicoAliquotaPrestadorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterIndiceEconomicoValor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterIndiceEconomicoValor")
    public JAXBElement<ObterIndiceEconomicoValor> createObterIndiceEconomicoValor(ObterIndiceEconomicoValor value) {
        return new JAXBElement<ObterIndiceEconomicoValor>(_ObterIndiceEconomicoValor_QNAME, ObterIndiceEconomicoValor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterIndiceEconomicoValorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterIndiceEconomicoValorResponse")
    public JAXBElement<ObterIndiceEconomicoValorResponse> createObterIndiceEconomicoValorResponse(ObterIndiceEconomicoValorResponse value) {
        return new JAXBElement<ObterIndiceEconomicoValorResponse>(_ObterIndiceEconomicoValorResponse_QNAME, ObterIndiceEconomicoValorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverRamoAtividade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerRamoAtividade")
    public JAXBElement<RemoverRamoAtividade> createRemoverRamoAtividade(RemoverRamoAtividade value) {
        return new JAXBElement<RemoverRamoAtividade>(_RemoverRamoAtividade_QNAME, RemoverRamoAtividade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterRelacionamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterRelacionamentoResponse")
    public JAXBElement<ObterRelacionamentoResponse> createObterRelacionamentoResponse(ObterRelacionamentoResponse value) {
        return new JAXBElement<ObterRelacionamentoResponse>(_ObterRelacionamentoResponse_QNAME, ObterRelacionamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClonarServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "clonarServicoResponse")
    public JAXBElement<ClonarServicoResponse> createClonarServicoResponse(ClonarServicoResponse value) {
        return new JAXBElement<ClonarServicoResponse>(_ClonarServicoResponse_QNAME, ClonarServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterCodigoCNAEResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterCodigoCNAEResponse")
    public JAXBElement<ObterCodigoCNAEResponse> createObterCodigoCNAEResponse(ObterCodigoCNAEResponse value) {
        return new JAXBElement<ObterCodigoCNAEResponse>(_ObterCodigoCNAEResponse_QNAME, ObterCodigoCNAEResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterArquivoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterArquivoResponse")
    public JAXBElement<ObterArquivoResponse> createObterArquivoResponse(ObterArquivoResponse value) {
        return new JAXBElement<ObterArquivoResponse>(_ObterArquivoResponse_QNAME, ObterArquivoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteCR")
    public JAXBElement<AdicionarClienteCR> createAdicionarClienteCR(AdicionarClienteCR value) {
        return new JAXBElement<AdicionarClienteCR>(_AdicionarClienteCR_QNAME, AdicionarClienteCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterMotivoCancelamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterMotivoCancelamento")
    public JAXBElement<ObterMotivoCancelamento> createObterMotivoCancelamento(ObterMotivoCancelamento value) {
        return new JAXBElement<ObterMotivoCancelamento>(_ObterMotivoCancelamento_QNAME, ObterMotivoCancelamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarArquivoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarArquivoResponse")
    public JAXBElement<AlterarArquivoResponse> createAlterarArquivoResponse(AlterarArquivoResponse value) {
        return new JAXBElement<AlterarArquivoResponse>(_AlterarArquivoResponse_QNAME, AlterarArquivoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteNFResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteNFResponse")
    public JAXBElement<AlterarClienteNFResponse> createAlterarClienteNFResponse(AlterarClienteNFResponse value) {
        return new JAXBElement<AlterarClienteNFResponse>(_AlterarClienteNFResponse_QNAME, AlterarClienteNFResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteNF }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteNF")
    public JAXBElement<RemoverClienteNF> createRemoverClienteNF(RemoverClienteNF value) {
        return new JAXBElement<RemoverClienteNF>(_RemoverClienteNF_QNAME, RemoverClienteNF.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarContato }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarContato")
    public JAXBElement<AdicionarContato> createAdicionarContato(AdicionarContato value) {
        return new JAXBElement<AdicionarContato>(_AdicionarContato_QNAME, AdicionarContato.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRamoAtividade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarRamoAtividade")
    public JAXBElement<ListarRamoAtividade> createListarRamoAtividade(ListarRamoAtividade value) {
        return new JAXBElement<ListarRamoAtividade>(_ListarRamoAtividade_QNAME, ListarRamoAtividade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReabrirClienteServicoConsumoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "reabrirClienteServicoConsumoResponse")
    public JAXBElement<ReabrirClienteServicoConsumoResponse> createReabrirClienteServicoConsumoResponse(ReabrirClienteServicoConsumoResponse value) {
        return new JAXBElement<ReabrirClienteServicoConsumoResponse>(_ReabrirClienteServicoConsumoResponse_QNAME, ReabrirClienteServicoConsumoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarEnderecoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarEnderecoResponse")
    public JAXBElement<AlterarEnderecoResponse> createAlterarEnderecoResponse(AlterarEnderecoResponse value) {
        return new JAXBElement<AlterarEnderecoResponse>(_AlterarEnderecoResponse_QNAME, AlterarEnderecoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegraClienteServicoImpostos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "regraClienteServicoImpostos")
    public JAXBElement<RegraClienteServicoImpostos> createRegraClienteServicoImpostos(RegraClienteServicoImpostos value) {
        return new JAXBElement<RegraClienteServicoImpostos>(_RegraClienteServicoImpostos_QNAME, RegraClienteServicoImpostos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterContatoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterContatoResponse")
    public JAXBElement<ObterContatoResponse> createObterContatoResponse(ObterContatoResponse value) {
        return new JAXBElement<ObterContatoResponse>(_ObterContatoResponse_QNAME, ObterContatoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteCRResponse")
    public JAXBElement<AlterarClienteCRResponse> createAlterarClienteCRResponse(AlterarClienteCRResponse value) {
        return new JAXBElement<AlterarClienteCRResponse>(_AlterarClienteCRResponse_QNAME, AlterarClienteCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReabrirClienteServicoConsumo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "reabrirClienteServicoConsumo")
    public JAXBElement<ReabrirClienteServicoConsumo> createReabrirClienteServicoConsumo(ReabrirClienteServicoConsumo value) {
        return new JAXBElement<ReabrirClienteServicoConsumo>(_ReabrirClienteServicoConsumo_QNAME, ReabrirClienteServicoConsumo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteServicoPorClienteServicoUuid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteServicoPorClienteServicoUuid")
    public JAXBElement<AlterarClienteServicoPorClienteServicoUuid> createAlterarClienteServicoPorClienteServicoUuid(AlterarClienteServicoPorClienteServicoUuid value) {
        return new JAXBElement<AlterarClienteServicoPorClienteServicoUuid>(_AlterarClienteServicoPorClienteServicoUuid_QNAME, AlterarClienteServicoPorClienteServicoUuid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterSimplesNacionalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterSimplesNacionalResponse")
    public JAXBElement<ObterSimplesNacionalResponse> createObterSimplesNacionalResponse(ObterSimplesNacionalResponse value) {
        return new JAXBElement<ObterSimplesNacionalResponse>(_ObterSimplesNacionalResponse_QNAME, ObterSimplesNacionalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverFamiliaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerFamiliaResponse")
    public JAXBElement<RemoverFamiliaResponse> createRemoverFamiliaResponse(RemoverFamiliaResponse value) {
        return new JAXBElement<RemoverFamiliaResponse>(_RemoverFamiliaResponse_QNAME, RemoverFamiliaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRamoAtividadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarRamoAtividadeResponse")
    public JAXBElement<ListarRamoAtividadeResponse> createListarRamoAtividadeResponse(ListarRamoAtividadeResponse value) {
        return new JAXBElement<ListarRamoAtividadeResponse>(_ListarRamoAtividadeResponse_QNAME, ListarRamoAtividadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterTemplateEmailPorTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterTemplateEmailPorTipo")
    public JAXBElement<ObterTemplateEmailPorTipo> createObterTemplateEmailPorTipo(ObterTemplateEmailPorTipo value) {
        return new JAXBElement<ObterTemplateEmailPorTipo>(_ObterTemplateEmailPorTipo_QNAME, ObterTemplateEmailPorTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterSimplesNacional }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterSimplesNacional")
    public JAXBElement<ObterSimplesNacional> createObterSimplesNacional(ObterSimplesNacional value) {
        return new JAXBElement<ObterSimplesNacional>(_ObterSimplesNacional_QNAME, ObterSimplesNacional.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterRegra }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterRegra")
    public JAXBElement<ObterRegra> createObterRegra(ObterRegra value) {
        return new JAXBElement<ObterRegra>(_ObterRegra_QNAME, ObterRegra.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteServicoCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteServicoCR")
    public JAXBElement<ListarClienteServicoCR> createListarClienteServicoCR(ListarClienteServicoCR value) {
        return new JAXBElement<ListarClienteServicoCR>(_ListarClienteServicoCR_QNAME, ListarClienteServicoCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AtivarClienteServicoPorClienteServicoUuid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "ativarClienteServicoPorClienteServicoUuid")
    public JAXBElement<AtivarClienteServicoPorClienteServicoUuid> createAtivarClienteServicoPorClienteServicoUuid(AtivarClienteServicoPorClienteServicoUuid value) {
        return new JAXBElement<AtivarClienteServicoPorClienteServicoUuid>(_AtivarClienteServicoPorClienteServicoUuid_QNAME, AtivarClienteServicoPorClienteServicoUuid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterTributacaoMunicipioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterTributacaoMunicipioResponse")
    public JAXBElement<ObterTributacaoMunicipioResponse> createObterTributacaoMunicipioResponse(ObterTributacaoMunicipioResponse value) {
        return new JAXBElement<ObterTributacaoMunicipioResponse>(_ObterTributacaoMunicipioResponse_QNAME, ObterTributacaoMunicipioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarFamiliaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarFamiliaResponse")
    public JAXBElement<AlterarFamiliaResponse> createAlterarFamiliaResponse(AlterarFamiliaResponse value) {
        return new JAXBElement<AlterarFamiliaResponse>(_AlterarFamiliaResponse_QNAME, AlterarFamiliaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteNFResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteNFResponse")
    public JAXBElement<ListarClienteNFResponse> createListarClienteNFResponse(ListarClienteNFResponse value) {
        return new JAXBElement<ListarClienteNFResponse>(_ListarClienteNFResponse_QNAME, ListarClienteNFResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterCidade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterCidade")
    public JAXBElement<ObterCidade> createObterCidade(ObterCidade value) {
        return new JAXBElement<ObterCidade>(_ObterCidade_QNAME, ObterCidade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarCicloFaturamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarCicloFaturamento")
    public JAXBElement<AlterarCicloFaturamento> createAlterarCicloFaturamento(AlterarCicloFaturamento value) {
        return new JAXBElement<AlterarCicloFaturamento>(_AlterarCicloFaturamento_QNAME, AlterarCicloFaturamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteCRTransacaoBoletoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteCRTransacaoBoletoResponse")
    public JAXBElement<ObterClienteCRTransacaoBoletoResponse> createObterClienteCRTransacaoBoletoResponse(ObterClienteCRTransacaoBoletoResponse value) {
        return new JAXBElement<ObterClienteCRTransacaoBoletoResponse>(_ObterClienteCRTransacaoBoletoResponse_QNAME, ObterClienteCRTransacaoBoletoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteCRResponse")
    public JAXBElement<ListarClienteCRResponse> createListarClienteCRResponse(ListarClienteCRResponse value) {
        return new JAXBElement<ListarClienteCRResponse>(_ListarClienteCRResponse_QNAME, ListarClienteCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverCicloFaturamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerCicloFaturamento")
    public JAXBElement<RemoverCicloFaturamento> createRemoverCicloFaturamento(RemoverCicloFaturamento value) {
        return new JAXBElement<RemoverCicloFaturamento>(_RemoverCicloFaturamento_QNAME, RemoverCicloFaturamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarCicloFaturamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarCicloFaturamentoResponse")
    public JAXBElement<ListarCicloFaturamentoResponse> createListarCicloFaturamentoResponse(ListarCicloFaturamentoResponse value) {
        return new JAXBElement<ListarCicloFaturamentoResponse>(_ListarCicloFaturamentoResponse_QNAME, ListarCicloFaturamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteServicoConsumoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteServicoConsumoResponse")
    public JAXBElement<ObterClienteServicoConsumoResponse> createObterClienteServicoConsumoResponse(ObterClienteServicoConsumoResponse value) {
        return new JAXBElement<ObterClienteServicoConsumoResponse>(_ObterClienteServicoConsumoResponse_QNAME, ObterClienteServicoConsumoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterServicoAliquotaPrestadorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterServicoAliquotaPrestadorResponse")
    public JAXBElement<ObterServicoAliquotaPrestadorResponse> createObterServicoAliquotaPrestadorResponse(ObterServicoAliquotaPrestadorResponse value) {
        return new JAXBElement<ObterServicoAliquotaPrestadorResponse>(_ObterServicoAliquotaPrestadorResponse_QNAME, ObterServicoAliquotaPrestadorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverEndereco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerEndereco")
    public JAXBElement<RemoverEndereco> createRemoverEndereco(RemoverEndereco value) {
        return new JAXBElement<RemoverEndereco>(_RemoverEndereco_QNAME, RemoverEndereco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteNF }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteNF")
    public JAXBElement<ObterClienteNF> createObterClienteNF(ObterClienteNF value) {
        return new JAXBElement<ObterClienteNF>(_ObterClienteNF_QNAME, ObterClienteNF.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTemplateEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTemplateEmail")
    public JAXBElement<ListarTemplateEmail> createListarTemplateEmail(ListarTemplateEmail value) {
        return new JAXBElement<ListarTemplateEmail>(_ListarTemplateEmail_QNAME, ListarTemplateEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarPeriodicidade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarPeriodicidade")
    public JAXBElement<AdicionarPeriodicidade> createAdicionarPeriodicidade(AdicionarPeriodicidade value) {
        return new JAXBElement<AdicionarPeriodicidade>(_AdicionarPeriodicidade_QNAME, AdicionarPeriodicidade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarRamoAtividade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarRamoAtividade")
    public JAXBElement<AdicionarRamoAtividade> createAdicionarRamoAtividade(AdicionarRamoAtividade value) {
        return new JAXBElement<AdicionarRamoAtividade>(_AdicionarRamoAtividade_QNAME, AdicionarRamoAtividade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarServicoResponse")
    public JAXBElement<AdicionarServicoResponse> createAdicionarServicoResponse(AdicionarServicoResponse value) {
        return new JAXBElement<AdicionarServicoResponse>(_AdicionarServicoResponse_QNAME, AdicionarServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteServicoConsumoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteServicoConsumoResponse")
    public JAXBElement<RemoverClienteServicoConsumoResponse> createRemoverClienteServicoConsumoResponse(RemoverClienteServicoConsumoResponse value) {
        return new JAXBElement<RemoverClienteServicoConsumoResponse>(_RemoverClienteServicoConsumoResponse_QNAME, RemoverClienteServicoConsumoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SmartbillAPIMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "SmartbillAPIMessage")
    public JAXBElement<SmartbillAPIMessage> createSmartbillAPIMessage(SmartbillAPIMessage value) {
        return new JAXBElement<SmartbillAPIMessage>(_SmartbillAPIMessage_QNAME, SmartbillAPIMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarIndiceEconomicoValorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarIndiceEconomicoValorResponse")
    public JAXBElement<ListarIndiceEconomicoValorResponse> createListarIndiceEconomicoValorResponse(ListarIndiceEconomicoValorResponse value) {
        return new JAXBElement<ListarIndiceEconomicoValorResponse>(_ListarIndiceEconomicoValorResponse_QNAME, ListarIndiceEconomicoValorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarUf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarUf")
    public JAXBElement<ListarUf> createListarUf(ListarUf value) {
        return new JAXBElement<ListarUf>(_ListarUf_QNAME, ListarUf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteServico")
    public JAXBElement<AlterarClienteServico> createAlterarClienteServico(AlterarClienteServico value) {
        return new JAXBElement<AlterarClienteServico>(_AlterarClienteServico_QNAME, AlterarClienteServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteResponse")
    public JAXBElement<AdicionarClienteResponse> createAdicionarClienteResponse(AdicionarClienteResponse value) {
        return new JAXBElement<AdicionarClienteResponse>(_AdicionarClienteResponse_QNAME, AdicionarClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteServicoCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteServicoCR")
    public JAXBElement<AlterarClienteServicoCR> createAlterarClienteServicoCR(AlterarClienteServicoCR value) {
        return new JAXBElement<AlterarClienteServicoCR>(_AlterarClienteServicoCR_QNAME, AlterarClienteServicoCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarMeioPagamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarMeioPagamento")
    public JAXBElement<ListarMeioPagamento> createListarMeioPagamento(ListarMeioPagamento value) {
        return new JAXBElement<ListarMeioPagamento>(_ListarMeioPagamento_QNAME, ListarMeioPagamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterNaturezaOperacaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterNaturezaOperacaoResponse")
    public JAXBElement<ObterNaturezaOperacaoResponse> createObterNaturezaOperacaoResponse(ObterNaturezaOperacaoResponse value) {
        return new JAXBElement<ObterNaturezaOperacaoResponse>(_ObterNaturezaOperacaoResponse_QNAME, ObterNaturezaOperacaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AtivarClienteServicoPorClienteServicoUuidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "ativarClienteServicoPorClienteServicoUuidResponse")
    public JAXBElement<AtivarClienteServicoPorClienteServicoUuidResponse> createAtivarClienteServicoPorClienteServicoUuidResponse(AtivarClienteServicoPorClienteServicoUuidResponse value) {
        return new JAXBElement<AtivarClienteServicoPorClienteServicoUuidResponse>(_AtivarClienteServicoPorClienteServicoUuidResponse_QNAME, AtivarClienteServicoPorClienteServicoUuidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteServicoCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteServicoCRResponse")
    public JAXBElement<AlterarClienteServicoCRResponse> createAlterarClienteServicoCRResponse(AlterarClienteServicoCRResponse value) {
        return new JAXBElement<AlterarClienteServicoCRResponse>(_AlterarClienteServicoCRResponse_QNAME, AlterarClienteServicoCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterTemplateEmailPorTipoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterTemplateEmailPorTipoResponse")
    public JAXBElement<ObterTemplateEmailPorTipoResponse> createObterTemplateEmailPorTipoResponse(ObterTemplateEmailPorTipoResponse value) {
        return new JAXBElement<ObterTemplateEmailPorTipoResponse>(_ObterTemplateEmailPorTipoResponse_QNAME, ObterTemplateEmailPorTipoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarMotivoCancelamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarMotivoCancelamento")
    public JAXBElement<AlterarMotivoCancelamento> createAlterarMotivoCancelamento(AlterarMotivoCancelamento value) {
        return new JAXBElement<AlterarMotivoCancelamento>(_AlterarMotivoCancelamento_QNAME, AlterarMotivoCancelamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRegimeTributario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarRegimeTributario")
    public JAXBElement<ListarRegimeTributario> createListarRegimeTributario(ListarRegimeTributario value) {
        return new JAXBElement<ListarRegimeTributario>(_ListarRegimeTributario_QNAME, ListarRegimeTributario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarMotivoCancelamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarMotivoCancelamento")
    public JAXBElement<ListarMotivoCancelamento> createListarMotivoCancelamento(ListarMotivoCancelamento value) {
        return new JAXBElement<ListarMotivoCancelamento>(_ListarMotivoCancelamento_QNAME, ListarMotivoCancelamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterTemplateEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterTemplateEmailResponse")
    public JAXBElement<ObterTemplateEmailResponse> createObterTemplateEmailResponse(ObterTemplateEmailResponse value) {
        return new JAXBElement<ObterTemplateEmailResponse>(_ObterTemplateEmailResponse_QNAME, ObterTemplateEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarRelacionamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarRelacionamentoResponse")
    public JAXBElement<AdicionarRelacionamentoResponse> createAdicionarRelacionamentoResponse(AdicionarRelacionamentoResponse value) {
        return new JAXBElement<AdicionarRelacionamentoResponse>(_AdicionarRelacionamentoResponse_QNAME, AdicionarRelacionamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarRelacionamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarRelacionamento")
    public JAXBElement<AlterarRelacionamento> createAlterarRelacionamento(AlterarRelacionamento value) {
        return new JAXBElement<AlterarRelacionamento>(_AlterarRelacionamento_QNAME, AlterarRelacionamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarNFsEmissaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "consultarNFsEmissaoResponse")
    public JAXBElement<ConsultarNFsEmissaoResponse> createConsultarNFsEmissaoResponse(ConsultarNFsEmissaoResponse value) {
        return new JAXBElement<ConsultarNFsEmissaoResponse>(_ConsultarNFsEmissaoResponse_QNAME, ConsultarNFsEmissaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverPeriodicidadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerPeriodicidadeResponse")
    public JAXBElement<RemoverPeriodicidadeResponse> createRemoverPeriodicidadeResponse(RemoverPeriodicidadeResponse value) {
        return new JAXBElement<RemoverPeriodicidadeResponse>(_RemoverPeriodicidadeResponse_QNAME, RemoverPeriodicidadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPeriodicidade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarPeriodicidade")
    public JAXBElement<ListarPeriodicidade> createListarPeriodicidade(ListarPeriodicidade value) {
        return new JAXBElement<ListarPeriodicidade>(_ListarPeriodicidade_QNAME, ListarPeriodicidade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarNFsEmissaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "cancelarNFsEmissaoResponse")
    public JAXBElement<CancelarNFsEmissaoResponse> createCancelarNFsEmissaoResponse(CancelarNFsEmissaoResponse value) {
        return new JAXBElement<CancelarNFsEmissaoResponse>(_CancelarNFsEmissaoResponse_QNAME, CancelarNFsEmissaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerServico")
    public JAXBElement<RemoverServico> createRemoverServico(RemoverServico value) {
        return new JAXBElement<RemoverServico>(_RemoverServico_QNAME, RemoverServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarServicoResponse")
    public JAXBElement<ListarServicoResponse> createListarServicoResponse(ListarServicoResponse value) {
        return new JAXBElement<ListarServicoResponse>(_ListarServicoResponse_QNAME, ListarServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarRelacionamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarRelacionamentoResponse")
    public JAXBElement<AlterarRelacionamentoResponse> createAlterarRelacionamentoResponse(AlterarRelacionamentoResponse value) {
        return new JAXBElement<AlterarRelacionamentoResponse>(_AlterarRelacionamentoResponse_QNAME, AlterarRelacionamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalClienteCRTransacaoBoleto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalClienteCRTransacaoBoleto")
    public JAXBElement<ListarTotalClienteCRTransacaoBoleto> createListarTotalClienteCRTransacaoBoleto(ListarTotalClienteCRTransacaoBoleto value) {
        return new JAXBElement<ListarTotalClienteCRTransacaoBoleto>(_ListarTotalClienteCRTransacaoBoleto_QNAME, ListarTotalClienteCRTransacaoBoleto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteCRResponse")
    public JAXBElement<AdicionarClienteCRResponse> createAdicionarClienteCRResponse(AdicionarClienteCRResponse value) {
        return new JAXBElement<AdicionarClienteCRResponse>(_AdicionarClienteCRResponse_QNAME, AdicionarClienteCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteServicoConsumo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteServicoConsumo")
    public JAXBElement<AlterarClienteServicoConsumo> createAlterarClienteServicoConsumo(AlterarClienteServicoConsumo value) {
        return new JAXBElement<AlterarClienteServicoConsumo>(_AlterarClienteServicoConsumo_QNAME, AlterarClienteServicoConsumo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaixarCobrancaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "baixarCobrancaResponse")
    public JAXBElement<BaixarCobrancaResponse> createBaixarCobrancaResponse(BaixarCobrancaResponse value) {
        return new JAXBElement<BaixarCobrancaResponse>(_BaixarCobrancaResponse_QNAME, BaixarCobrancaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteCRResponse")
    public JAXBElement<ObterClienteCRResponse> createObterClienteCRResponse(ObterClienteCRResponse value) {
        return new JAXBElement<ObterClienteCRResponse>(_ObterClienteCRResponse_QNAME, ObterClienteCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarServicoAliquotaPrestadorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarServicoAliquotaPrestadorResponse")
    public JAXBElement<ListarServicoAliquotaPrestadorResponse> createListarServicoAliquotaPrestadorResponse(ListarServicoAliquotaPrestadorResponse value) {
        return new JAXBElement<ListarServicoAliquotaPrestadorResponse>(_ListarServicoAliquotaPrestadorResponse_QNAME, ListarServicoAliquotaPrestadorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GerarNFsEmissaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "gerarNFsEmissaoResponse")
    public JAXBElement<GerarNFsEmissaoResponse> createGerarNFsEmissaoResponse(GerarNFsEmissaoResponse value) {
        return new JAXBElement<GerarNFsEmissaoResponse>(_GerarNFsEmissaoResponse_QNAME, GerarNFsEmissaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterCicloFaturamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterCicloFaturamento")
    public JAXBElement<ObterCicloFaturamento> createObterCicloFaturamento(ObterCicloFaturamento value) {
        return new JAXBElement<ObterCicloFaturamento>(_ObterCicloFaturamento_QNAME, ObterCicloFaturamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterMotivoSuspensaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterMotivoSuspensaoResponse")
    public JAXBElement<ObterMotivoSuspensaoResponse> createObterMotivoSuspensaoResponse(ObterMotivoSuspensaoResponse value) {
        return new JAXBElement<ObterMotivoSuspensaoResponse>(_ObterMotivoSuspensaoResponse_QNAME, ObterMotivoSuspensaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterRegimeTributario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterRegimeTributario")
    public JAXBElement<ObterRegimeTributario> createObterRegimeTributario(ObterRegimeTributario value) {
        return new JAXBElement<ObterRegimeTributario>(_ObterRegimeTributario_QNAME, ObterRegimeTributario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarNFsEmissao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "cancelarNFsEmissao")
    public JAXBElement<CancelarNFsEmissao> createCancelarNFsEmissao(CancelarNFsEmissao value) {
        return new JAXBElement<CancelarNFsEmissao>(_CancelarNFsEmissao_QNAME, CancelarNFsEmissao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalClienteNF }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalClienteNF")
    public JAXBElement<ListarTotalClienteNF> createListarTotalClienteNF(ListarTotalClienteNF value) {
        return new JAXBElement<ListarTotalClienteNF>(_ListarTotalClienteNF_QNAME, ListarTotalClienteNF.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarFamiliaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarFamiliaResponse")
    public JAXBElement<AdicionarFamiliaResponse> createAdicionarFamiliaResponse(AdicionarFamiliaResponse value) {
        return new JAXBElement<AdicionarFamiliaResponse>(_AdicionarFamiliaResponse_QNAME, AdicionarFamiliaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SolicitarClienteCartaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "solicitarClienteCartaoResponse")
    public JAXBElement<SolicitarClienteCartaoResponse> createSolicitarClienteCartaoResponse(SolicitarClienteCartaoResponse value) {
        return new JAXBElement<SolicitarClienteCartaoResponse>(_SolicitarClienteCartaoResponse_QNAME, SolicitarClienteCartaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarNFsEmissaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "enviarNFsEmissaoResponse")
    public JAXBElement<EnviarNFsEmissaoResponse> createEnviarNFsEmissaoResponse(EnviarNFsEmissaoResponse value) {
        return new JAXBElement<EnviarNFsEmissaoResponse>(_EnviarNFsEmissaoResponse_QNAME, EnviarNFsEmissaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteServicoCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteServicoCR")
    public JAXBElement<ObterClienteServicoCR> createObterClienteServicoCR(ObterClienteServicoCR value) {
        return new JAXBElement<ObterClienteServicoCR>(_ObterClienteServicoCR_QNAME, ObterClienteServicoCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterIndiceEconomicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterIndiceEconomicoResponse")
    public JAXBElement<ObterIndiceEconomicoResponse> createObterIndiceEconomicoResponse(ObterIndiceEconomicoResponse value) {
        return new JAXBElement<ObterIndiceEconomicoResponse>(_ObterIndiceEconomicoResponse_QNAME, ObterIndiceEconomicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarMotivoCancelamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarMotivoCancelamento")
    public JAXBElement<AdicionarMotivoCancelamento> createAdicionarMotivoCancelamento(AdicionarMotivoCancelamento value) {
        return new JAXBElement<AdicionarMotivoCancelamento>(_AdicionarMotivoCancelamento_QNAME, AdicionarMotivoCancelamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTributacaoMunicipioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTributacaoMunicipioResponse")
    public JAXBElement<ListarTributacaoMunicipioResponse> createListarTributacaoMunicipioResponse(ListarTributacaoMunicipioResponse value) {
        return new JAXBElement<ListarTributacaoMunicipioResponse>(_ListarTributacaoMunicipioResponse_QNAME, ListarTributacaoMunicipioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverMotivoSuspensaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerMotivoSuspensaoResponse")
    public JAXBElement<RemoverMotivoSuspensaoResponse> createRemoverMotivoSuspensaoResponse(RemoverMotivoSuspensaoResponse value) {
        return new JAXBElement<RemoverMotivoSuspensaoResponse>(_RemoverMotivoSuspensaoResponse_QNAME, RemoverMotivoSuspensaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarEnderecoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarEnderecoResponse")
    public JAXBElement<AdicionarEnderecoResponse> createAdicionarEnderecoResponse(AdicionarEnderecoResponse value) {
        return new JAXBElement<AdicionarEnderecoResponse>(_AdicionarEnderecoResponse_QNAME, AdicionarEnderecoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteServicoConsumo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteServicoConsumo")
    public JAXBElement<RemoverClienteServicoConsumo> createRemoverClienteServicoConsumo(RemoverClienteServicoConsumo value) {
        return new JAXBElement<RemoverClienteServicoConsumo>(_RemoverClienteServicoConsumo_QNAME, RemoverClienteServicoConsumo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteServicoConsumoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteServicoConsumoResponse")
    public JAXBElement<ListarClienteServicoConsumoResponse> createListarClienteServicoConsumoResponse(ListarClienteServicoConsumoResponse value) {
        return new JAXBElement<ListarClienteServicoConsumoResponse>(_ListarClienteServicoConsumoResponse_QNAME, ListarClienteServicoConsumoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarFamilia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarFamilia")
    public JAXBElement<AlterarFamilia> createAlterarFamilia(AlterarFamilia value) {
        return new JAXBElement<AlterarFamilia>(_AlterarFamilia_QNAME, AlterarFamilia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverFamilia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerFamilia")
    public JAXBElement<RemoverFamilia> createRemoverFamilia(RemoverFamilia value) {
        return new JAXBElement<RemoverFamilia>(_RemoverFamilia_QNAME, RemoverFamilia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarRamoAtividade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarRamoAtividade")
    public JAXBElement<AlterarRamoAtividade> createAlterarRamoAtividade(AlterarRamoAtividade value) {
        return new JAXBElement<AlterarRamoAtividade>(_AlterarRamoAtividade_QNAME, AlterarRamoAtividade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteNF }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteNF")
    public JAXBElement<AdicionarClienteNF> createAdicionarClienteNF(AdicionarClienteNF value) {
        return new JAXBElement<AdicionarClienteNF>(_AdicionarClienteNF_QNAME, AdicionarClienteNF.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarNaturezaOperacaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarNaturezaOperacaoResponse")
    public JAXBElement<ListarNaturezaOperacaoResponse> createListarNaturezaOperacaoResponse(ListarNaturezaOperacaoResponse value) {
        return new JAXBElement<ListarNaturezaOperacaoResponse>(_ListarNaturezaOperacaoResponse_QNAME, ListarNaturezaOperacaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterUfResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterUfResponse")
    public JAXBElement<ObterUfResponse> createObterUfResponse(ObterUfResponse value) {
        return new JAXBElement<ObterUfResponse>(_ObterUfResponse_QNAME, ObterUfResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteServicoConsumoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteServicoConsumoResponse")
    public JAXBElement<AlterarClienteServicoConsumoResponse> createAlterarClienteServicoConsumoResponse(AlterarClienteServicoConsumoResponse value) {
        return new JAXBElement<AlterarClienteServicoConsumoResponse>(_AlterarClienteServicoConsumoResponse_QNAME, AlterarClienteServicoConsumoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AutorizarClienteCartao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "autorizarClienteCartao")
    public JAXBElement<AutorizarClienteCartao> createAutorizarClienteCartao(AutorizarClienteCartao value) {
        return new JAXBElement<AutorizarClienteCartao>(_AutorizarClienteCartao_QNAME, AutorizarClienteCartao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterCicloFaturamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterCicloFaturamentoResponse")
    public JAXBElement<ObterCicloFaturamentoResponse> createObterCicloFaturamentoResponse(ObterCicloFaturamentoResponse value) {
        return new JAXBElement<ObterCicloFaturamentoResponse>(_ObterCicloFaturamentoResponse_QNAME, ObterCicloFaturamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarCobrancaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "consultarCobrancaResponse")
    public JAXBElement<ConsultarCobrancaResponse> createConsultarCobrancaResponse(ConsultarCobrancaResponse value) {
        return new JAXBElement<ConsultarCobrancaResponse>(_ConsultarCobrancaResponse_QNAME, ConsultarCobrancaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarCicloFaturamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarCicloFaturamento")
    public JAXBElement<ListarCicloFaturamento> createListarCicloFaturamento(ListarCicloFaturamento value) {
        return new JAXBElement<ListarCicloFaturamento>(_ListarCicloFaturamento_QNAME, ListarCicloFaturamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteCRResponse")
    public JAXBElement<RemoverClienteCRResponse> createRemoverClienteCRResponse(RemoverClienteCRResponse value) {
        return new JAXBElement<RemoverClienteCRResponse>(_RemoverClienteCRResponse_QNAME, RemoverClienteCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteNF }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteNF")
    public JAXBElement<AlterarClienteNF> createAlterarClienteNF(AlterarClienteNF value) {
        return new JAXBElement<AlterarClienteNF>(_AlterarClienteNF_QNAME, AlterarClienteNF.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterPeriodicidade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterPeriodicidade")
    public JAXBElement<ObterPeriodicidade> createObterPeriodicidade(ObterPeriodicidade value) {
        return new JAXBElement<ObterPeriodicidade>(_ObterPeriodicidade_QNAME, ObterPeriodicidade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverEnderecoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerEnderecoResponse")
    public JAXBElement<RemoverEnderecoResponse> createRemoverEnderecoResponse(RemoverEnderecoResponse value) {
        return new JAXBElement<RemoverEnderecoResponse>(_RemoverEnderecoResponse_QNAME, RemoverEnderecoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarNFsEmissao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "enviarNFsEmissao")
    public JAXBElement<EnviarNFsEmissao> createEnviarNFsEmissao(EnviarNFsEmissao value) {
        return new JAXBElement<EnviarNFsEmissao>(_EnviarNFsEmissao_QNAME, EnviarNFsEmissao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarCicloFaturamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarCicloFaturamentoResponse")
    public JAXBElement<AdicionarCicloFaturamentoResponse> createAdicionarCicloFaturamentoResponse(AdicionarCicloFaturamentoResponse value) {
        return new JAXBElement<AdicionarCicloFaturamentoResponse>(_AdicionarCicloFaturamentoResponse_QNAME, AdicionarCicloFaturamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaixarCobranca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "baixarCobranca")
    public JAXBElement<BaixarCobranca> createBaixarCobranca(BaixarCobranca value) {
        return new JAXBElement<BaixarCobranca>(_BaixarCobranca_QNAME, BaixarCobranca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarPeriodicidade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarPeriodicidade")
    public JAXBElement<AlterarPeriodicidade> createAlterarPeriodicidade(AlterarPeriodicidade value) {
        return new JAXBElement<AlterarPeriodicidade>(_AlterarPeriodicidade_QNAME, AlterarPeriodicidade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterMeioPagamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterMeioPagamento")
    public JAXBElement<ObterMeioPagamento> createObterMeioPagamento(ObterMeioPagamento value) {
        return new JAXBElement<ObterMeioPagamento>(_ObterMeioPagamento_QNAME, ObterMeioPagamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalServico")
    public JAXBElement<ListarTotalServico> createListarTotalServico(ListarTotalServico value) {
        return new JAXBElement<ListarTotalServico>(_ListarTotalServico_QNAME, ListarTotalServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterCidadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterCidadeResponse")
    public JAXBElement<ObterCidadeResponse> createObterCidadeResponse(ObterCidadeResponse value) {
        return new JAXBElement<ObterCidadeResponse>(_ObterCidadeResponse_QNAME, ObterCidadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterEndereco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterEndereco")
    public JAXBElement<ObterEndereco> createObterEndereco(ObterEndereco value) {
        return new JAXBElement<ObterEndereco>(_ObterEndereco_QNAME, ObterEndereco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarSegmento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarSegmento")
    public JAXBElement<ListarSegmento> createListarSegmento(ListarSegmento value) {
        return new JAXBElement<ListarSegmento>(_ListarSegmento_QNAME, ListarSegmento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterEnderecoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterEnderecoResponse")
    public JAXBElement<ObterEnderecoResponse> createObterEnderecoResponse(ObterEnderecoResponse value) {
        return new JAXBElement<ObterEnderecoResponse>(_ObterEnderecoResponse_QNAME, ObterEnderecoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalServicoResponse")
    public JAXBElement<ListarTotalServicoResponse> createListarTotalServicoResponse(ListarTotalServicoResponse value) {
        return new JAXBElement<ListarTotalServicoResponse>(_ListarTotalServicoResponse_QNAME, ListarTotalServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterSegmento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterSegmento")
    public JAXBElement<ObterSegmento> createObterSegmento(ObterSegmento value) {
        return new JAXBElement<ObterSegmento>(_ObterSegmento_QNAME, ObterSegmento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClonarClienteServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "clonarClienteServico")
    public JAXBElement<ClonarClienteServico> createClonarClienteServico(ClonarClienteServico value) {
        return new JAXBElement<ClonarClienteServico>(_ClonarClienteServico_QNAME, ClonarClienteServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterGerenteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterGerenteResponse")
    public JAXBElement<ObterGerenteResponse> createObterGerenteResponse(ObterGerenteResponse value) {
        return new JAXBElement<ObterGerenteResponse>(_ObterGerenteResponse_QNAME, ObterGerenteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarCicloFaturamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarCicloFaturamento")
    public JAXBElement<AdicionarCicloFaturamento> createAdicionarCicloFaturamento(AdicionarCicloFaturamento value) {
        return new JAXBElement<AdicionarCicloFaturamento>(_AdicionarCicloFaturamento_QNAME, AdicionarCicloFaturamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarCodigoCNAE }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarCodigoCNAE")
    public JAXBElement<ListarCodigoCNAE> createListarCodigoCNAE(ListarCodigoCNAE value) {
        return new JAXBElement<ListarCodigoCNAE>(_ListarCodigoCNAE_QNAME, ListarCodigoCNAE.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarIndiceEconomicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarIndiceEconomicoResponse")
    public JAXBElement<ListarIndiceEconomicoResponse> createListarIndiceEconomicoResponse(ListarIndiceEconomicoResponse value) {
        return new JAXBElement<ListarIndiceEconomicoResponse>(_ListarIndiceEconomicoResponse_QNAME, ListarIndiceEconomicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarMotivoCancelamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarMotivoCancelamentoResponse")
    public JAXBElement<AlterarMotivoCancelamentoResponse> createAlterarMotivoCancelamentoResponse(AlterarMotivoCancelamentoResponse value) {
        return new JAXBElement<AlterarMotivoCancelamentoResponse>(_AlterarMotivoCancelamentoResponse_QNAME, AlterarMotivoCancelamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterMotivoSuspensao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterMotivoSuspensao")
    public JAXBElement<ObterMotivoSuspensao> createObterMotivoSuspensao(ObterMotivoSuspensao value) {
        return new JAXBElement<ObterMotivoSuspensao>(_ObterMotivoSuspensao_QNAME, ObterMotivoSuspensao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverRelacionamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerRelacionamentoResponse")
    public JAXBElement<RemoverRelacionamentoResponse> createRemoverRelacionamentoResponse(RemoverRelacionamentoResponse value) {
        return new JAXBElement<RemoverRelacionamentoResponse>(_RemoverRelacionamentoResponse_QNAME, RemoverRelacionamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteServicoResponse")
    public JAXBElement<AlterarClienteServicoResponse> createAlterarClienteServicoResponse(AlterarClienteServicoResponse value) {
        return new JAXBElement<AlterarClienteServicoResponse>(_AlterarClienteServicoResponse_QNAME, AlterarClienteServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarSimplesNacionalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarSimplesNacionalResponse")
    public JAXBElement<ListarSimplesNacionalResponse> createListarSimplesNacionalResponse(ListarSimplesNacionalResponse value) {
        return new JAXBElement<ListarSimplesNacionalResponse>(_ListarSimplesNacionalResponse_QNAME, ListarSimplesNacionalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterRegraResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterRegraResponse")
    public JAXBElement<ObterRegraResponse> createObterRegraResponse(ObterRegraResponse value) {
        return new JAXBElement<ObterRegraResponse>(_ObterRegraResponse_QNAME, ObterRegraResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarContato }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarContato")
    public JAXBElement<AlterarContato> createAlterarContato(AlterarContato value) {
        return new JAXBElement<AlterarContato>(_AlterarContato_QNAME, AlterarContato.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteResponse")
    public JAXBElement<AlterarClienteResponse> createAlterarClienteResponse(AlterarClienteResponse value) {
        return new JAXBElement<AlterarClienteResponse>(_AlterarClienteResponse_QNAME, AlterarClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalClienteCRTransacaoBoletoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalClienteCRTransacaoBoletoResponse")
    public JAXBElement<ListarTotalClienteCRTransacaoBoletoResponse> createListarTotalClienteCRTransacaoBoletoResponse(ListarTotalClienteCRTransacaoBoletoResponse value) {
        return new JAXBElement<ListarTotalClienteCRTransacaoBoletoResponse>(_ListarTotalClienteCRTransacaoBoletoResponse_QNAME, ListarTotalClienteCRTransacaoBoletoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegraClienteServicoImpostosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "regraClienteServicoImpostosResponse")
    public JAXBElement<RegraClienteServicoImpostosResponse> createRegraClienteServicoImpostosResponse(RegraClienteServicoImpostosResponse value) {
        return new JAXBElement<RegraClienteServicoImpostosResponse>(_RegraClienteServicoImpostosResponse_QNAME, RegraClienteServicoImpostosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteServicoResponse")
    public JAXBElement<ObterClienteServicoResponse> createObterClienteServicoResponse(ObterClienteServicoResponse value) {
        return new JAXBElement<ObterClienteServicoResponse>(_ObterClienteServicoResponse_QNAME, ObterClienteServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarMotivoSuspensaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarMotivoSuspensaoResponse")
    public JAXBElement<AlterarMotivoSuspensaoResponse> createAlterarMotivoSuspensaoResponse(AlterarMotivoSuspensaoResponse value) {
        return new JAXBElement<AlterarMotivoSuspensaoResponse>(_AlterarMotivoSuspensaoResponse_QNAME, AlterarMotivoSuspensaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailCobrancaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "emailCobrancaResponse")
    public JAXBElement<EmailCobrancaResponse> createEmailCobrancaResponse(EmailCobrancaResponse value) {
        return new JAXBElement<EmailCobrancaResponse>(_EmailCobrancaResponse_QNAME, EmailCobrancaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterNaturezaOperacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterNaturezaOperacao")
    public JAXBElement<ObterNaturezaOperacao> createObterNaturezaOperacao(ObterNaturezaOperacao value) {
        return new JAXBElement<ObterNaturezaOperacao>(_ObterNaturezaOperacao_QNAME, ObterNaturezaOperacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarCicloFaturamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarCicloFaturamentoResponse")
    public JAXBElement<AlterarCicloFaturamentoResponse> createAlterarCicloFaturamentoResponse(AlterarCicloFaturamentoResponse value) {
        return new JAXBElement<AlterarCicloFaturamentoResponse>(_AlterarCicloFaturamentoResponse_QNAME, AlterarCicloFaturamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerCliente")
    public JAXBElement<RemoverCliente> createRemoverCliente(RemoverCliente value) {
        return new JAXBElement<RemoverCliente>(_RemoverCliente_QNAME, RemoverCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SuspenderClienteServicoPorClienteServicoUuid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "suspenderClienteServicoPorClienteServicoUuid")
    public JAXBElement<SuspenderClienteServicoPorClienteServicoUuid> createSuspenderClienteServicoPorClienteServicoUuid(SuspenderClienteServicoPorClienteServicoUuid value) {
        return new JAXBElement<SuspenderClienteServicoPorClienteServicoUuid>(_SuspenderClienteServicoPorClienteServicoUuid_QNAME, SuspenderClienteServicoPorClienteServicoUuid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteServico")
    public JAXBElement<ObterClienteServico> createObterClienteServico(ObterClienteServico value) {
        return new JAXBElement<ObterClienteServico>(_ObterClienteServico_QNAME, ObterClienteServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarIndiceEconomico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarIndiceEconomico")
    public JAXBElement<ListarIndiceEconomico> createListarIndiceEconomico(ListarIndiceEconomico value) {
        return new JAXBElement<ListarIndiceEconomico>(_ListarIndiceEconomico_QNAME, ListarIndiceEconomico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarClienteCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "cancelarClienteCRResponse")
    public JAXBElement<CancelarClienteCRResponse> createCancelarClienteCRResponse(CancelarClienteCRResponse value) {
        return new JAXBElement<CancelarClienteCRResponse>(_CancelarClienteCRResponse_QNAME, CancelarClienteCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReativarCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "reativarCliente")
    public JAXBElement<ReativarCliente> createReativarCliente(ReativarCliente value) {
        return new JAXBElement<ReativarCliente>(_ReativarCliente_QNAME, ReativarCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteServicoResponse")
    public JAXBElement<RemoverClienteServicoResponse> createRemoverClienteServicoResponse(RemoverClienteServicoResponse value) {
        return new JAXBElement<RemoverClienteServicoResponse>(_RemoverClienteServicoResponse_QNAME, RemoverClienteServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerServicoResponse")
    public JAXBElement<RemoverServicoResponse> createRemoverServicoResponse(RemoverServicoResponse value) {
        return new JAXBElement<RemoverServicoResponse>(_RemoverServicoResponse_QNAME, RemoverServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterTemplateEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterTemplateEmail")
    public JAXBElement<ObterTemplateEmail> createObterTemplateEmail(ObterTemplateEmail value) {
        return new JAXBElement<ObterTemplateEmail>(_ObterTemplateEmail_QNAME, ObterTemplateEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteCR")
    public JAXBElement<RemoverClienteCR> createRemoverClienteCR(RemoverClienteCR value) {
        return new JAXBElement<RemoverClienteCR>(_RemoverClienteCR_QNAME, RemoverClienteCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReativarClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "reativarClienteResponse")
    public JAXBElement<ReativarClienteResponse> createReativarClienteResponse(ReativarClienteResponse value) {
        return new JAXBElement<ReativarClienteResponse>(_ReativarClienteResponse_QNAME, ReativarClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FecharClienteServicoConsumoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "fecharClienteServicoConsumoResponse")
    public JAXBElement<FecharClienteServicoConsumoResponse> createFecharClienteServicoConsumoResponse(FecharClienteServicoConsumoResponse value) {
        return new JAXBElement<FecharClienteServicoConsumoResponse>(_FecharClienteServicoConsumoResponse_QNAME, FecharClienteServicoConsumoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailCobranca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "emailCobranca")
    public JAXBElement<EmailCobranca> createEmailCobranca(EmailCobranca value) {
        return new JAXBElement<EmailCobranca>(_EmailCobranca_QNAME, EmailCobranca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarSegmentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarSegmentoResponse")
    public JAXBElement<ListarSegmentoResponse> createListarSegmentoResponse(ListarSegmentoResponse value) {
        return new JAXBElement<ListarSegmentoResponse>(_ListarSegmentoResponse_QNAME, ListarSegmentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteServicoConsumo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteServicoConsumo")
    public JAXBElement<AdicionarClienteServicoConsumo> createAdicionarClienteServicoConsumo(AdicionarClienteServicoConsumo value) {
        return new JAXBElement<AdicionarClienteServicoConsumo>(_AdicionarClienteServicoConsumo_QNAME, AdicionarClienteServicoConsumo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteCRTransacaoBoleto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteCRTransacaoBoleto")
    public JAXBElement<ObterClienteCRTransacaoBoleto> createObterClienteCRTransacaoBoleto(ObterClienteCRTransacaoBoleto value) {
        return new JAXBElement<ObterClienteCRTransacaoBoleto>(_ObterClienteCRTransacaoBoleto_QNAME, ObterClienteCRTransacaoBoleto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterRegimeTributarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterRegimeTributarioResponse")
    public JAXBElement<ObterRegimeTributarioResponse> createObterRegimeTributarioResponse(ObterRegimeTributarioResponse value) {
        return new JAXBElement<ObterRegimeTributarioResponse>(_ObterRegimeTributarioResponse_QNAME, ObterRegimeTributarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarMotivoSuspensao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarMotivoSuspensao")
    public JAXBElement<AdicionarMotivoSuspensao> createAdicionarMotivoSuspensao(AdicionarMotivoSuspensao value) {
        return new JAXBElement<AdicionarMotivoSuspensao>(_AdicionarMotivoSuspensao_QNAME, AdicionarMotivoSuspensao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarArquivoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarArquivoResponse")
    public JAXBElement<AdicionarArquivoResponse> createAdicionarArquivoResponse(AdicionarArquivoResponse value) {
        return new JAXBElement<AdicionarArquivoResponse>(_AdicionarArquivoResponse_QNAME, AdicionarArquivoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterGerente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterGerente")
    public JAXBElement<ObterGerente> createObterGerente(ObterGerente value) {
        return new JAXBElement<ObterGerente>(_ObterGerente_QNAME, ObterGerente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteServicoCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteServicoCR")
    public JAXBElement<RemoverClienteServicoCR> createRemoverClienteServicoCR(RemoverClienteServicoCR value) {
        return new JAXBElement<RemoverClienteServicoCR>(_RemoverClienteServicoCR_QNAME, RemoverClienteServicoCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarMotivoSuspensao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarMotivoSuspensao")
    public JAXBElement<AlterarMotivoSuspensao> createAlterarMotivoSuspensao(AlterarMotivoSuspensao value) {
        return new JAXBElement<AlterarMotivoSuspensao>(_AlterarMotivoSuspensao_QNAME, AlterarMotivoSuspensao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarMotivoSuspensaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarMotivoSuspensaoResponse")
    public JAXBElement<ListarMotivoSuspensaoResponse> createListarMotivoSuspensaoResponse(ListarMotivoSuspensaoResponse value) {
        return new JAXBElement<ListarMotivoSuspensaoResponse>(_ListarMotivoSuspensaoResponse_QNAME, ListarMotivoSuspensaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GerarCobranca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "gerarCobranca")
    public JAXBElement<GerarCobranca> createGerarCobranca(GerarCobranca value) {
        return new JAXBElement<GerarCobranca>(_GerarCobranca_QNAME, GerarCobranca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterContato }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterContato")
    public JAXBElement<ObterContato> createObterContato(ObterContato value) {
        return new JAXBElement<ObterContato>(_ObterContato_QNAME, ObterContato.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteServico")
    public JAXBElement<AdicionarClienteServico> createAdicionarClienteServico(AdicionarClienteServico value) {
        return new JAXBElement<AdicionarClienteServico>(_AdicionarClienteServico_QNAME, AdicionarClienteServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverMotivoSuspensao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerMotivoSuspensao")
    public JAXBElement<RemoverMotivoSuspensao> createRemoverMotivoSuspensao(RemoverMotivoSuspensao value) {
        return new JAXBElement<RemoverMotivoSuspensao>(_RemoverMotivoSuspensao_QNAME, RemoverMotivoSuspensao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteServicoResponse")
    public JAXBElement<AdicionarClienteServicoResponse> createAdicionarClienteServicoResponse(AdicionarClienteServicoResponse value) {
        return new JAXBElement<AdicionarClienteServicoResponse>(_AdicionarClienteServicoResponse_QNAME, AdicionarClienteServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteCR")
    public JAXBElement<ObterClienteCR> createObterClienteCR(ObterClienteCR value) {
        return new JAXBElement<ObterClienteCR>(_ObterClienteCR_QNAME, ObterClienteCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteCartaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteCartaoResponse")
    public JAXBElement<RemoverClienteCartaoResponse> createRemoverClienteCartaoResponse(RemoverClienteCartaoResponse value) {
        return new JAXBElement<RemoverClienteCartaoResponse>(_RemoverClienteCartaoResponse_QNAME, RemoverClienteCartaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverPeriodicidade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerPeriodicidade")
    public JAXBElement<RemoverPeriodicidade> createRemoverPeriodicidade(RemoverPeriodicidade value) {
        return new JAXBElement<RemoverPeriodicidade>(_RemoverPeriodicidade_QNAME, RemoverPeriodicidade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteCRCadastroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteCRCadastroResponse")
    public JAXBElement<AlterarClienteCRCadastroResponse> createAlterarClienteCRCadastroResponse(AlterarClienteCRCadastroResponse value) {
        return new JAXBElement<AlterarClienteCRCadastroResponse>(_AlterarClienteCRCadastroResponse_QNAME, AlterarClienteCRCadastroResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTributacaoMunicipio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTributacaoMunicipio")
    public JAXBElement<ListarTributacaoMunicipio> createListarTributacaoMunicipio(ListarTributacaoMunicipio value) {
        return new JAXBElement<ListarTributacaoMunicipio>(_ListarTributacaoMunicipio_QNAME, ListarTributacaoMunicipio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarCliente")
    public JAXBElement<ListarCliente> createListarCliente(ListarCliente value) {
        return new JAXBElement<ListarCliente>(_ListarCliente_QNAME, ListarCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverCicloFaturamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerCicloFaturamentoResponse")
    public JAXBElement<RemoverCicloFaturamentoResponse> createRemoverCicloFaturamentoResponse(RemoverCicloFaturamentoResponse value) {
        return new JAXBElement<RemoverCicloFaturamentoResponse>(_RemoverCicloFaturamentoResponse_QNAME, RemoverCicloFaturamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteCRCadastro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteCRCadastro")
    public JAXBElement<ObterClienteCRCadastro> createObterClienteCRCadastro(ObterClienteCRCadastro value) {
        return new JAXBElement<ObterClienteCRCadastro>(_ObterClienteCRCadastro_QNAME, ObterClienteCRCadastro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarRamoAtividadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarRamoAtividadeResponse")
    public JAXBElement<AdicionarRamoAtividadeResponse> createAdicionarRamoAtividadeResponse(AdicionarRamoAtividadeResponse value) {
        return new JAXBElement<AdicionarRamoAtividadeResponse>(_AdicionarRamoAtividadeResponse_QNAME, AdicionarRamoAtividadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarClienteNFResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarClienteNFResponse")
    public JAXBElement<AdicionarClienteNFResponse> createAdicionarClienteNFResponse(AdicionarClienteNFResponse value) {
        return new JAXBElement<AdicionarClienteNFResponse>(_AdicionarClienteNFResponse_QNAME, AdicionarClienteNFResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalArquivo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalArquivo")
    public JAXBElement<ListarTotalArquivo> createListarTotalArquivo(ListarTotalArquivo value) {
        return new JAXBElement<ListarTotalArquivo>(_ListarTotalArquivo_QNAME, ListarTotalArquivo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarRelacionamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarRelacionamento")
    public JAXBElement<AdicionarRelacionamento> createAdicionarRelacionamento(AdicionarRelacionamento value) {
        return new JAXBElement<AdicionarRelacionamento>(_AdicionarRelacionamento_QNAME, AdicionarRelacionamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterConfiguracao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterConfiguracao")
    public JAXBElement<ObterConfiguracao> createObterConfiguracao(ObterConfiguracao value) {
        return new JAXBElement<ObterConfiguracao>(_ObterConfiguracao_QNAME, ObterConfiguracao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterPeriodicidadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterPeriodicidadeResponse")
    public JAXBElement<ObterPeriodicidadeResponse> createObterPeriodicidadeResponse(ObterPeriodicidadeResponse value) {
        return new JAXBElement<ObterPeriodicidadeResponse>(_ObterPeriodicidadeResponse_QNAME, ObterPeriodicidadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteServico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteServico")
    public JAXBElement<ListarClienteServico> createListarClienteServico(ListarClienteServico value) {
        return new JAXBElement<ListarClienteServico>(_ListarClienteServico_QNAME, ListarClienteServico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterUuidUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterUuidUsuario")
    public JAXBElement<ObterUuidUsuario> createObterUuidUsuario(ObterUuidUsuario value) {
        return new JAXBElement<ObterUuidUsuario>(_ObterUuidUsuario_QNAME, ObterUuidUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterFamilia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterFamilia")
    public JAXBElement<ObterFamilia> createObterFamilia(ObterFamilia value) {
        return new JAXBElement<ObterFamilia>(_ObterFamilia_QNAME, ObterFamilia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverArquivo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerArquivo")
    public JAXBElement<RemoverArquivo> createRemoverArquivo(RemoverArquivo value) {
        return new JAXBElement<RemoverArquivo>(_RemoverArquivo_QNAME, RemoverArquivo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarMotivoCancelamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarMotivoCancelamentoResponse")
    public JAXBElement<ListarMotivoCancelamentoResponse> createListarMotivoCancelamentoResponse(ListarMotivoCancelamentoResponse value) {
        return new JAXBElement<ListarMotivoCancelamentoResponse>(_ListarMotivoCancelamentoResponse_QNAME, ListarMotivoCancelamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteServicoPorClienteServicoUuidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteServicoPorClienteServicoUuidResponse")
    public JAXBElement<AlterarClienteServicoPorClienteServicoUuidResponse> createAlterarClienteServicoPorClienteServicoUuidResponse(AlterarClienteServicoPorClienteServicoUuidResponse value) {
        return new JAXBElement<AlterarClienteServicoPorClienteServicoUuidResponse>(_AlterarClienteServicoPorClienteServicoUuidResponse_QNAME, AlterarClienteServicoPorClienteServicoUuidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteResponse")
    public JAXBElement<ObterClienteResponse> createObterClienteResponse(ObterClienteResponse value) {
        return new JAXBElement<ObterClienteResponse>(_ObterClienteResponse_QNAME, ObterClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarContatoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarContatoResponse")
    public JAXBElement<AdicionarContatoResponse> createAdicionarContatoResponse(AdicionarContatoResponse value) {
        return new JAXBElement<AdicionarContatoResponse>(_AdicionarContatoResponse_QNAME, AdicionarContatoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarMotivoCancelamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarMotivoCancelamentoResponse")
    public JAXBElement<AdicionarMotivoCancelamentoResponse> createAdicionarMotivoCancelamentoResponse(AdicionarMotivoCancelamentoResponse value) {
        return new JAXBElement<AdicionarMotivoCancelamentoResponse>(_AdicionarMotivoCancelamentoResponse_QNAME, AdicionarMotivoCancelamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarPeriodicidadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarPeriodicidadeResponse")
    public JAXBElement<AdicionarPeriodicidadeResponse> createAdicionarPeriodicidadeResponse(AdicionarPeriodicidadeResponse value) {
        return new JAXBElement<AdicionarPeriodicidadeResponse>(_AdicionarPeriodicidadeResponse_QNAME, AdicionarPeriodicidadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalServicoAliquotaPrestador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalServicoAliquotaPrestador")
    public JAXBElement<ListarTotalServicoAliquotaPrestador> createListarTotalServicoAliquotaPrestador(ListarTotalServicoAliquotaPrestador value) {
        return new JAXBElement<ListarTotalServicoAliquotaPrestador>(_ListarTotalServicoAliquotaPrestador_QNAME, ListarTotalServicoAliquotaPrestador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarNaturezaOperacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarNaturezaOperacao")
    public JAXBElement<ListarNaturezaOperacao> createListarNaturezaOperacao(ListarNaturezaOperacao value) {
        return new JAXBElement<ListarNaturezaOperacao>(_ListarNaturezaOperacao_QNAME, ListarNaturezaOperacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteResponse")
    public JAXBElement<RemoverClienteResponse> createRemoverClienteResponse(RemoverClienteResponse value) {
        return new JAXBElement<RemoverClienteResponse>(_RemoverClienteResponse_QNAME, RemoverClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterRamoAtividadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterRamoAtividadeResponse")
    public JAXBElement<ObterRamoAtividadeResponse> createObterRamoAtividadeResponse(ObterRamoAtividadeResponse value) {
        return new JAXBElement<ObterRamoAtividadeResponse>(_ObterRamoAtividadeResponse_QNAME, ObterRamoAtividadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarSegmento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarSegmento")
    public JAXBElement<AdicionarSegmento> createAdicionarSegmento(AdicionarSegmento value) {
        return new JAXBElement<AdicionarSegmento>(_AdicionarSegmento_QNAME, AdicionarSegmento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarContatoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarContatoResponse")
    public JAXBElement<ListarContatoResponse> createListarContatoResponse(ListarContatoResponse value) {
        return new JAXBElement<ListarContatoResponse>(_ListarContatoResponse_QNAME, ListarContatoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarClienteCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarClienteCR")
    public JAXBElement<ListarClienteCR> createListarClienteCR(ListarClienteCR value) {
        return new JAXBElement<ListarClienteCR>(_ListarClienteCR_QNAME, ListarClienteCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarArquivo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarArquivo")
    public JAXBElement<AdicionarArquivo> createAdicionarArquivo(AdicionarArquivo value) {
        return new JAXBElement<AdicionarArquivo>(_AdicionarArquivo_QNAME, AdicionarArquivo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarServicoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarServicoResponse")
    public JAXBElement<AlterarServicoResponse> createAlterarServicoResponse(AlterarServicoResponse value) {
        return new JAXBElement<AlterarServicoResponse>(_AlterarServicoResponse_QNAME, AlterarServicoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPeriodicidadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarPeriodicidadeResponse")
    public JAXBElement<ListarPeriodicidadeResponse> createListarPeriodicidadeResponse(ListarPeriodicidadeResponse value) {
        return new JAXBElement<ListarPeriodicidadeResponse>(_ListarPeriodicidadeResponse_QNAME, ListarPeriodicidadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarCodigoCNAEResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarCodigoCNAEResponse")
    public JAXBElement<ListarCodigoCNAEResponse> createListarCodigoCNAEResponse(ListarCodigoCNAEResponse value) {
        return new JAXBElement<ListarCodigoCNAEResponse>(_ListarCodigoCNAEResponse_QNAME, ListarCodigoCNAEResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarEnderecoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarEnderecoResponse")
    public JAXBElement<ListarEnderecoResponse> createListarEnderecoResponse(ListarEnderecoResponse value) {
        return new JAXBElement<ListarEnderecoResponse>(_ListarEnderecoResponse_QNAME, ListarEnderecoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GerarNFsEmissao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "gerarNFsEmissao")
    public JAXBElement<GerarNFsEmissao> createGerarNFsEmissao(GerarNFsEmissao value) {
        return new JAXBElement<GerarNFsEmissao>(_GerarNFsEmissao_QNAME, GerarNFsEmissao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteNFResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteNFResponse")
    public JAXBElement<ObterClienteNFResponse> createObterClienteNFResponse(ObterClienteNFResponse value) {
        return new JAXBElement<ObterClienteNFResponse>(_ObterClienteNFResponse_QNAME, ObterClienteNFResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarRegimeTributarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarRegimeTributarioResponse")
    public JAXBElement<ListarRegimeTributarioResponse> createListarRegimeTributarioResponse(ListarRegimeTributarioResponse value) {
        return new JAXBElement<ListarRegimeTributarioResponse>(_ListarRegimeTributarioResponse_QNAME, ListarRegimeTributarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterMotivoCancelamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterMotivoCancelamentoResponse")
    public JAXBElement<ObterMotivoCancelamentoResponse> createObterMotivoCancelamentoResponse(ObterMotivoCancelamentoResponse value) {
        return new JAXBElement<ObterMotivoCancelamentoResponse>(_ObterMotivoCancelamentoResponse_QNAME, ObterMotivoCancelamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AlterarClienteCR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "alterarClienteCR")
    public JAXBElement<AlterarClienteCR> createAlterarClienteCR(AlterarClienteCR value) {
        return new JAXBElement<AlterarClienteCR>(_AlterarClienteCR_QNAME, AlterarClienteCR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdicionarMotivoSuspensaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "adicionarMotivoSuspensaoResponse")
    public JAXBElement<AdicionarMotivoSuspensaoResponse> createAdicionarMotivoSuspensaoResponse(AdicionarMotivoSuspensaoResponse value) {
        return new JAXBElement<AdicionarMotivoSuspensaoResponse>(_AdicionarMotivoSuspensaoResponse_QNAME, AdicionarMotivoSuspensaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteCartao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteCartao")
    public JAXBElement<RemoverClienteCartao> createRemoverClienteCartao(RemoverClienteCartao value) {
        return new JAXBElement<RemoverClienteCartao>(_RemoverClienteCartao_QNAME, RemoverClienteCartao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverMotivoCancelamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerMotivoCancelamentoResponse")
    public JAXBElement<RemoverMotivoCancelamentoResponse> createRemoverMotivoCancelamentoResponse(RemoverMotivoCancelamentoResponse value) {
        return new JAXBElement<RemoverMotivoCancelamentoResponse>(_RemoverMotivoCancelamentoResponse_QNAME, RemoverMotivoCancelamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterUuidUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterUuidUsuarioResponse")
    public JAXBElement<ObterUuidUsuarioResponse> createObterUuidUsuarioResponse(ObterUuidUsuarioResponse value) {
        return new JAXBElement<ObterUuidUsuarioResponse>(_ObterUuidUsuarioResponse_QNAME, ObterUuidUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarConfiguracaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarConfiguracaoResponse")
    public JAXBElement<ListarConfiguracaoResponse> createListarConfiguracaoResponse(ListarConfiguracaoResponse value) {
        return new JAXBElement<ListarConfiguracaoResponse>(_ListarConfiguracaoResponse_QNAME, ListarConfiguracaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverSegmento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerSegmento")
    public JAXBElement<RemoverSegmento> createRemoverSegmento(RemoverSegmento value) {
        return new JAXBElement<RemoverSegmento>(_RemoverSegmento_QNAME, RemoverSegmento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteServicoConsumo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteServicoConsumo")
    public JAXBElement<ObterClienteServicoConsumo> createObterClienteServicoConsumo(ObterClienteServicoConsumo value) {
        return new JAXBElement<ObterClienteServicoConsumo>(_ObterClienteServicoConsumo_QNAME, ObterClienteServicoConsumo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarTotalClienteCRResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarTotalClienteCRResponse")
    public JAXBElement<ListarTotalClienteCRResponse> createListarTotalClienteCRResponse(ListarTotalClienteCRResponse value) {
        return new JAXBElement<ListarTotalClienteCRResponse>(_ListarTotalClienteCRResponse_QNAME, ListarTotalClienteCRResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverRamoAtividadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerRamoAtividadeResponse")
    public JAXBElement<RemoverRamoAtividadeResponse> createRemoverRamoAtividadeResponse(RemoverRamoAtividadeResponse value) {
        return new JAXBElement<RemoverRamoAtividadeResponse>(_RemoverRamoAtividadeResponse_QNAME, RemoverRamoAtividadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterCodigoCNAE }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterCodigoCNAE")
    public JAXBElement<ObterCodigoCNAE> createObterCodigoCNAE(ObterCodigoCNAE value) {
        return new JAXBElement<ObterCodigoCNAE>(_ObterCodigoCNAE_QNAME, ObterCodigoCNAE.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoverClienteNFResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "removerClienteNFResponse")
    public JAXBElement<RemoverClienteNFResponse> createRemoverClienteNFResponse(RemoverClienteNFResponse value) {
        return new JAXBElement<RemoverClienteNFResponse>(_RemoverClienteNFResponse_QNAME, RemoverClienteNFResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarContato }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarContato")
    public JAXBElement<ListarContato> createListarContato(ListarContato value) {
        return new JAXBElement<ListarContato>(_ListarContato_QNAME, ListarContato.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarConfiguracao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarConfiguracao")
    public JAXBElement<ListarConfiguracao> createListarConfiguracao(ListarConfiguracao value) {
        return new JAXBElement<ListarConfiguracao>(_ListarConfiguracao_QNAME, ListarConfiguracao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarMeioPagamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarMeioPagamentoResponse")
    public JAXBElement<ListarMeioPagamentoResponse> createListarMeioPagamentoResponse(ListarMeioPagamentoResponse value) {
        return new JAXBElement<ListarMeioPagamentoResponse>(_ListarMeioPagamentoResponse_QNAME, ListarMeioPagamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterClienteCRCadastroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterClienteCRCadastroResponse")
    public JAXBElement<ObterClienteCRCadastroResponse> createObterClienteCRCadastroResponse(ObterClienteCRCadastroResponse value) {
        return new JAXBElement<ObterClienteCRCadastroResponse>(_ObterClienteCRCadastroResponse_QNAME, ObterClienteCRCadastroResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarIndiceEconomicoValor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "listarIndiceEconomicoValor")
    public JAXBElement<ListarIndiceEconomicoValor> createListarIndiceEconomicoValor(ListarIndiceEconomicoValor value) {
        return new JAXBElement<ListarIndiceEconomicoValor>(_ListarIndiceEconomicoValor_QNAME, ListarIndiceEconomicoValor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterMeioPagamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.api.smartbill.com.br/", name = "obterMeioPagamentoResponse")
    public JAXBElement<ObterMeioPagamentoResponse> createObterMeioPagamentoResponse(ObterMeioPagamentoResponse value) {
        return new JAXBElement<ObterMeioPagamentoResponse>(_ObterMeioPagamentoResponse_QNAME, ObterMeioPagamentoResponse.class, null, value);
    }

}
