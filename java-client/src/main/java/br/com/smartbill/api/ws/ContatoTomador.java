
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for contatoTomador complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="contatoTomador">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.api.smartbill.com.br/}copyFields">
 *       &lt;sequence>
 *         &lt;element name="celularDdd" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="celularNumero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="faxDdd" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="faxNumero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefoneDdd" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="telefoneNumero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contatoTomador", propOrder = {
    "celularDdd",
    "celularNumero",
    "cpf",
    "email",
    "faxDdd",
    "faxNumero",
    "nome",
    "telefoneDdd",
    "telefoneNumero"
})
public class ContatoTomador
    extends CopyFields
{

    protected Integer celularDdd;
    protected String celularNumero;
    protected String cpf;
    protected String email;
    protected Integer faxDdd;
    protected String faxNumero;
    protected String nome;
    protected Integer telefoneDdd;
    protected String telefoneNumero;

    /**
     * Gets the value of the celularDdd property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCelularDdd() {
        return celularDdd;
    }

    /**
     * Sets the value of the celularDdd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCelularDdd(Integer value) {
        this.celularDdd = value;
    }

    /**
     * Gets the value of the celularNumero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelularNumero() {
        return celularNumero;
    }

    /**
     * Sets the value of the celularNumero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelularNumero(String value) {
        this.celularNumero = value;
    }

    /**
     * Gets the value of the cpf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * Sets the value of the cpf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpf(String value) {
        this.cpf = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the faxDdd property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFaxDdd() {
        return faxDdd;
    }

    /**
     * Sets the value of the faxDdd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFaxDdd(Integer value) {
        this.faxDdd = value;
    }

    /**
     * Gets the value of the faxNumero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxNumero() {
        return faxNumero;
    }

    /**
     * Sets the value of the faxNumero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxNumero(String value) {
        this.faxNumero = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the telefoneDdd property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTelefoneDdd() {
        return telefoneDdd;
    }

    /**
     * Sets the value of the telefoneDdd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTelefoneDdd(Integer value) {
        this.telefoneDdd = value;
    }

    /**
     * Gets the value of the telefoneNumero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefoneNumero() {
        return telefoneNumero;
    }

    /**
     * Sets the value of the telefoneNumero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefoneNumero(String value) {
        this.telefoneNumero = value;
    }

}
