
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteCR complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteCR">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anoReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="boletos" type="{http://ws.api.smartbill.com.br/}clienteCRTransacaoBoleto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="cartoes" type="{http://ws.api.smartbill.com.br/}clienteCRTransacaoCartao" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="cliente" type="{http://ws.api.smartbill.com.br/}cliente" minOccurs="0"/>
 *         &lt;element name="clienteCRCadastroUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataBaixa" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataCancelamento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataEmissao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataEnvio" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataEnvioAtraso" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataPagamento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataVencimento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataVencimentoOriginal" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="depositos" type="{http://ws.api.smartbill.com.br/}clienteCRTransacaoDeposito" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mesReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="motivoCancelamento" type="{http://ws.api.smartbill.com.br/}motivoCancelamento" minOccurs="0"/>
 *         &lt;element name="notasFiscais" type="{http://ws.api.smartbill.com.br/}clienteNF" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="projecaoFaturamento" type="{http://ws.api.smartbill.com.br/}projecaoFaturamento" minOccurs="0"/>
 *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="serie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="servicosCR" type="{http://ws.api.smartbill.com.br/}clienteServicoCR" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subSerie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoCREstado" type="{http://ws.api.smartbill.com.br/}tipoCREstado" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valorDesconto" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorIOF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorImposto" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorJurosMora" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorMulta" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorPago" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorTarifaBoleto" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorTotalOriginal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteCR", propOrder = {
    "anoReferencia",
    "boletos",
    "cartoes",
    "cliente",
    "clienteCRCadastroUuid",
    "codigo",
    "criacao",
    "dataBaixa",
    "dataCancelamento",
    "dataEmissao",
    "dataEnvio",
    "dataEnvioAtraso",
    "dataPagamento",
    "dataVencimento",
    "dataVencimentoOriginal",
    "depositos",
    "exclusao",
    "gerenteUuid",
    "mesReferencia",
    "modificacao",
    "motivoCancelamento",
    "notasFiscais",
    "projecaoFaturamento",
    "referencia",
    "serie",
    "servicosCR",
    "subSerie",
    "tipoCREstado",
    "uuid",
    "valorDesconto",
    "valorIOF",
    "valorImposto",
    "valorJurosMora",
    "valorMulta",
    "valorPago",
    "valorTarifaBoleto",
    "valorTotal",
    "valorTotalOriginal"
})
public class ClienteCR {

    protected Integer anoReferencia;
    @XmlElement(nillable = true)
    protected List<ClienteCRTransacaoBoleto> boletos;
    @XmlElement(nillable = true)
    protected List<ClienteCRTransacaoCartao> cartoes;
    protected Cliente cliente;
    protected String clienteCRCadastroUuid;
    protected String codigo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataBaixa;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCancelamento;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEmissao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEnvio;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEnvioAtraso;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataPagamento;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataVencimento;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataVencimentoOriginal;
    @XmlElement(nillable = true)
    protected List<ClienteCRTransacaoDeposito> depositos;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    protected Integer mesReferencia;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected MotivoCancelamento motivoCancelamento;
    @XmlElement(nillable = true)
    protected List<ClienteNF> notasFiscais;
    protected ProjecaoFaturamento projecaoFaturamento;
    protected Integer referencia;
    protected String serie;
    @XmlElement(nillable = true)
    protected List<ClienteServicoCR> servicosCR;
    protected String subSerie;
    protected TipoCREstado tipoCREstado;
    protected String uuid;
    protected BigDecimal valorDesconto;
    protected BigDecimal valorIOF;
    protected BigDecimal valorImposto;
    protected BigDecimal valorJurosMora;
    protected BigDecimal valorMulta;
    protected BigDecimal valorPago;
    protected BigDecimal valorTarifaBoleto;
    protected BigDecimal valorTotal;
    protected BigDecimal valorTotalOriginal;

    /**
     * Gets the value of the anoReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnoReferencia() {
        return anoReferencia;
    }

    /**
     * Sets the value of the anoReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnoReferencia(Integer value) {
        this.anoReferencia = value;
    }

    /**
     * Gets the value of the boletos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the boletos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBoletos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteCRTransacaoBoleto }
     * 
     * 
     */
    public List<ClienteCRTransacaoBoleto> getBoletos() {
        if (boletos == null) {
            boletos = new ArrayList<ClienteCRTransacaoBoleto>();
        }
        return this.boletos;
    }

    /**
     * Gets the value of the cartoes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cartoes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCartoes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteCRTransacaoCartao }
     * 
     * 
     */
    public List<ClienteCRTransacaoCartao> getCartoes() {
        if (cartoes == null) {
            cartoes = new ArrayList<ClienteCRTransacaoCartao>();
        }
        return this.cartoes;
    }

    /**
     * Gets the value of the cliente property.
     * 
     * @return
     *     possible object is
     *     {@link Cliente }
     *     
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * Sets the value of the cliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cliente }
     *     
     */
    public void setCliente(Cliente value) {
        this.cliente = value;
    }

    /**
     * Gets the value of the clienteCRCadastroUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteCRCadastroUuid() {
        return clienteCRCadastroUuid;
    }

    /**
     * Sets the value of the clienteCRCadastroUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteCRCadastroUuid(String value) {
        this.clienteCRCadastroUuid = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the dataBaixa property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataBaixa() {
        return dataBaixa;
    }

    /**
     * Sets the value of the dataBaixa property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataBaixa(XMLGregorianCalendar value) {
        this.dataBaixa = value;
    }

    /**
     * Gets the value of the dataCancelamento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCancelamento() {
        return dataCancelamento;
    }

    /**
     * Sets the value of the dataCancelamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCancelamento(XMLGregorianCalendar value) {
        this.dataCancelamento = value;
    }

    /**
     * Gets the value of the dataEmissao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEmissao() {
        return dataEmissao;
    }

    /**
     * Sets the value of the dataEmissao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEmissao(XMLGregorianCalendar value) {
        this.dataEmissao = value;
    }

    /**
     * Gets the value of the dataEnvio property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEnvio() {
        return dataEnvio;
    }

    /**
     * Sets the value of the dataEnvio property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEnvio(XMLGregorianCalendar value) {
        this.dataEnvio = value;
    }

    /**
     * Gets the value of the dataEnvioAtraso property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEnvioAtraso() {
        return dataEnvioAtraso;
    }

    /**
     * Sets the value of the dataEnvioAtraso property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEnvioAtraso(XMLGregorianCalendar value) {
        this.dataEnvioAtraso = value;
    }

    /**
     * Gets the value of the dataPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataPagamento() {
        return dataPagamento;
    }

    /**
     * Sets the value of the dataPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataPagamento(XMLGregorianCalendar value) {
        this.dataPagamento = value;
    }

    /**
     * Gets the value of the dataVencimento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataVencimento() {
        return dataVencimento;
    }

    /**
     * Sets the value of the dataVencimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataVencimento(XMLGregorianCalendar value) {
        this.dataVencimento = value;
    }

    /**
     * Gets the value of the dataVencimentoOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataVencimentoOriginal() {
        return dataVencimentoOriginal;
    }

    /**
     * Sets the value of the dataVencimentoOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataVencimentoOriginal(XMLGregorianCalendar value) {
        this.dataVencimentoOriginal = value;
    }

    /**
     * Gets the value of the depositos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the depositos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepositos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteCRTransacaoDeposito }
     * 
     * 
     */
    public List<ClienteCRTransacaoDeposito> getDepositos() {
        if (depositos == null) {
            depositos = new ArrayList<ClienteCRTransacaoDeposito>();
        }
        return this.depositos;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the mesReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMesReferencia() {
        return mesReferencia;
    }

    /**
     * Sets the value of the mesReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMesReferencia(Integer value) {
        this.mesReferencia = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the motivoCancelamento property.
     * 
     * @return
     *     possible object is
     *     {@link MotivoCancelamento }
     *     
     */
    public MotivoCancelamento getMotivoCancelamento() {
        return motivoCancelamento;
    }

    /**
     * Sets the value of the motivoCancelamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link MotivoCancelamento }
     *     
     */
    public void setMotivoCancelamento(MotivoCancelamento value) {
        this.motivoCancelamento = value;
    }

    /**
     * Gets the value of the notasFiscais property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the notasFiscais property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotasFiscais().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteNF }
     * 
     * 
     */
    public List<ClienteNF> getNotasFiscais() {
        if (notasFiscais == null) {
            notasFiscais = new ArrayList<ClienteNF>();
        }
        return this.notasFiscais;
    }

    /**
     * Gets the value of the projecaoFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link ProjecaoFaturamento }
     *     
     */
    public ProjecaoFaturamento getProjecaoFaturamento() {
        return projecaoFaturamento;
    }

    /**
     * Sets the value of the projecaoFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjecaoFaturamento }
     *     
     */
    public void setProjecaoFaturamento(ProjecaoFaturamento value) {
        this.projecaoFaturamento = value;
    }

    /**
     * Gets the value of the referencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReferencia() {
        return referencia;
    }

    /**
     * Sets the value of the referencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReferencia(Integer value) {
        this.referencia = value;
    }

    /**
     * Gets the value of the serie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerie() {
        return serie;
    }

    /**
     * Sets the value of the serie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerie(String value) {
        this.serie = value;
    }

    /**
     * Gets the value of the servicosCR property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servicosCR property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicosCR().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteServicoCR }
     * 
     * 
     */
    public List<ClienteServicoCR> getServicosCR() {
        if (servicosCR == null) {
            servicosCR = new ArrayList<ClienteServicoCR>();
        }
        return this.servicosCR;
    }

    /**
     * Gets the value of the subSerie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubSerie() {
        return subSerie;
    }

    /**
     * Sets the value of the subSerie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubSerie(String value) {
        this.subSerie = value;
    }

    /**
     * Gets the value of the tipoCREstado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCREstado }
     *     
     */
    public TipoCREstado getTipoCREstado() {
        return tipoCREstado;
    }

    /**
     * Sets the value of the tipoCREstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCREstado }
     *     
     */
    public void setTipoCREstado(TipoCREstado value) {
        this.tipoCREstado = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the valorDesconto property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    /**
     * Sets the value of the valorDesconto property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorDesconto(BigDecimal value) {
        this.valorDesconto = value;
    }

    /**
     * Gets the value of the valorIOF property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorIOF() {
        return valorIOF;
    }

    /**
     * Sets the value of the valorIOF property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorIOF(BigDecimal value) {
        this.valorIOF = value;
    }

    /**
     * Gets the value of the valorImposto property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorImposto() {
        return valorImposto;
    }

    /**
     * Sets the value of the valorImposto property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorImposto(BigDecimal value) {
        this.valorImposto = value;
    }

    /**
     * Gets the value of the valorJurosMora property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorJurosMora() {
        return valorJurosMora;
    }

    /**
     * Sets the value of the valorJurosMora property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorJurosMora(BigDecimal value) {
        this.valorJurosMora = value;
    }

    /**
     * Gets the value of the valorMulta property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorMulta() {
        return valorMulta;
    }

    /**
     * Sets the value of the valorMulta property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorMulta(BigDecimal value) {
        this.valorMulta = value;
    }

    /**
     * Gets the value of the valorPago property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorPago() {
        return valorPago;
    }

    /**
     * Sets the value of the valorPago property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorPago(BigDecimal value) {
        this.valorPago = value;
    }

    /**
     * Gets the value of the valorTarifaBoleto property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorTarifaBoleto() {
        return valorTarifaBoleto;
    }

    /**
     * Sets the value of the valorTarifaBoleto property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorTarifaBoleto(BigDecimal value) {
        this.valorTarifaBoleto = value;
    }

    /**
     * Gets the value of the valorTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    /**
     * Sets the value of the valorTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorTotal(BigDecimal value) {
        this.valorTotal = value;
    }

    /**
     * Gets the value of the valorTotalOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorTotalOriginal() {
        return valorTotalOriginal;
    }

    /**
     * Sets the value of the valorTotalOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorTotalOriginal(BigDecimal value) {
        this.valorTotalOriginal = value;
    }

}
