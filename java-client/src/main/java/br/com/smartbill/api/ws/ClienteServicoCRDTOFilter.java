
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for clienteServicoCRDTOFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteServicoCRDTOFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="uuidCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuidClienteCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuidClienteServico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuidClienteServicoConsumo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteServicoCRDTOFilter", propOrder = {
    "uuidCliente",
    "uuidClienteCR",
    "uuidClienteServico",
    "uuidClienteServicoConsumo"
})
public class ClienteServicoCRDTOFilter {

    protected String uuidCliente;
    protected String uuidClienteCR;
    protected String uuidClienteServico;
    protected String uuidClienteServicoConsumo;

    /**
     * Gets the value of the uuidCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidCliente() {
        return uuidCliente;
    }

    /**
     * Sets the value of the uuidCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidCliente(String value) {
        this.uuidCliente = value;
    }

    /**
     * Gets the value of the uuidClienteCR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidClienteCR() {
        return uuidClienteCR;
    }

    /**
     * Sets the value of the uuidClienteCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidClienteCR(String value) {
        this.uuidClienteCR = value;
    }

    /**
     * Gets the value of the uuidClienteServico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidClienteServico() {
        return uuidClienteServico;
    }

    /**
     * Sets the value of the uuidClienteServico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidClienteServico(String value) {
        this.uuidClienteServico = value;
    }

    /**
     * Gets the value of the uuidClienteServicoConsumo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidClienteServicoConsumo() {
        return uuidClienteServicoConsumo;
    }

    /**
     * Sets the value of the uuidClienteServicoConsumo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidClienteServicoConsumo(String value) {
        this.uuidClienteServicoConsumo = value;
    }

}
