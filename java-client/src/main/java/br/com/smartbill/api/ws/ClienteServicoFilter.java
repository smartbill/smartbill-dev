
package br.com.smartbill.api.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for clienteServicoFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteServicoFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clienteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="excluindoUnidadesMetricas" type="{http://ws.api.smartbill.com.br/}unidadeMetrica" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="servicoEstado" type="{http://ws.api.smartbill.com.br/}servicoEstado" minOccurs="0"/>
 *         &lt;element name="servicoUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoConsumoEstado" type="{http://ws.api.smartbill.com.br/}tipoConsumoEstado" minOccurs="0"/>
 *         &lt;element name="tipoServico" type="{http://ws.api.smartbill.com.br/}tipoServico" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteServicoFilter", propOrder = {
    "clienteUuid",
    "codigoContrato",
    "excluindoUnidadesMetricas",
    "servicoEstado",
    "servicoUuid",
    "tipoConsumoEstado",
    "tipoServico"
})
public class ClienteServicoFilter {

    protected String clienteUuid;
    protected String codigoContrato;
    @XmlElement(nillable = true)
    protected List<UnidadeMetrica> excluindoUnidadesMetricas;
    protected ServicoEstado servicoEstado;
    protected String servicoUuid;
    protected TipoConsumoEstado tipoConsumoEstado;
    protected TipoServico tipoServico;

    /**
     * Gets the value of the clienteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteUuid() {
        return clienteUuid;
    }

    /**
     * Sets the value of the clienteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteUuid(String value) {
        this.clienteUuid = value;
    }

    /**
     * Gets the value of the codigoContrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoContrato() {
        return codigoContrato;
    }

    /**
     * Sets the value of the codigoContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoContrato(String value) {
        this.codigoContrato = value;
    }

    /**
     * Gets the value of the excluindoUnidadesMetricas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the excluindoUnidadesMetricas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExcluindoUnidadesMetricas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UnidadeMetrica }
     * 
     * 
     */
    public List<UnidadeMetrica> getExcluindoUnidadesMetricas() {
        if (excluindoUnidadesMetricas == null) {
            excluindoUnidadesMetricas = new ArrayList<UnidadeMetrica>();
        }
        return this.excluindoUnidadesMetricas;
    }

    /**
     * Gets the value of the servicoEstado property.
     * 
     * @return
     *     possible object is
     *     {@link ServicoEstado }
     *     
     */
    public ServicoEstado getServicoEstado() {
        return servicoEstado;
    }

    /**
     * Sets the value of the servicoEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicoEstado }
     *     
     */
    public void setServicoEstado(ServicoEstado value) {
        this.servicoEstado = value;
    }

    /**
     * Gets the value of the servicoUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicoUuid() {
        return servicoUuid;
    }

    /**
     * Sets the value of the servicoUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicoUuid(String value) {
        this.servicoUuid = value;
    }

    /**
     * Gets the value of the tipoConsumoEstado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoConsumoEstado }
     *     
     */
    public TipoConsumoEstado getTipoConsumoEstado() {
        return tipoConsumoEstado;
    }

    /**
     * Sets the value of the tipoConsumoEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConsumoEstado }
     *     
     */
    public void setTipoConsumoEstado(TipoConsumoEstado value) {
        this.tipoConsumoEstado = value;
    }

    /**
     * Gets the value of the tipoServico property.
     * 
     * @return
     *     possible object is
     *     {@link TipoServico }
     *     
     */
    public TipoServico getTipoServico() {
        return tipoServico;
    }

    /**
     * Sets the value of the tipoServico property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoServico }
     *     
     */
    public void setTipoServico(TipoServico value) {
        this.tipoServico = value;
    }

}
