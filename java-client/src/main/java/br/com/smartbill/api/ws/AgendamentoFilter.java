
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for agendamentoFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="agendamentoFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="modeloNome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modeloUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "agendamentoFilter", propOrder = {
    "modeloNome",
    "modeloUuid"
})
public class AgendamentoFilter {

    protected String modeloNome;
    protected String modeloUuid;

    /**
     * Gets the value of the modeloNome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModeloNome() {
        return modeloNome;
    }

    /**
     * Sets the value of the modeloNome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeloNome(String value) {
        this.modeloNome = value;
    }

    /**
     * Gets the value of the modeloUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModeloUuid() {
        return modeloUuid;
    }

    /**
     * Sets the value of the modeloUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeloUuid(String value) {
        this.modeloUuid = value;
    }

}
