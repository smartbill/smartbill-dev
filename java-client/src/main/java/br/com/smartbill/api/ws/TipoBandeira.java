
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoBandeira.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoBandeira">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="VISA"/>
 *     &lt;enumeration value="MASTERCARD"/>
 *     &lt;enumeration value="HIPERCARD"/>
 *     &lt;enumeration value="AMEX"/>
 *     &lt;enumeration value="DINERS"/>
 *     &lt;enumeration value="ELO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoBandeira")
@XmlEnum
public enum TipoBandeira {

    VISA,
    MASTERCARD,
    HIPERCARD,
    AMEX,
    DINERS,
    ELO;

    public String value() {
        return name();
    }

    public static TipoBandeira fromValue(String v) {
        return valueOf(v);
    }

}
