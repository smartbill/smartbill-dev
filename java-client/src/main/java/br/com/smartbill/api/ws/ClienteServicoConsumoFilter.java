
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for clienteServicoConsumoFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteServicoConsumoFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anoReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="clienteServicoUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clienteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mesReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="tipoConsumo" type="{http://ws.api.smartbill.com.br/}tipoConsumo" minOccurs="0"/>
 *         &lt;element name="tipoConsumoEstado" type="{http://ws.api.smartbill.com.br/}tipoConsumoEstado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteServicoConsumoFilter", propOrder = {
    "anoReferencia",
    "clienteServicoUuid",
    "clienteUuid",
    "mesReferencia",
    "tipoConsumo",
    "tipoConsumoEstado"
})
public class ClienteServicoConsumoFilter {

    protected Integer anoReferencia;
    protected String clienteServicoUuid;
    protected String clienteUuid;
    protected Integer mesReferencia;
    protected TipoConsumo tipoConsumo;
    protected TipoConsumoEstado tipoConsumoEstado;

    /**
     * Gets the value of the anoReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnoReferencia() {
        return anoReferencia;
    }

    /**
     * Sets the value of the anoReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnoReferencia(Integer value) {
        this.anoReferencia = value;
    }

    /**
     * Gets the value of the clienteServicoUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteServicoUuid() {
        return clienteServicoUuid;
    }

    /**
     * Sets the value of the clienteServicoUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteServicoUuid(String value) {
        this.clienteServicoUuid = value;
    }

    /**
     * Gets the value of the clienteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteUuid() {
        return clienteUuid;
    }

    /**
     * Sets the value of the clienteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteUuid(String value) {
        this.clienteUuid = value;
    }

    /**
     * Gets the value of the mesReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMesReferencia() {
        return mesReferencia;
    }

    /**
     * Sets the value of the mesReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMesReferencia(Integer value) {
        this.mesReferencia = value;
    }

    /**
     * Gets the value of the tipoConsumo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoConsumo }
     *     
     */
    public TipoConsumo getTipoConsumo() {
        return tipoConsumo;
    }

    /**
     * Sets the value of the tipoConsumo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConsumo }
     *     
     */
    public void setTipoConsumo(TipoConsumo value) {
        this.tipoConsumo = value;
    }

    /**
     * Gets the value of the tipoConsumoEstado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoConsumoEstado }
     *     
     */
    public TipoConsumoEstado getTipoConsumoEstado() {
        return tipoConsumoEstado;
    }

    /**
     * Sets the value of the tipoConsumoEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConsumoEstado }
     *     
     */
    public void setTipoConsumoEstado(TipoConsumoEstado value) {
        this.tipoConsumoEstado = value;
    }

}
