
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for layoutEmail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="layoutEmail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cabecalhoEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="campoCabecalhoEmail" type="{http://ws.api.smartbill.com.br/}tipoCampoEmail" minOccurs="0"/>
 *         &lt;element name="campoRodapeEmail" type="{http://ws.api.smartbill.com.br/}tipoCampoEmail" minOccurs="0"/>
 *         &lt;element name="corlinhaSeparadoraCabecalhoEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="corlinhaSeparadoraRodapeEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="corpoEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="espessuralinhaSeparadoraCabecalhoEmail" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="espessuralinhaSeparadoraRodapeEmail" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rodapeEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "layoutEmail", propOrder = {
    "cabecalhoEmail",
    "campoCabecalhoEmail",
    "campoRodapeEmail",
    "corlinhaSeparadoraCabecalhoEmail",
    "corlinhaSeparadoraRodapeEmail",
    "corpoEmail",
    "criacao",
    "espessuralinhaSeparadoraCabecalhoEmail",
    "espessuralinhaSeparadoraRodapeEmail",
    "exclusao",
    "gerenteUuid",
    "modificacao",
    "nome",
    "rodapeEmail",
    "uuid"
})
public class LayoutEmail {

    protected String cabecalhoEmail;
    protected TipoCampoEmail campoCabecalhoEmail;
    protected TipoCampoEmail campoRodapeEmail;
    protected String corlinhaSeparadoraCabecalhoEmail;
    protected String corlinhaSeparadoraRodapeEmail;
    protected String corpoEmail;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    protected int espessuralinhaSeparadoraCabecalhoEmail;
    protected int espessuralinhaSeparadoraRodapeEmail;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected String nome;
    protected String rodapeEmail;
    protected String uuid;

    /**
     * Gets the value of the cabecalhoEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabecalhoEmail() {
        return cabecalhoEmail;
    }

    /**
     * Sets the value of the cabecalhoEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabecalhoEmail(String value) {
        this.cabecalhoEmail = value;
    }

    /**
     * Gets the value of the campoCabecalhoEmail property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCampoEmail }
     *     
     */
    public TipoCampoEmail getCampoCabecalhoEmail() {
        return campoCabecalhoEmail;
    }

    /**
     * Sets the value of the campoCabecalhoEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCampoEmail }
     *     
     */
    public void setCampoCabecalhoEmail(TipoCampoEmail value) {
        this.campoCabecalhoEmail = value;
    }

    /**
     * Gets the value of the campoRodapeEmail property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCampoEmail }
     *     
     */
    public TipoCampoEmail getCampoRodapeEmail() {
        return campoRodapeEmail;
    }

    /**
     * Sets the value of the campoRodapeEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCampoEmail }
     *     
     */
    public void setCampoRodapeEmail(TipoCampoEmail value) {
        this.campoRodapeEmail = value;
    }

    /**
     * Gets the value of the corlinhaSeparadoraCabecalhoEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorlinhaSeparadoraCabecalhoEmail() {
        return corlinhaSeparadoraCabecalhoEmail;
    }

    /**
     * Sets the value of the corlinhaSeparadoraCabecalhoEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorlinhaSeparadoraCabecalhoEmail(String value) {
        this.corlinhaSeparadoraCabecalhoEmail = value;
    }

    /**
     * Gets the value of the corlinhaSeparadoraRodapeEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorlinhaSeparadoraRodapeEmail() {
        return corlinhaSeparadoraRodapeEmail;
    }

    /**
     * Sets the value of the corlinhaSeparadoraRodapeEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorlinhaSeparadoraRodapeEmail(String value) {
        this.corlinhaSeparadoraRodapeEmail = value;
    }

    /**
     * Gets the value of the corpoEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpoEmail() {
        return corpoEmail;
    }

    /**
     * Sets the value of the corpoEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpoEmail(String value) {
        this.corpoEmail = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the espessuralinhaSeparadoraCabecalhoEmail property.
     * 
     */
    public int getEspessuralinhaSeparadoraCabecalhoEmail() {
        return espessuralinhaSeparadoraCabecalhoEmail;
    }

    /**
     * Sets the value of the espessuralinhaSeparadoraCabecalhoEmail property.
     * 
     */
    public void setEspessuralinhaSeparadoraCabecalhoEmail(int value) {
        this.espessuralinhaSeparadoraCabecalhoEmail = value;
    }

    /**
     * Gets the value of the espessuralinhaSeparadoraRodapeEmail property.
     * 
     */
    public int getEspessuralinhaSeparadoraRodapeEmail() {
        return espessuralinhaSeparadoraRodapeEmail;
    }

    /**
     * Sets the value of the espessuralinhaSeparadoraRodapeEmail property.
     * 
     */
    public void setEspessuralinhaSeparadoraRodapeEmail(int value) {
        this.espessuralinhaSeparadoraRodapeEmail = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the rodapeEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRodapeEmail() {
        return rodapeEmail;
    }

    /**
     * Sets the value of the rodapeEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRodapeEmail(String value) {
        this.rodapeEmail = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
