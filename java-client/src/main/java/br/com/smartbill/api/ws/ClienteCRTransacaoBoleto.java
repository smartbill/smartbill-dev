
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteCRTransacaoBoleto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteCRTransacaoBoleto">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.api.smartbill.com.br/}clienteCRTransacao">
 *       &lt;sequence>
 *         &lt;element name="boletoUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chavePedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chaveRequisicao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chaveTransacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoBarra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataVencimento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="nossoNumero" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="referenciaPedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referenciaTransacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="situacaoPedido" type="{http://ws.api.smartbill.com.br/}tipoStatusGateway" minOccurs="0"/>
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteCRTransacaoBoleto", propOrder = {
    "boletoUrl",
    "chavePedido",
    "chaveRequisicao",
    "chaveTransacao",
    "codigoBarra",
    "dataVencimento",
    "nossoNumero",
    "referenciaPedido",
    "referenciaTransacao",
    "situacaoPedido",
    "valor"
})
public class ClienteCRTransacaoBoleto
    extends ClienteCRTransacao
{

    protected String boletoUrl;
    protected String chavePedido;
    protected String chaveRequisicao;
    protected String chaveTransacao;
    protected String codigoBarra;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataVencimento;
    protected Long nossoNumero;
    protected String referenciaPedido;
    protected String referenciaTransacao;
    protected TipoStatusGateway situacaoPedido;
    protected BigDecimal valor;

    /**
     * Gets the value of the boletoUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoletoUrl() {
        return boletoUrl;
    }

    /**
     * Sets the value of the boletoUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoletoUrl(String value) {
        this.boletoUrl = value;
    }

    /**
     * Gets the value of the chavePedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChavePedido() {
        return chavePedido;
    }

    /**
     * Sets the value of the chavePedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChavePedido(String value) {
        this.chavePedido = value;
    }

    /**
     * Gets the value of the chaveRequisicao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChaveRequisicao() {
        return chaveRequisicao;
    }

    /**
     * Sets the value of the chaveRequisicao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChaveRequisicao(String value) {
        this.chaveRequisicao = value;
    }

    /**
     * Gets the value of the chaveTransacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChaveTransacao() {
        return chaveTransacao;
    }

    /**
     * Sets the value of the chaveTransacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChaveTransacao(String value) {
        this.chaveTransacao = value;
    }

    /**
     * Gets the value of the codigoBarra property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoBarra() {
        return codigoBarra;
    }

    /**
     * Sets the value of the codigoBarra property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoBarra(String value) {
        this.codigoBarra = value;
    }

    /**
     * Gets the value of the dataVencimento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataVencimento() {
        return dataVencimento;
    }

    /**
     * Sets the value of the dataVencimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataVencimento(XMLGregorianCalendar value) {
        this.dataVencimento = value;
    }

    /**
     * Gets the value of the nossoNumero property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNossoNumero() {
        return nossoNumero;
    }

    /**
     * Sets the value of the nossoNumero property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNossoNumero(Long value) {
        this.nossoNumero = value;
    }

    /**
     * Gets the value of the referenciaPedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaPedido() {
        return referenciaPedido;
    }

    /**
     * Sets the value of the referenciaPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaPedido(String value) {
        this.referenciaPedido = value;
    }

    /**
     * Gets the value of the referenciaTransacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaTransacao() {
        return referenciaTransacao;
    }

    /**
     * Sets the value of the referenciaTransacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaTransacao(String value) {
        this.referenciaTransacao = value;
    }

    /**
     * Gets the value of the situacaoPedido property.
     * 
     * @return
     *     possible object is
     *     {@link TipoStatusGateway }
     *     
     */
    public TipoStatusGateway getSituacaoPedido() {
        return situacaoPedido;
    }

    /**
     * Sets the value of the situacaoPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoStatusGateway }
     *     
     */
    public void setSituacaoPedido(TipoStatusGateway value) {
        this.situacaoPedido = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValor(BigDecimal value) {
        this.valor = value;
    }

}
