
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for relacionamento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="relacionamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clienteCRTransacaoUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clienteCRUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clienteNFUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clienteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="comentario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="emailUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enviarEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="tipoRelacionamento" type="{http://ws.api.smartbill.com.br/}tipoRelacionamento" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "relacionamento", propOrder = {
    "clienteCRTransacaoUuid",
    "clienteCRUuid",
    "clienteNFUuid",
    "clienteUuid",
    "comentario",
    "criacao",
    "emailUuid",
    "enviarEmail",
    "exclusao",
    "gerenteUuid",
    "modificacao",
    "tipoRelacionamento",
    "uuid"
})
public class Relacionamento {

    protected String clienteCRTransacaoUuid;
    protected String clienteCRUuid;
    protected String clienteNFUuid;
    protected String clienteUuid;
    protected String comentario;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    protected String emailUuid;
    protected boolean enviarEmail;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected TipoRelacionamento tipoRelacionamento;
    protected String uuid;

    /**
     * Gets the value of the clienteCRTransacaoUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteCRTransacaoUuid() {
        return clienteCRTransacaoUuid;
    }

    /**
     * Sets the value of the clienteCRTransacaoUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteCRTransacaoUuid(String value) {
        this.clienteCRTransacaoUuid = value;
    }

    /**
     * Gets the value of the clienteCRUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteCRUuid() {
        return clienteCRUuid;
    }

    /**
     * Sets the value of the clienteCRUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteCRUuid(String value) {
        this.clienteCRUuid = value;
    }

    /**
     * Gets the value of the clienteNFUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteNFUuid() {
        return clienteNFUuid;
    }

    /**
     * Sets the value of the clienteNFUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteNFUuid(String value) {
        this.clienteNFUuid = value;
    }

    /**
     * Gets the value of the clienteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteUuid() {
        return clienteUuid;
    }

    /**
     * Sets the value of the clienteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteUuid(String value) {
        this.clienteUuid = value;
    }

    /**
     * Gets the value of the comentario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * Sets the value of the comentario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComentario(String value) {
        this.comentario = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the emailUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailUuid() {
        return emailUuid;
    }

    /**
     * Sets the value of the emailUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailUuid(String value) {
        this.emailUuid = value;
    }

    /**
     * Gets the value of the enviarEmail property.
     * 
     */
    public boolean isEnviarEmail() {
        return enviarEmail;
    }

    /**
     * Sets the value of the enviarEmail property.
     * 
     */
    public void setEnviarEmail(boolean value) {
        this.enviarEmail = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the tipoRelacionamento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoRelacionamento }
     *     
     */
    public TipoRelacionamento getTipoRelacionamento() {
        return tipoRelacionamento;
    }

    /**
     * Sets the value of the tipoRelacionamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoRelacionamento }
     *     
     */
    public void setTipoRelacionamento(TipoRelacionamento value) {
        this.tipoRelacionamento = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
