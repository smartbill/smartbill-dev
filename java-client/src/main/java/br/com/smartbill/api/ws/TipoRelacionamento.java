
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoRelacionamento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoRelacionamento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ANOTACAO"/>
 *     &lt;enumeration value="COBRANCA"/>
 *     &lt;enumeration value="RENOVACAO_DE_CONTRATO"/>
 *     &lt;enumeration value="CANCELAMENTO_DE_CONTRATO"/>
 *     &lt;enumeration value="REAJUSTE_DE_CONTRATO"/>
 *     &lt;enumeration value="AUMENTO_DE_PLANO"/>
 *     &lt;enumeration value="VENDA_DE_NOVOS_MODULOS_ADDONS"/>
 *     &lt;enumeration value="BOLETO"/>
 *     &lt;enumeration value="CARTAO_CREDITO"/>
 *     &lt;enumeration value="NOTA_FISCAL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoRelacionamento")
@XmlEnum
public enum TipoRelacionamento {

    ANOTACAO,
    COBRANCA,
    RENOVACAO_DE_CONTRATO,
    CANCELAMENTO_DE_CONTRATO,
    REAJUSTE_DE_CONTRATO,
    AUMENTO_DE_PLANO,
    VENDA_DE_NOVOS_MODULOS_ADDONS,
    BOLETO,
    CARTAO_CREDITO,
    NOTA_FISCAL;

    public String value() {
        return name();
    }

    public static TipoRelacionamento fromValue(String v) {
        return valueOf(v);
    }

}
