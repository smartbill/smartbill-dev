
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoNFCancelamento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoNFCancelamento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ERRO_NA_EMISSAO"/>
 *     &lt;enumeration value="SERVICO_NAO_PRESTADO"/>
 *     &lt;enumeration value="DUPLICIDADE_NA_NOTA"/>
 *     &lt;enumeration value="OUTROS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoNFCancelamento")
@XmlEnum
public enum TipoNFCancelamento {

    ERRO_NA_EMISSAO,
    SERVICO_NAO_PRESTADO,
    DUPLICIDADE_NA_NOTA,
    OUTROS;

    public String value() {
        return name();
    }

    public static TipoNFCancelamento fromValue(String v) {
        return valueOf(v);
    }

}
