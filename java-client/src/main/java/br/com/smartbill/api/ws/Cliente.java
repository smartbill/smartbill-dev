
package br.com.smartbill.api.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for cliente complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cliente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bloqueioFaturamento" type="{http://ws.api.smartbill.com.br/}bloqueioFaturamento" minOccurs="0"/>
 *         &lt;element name="cartoes" type="{http://ws.api.smartbill.com.br/}clienteCartao" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="cicloFaturamento" type="{http://ws.api.smartbill.com.br/}cicloFaturamento" minOccurs="0"/>
 *         &lt;element name="cnpjCpf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="comoConheceu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="complementoDoCorpoDaNFe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contatos" type="{http://ws.api.smartbill.com.br/}contato" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="diaVencimento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="documentoFiscal" type="{http://ws.api.smartbill.com.br/}documentoFiscal" minOccurs="0"/>
 *         &lt;element name="domicilioFaturamento" type="{http://ws.api.smartbill.com.br/}cidade" minOccurs="0"/>
 *         &lt;element name="dominio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emailGeral" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enderecos" type="{http://ws.api.smartbill.com.br/}endereco" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ieRg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="im" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indicadoPor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="meioPagamento" type="{http://ws.api.smartbill.com.br/}meioPagamento" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="nomeFantasia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orgaoPublico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ramoAtividade" type="{http://ws.api.smartbill.com.br/}ramoAtividade" minOccurs="0"/>
 *         &lt;element name="razaoSocial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefoneDdd" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="telefoneNumero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoPessoa" type="{http://ws.api.smartbill.com.br/}tipoPessoa" minOccurs="0"/>
 *         &lt;element name="urlPropria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cliente", propOrder = {
    "bloqueioFaturamento",
    "cartoes",
    "cicloFaturamento",
    "cnpjCpf",
    "codigo",
    "comoConheceu",
    "complementoDoCorpoDaNFe",
    "contatos",
    "criacao",
    "diaVencimento",
    "documentoFiscal",
    "domicilioFaturamento",
    "dominio",
    "emailGeral",
    "enderecos",
    "exclusao",
    "gerenteUuid",
    "ieRg",
    "im",
    "indicadoPor",
    "meioPagamento",
    "modificacao",
    "nomeFantasia",
    "orgaoPublico",
    "ramoAtividade",
    "razaoSocial",
    "telefoneDdd",
    "telefoneNumero",
    "tipoPessoa",
    "urlPropria",
    "uuid"
})
public class Cliente {

    protected BloqueioFaturamento bloqueioFaturamento;
    @XmlElement(nillable = true)
    protected List<ClienteCartao> cartoes;
    protected CicloFaturamento cicloFaturamento;
    protected String cnpjCpf;
    protected String codigo;
    protected String comoConheceu;
    protected String complementoDoCorpoDaNFe;
    @XmlElement(nillable = true)
    protected List<Contato> contatos;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    protected Integer diaVencimento;
    protected DocumentoFiscal documentoFiscal;
    protected Cidade domicilioFaturamento;
    protected String dominio;
    protected String emailGeral;
    @XmlElement(nillable = true)
    protected List<Endereco> enderecos;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    protected String ieRg;
    protected String im;
    protected String indicadoPor;
    protected MeioPagamento meioPagamento;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected String nomeFantasia;
    protected boolean orgaoPublico;
    protected RamoAtividade ramoAtividade;
    protected String razaoSocial;
    protected Integer telefoneDdd;
    protected String telefoneNumero;
    protected TipoPessoa tipoPessoa;
    protected String urlPropria;
    protected String uuid;

    /**
     * Gets the value of the bloqueioFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link BloqueioFaturamento }
     *     
     */
    public BloqueioFaturamento getBloqueioFaturamento() {
        return bloqueioFaturamento;
    }

    /**
     * Sets the value of the bloqueioFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link BloqueioFaturamento }
     *     
     */
    public void setBloqueioFaturamento(BloqueioFaturamento value) {
        this.bloqueioFaturamento = value;
    }

    /**
     * Gets the value of the cartoes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cartoes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCartoes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteCartao }
     * 
     * 
     */
    public List<ClienteCartao> getCartoes() {
        if (cartoes == null) {
            cartoes = new ArrayList<ClienteCartao>();
        }
        return this.cartoes;
    }

    /**
     * Gets the value of the cicloFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link CicloFaturamento }
     *     
     */
    public CicloFaturamento getCicloFaturamento() {
        return cicloFaturamento;
    }

    /**
     * Sets the value of the cicloFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link CicloFaturamento }
     *     
     */
    public void setCicloFaturamento(CicloFaturamento value) {
        this.cicloFaturamento = value;
    }

    /**
     * Gets the value of the cnpjCpf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnpjCpf() {
        return cnpjCpf;
    }

    /**
     * Sets the value of the cnpjCpf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnpjCpf(String value) {
        this.cnpjCpf = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the comoConheceu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComoConheceu() {
        return comoConheceu;
    }

    /**
     * Sets the value of the comoConheceu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComoConheceu(String value) {
        this.comoConheceu = value;
    }

    /**
     * Gets the value of the complementoDoCorpoDaNFe property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplementoDoCorpoDaNFe() {
        return complementoDoCorpoDaNFe;
    }

    /**
     * Sets the value of the complementoDoCorpoDaNFe property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplementoDoCorpoDaNFe(String value) {
        this.complementoDoCorpoDaNFe = value;
    }

    /**
     * Gets the value of the contatos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contatos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContatos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Contato }
     * 
     * 
     */
    public List<Contato> getContatos() {
        if (contatos == null) {
            contatos = new ArrayList<Contato>();
        }
        return this.contatos;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the diaVencimento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDiaVencimento() {
        return diaVencimento;
    }

    /**
     * Sets the value of the diaVencimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDiaVencimento(Integer value) {
        this.diaVencimento = value;
    }

    /**
     * Gets the value of the documentoFiscal property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFiscal }
     *     
     */
    public DocumentoFiscal getDocumentoFiscal() {
        return documentoFiscal;
    }

    /**
     * Sets the value of the documentoFiscal property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFiscal }
     *     
     */
    public void setDocumentoFiscal(DocumentoFiscal value) {
        this.documentoFiscal = value;
    }

    /**
     * Gets the value of the domicilioFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link Cidade }
     *     
     */
    public Cidade getDomicilioFaturamento() {
        return domicilioFaturamento;
    }

    /**
     * Sets the value of the domicilioFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cidade }
     *     
     */
    public void setDomicilioFaturamento(Cidade value) {
        this.domicilioFaturamento = value;
    }

    /**
     * Gets the value of the dominio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDominio() {
        return dominio;
    }

    /**
     * Sets the value of the dominio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDominio(String value) {
        this.dominio = value;
    }

    /**
     * Gets the value of the emailGeral property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailGeral() {
        return emailGeral;
    }

    /**
     * Sets the value of the emailGeral property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailGeral(String value) {
        this.emailGeral = value;
    }

    /**
     * Gets the value of the enderecos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the enderecos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEnderecos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Endereco }
     * 
     * 
     */
    public List<Endereco> getEnderecos() {
        if (enderecos == null) {
            enderecos = new ArrayList<Endereco>();
        }
        return this.enderecos;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the ieRg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIeRg() {
        return ieRg;
    }

    /**
     * Sets the value of the ieRg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIeRg(String value) {
        this.ieRg = value;
    }

    /**
     * Gets the value of the im property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIm() {
        return im;
    }

    /**
     * Sets the value of the im property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIm(String value) {
        this.im = value;
    }

    /**
     * Gets the value of the indicadoPor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadoPor() {
        return indicadoPor;
    }

    /**
     * Sets the value of the indicadoPor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadoPor(String value) {
        this.indicadoPor = value;
    }

    /**
     * Gets the value of the meioPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link MeioPagamento }
     *     
     */
    public MeioPagamento getMeioPagamento() {
        return meioPagamento;
    }

    /**
     * Sets the value of the meioPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link MeioPagamento }
     *     
     */
    public void setMeioPagamento(MeioPagamento value) {
        this.meioPagamento = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the nomeFantasia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeFantasia() {
        return nomeFantasia;
    }

    /**
     * Sets the value of the nomeFantasia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeFantasia(String value) {
        this.nomeFantasia = value;
    }

    /**
     * Gets the value of the orgaoPublico property.
     * 
     */
    public boolean isOrgaoPublico() {
        return orgaoPublico;
    }

    /**
     * Sets the value of the orgaoPublico property.
     * 
     */
    public void setOrgaoPublico(boolean value) {
        this.orgaoPublico = value;
    }

    /**
     * Gets the value of the ramoAtividade property.
     * 
     * @return
     *     possible object is
     *     {@link RamoAtividade }
     *     
     */
    public RamoAtividade getRamoAtividade() {
        return ramoAtividade;
    }

    /**
     * Sets the value of the ramoAtividade property.
     * 
     * @param value
     *     allowed object is
     *     {@link RamoAtividade }
     *     
     */
    public void setRamoAtividade(RamoAtividade value) {
        this.ramoAtividade = value;
    }

    /**
     * Gets the value of the razaoSocial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazaoSocial() {
        return razaoSocial;
    }

    /**
     * Sets the value of the razaoSocial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazaoSocial(String value) {
        this.razaoSocial = value;
    }

    /**
     * Gets the value of the telefoneDdd property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTelefoneDdd() {
        return telefoneDdd;
    }

    /**
     * Sets the value of the telefoneDdd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTelefoneDdd(Integer value) {
        this.telefoneDdd = value;
    }

    /**
     * Gets the value of the telefoneNumero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefoneNumero() {
        return telefoneNumero;
    }

    /**
     * Sets the value of the telefoneNumero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefoneNumero(String value) {
        this.telefoneNumero = value;
    }

    /**
     * Gets the value of the tipoPessoa property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPessoa }
     *     
     */
    public TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }

    /**
     * Sets the value of the tipoPessoa property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPessoa }
     *     
     */
    public void setTipoPessoa(TipoPessoa value) {
        this.tipoPessoa = value;
    }

    /**
     * Gets the value of the urlPropria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlPropria() {
        return urlPropria;
    }

    /**
     * Sets the value of the urlPropria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlPropria(String value) {
        this.urlPropria = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
