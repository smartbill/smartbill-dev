
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoStatusEmailCobranca.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoStatusEmailCobranca">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="APROVADO"/>
 *     &lt;enumeration value="ETAPA_UM"/>
 *     &lt;enumeration value="ETAPA_DOIS"/>
 *     &lt;enumeration value="NAO_CONFIGURADO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoStatusEmailCobranca")
@XmlEnum
public enum TipoStatusEmailCobranca {

    APROVADO,
    ETAPA_UM,
    ETAPA_DOIS,
    NAO_CONFIGURADO;

    public String value() {
        return name();
    }

    public static TipoStatusEmailCobranca fromValue(String v) {
        return valueOf(v);
    }

}
