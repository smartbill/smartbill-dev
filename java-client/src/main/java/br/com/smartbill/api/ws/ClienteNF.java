
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteNF complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteNF">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="alertaRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aliquotaCOFINS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaCSLL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaINSS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaIR" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaISS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaISSRetido" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaPIS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCNAE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoServicoNF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoVerificacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataEmissao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descricaoRPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="erroRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="incentivadorCultural" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loteNF" type="{http://ws.api.smartbill.com.br/}loteNF" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="motivoCancelamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="naturezaOperacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroNFS" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="numeroRPS" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="regimeTributario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serieRPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="servicosCR" type="{http://ws.api.smartbill.com.br/}clienteServicoCR" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="simplesNacional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="textoEnvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="textoRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoAmbiente" type="{http://ws.api.smartbill.com.br/}tipoAmbiente" minOccurs="0"/>
 *         &lt;element name="tipoNFCancelamento" type="{http://ws.api.smartbill.com.br/}tipoNFCancelamento" minOccurs="0"/>
 *         &lt;element name="tipoNFEstado" type="{http://ws.api.smartbill.com.br/}tipoNFEstado" minOccurs="0"/>
 *         &lt;element name="tributacaoMunicipio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="urlConsulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valorCOFINS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorCSLL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorDeducao" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorINSS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorIR" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorISS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorISSRetido" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorPIS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valorServicos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteNF", propOrder = {
    "alertaRetorno",
    "aliquotaCOFINS",
    "aliquotaCSLL",
    "aliquotaINSS",
    "aliquotaIR",
    "aliquotaISS",
    "aliquotaISSRetido",
    "aliquotaPIS",
    "codigo",
    "codigoCNAE",
    "codigoServicoNF",
    "codigoVerificacao",
    "criacao",
    "dataEmissao",
    "descricaoRPS",
    "erroRetorno",
    "exclusao",
    "gerenteUuid",
    "incentivadorCultural",
    "loteNF",
    "modificacao",
    "motivoCancelamento",
    "naturezaOperacao",
    "numeroNFS",
    "numeroRPS",
    "regimeTributario",
    "serieRPS",
    "servicosCR",
    "simplesNacional",
    "textoEnvio",
    "textoRetorno",
    "tipoAmbiente",
    "tipoNFCancelamento",
    "tipoNFEstado",
    "tributacaoMunicipio",
    "urlConsulta",
    "uuid",
    "valorCOFINS",
    "valorCSLL",
    "valorDeducao",
    "valorINSS",
    "valorIR",
    "valorISS",
    "valorISSRetido",
    "valorPIS",
    "valorServicos"
})
public class ClienteNF {

    protected String alertaRetorno;
    protected BigDecimal aliquotaCOFINS;
    protected BigDecimal aliquotaCSLL;
    protected BigDecimal aliquotaINSS;
    protected BigDecimal aliquotaIR;
    protected BigDecimal aliquotaISS;
    protected BigDecimal aliquotaISSRetido;
    protected BigDecimal aliquotaPIS;
    protected String codigo;
    protected String codigoCNAE;
    protected String codigoServicoNF;
    protected String codigoVerificacao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEmissao;
    protected String descricaoRPS;
    protected String erroRetorno;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    protected String incentivadorCultural;
    protected LoteNF loteNF;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected String motivoCancelamento;
    protected String naturezaOperacao;
    protected BigInteger numeroNFS;
    protected Long numeroRPS;
    protected String regimeTributario;
    protected String serieRPS;
    @XmlElement(nillable = true)
    protected List<ClienteServicoCR> servicosCR;
    protected String simplesNacional;
    protected String textoEnvio;
    protected String textoRetorno;
    protected TipoAmbiente tipoAmbiente;
    protected TipoNFCancelamento tipoNFCancelamento;
    protected TipoNFEstado tipoNFEstado;
    protected String tributacaoMunicipio;
    protected String urlConsulta;
    protected String uuid;
    protected BigDecimal valorCOFINS;
    protected BigDecimal valorCSLL;
    protected BigDecimal valorDeducao;
    protected BigDecimal valorINSS;
    protected BigDecimal valorIR;
    protected BigDecimal valorISS;
    protected BigDecimal valorISSRetido;
    protected BigDecimal valorPIS;
    protected BigDecimal valorServicos;

    /**
     * Gets the value of the alertaRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlertaRetorno() {
        return alertaRetorno;
    }

    /**
     * Sets the value of the alertaRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlertaRetorno(String value) {
        this.alertaRetorno = value;
    }

    /**
     * Gets the value of the aliquotaCOFINS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaCOFINS() {
        return aliquotaCOFINS;
    }

    /**
     * Sets the value of the aliquotaCOFINS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaCOFINS(BigDecimal value) {
        this.aliquotaCOFINS = value;
    }

    /**
     * Gets the value of the aliquotaCSLL property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaCSLL() {
        return aliquotaCSLL;
    }

    /**
     * Sets the value of the aliquotaCSLL property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaCSLL(BigDecimal value) {
        this.aliquotaCSLL = value;
    }

    /**
     * Gets the value of the aliquotaINSS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaINSS() {
        return aliquotaINSS;
    }

    /**
     * Sets the value of the aliquotaINSS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaINSS(BigDecimal value) {
        this.aliquotaINSS = value;
    }

    /**
     * Gets the value of the aliquotaIR property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaIR() {
        return aliquotaIR;
    }

    /**
     * Sets the value of the aliquotaIR property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaIR(BigDecimal value) {
        this.aliquotaIR = value;
    }

    /**
     * Gets the value of the aliquotaISS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISS() {
        return aliquotaISS;
    }

    /**
     * Sets the value of the aliquotaISS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISS(BigDecimal value) {
        this.aliquotaISS = value;
    }

    /**
     * Gets the value of the aliquotaISSRetido property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISSRetido() {
        return aliquotaISSRetido;
    }

    /**
     * Sets the value of the aliquotaISSRetido property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISSRetido(BigDecimal value) {
        this.aliquotaISSRetido = value;
    }

    /**
     * Gets the value of the aliquotaPIS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaPIS() {
        return aliquotaPIS;
    }

    /**
     * Sets the value of the aliquotaPIS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaPIS(BigDecimal value) {
        this.aliquotaPIS = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the codigoCNAE property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCNAE() {
        return codigoCNAE;
    }

    /**
     * Sets the value of the codigoCNAE property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCNAE(String value) {
        this.codigoCNAE = value;
    }

    /**
     * Gets the value of the codigoServicoNF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoServicoNF() {
        return codigoServicoNF;
    }

    /**
     * Sets the value of the codigoServicoNF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoServicoNF(String value) {
        this.codigoServicoNF = value;
    }

    /**
     * Gets the value of the codigoVerificacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoVerificacao() {
        return codigoVerificacao;
    }

    /**
     * Sets the value of the codigoVerificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoVerificacao(String value) {
        this.codigoVerificacao = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the dataEmissao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEmissao() {
        return dataEmissao;
    }

    /**
     * Sets the value of the dataEmissao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEmissao(XMLGregorianCalendar value) {
        this.dataEmissao = value;
    }

    /**
     * Gets the value of the descricaoRPS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoRPS() {
        return descricaoRPS;
    }

    /**
     * Sets the value of the descricaoRPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoRPS(String value) {
        this.descricaoRPS = value;
    }

    /**
     * Gets the value of the erroRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErroRetorno() {
        return erroRetorno;
    }

    /**
     * Sets the value of the erroRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErroRetorno(String value) {
        this.erroRetorno = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the incentivadorCultural property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncentivadorCultural() {
        return incentivadorCultural;
    }

    /**
     * Sets the value of the incentivadorCultural property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncentivadorCultural(String value) {
        this.incentivadorCultural = value;
    }

    /**
     * Gets the value of the loteNF property.
     * 
     * @return
     *     possible object is
     *     {@link LoteNF }
     *     
     */
    public LoteNF getLoteNF() {
        return loteNF;
    }

    /**
     * Sets the value of the loteNF property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoteNF }
     *     
     */
    public void setLoteNF(LoteNF value) {
        this.loteNF = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the motivoCancelamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoCancelamento() {
        return motivoCancelamento;
    }

    /**
     * Sets the value of the motivoCancelamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoCancelamento(String value) {
        this.motivoCancelamento = value;
    }

    /**
     * Gets the value of the naturezaOperacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNaturezaOperacao() {
        return naturezaOperacao;
    }

    /**
     * Sets the value of the naturezaOperacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNaturezaOperacao(String value) {
        this.naturezaOperacao = value;
    }

    /**
     * Gets the value of the numeroNFS property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumeroNFS() {
        return numeroNFS;
    }

    /**
     * Sets the value of the numeroNFS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumeroNFS(BigInteger value) {
        this.numeroNFS = value;
    }

    /**
     * Gets the value of the numeroRPS property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroRPS() {
        return numeroRPS;
    }

    /**
     * Sets the value of the numeroRPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroRPS(Long value) {
        this.numeroRPS = value;
    }

    /**
     * Gets the value of the regimeTributario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegimeTributario() {
        return regimeTributario;
    }

    /**
     * Sets the value of the regimeTributario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegimeTributario(String value) {
        this.regimeTributario = value;
    }

    /**
     * Gets the value of the serieRPS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerieRPS() {
        return serieRPS;
    }

    /**
     * Sets the value of the serieRPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerieRPS(String value) {
        this.serieRPS = value;
    }

    /**
     * Gets the value of the servicosCR property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servicosCR property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicosCR().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteServicoCR }
     * 
     * 
     */
    public List<ClienteServicoCR> getServicosCR() {
        if (servicosCR == null) {
            servicosCR = new ArrayList<ClienteServicoCR>();
        }
        return this.servicosCR;
    }

    /**
     * Gets the value of the simplesNacional property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSimplesNacional() {
        return simplesNacional;
    }

    /**
     * Sets the value of the simplesNacional property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSimplesNacional(String value) {
        this.simplesNacional = value;
    }

    /**
     * Gets the value of the textoEnvio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextoEnvio() {
        return textoEnvio;
    }

    /**
     * Sets the value of the textoEnvio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextoEnvio(String value) {
        this.textoEnvio = value;
    }

    /**
     * Gets the value of the textoRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextoRetorno() {
        return textoRetorno;
    }

    /**
     * Sets the value of the textoRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextoRetorno(String value) {
        this.textoRetorno = value;
    }

    /**
     * Gets the value of the tipoAmbiente property.
     * 
     * @return
     *     possible object is
     *     {@link TipoAmbiente }
     *     
     */
    public TipoAmbiente getTipoAmbiente() {
        return tipoAmbiente;
    }

    /**
     * Sets the value of the tipoAmbiente property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoAmbiente }
     *     
     */
    public void setTipoAmbiente(TipoAmbiente value) {
        this.tipoAmbiente = value;
    }

    /**
     * Gets the value of the tipoNFCancelamento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoNFCancelamento }
     *     
     */
    public TipoNFCancelamento getTipoNFCancelamento() {
        return tipoNFCancelamento;
    }

    /**
     * Sets the value of the tipoNFCancelamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoNFCancelamento }
     *     
     */
    public void setTipoNFCancelamento(TipoNFCancelamento value) {
        this.tipoNFCancelamento = value;
    }

    /**
     * Gets the value of the tipoNFEstado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoNFEstado }
     *     
     */
    public TipoNFEstado getTipoNFEstado() {
        return tipoNFEstado;
    }

    /**
     * Sets the value of the tipoNFEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoNFEstado }
     *     
     */
    public void setTipoNFEstado(TipoNFEstado value) {
        this.tipoNFEstado = value;
    }

    /**
     * Gets the value of the tributacaoMunicipio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTributacaoMunicipio() {
        return tributacaoMunicipio;
    }

    /**
     * Sets the value of the tributacaoMunicipio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTributacaoMunicipio(String value) {
        this.tributacaoMunicipio = value;
    }

    /**
     * Gets the value of the urlConsulta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlConsulta() {
        return urlConsulta;
    }

    /**
     * Sets the value of the urlConsulta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlConsulta(String value) {
        this.urlConsulta = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the valorCOFINS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorCOFINS() {
        return valorCOFINS;
    }

    /**
     * Sets the value of the valorCOFINS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorCOFINS(BigDecimal value) {
        this.valorCOFINS = value;
    }

    /**
     * Gets the value of the valorCSLL property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorCSLL() {
        return valorCSLL;
    }

    /**
     * Sets the value of the valorCSLL property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorCSLL(BigDecimal value) {
        this.valorCSLL = value;
    }

    /**
     * Gets the value of the valorDeducao property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorDeducao() {
        return valorDeducao;
    }

    /**
     * Sets the value of the valorDeducao property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorDeducao(BigDecimal value) {
        this.valorDeducao = value;
    }

    /**
     * Gets the value of the valorINSS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorINSS() {
        return valorINSS;
    }

    /**
     * Sets the value of the valorINSS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorINSS(BigDecimal value) {
        this.valorINSS = value;
    }

    /**
     * Gets the value of the valorIR property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorIR() {
        return valorIR;
    }

    /**
     * Sets the value of the valorIR property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorIR(BigDecimal value) {
        this.valorIR = value;
    }

    /**
     * Gets the value of the valorISS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorISS() {
        return valorISS;
    }

    /**
     * Sets the value of the valorISS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorISS(BigDecimal value) {
        this.valorISS = value;
    }

    /**
     * Gets the value of the valorISSRetido property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorISSRetido() {
        return valorISSRetido;
    }

    /**
     * Sets the value of the valorISSRetido property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorISSRetido(BigDecimal value) {
        this.valorISSRetido = value;
    }

    /**
     * Gets the value of the valorPIS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorPIS() {
        return valorPIS;
    }

    /**
     * Sets the value of the valorPIS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorPIS(BigDecimal value) {
        this.valorPIS = value;
    }

    /**
     * Gets the value of the valorServicos property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorServicos() {
        return valorServicos;
    }

    /**
     * Sets the value of the valorServicos property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorServicos(BigDecimal value) {
        this.valorServicos = value;
    }

}
