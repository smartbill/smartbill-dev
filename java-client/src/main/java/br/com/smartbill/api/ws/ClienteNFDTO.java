
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteNFDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteNFDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="alertaRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="anoReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cicloFaturamento" type="{http://ws.api.smartbill.com.br/}cicloFaturamento" minOccurs="0"/>
 *         &lt;element name="clienteCRUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cnpjCpfTomador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataEmissao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="erroRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mesReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroNFS" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="numeroRPS" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="permissao" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="razaoSocialTomador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serieRPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoNFEstado" type="{http://ws.api.smartbill.com.br/}tipoNFEstado" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valorServicos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteNFDTO", propOrder = {
    "alertaRetorno",
    "anoReferencia",
    "cicloFaturamento",
    "clienteCRUuid",
    "cnpjCpfTomador",
    "codigoCR",
    "codigoContrato",
    "dataEmissao",
    "erroRetorno",
    "mesReferencia",
    "numeroNFS",
    "numeroRPS",
    "permissao",
    "razaoSocialTomador",
    "serieRPS",
    "tipoNFEstado",
    "uuid",
    "valorServicos"
})
public class ClienteNFDTO {

    protected String alertaRetorno;
    protected Integer anoReferencia;
    protected CicloFaturamento cicloFaturamento;
    protected String clienteCRUuid;
    protected String cnpjCpfTomador;
    protected String codigoCR;
    protected String codigoContrato;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEmissao;
    protected String erroRetorno;
    protected Integer mesReferencia;
    protected BigInteger numeroNFS;
    protected Long numeroRPS;
    @XmlElement(nillable = true)
    protected List<Integer> permissao;
    protected String razaoSocialTomador;
    protected String serieRPS;
    protected TipoNFEstado tipoNFEstado;
    protected String uuid;
    protected BigDecimal valorServicos;

    /**
     * Gets the value of the alertaRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlertaRetorno() {
        return alertaRetorno;
    }

    /**
     * Sets the value of the alertaRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlertaRetorno(String value) {
        this.alertaRetorno = value;
    }

    /**
     * Gets the value of the anoReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnoReferencia() {
        return anoReferencia;
    }

    /**
     * Sets the value of the anoReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnoReferencia(Integer value) {
        this.anoReferencia = value;
    }

    /**
     * Gets the value of the cicloFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link CicloFaturamento }
     *     
     */
    public CicloFaturamento getCicloFaturamento() {
        return cicloFaturamento;
    }

    /**
     * Sets the value of the cicloFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link CicloFaturamento }
     *     
     */
    public void setCicloFaturamento(CicloFaturamento value) {
        this.cicloFaturamento = value;
    }

    /**
     * Gets the value of the clienteCRUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteCRUuid() {
        return clienteCRUuid;
    }

    /**
     * Sets the value of the clienteCRUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteCRUuid(String value) {
        this.clienteCRUuid = value;
    }

    /**
     * Gets the value of the cnpjCpfTomador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnpjCpfTomador() {
        return cnpjCpfTomador;
    }

    /**
     * Sets the value of the cnpjCpfTomador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnpjCpfTomador(String value) {
        this.cnpjCpfTomador = value;
    }

    /**
     * Gets the value of the codigoCR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCR() {
        return codigoCR;
    }

    /**
     * Sets the value of the codigoCR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCR(String value) {
        this.codigoCR = value;
    }

    /**
     * Gets the value of the codigoContrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoContrato() {
        return codigoContrato;
    }

    /**
     * Sets the value of the codigoContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoContrato(String value) {
        this.codigoContrato = value;
    }

    /**
     * Gets the value of the dataEmissao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEmissao() {
        return dataEmissao;
    }

    /**
     * Sets the value of the dataEmissao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEmissao(XMLGregorianCalendar value) {
        this.dataEmissao = value;
    }

    /**
     * Gets the value of the erroRetorno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErroRetorno() {
        return erroRetorno;
    }

    /**
     * Sets the value of the erroRetorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErroRetorno(String value) {
        this.erroRetorno = value;
    }

    /**
     * Gets the value of the mesReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMesReferencia() {
        return mesReferencia;
    }

    /**
     * Sets the value of the mesReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMesReferencia(Integer value) {
        this.mesReferencia = value;
    }

    /**
     * Gets the value of the numeroNFS property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumeroNFS() {
        return numeroNFS;
    }

    /**
     * Sets the value of the numeroNFS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumeroNFS(BigInteger value) {
        this.numeroNFS = value;
    }

    /**
     * Gets the value of the numeroRPS property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroRPS() {
        return numeroRPS;
    }

    /**
     * Sets the value of the numeroRPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroRPS(Long value) {
        this.numeroRPS = value;
    }

    /**
     * Gets the value of the permissao property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the permissao property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPermissao().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getPermissao() {
        if (permissao == null) {
            permissao = new ArrayList<Integer>();
        }
        return this.permissao;
    }

    /**
     * Gets the value of the razaoSocialTomador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazaoSocialTomador() {
        return razaoSocialTomador;
    }

    /**
     * Sets the value of the razaoSocialTomador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazaoSocialTomador(String value) {
        this.razaoSocialTomador = value;
    }

    /**
     * Gets the value of the serieRPS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerieRPS() {
        return serieRPS;
    }

    /**
     * Sets the value of the serieRPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerieRPS(String value) {
        this.serieRPS = value;
    }

    /**
     * Gets the value of the tipoNFEstado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoNFEstado }
     *     
     */
    public TipoNFEstado getTipoNFEstado() {
        return tipoNFEstado;
    }

    /**
     * Sets the value of the tipoNFEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoNFEstado }
     *     
     */
    public void setTipoNFEstado(TipoNFEstado value) {
        this.tipoNFEstado = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the valorServicos property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorServicos() {
        return valorServicos;
    }

    /**
     * Sets the value of the valorServicos property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorServicos(BigDecimal value) {
        this.valorServicos = value;
    }

}
