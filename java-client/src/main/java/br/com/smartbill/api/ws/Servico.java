
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for servico complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="servico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ativo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoServicoNF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descricaoNF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="familia" type="{http://ws.api.smartbill.com.br/}familia" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="servicos" type="{http://ws.api.smartbill.com.br/}servico" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tarifaMinima" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="taxaAtivacao" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tipoConsumo" type="{http://ws.api.smartbill.com.br/}tipoConsumo" minOccurs="0"/>
 *         &lt;element name="tipoServico" type="{http://ws.api.smartbill.com.br/}tipoServico" minOccurs="0"/>
 *         &lt;element name="tributacaoMunicipio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unidadeMedida" type="{http://ws.api.smartbill.com.br/}unidadeMedida" minOccurs="0"/>
 *         &lt;element name="unidadeMetrica" type="{http://ws.api.smartbill.com.br/}unidadeMetrica" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valores" type="{http://ws.api.smartbill.com.br/}servicoValor" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "servico", propOrder = {
    "ativo",
    "codigo",
    "codigoServicoNF",
    "criacao",
    "descricao",
    "descricaoNF",
    "exclusao",
    "familia",
    "gerenteUuid",
    "modificacao",
    "nome",
    "servicos",
    "tarifaMinima",
    "taxaAtivacao",
    "tipoConsumo",
    "tipoServico",
    "tributacaoMunicipio",
    "unidadeMedida",
    "unidadeMetrica",
    "uuid",
    "valor",
    "valores"
})
public class Servico {

    protected boolean ativo;
    protected String codigo;
    protected String codigoServicoNF;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    protected String descricao;
    protected String descricaoNF;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected Familia familia;
    protected String gerenteUuid;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected String nome;
    @XmlElement(nillable = true)
    protected List<Servico> servicos;
    protected BigDecimal tarifaMinima;
    protected BigDecimal taxaAtivacao;
    protected TipoConsumo tipoConsumo;
    protected TipoServico tipoServico;
    protected String tributacaoMunicipio;
    protected UnidadeMedida unidadeMedida;
    protected UnidadeMetrica unidadeMetrica;
    protected String uuid;
    protected BigDecimal valor;
    @XmlElement(nillable = true)
    protected List<ServicoValor> valores;

    /**
     * Gets the value of the ativo property.
     * 
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * Sets the value of the ativo property.
     * 
     */
    public void setAtivo(boolean value) {
        this.ativo = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the codigoServicoNF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoServicoNF() {
        return codigoServicoNF;
    }

    /**
     * Sets the value of the codigoServicoNF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoServicoNF(String value) {
        this.codigoServicoNF = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the descricao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Sets the value of the descricao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Gets the value of the descricaoNF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoNF() {
        return descricaoNF;
    }

    /**
     * Sets the value of the descricaoNF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoNF(String value) {
        this.descricaoNF = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the familia property.
     * 
     * @return
     *     possible object is
     *     {@link Familia }
     *     
     */
    public Familia getFamilia() {
        return familia;
    }

    /**
     * Sets the value of the familia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Familia }
     *     
     */
    public void setFamilia(Familia value) {
        this.familia = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the servicos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servicos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Servico }
     * 
     * 
     */
    public List<Servico> getServicos() {
        if (servicos == null) {
            servicos = new ArrayList<Servico>();
        }
        return this.servicos;
    }

    /**
     * Gets the value of the tarifaMinima property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaMinima() {
        return tarifaMinima;
    }

    /**
     * Sets the value of the tarifaMinima property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaMinima(BigDecimal value) {
        this.tarifaMinima = value;
    }

    /**
     * Gets the value of the taxaAtivacao property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxaAtivacao() {
        return taxaAtivacao;
    }

    /**
     * Sets the value of the taxaAtivacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxaAtivacao(BigDecimal value) {
        this.taxaAtivacao = value;
    }

    /**
     * Gets the value of the tipoConsumo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoConsumo }
     *     
     */
    public TipoConsumo getTipoConsumo() {
        return tipoConsumo;
    }

    /**
     * Sets the value of the tipoConsumo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConsumo }
     *     
     */
    public void setTipoConsumo(TipoConsumo value) {
        this.tipoConsumo = value;
    }

    /**
     * Gets the value of the tipoServico property.
     * 
     * @return
     *     possible object is
     *     {@link TipoServico }
     *     
     */
    public TipoServico getTipoServico() {
        return tipoServico;
    }

    /**
     * Sets the value of the tipoServico property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoServico }
     *     
     */
    public void setTipoServico(TipoServico value) {
        this.tipoServico = value;
    }

    /**
     * Gets the value of the tributacaoMunicipio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTributacaoMunicipio() {
        return tributacaoMunicipio;
    }

    /**
     * Sets the value of the tributacaoMunicipio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTributacaoMunicipio(String value) {
        this.tributacaoMunicipio = value;
    }

    /**
     * Gets the value of the unidadeMedida property.
     * 
     * @return
     *     possible object is
     *     {@link UnidadeMedida }
     *     
     */
    public UnidadeMedida getUnidadeMedida() {
        return unidadeMedida;
    }

    /**
     * Sets the value of the unidadeMedida property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnidadeMedida }
     *     
     */
    public void setUnidadeMedida(UnidadeMedida value) {
        this.unidadeMedida = value;
    }

    /**
     * Gets the value of the unidadeMetrica property.
     * 
     * @return
     *     possible object is
     *     {@link UnidadeMetrica }
     *     
     */
    public UnidadeMetrica getUnidadeMetrica() {
        return unidadeMetrica;
    }

    /**
     * Sets the value of the unidadeMetrica property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnidadeMetrica }
     *     
     */
    public void setUnidadeMetrica(UnidadeMetrica value) {
        this.unidadeMetrica = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValor(BigDecimal value) {
        this.valor = value;
    }

    /**
     * Gets the value of the valores property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valores property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValores().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServicoValor }
     * 
     * 
     */
    public List<ServicoValor> getValores() {
        if (valores == null) {
            valores = new ArrayList<ServicoValor>();
        }
        return this.valores;
    }

}
