
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listarTotalServicoAliquotaPrestador complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listarTotalServicoAliquotaPrestador">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://ws.api.smartbill.com.br/}servicoAliquotaPrestadorFilter" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listarTotalServicoAliquotaPrestador", propOrder = {
    "arg0"
})
public class ListarTotalServicoAliquotaPrestador {

    protected ServicoAliquotaPrestadorFilter arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link ServicoAliquotaPrestadorFilter }
     *     
     */
    public ServicoAliquotaPrestadorFilter getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicoAliquotaPrestadorFilter }
     *     
     */
    public void setArg0(ServicoAliquotaPrestadorFilter value) {
        this.arg0 = value;
    }

}
