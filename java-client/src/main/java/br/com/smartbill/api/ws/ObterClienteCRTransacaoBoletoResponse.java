
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for obterClienteCRTransacaoBoletoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="obterClienteCRTransacaoBoletoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.api.smartbill.com.br/}clienteCRTransacaoBoleto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obterClienteCRTransacaoBoletoResponse", propOrder = {
    "_return"
})
public class ObterClienteCRTransacaoBoletoResponse {

    @XmlElement(name = "return")
    protected ClienteCRTransacaoBoleto _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link ClienteCRTransacaoBoleto }
     *     
     */
    public ClienteCRTransacaoBoleto getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClienteCRTransacaoBoleto }
     *     
     */
    public void setReturn(ClienteCRTransacaoBoleto value) {
        this._return = value;
    }

}
