
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoNFEstado.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoNFEstado">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NAO_EMITIDA"/>
 *     &lt;enumeration value="SOLICITADA"/>
 *     &lt;enumeration value="GERADA"/>
 *     &lt;enumeration value="ENVIADA"/>
 *     &lt;enumeration value="APROVADA"/>
 *     &lt;enumeration value="REJEITADA"/>
 *     &lt;enumeration value="DUPLICIDADE"/>
 *     &lt;enumeration value="CANCELADA"/>
 *     &lt;enumeration value="ERRO_GERAL"/>
 *     &lt;enumeration value="MANUAL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoNFEstado")
@XmlEnum
public enum TipoNFEstado {

    NAO_EMITIDA,
    SOLICITADA,
    GERADA,
    ENVIADA,
    APROVADA,
    REJEITADA,
    DUPLICIDADE,
    CANCELADA,
    ERRO_GERAL,
    MANUAL;

    public String value() {
        return name();
    }

    public static TipoNFEstado fromValue(String v) {
        return valueOf(v);
    }

}
