
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cidade complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cidade">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="disponivelParaCadastro" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ibge" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uf" type="{http://ws.api.smartbill.com.br/}uf" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cidade", propOrder = {
    "disponivelParaCadastro",
    "ibge",
    "nome",
    "uf",
    "uuid"
})
public class Cidade {

    protected boolean disponivelParaCadastro;
    protected Long ibge;
    protected String nome;
    protected Uf uf;
    protected String uuid;

    /**
     * Gets the value of the disponivelParaCadastro property.
     * 
     */
    public boolean isDisponivelParaCadastro() {
        return disponivelParaCadastro;
    }

    /**
     * Sets the value of the disponivelParaCadastro property.
     * 
     */
    public void setDisponivelParaCadastro(boolean value) {
        this.disponivelParaCadastro = value;
    }

    /**
     * Gets the value of the ibge property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIbge() {
        return ibge;
    }

    /**
     * Sets the value of the ibge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIbge(Long value) {
        this.ibge = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the uf property.
     * 
     * @return
     *     possible object is
     *     {@link Uf }
     *     
     */
    public Uf getUf() {
        return uf;
    }

    /**
     * Sets the value of the uf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Uf }
     *     
     */
    public void setUf(Uf value) {
        this.uf = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
