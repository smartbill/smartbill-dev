
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMeioPagamento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoMeioPagamento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BOLETO"/>
 *     &lt;enumeration value="CARTAO_CREDITO"/>
 *     &lt;enumeration value="DINHEIRO"/>
 *     &lt;enumeration value="CHEQUE"/>
 *     &lt;enumeration value="DEPOSITO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoMeioPagamento")
@XmlEnum
public enum TipoMeioPagamento {

    BOLETO,
    CARTAO_CREDITO,
    DINHEIRO,
    CHEQUE,
    DEPOSITO;

    public String value() {
        return name();
    }

    public static TipoMeioPagamento fromValue(String v) {
        return valueOf(v);
    }

}
