
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for status.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="status">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="CREATED"/>
 *     &lt;enumeration value="ACCEPTED"/>
 *     &lt;enumeration value="NO_CONTENT"/>
 *     &lt;enumeration value="MOVED_PERMANENTLY"/>
 *     &lt;enumeration value="SEE_OTHER"/>
 *     &lt;enumeration value="NOT_MODIFIED"/>
 *     &lt;enumeration value="TEMPORARY_REDIRECT"/>
 *     &lt;enumeration value="BAD_REQUEST"/>
 *     &lt;enumeration value="UNAUTHORIZED"/>
 *     &lt;enumeration value="FORBIDDEN"/>
 *     &lt;enumeration value="NOT_FOUND"/>
 *     &lt;enumeration value="NOT_ACCEPTABLE"/>
 *     &lt;enumeration value="CONFLICT"/>
 *     &lt;enumeration value="GONE"/>
 *     &lt;enumeration value="PRECONDITION_FAILED"/>
 *     &lt;enumeration value="UNSUPPORTED_MEDIA_TYPE"/>
 *     &lt;enumeration value="INTERNAL_SERVER_ERROR"/>
 *     &lt;enumeration value="SERVICE_UNAVAILABLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "status")
@XmlEnum
public enum Status {

    OK,
    CREATED,
    ACCEPTED,
    NO_CONTENT,
    MOVED_PERMANENTLY,
    SEE_OTHER,
    NOT_MODIFIED,
    TEMPORARY_REDIRECT,
    BAD_REQUEST,
    UNAUTHORIZED,
    FORBIDDEN,
    NOT_FOUND,
    NOT_ACCEPTABLE,
    CONFLICT,
    GONE,
    PRECONDITION_FAILED,
    UNSUPPORTED_MEDIA_TYPE,
    INTERNAL_SERVER_ERROR,
    SERVICE_UNAVAILABLE;

    public String value() {
        return name();
    }

    public static Status fromValue(String v) {
        return valueOf(v);
    }

}
