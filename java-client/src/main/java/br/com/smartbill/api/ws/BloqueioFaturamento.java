
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bloqueioFaturamento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="bloqueioFaturamento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UM_CR"/>
 *     &lt;enumeration value="DOIS_CR"/>
 *     &lt;enumeration value="TRES_CR"/>
 *     &lt;enumeration value="SEIS_CR"/>
 *     &lt;enumeration value="DOZE_CR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "bloqueioFaturamento")
@XmlEnum
public enum BloqueioFaturamento {

    UM_CR,
    DOIS_CR,
    TRES_CR,
    SEIS_CR,
    DOZE_CR;

    public String value() {
        return name();
    }

    public static BloqueioFaturamento fromValue(String v) {
        return valueOf(v);
    }

}
