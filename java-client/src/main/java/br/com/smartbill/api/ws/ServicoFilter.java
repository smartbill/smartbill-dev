
package br.com.smartbill.api.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for servicoFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="servicoFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ativo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="excluindoIds" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="familia" type="{http://ws.api.smartbill.com.br/}familia" minOccurs="0"/>
 *         &lt;element name="generalFilter" type="{http://ws.api.smartbill.com.br/}generalFilter" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="servicosAdicional" type="{http://ws.api.smartbill.com.br/}servico" minOccurs="0"/>
 *         &lt;element name="tiposServico" type="{http://ws.api.smartbill.com.br/}tipoServico" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "servicoFilter", propOrder = {
    "ativo",
    "codigo",
    "excluindoIds",
    "familia",
    "generalFilter",
    "nome",
    "servicosAdicional",
    "tiposServico"
})
public class ServicoFilter {

    protected Boolean ativo;
    protected String codigo;
    @XmlElement(nillable = true)
    protected List<String> excluindoIds;
    protected Familia familia;
    protected GeneralFilter generalFilter;
    protected String nome;
    protected Servico servicosAdicional;
    @XmlElement(nillable = true)
    protected List<TipoServico> tiposServico;

    /**
     * Gets the value of the ativo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAtivo() {
        return ativo;
    }

    /**
     * Sets the value of the ativo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAtivo(Boolean value) {
        this.ativo = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the excluindoIds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the excluindoIds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExcluindoIds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getExcluindoIds() {
        if (excluindoIds == null) {
            excluindoIds = new ArrayList<String>();
        }
        return this.excluindoIds;
    }

    /**
     * Gets the value of the familia property.
     * 
     * @return
     *     possible object is
     *     {@link Familia }
     *     
     */
    public Familia getFamilia() {
        return familia;
    }

    /**
     * Sets the value of the familia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Familia }
     *     
     */
    public void setFamilia(Familia value) {
        this.familia = value;
    }

    /**
     * Gets the value of the generalFilter property.
     * 
     * @return
     *     possible object is
     *     {@link GeneralFilter }
     *     
     */
    public GeneralFilter getGeneralFilter() {
        return generalFilter;
    }

    /**
     * Sets the value of the generalFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralFilter }
     *     
     */
    public void setGeneralFilter(GeneralFilter value) {
        this.generalFilter = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the servicosAdicional property.
     * 
     * @return
     *     possible object is
     *     {@link Servico }
     *     
     */
    public Servico getServicosAdicional() {
        return servicosAdicional;
    }

    /**
     * Sets the value of the servicosAdicional property.
     * 
     * @param value
     *     allowed object is
     *     {@link Servico }
     *     
     */
    public void setServicosAdicional(Servico value) {
        this.servicosAdicional = value;
    }

    /**
     * Gets the value of the tiposServico property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tiposServico property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTiposServico().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoServico }
     * 
     * 
     */
    public List<TipoServico> getTiposServico() {
        if (tiposServico == null) {
            tiposServico = new ArrayList<TipoServico>();
        }
        return this.tiposServico;
    }

}
