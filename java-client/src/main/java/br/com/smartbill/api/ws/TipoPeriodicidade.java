
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoPeriodicidade.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoPeriodicidade">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PONTUAL"/>
 *     &lt;enumeration value="MENSAL"/>
 *     &lt;enumeration value="BIMESTRAL"/>
 *     &lt;enumeration value="TRIMESTRAL"/>
 *     &lt;enumeration value="SEMESTRAL"/>
 *     &lt;enumeration value="ANUAL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoPeriodicidade")
@XmlEnum
public enum TipoPeriodicidade {

    PONTUAL,
    MENSAL,
    BIMESTRAL,
    TRIMESTRAL,
    SEMESTRAL,
    ANUAL;

    public String value() {
        return name();
    }

    public static TipoPeriodicidade fromValue(String v) {
        return valueOf(v);
    }

}
