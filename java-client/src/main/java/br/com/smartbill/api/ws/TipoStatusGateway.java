
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoStatusGateway.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoStatusGateway">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ABERTO"/>
 *     &lt;enumeration value="FECHADO"/>
 *     &lt;enumeration value="PAGO"/>
 *     &lt;enumeration value="PAGO_A_MAIS"/>
 *     &lt;enumeration value="CANCELADO"/>
 *     &lt;enumeration value="PAGO_A_MENOS"/>
 *     &lt;enumeration value="ERRO_GERAL"/>
 *     &lt;enumeration value="PENDENTE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoStatusGateway")
@XmlEnum
public enum TipoStatusGateway {

    ABERTO,
    FECHADO,
    PAGO,
    PAGO_A_MAIS,
    CANCELADO,
    PAGO_A_MENOS,
    ERRO_GERAL,
    PENDENTE;

    public String value() {
        return name();
    }

    public static TipoStatusGateway fromValue(String v) {
        return valueOf(v);
    }

}
