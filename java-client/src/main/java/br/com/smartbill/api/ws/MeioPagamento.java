
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for meioPagamento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="meioPagamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agenciaDigito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ativo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="banco" type="{http://ws.api.smartbill.com.br/}tipoBanco" minOccurs="0"/>
 *         &lt;element name="conta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contaDigito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="exibirNoFormularioDeContratoExterno" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="instrucoesDeCobrancaLinha1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="instrucoesDeCobrancaLinha2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="instrucoesDeCobrancaLinha3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="instrucoesDeCobrancaLinha4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="instrucoesDeCobrancaLinha5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="moraMensalAposVencimento" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="multaAposVencimento" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nossoNumeroInicial" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="numeroCarteira" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroConvenio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="quantidadeDiasCobrancaAposVencimento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="quantidadeDiasConsiderarVencido" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="quantidadeDiasEnvioAntecipado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tarifaBoleto" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tipoMeioPagamento" type="{http://ws.api.smartbill.com.br/}tipoMeioPagamento" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "meioPagamento", propOrder = {
    "agencia",
    "agenciaDigito",
    "ativo",
    "banco",
    "conta",
    "contaDigito",
    "criacao",
    "exclusao",
    "exibirNoFormularioDeContratoExterno",
    "gerenteUuid",
    "instrucoesDeCobrancaLinha1",
    "instrucoesDeCobrancaLinha2",
    "instrucoesDeCobrancaLinha3",
    "instrucoesDeCobrancaLinha4",
    "instrucoesDeCobrancaLinha5",
    "modificacao",
    "moraMensalAposVencimento",
    "multaAposVencimento",
    "nome",
    "nossoNumeroInicial",
    "numeroCarteira",
    "numeroConvenio",
    "quantidadeDiasCobrancaAposVencimento",
    "quantidadeDiasConsiderarVencido",
    "quantidadeDiasEnvioAntecipado",
    "tarifaBoleto",
    "tipoMeioPagamento",
    "uuid"
})
public class MeioPagamento {

    protected String agencia;
    protected String agenciaDigito;
    protected boolean ativo;
    protected TipoBanco banco;
    protected String conta;
    protected String contaDigito;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected boolean exibirNoFormularioDeContratoExterno;
    protected String gerenteUuid;
    protected String instrucoesDeCobrancaLinha1;
    protected String instrucoesDeCobrancaLinha2;
    protected String instrucoesDeCobrancaLinha3;
    protected String instrucoesDeCobrancaLinha4;
    protected String instrucoesDeCobrancaLinha5;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected BigDecimal moraMensalAposVencimento;
    protected BigDecimal multaAposVencimento;
    protected String nome;
    protected Long nossoNumeroInicial;
    protected String numeroCarteira;
    protected String numeroConvenio;
    protected int quantidadeDiasCobrancaAposVencimento;
    protected int quantidadeDiasConsiderarVencido;
    protected int quantidadeDiasEnvioAntecipado;
    protected BigDecimal tarifaBoleto;
    protected TipoMeioPagamento tipoMeioPagamento;
    protected String uuid;

    /**
     * Gets the value of the agencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Sets the value of the agencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencia(String value) {
        this.agencia = value;
    }

    /**
     * Gets the value of the agenciaDigito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenciaDigito() {
        return agenciaDigito;
    }

    /**
     * Sets the value of the agenciaDigito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenciaDigito(String value) {
        this.agenciaDigito = value;
    }

    /**
     * Gets the value of the ativo property.
     * 
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * Sets the value of the ativo property.
     * 
     */
    public void setAtivo(boolean value) {
        this.ativo = value;
    }

    /**
     * Gets the value of the banco property.
     * 
     * @return
     *     possible object is
     *     {@link TipoBanco }
     *     
     */
    public TipoBanco getBanco() {
        return banco;
    }

    /**
     * Sets the value of the banco property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoBanco }
     *     
     */
    public void setBanco(TipoBanco value) {
        this.banco = value;
    }

    /**
     * Gets the value of the conta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConta() {
        return conta;
    }

    /**
     * Sets the value of the conta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConta(String value) {
        this.conta = value;
    }

    /**
     * Gets the value of the contaDigito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContaDigito() {
        return contaDigito;
    }

    /**
     * Sets the value of the contaDigito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContaDigito(String value) {
        this.contaDigito = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the exibirNoFormularioDeContratoExterno property.
     * 
     */
    public boolean isExibirNoFormularioDeContratoExterno() {
        return exibirNoFormularioDeContratoExterno;
    }

    /**
     * Sets the value of the exibirNoFormularioDeContratoExterno property.
     * 
     */
    public void setExibirNoFormularioDeContratoExterno(boolean value) {
        this.exibirNoFormularioDeContratoExterno = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the instrucoesDeCobrancaLinha1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstrucoesDeCobrancaLinha1() {
        return instrucoesDeCobrancaLinha1;
    }

    /**
     * Sets the value of the instrucoesDeCobrancaLinha1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstrucoesDeCobrancaLinha1(String value) {
        this.instrucoesDeCobrancaLinha1 = value;
    }

    /**
     * Gets the value of the instrucoesDeCobrancaLinha2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstrucoesDeCobrancaLinha2() {
        return instrucoesDeCobrancaLinha2;
    }

    /**
     * Sets the value of the instrucoesDeCobrancaLinha2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstrucoesDeCobrancaLinha2(String value) {
        this.instrucoesDeCobrancaLinha2 = value;
    }

    /**
     * Gets the value of the instrucoesDeCobrancaLinha3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstrucoesDeCobrancaLinha3() {
        return instrucoesDeCobrancaLinha3;
    }

    /**
     * Sets the value of the instrucoesDeCobrancaLinha3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstrucoesDeCobrancaLinha3(String value) {
        this.instrucoesDeCobrancaLinha3 = value;
    }

    /**
     * Gets the value of the instrucoesDeCobrancaLinha4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstrucoesDeCobrancaLinha4() {
        return instrucoesDeCobrancaLinha4;
    }

    /**
     * Sets the value of the instrucoesDeCobrancaLinha4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstrucoesDeCobrancaLinha4(String value) {
        this.instrucoesDeCobrancaLinha4 = value;
    }

    /**
     * Gets the value of the instrucoesDeCobrancaLinha5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstrucoesDeCobrancaLinha5() {
        return instrucoesDeCobrancaLinha5;
    }

    /**
     * Sets the value of the instrucoesDeCobrancaLinha5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstrucoesDeCobrancaLinha5(String value) {
        this.instrucoesDeCobrancaLinha5 = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the moraMensalAposVencimento property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMoraMensalAposVencimento() {
        return moraMensalAposVencimento;
    }

    /**
     * Sets the value of the moraMensalAposVencimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMoraMensalAposVencimento(BigDecimal value) {
        this.moraMensalAposVencimento = value;
    }

    /**
     * Gets the value of the multaAposVencimento property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMultaAposVencimento() {
        return multaAposVencimento;
    }

    /**
     * Sets the value of the multaAposVencimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMultaAposVencimento(BigDecimal value) {
        this.multaAposVencimento = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the nossoNumeroInicial property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNossoNumeroInicial() {
        return nossoNumeroInicial;
    }

    /**
     * Sets the value of the nossoNumeroInicial property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNossoNumeroInicial(Long value) {
        this.nossoNumeroInicial = value;
    }

    /**
     * Gets the value of the numeroCarteira property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCarteira() {
        return numeroCarteira;
    }

    /**
     * Sets the value of the numeroCarteira property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCarteira(String value) {
        this.numeroCarteira = value;
    }

    /**
     * Gets the value of the numeroConvenio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroConvenio() {
        return numeroConvenio;
    }

    /**
     * Sets the value of the numeroConvenio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroConvenio(String value) {
        this.numeroConvenio = value;
    }

    /**
     * Gets the value of the quantidadeDiasCobrancaAposVencimento property.
     * 
     */
    public int getQuantidadeDiasCobrancaAposVencimento() {
        return quantidadeDiasCobrancaAposVencimento;
    }

    /**
     * Sets the value of the quantidadeDiasCobrancaAposVencimento property.
     * 
     */
    public void setQuantidadeDiasCobrancaAposVencimento(int value) {
        this.quantidadeDiasCobrancaAposVencimento = value;
    }

    /**
     * Gets the value of the quantidadeDiasConsiderarVencido property.
     * 
     */
    public int getQuantidadeDiasConsiderarVencido() {
        return quantidadeDiasConsiderarVencido;
    }

    /**
     * Sets the value of the quantidadeDiasConsiderarVencido property.
     * 
     */
    public void setQuantidadeDiasConsiderarVencido(int value) {
        this.quantidadeDiasConsiderarVencido = value;
    }

    /**
     * Gets the value of the quantidadeDiasEnvioAntecipado property.
     * 
     */
    public int getQuantidadeDiasEnvioAntecipado() {
        return quantidadeDiasEnvioAntecipado;
    }

    /**
     * Sets the value of the quantidadeDiasEnvioAntecipado property.
     * 
     */
    public void setQuantidadeDiasEnvioAntecipado(int value) {
        this.quantidadeDiasEnvioAntecipado = value;
    }

    /**
     * Gets the value of the tarifaBoleto property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaBoleto() {
        return tarifaBoleto;
    }

    /**
     * Sets the value of the tarifaBoleto property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaBoleto(BigDecimal value) {
        this.tarifaBoleto = value;
    }

    /**
     * Gets the value of the tipoMeioPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoMeioPagamento }
     *     
     */
    public TipoMeioPagamento getTipoMeioPagamento() {
        return tipoMeioPagamento;
    }

    /**
     * Sets the value of the tipoMeioPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoMeioPagamento }
     *     
     */
    public void setTipoMeioPagamento(TipoMeioPagamento value) {
        this.tipoMeioPagamento = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
