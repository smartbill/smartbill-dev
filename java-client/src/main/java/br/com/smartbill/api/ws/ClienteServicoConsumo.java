
package br.com.smartbill.api.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteServicoConsumo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteServicoConsumo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anoReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="clienteServicoUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clienteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mesReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="quantidades" type="{http://ws.api.smartbill.com.br/}clienteServicoConsumoQuantidade" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tipoConsumo" type="{http://ws.api.smartbill.com.br/}tipoConsumo" minOccurs="0"/>
 *         &lt;element name="tipoConsumoEstado" type="{http://ws.api.smartbill.com.br/}tipoConsumoEstado" minOccurs="0"/>
 *         &lt;element name="unidadeMedida" type="{http://ws.api.smartbill.com.br/}unidadeMedida" minOccurs="0"/>
 *         &lt;element name="unidadeMetrica" type="{http://ws.api.smartbill.com.br/}unidadeMetrica" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteServicoConsumo", propOrder = {
    "anoReferencia",
    "clienteServicoUuid",
    "clienteUuid",
    "criacao",
    "exclusao",
    "gerenteUuid",
    "mesReferencia",
    "modificacao",
    "quantidades",
    "tipoConsumo",
    "tipoConsumoEstado",
    "unidadeMedida",
    "unidadeMetrica",
    "uuid"
})
public class ClienteServicoConsumo {

    protected Integer anoReferencia;
    protected String clienteServicoUuid;
    protected String clienteUuid;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    protected Integer mesReferencia;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    @XmlElement(nillable = true)
    protected List<ClienteServicoConsumoQuantidade> quantidades;
    protected TipoConsumo tipoConsumo;
    protected TipoConsumoEstado tipoConsumoEstado;
    protected UnidadeMedida unidadeMedida;
    protected UnidadeMetrica unidadeMetrica;
    protected String uuid;

    /**
     * Gets the value of the anoReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnoReferencia() {
        return anoReferencia;
    }

    /**
     * Sets the value of the anoReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnoReferencia(Integer value) {
        this.anoReferencia = value;
    }

    /**
     * Gets the value of the clienteServicoUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteServicoUuid() {
        return clienteServicoUuid;
    }

    /**
     * Sets the value of the clienteServicoUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteServicoUuid(String value) {
        this.clienteServicoUuid = value;
    }

    /**
     * Gets the value of the clienteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteUuid() {
        return clienteUuid;
    }

    /**
     * Sets the value of the clienteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteUuid(String value) {
        this.clienteUuid = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the mesReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMesReferencia() {
        return mesReferencia;
    }

    /**
     * Sets the value of the mesReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMesReferencia(Integer value) {
        this.mesReferencia = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the quantidades property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the quantidades property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuantidades().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteServicoConsumoQuantidade }
     * 
     * 
     */
    public List<ClienteServicoConsumoQuantidade> getQuantidades() {
        if (quantidades == null) {
            quantidades = new ArrayList<ClienteServicoConsumoQuantidade>();
        }
        return this.quantidades;
    }

    /**
     * Gets the value of the tipoConsumo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoConsumo }
     *     
     */
    public TipoConsumo getTipoConsumo() {
        return tipoConsumo;
    }

    /**
     * Sets the value of the tipoConsumo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConsumo }
     *     
     */
    public void setTipoConsumo(TipoConsumo value) {
        this.tipoConsumo = value;
    }

    /**
     * Gets the value of the tipoConsumoEstado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoConsumoEstado }
     *     
     */
    public TipoConsumoEstado getTipoConsumoEstado() {
        return tipoConsumoEstado;
    }

    /**
     * Sets the value of the tipoConsumoEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConsumoEstado }
     *     
     */
    public void setTipoConsumoEstado(TipoConsumoEstado value) {
        this.tipoConsumoEstado = value;
    }

    /**
     * Gets the value of the unidadeMedida property.
     * 
     * @return
     *     possible object is
     *     {@link UnidadeMedida }
     *     
     */
    public UnidadeMedida getUnidadeMedida() {
        return unidadeMedida;
    }

    /**
     * Sets the value of the unidadeMedida property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnidadeMedida }
     *     
     */
    public void setUnidadeMedida(UnidadeMedida value) {
        this.unidadeMedida = value;
    }

    /**
     * Gets the value of the unidadeMetrica property.
     * 
     * @return
     *     possible object is
     *     {@link UnidadeMetrica }
     *     
     */
    public UnidadeMetrica getUnidadeMetrica() {
        return unidadeMetrica;
    }

    /**
     * Sets the value of the unidadeMetrica property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnidadeMetrica }
     *     
     */
    public void setUnidadeMetrica(UnidadeMetrica value) {
        this.unidadeMetrica = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
