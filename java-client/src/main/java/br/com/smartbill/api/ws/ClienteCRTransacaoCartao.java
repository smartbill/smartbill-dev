
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteCRTransacaoCartao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteCRTransacaoCartao">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.api.smartbill.com.br/}clienteCRTransacao">
 *       &lt;sequence>
 *         &lt;element name="chaveCompra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chavePedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chaveRequisicao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chaveTransacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoAdquirente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoAutorizacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataTransacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="identificacaoTransacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mensagemAdquirente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referenciaPedido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referenciaTransacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="situacaoPedido" type="{http://ws.api.smartbill.com.br/}tipoStatusGateway" minOccurs="0"/>
 *         &lt;element name="tipoBandeira" type="{http://ws.api.smartbill.com.br/}tipoBandeira" minOccurs="0"/>
 *         &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteCRTransacaoCartao", propOrder = {
    "chaveCompra",
    "chavePedido",
    "chaveRequisicao",
    "chaveTransacao",
    "codigoAdquirente",
    "codigoAutorizacao",
    "dataTransacao",
    "identificacaoTransacao",
    "mensagemAdquirente",
    "numero",
    "referenciaPedido",
    "referenciaTransacao",
    "situacaoPedido",
    "tipoBandeira",
    "titular",
    "valor"
})
public class ClienteCRTransacaoCartao
    extends ClienteCRTransacao
{

    protected String chaveCompra;
    protected String chavePedido;
    protected String chaveRequisicao;
    protected String chaveTransacao;
    protected String codigoAdquirente;
    protected String codigoAutorizacao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataTransacao;
    protected String identificacaoTransacao;
    protected String mensagemAdquirente;
    protected String numero;
    protected String referenciaPedido;
    protected String referenciaTransacao;
    protected TipoStatusGateway situacaoPedido;
    protected TipoBandeira tipoBandeira;
    protected String titular;
    protected BigDecimal valor;

    /**
     * Gets the value of the chaveCompra property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChaveCompra() {
        return chaveCompra;
    }

    /**
     * Sets the value of the chaveCompra property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChaveCompra(String value) {
        this.chaveCompra = value;
    }

    /**
     * Gets the value of the chavePedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChavePedido() {
        return chavePedido;
    }

    /**
     * Sets the value of the chavePedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChavePedido(String value) {
        this.chavePedido = value;
    }

    /**
     * Gets the value of the chaveRequisicao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChaveRequisicao() {
        return chaveRequisicao;
    }

    /**
     * Sets the value of the chaveRequisicao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChaveRequisicao(String value) {
        this.chaveRequisicao = value;
    }

    /**
     * Gets the value of the chaveTransacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChaveTransacao() {
        return chaveTransacao;
    }

    /**
     * Sets the value of the chaveTransacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChaveTransacao(String value) {
        this.chaveTransacao = value;
    }

    /**
     * Gets the value of the codigoAdquirente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAdquirente() {
        return codigoAdquirente;
    }

    /**
     * Sets the value of the codigoAdquirente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAdquirente(String value) {
        this.codigoAdquirente = value;
    }

    /**
     * Gets the value of the codigoAutorizacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAutorizacao() {
        return codigoAutorizacao;
    }

    /**
     * Sets the value of the codigoAutorizacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAutorizacao(String value) {
        this.codigoAutorizacao = value;
    }

    /**
     * Gets the value of the dataTransacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataTransacao() {
        return dataTransacao;
    }

    /**
     * Sets the value of the dataTransacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataTransacao(XMLGregorianCalendar value) {
        this.dataTransacao = value;
    }

    /**
     * Gets the value of the identificacaoTransacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificacaoTransacao() {
        return identificacaoTransacao;
    }

    /**
     * Sets the value of the identificacaoTransacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificacaoTransacao(String value) {
        this.identificacaoTransacao = value;
    }

    /**
     * Gets the value of the mensagemAdquirente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagemAdquirente() {
        return mensagemAdquirente;
    }

    /**
     * Sets the value of the mensagemAdquirente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagemAdquirente(String value) {
        this.mensagemAdquirente = value;
    }

    /**
     * Gets the value of the numero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the referenciaPedido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaPedido() {
        return referenciaPedido;
    }

    /**
     * Sets the value of the referenciaPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaPedido(String value) {
        this.referenciaPedido = value;
    }

    /**
     * Gets the value of the referenciaTransacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenciaTransacao() {
        return referenciaTransacao;
    }

    /**
     * Sets the value of the referenciaTransacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenciaTransacao(String value) {
        this.referenciaTransacao = value;
    }

    /**
     * Gets the value of the situacaoPedido property.
     * 
     * @return
     *     possible object is
     *     {@link TipoStatusGateway }
     *     
     */
    public TipoStatusGateway getSituacaoPedido() {
        return situacaoPedido;
    }

    /**
     * Sets the value of the situacaoPedido property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoStatusGateway }
     *     
     */
    public void setSituacaoPedido(TipoStatusGateway value) {
        this.situacaoPedido = value;
    }

    /**
     * Gets the value of the tipoBandeira property.
     * 
     * @return
     *     possible object is
     *     {@link TipoBandeira }
     *     
     */
    public TipoBandeira getTipoBandeira() {
        return tipoBandeira;
    }

    /**
     * Sets the value of the tipoBandeira property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoBandeira }
     *     
     */
    public void setTipoBandeira(TipoBandeira value) {
        this.tipoBandeira = value;
    }

    /**
     * Gets the value of the titular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitular() {
        return titular;
    }

    /**
     * Sets the value of the titular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitular(String value) {
        this.titular = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValor(BigDecimal value) {
        this.valor = value;
    }

}
