
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for servicoEstado.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="servicoEstado">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EM_EDICAO"/>
 *     &lt;enumeration value="TRIAL"/>
 *     &lt;enumeration value="ATIVO"/>
 *     &lt;enumeration value="CANCELADO"/>
 *     &lt;enumeration value="SUSPENSO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "servicoEstado")
@XmlEnum
public enum ServicoEstado {

    EM_EDICAO,
    TRIAL,
    ATIVO,
    CANCELADO,
    SUSPENSO;

    public String value() {
        return name();
    }

    public static ServicoEstado fromValue(String v) {
        return valueOf(v);
    }

}
