
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoVigencia.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoVigencia">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MES"/>
 *     &lt;enumeration value="BIMESTRE"/>
 *     &lt;enumeration value="TRIMESTRE"/>
 *     &lt;enumeration value="SEMESTRE"/>
 *     &lt;enumeration value="ANO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoVigencia")
@XmlEnum
public enum TipoVigencia {

    MES,
    BIMESTRE,
    TRIMESTRE,
    SEMESTRE,
    ANO;

    public String value() {
        return name();
    }

    public static TipoVigencia fromValue(String v) {
        return valueOf(v);
    }

}
