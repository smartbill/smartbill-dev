
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoFaturamento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoFaturamento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PRE_PAGO"/>
 *     &lt;enumeration value="POS_PAGO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoFaturamento")
@XmlEnum
public enum TipoFaturamento {

    PRE_PAGO,
    POS_PAGO;

    public String value() {
        return name();
    }

    public static TipoFaturamento fromValue(String v) {
        return valueOf(v);
    }

}
