
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoConsumo.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoConsumo">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SEGUNDO"/>
 *     &lt;enumeration value="MINUTO"/>
 *     &lt;enumeration value="HORA"/>
 *     &lt;enumeration value="DIA"/>
 *     &lt;enumeration value="MES"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoConsumo")
@XmlEnum
public enum TipoConsumo {

    SEGUNDO,
    MINUTO,
    HORA,
    DIA,
    MES;

    public String value() {
        return name();
    }

    public static TipoConsumo fromValue(String v) {
        return valueOf(v);
    }

}
