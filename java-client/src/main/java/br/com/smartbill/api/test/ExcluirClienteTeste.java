package br.com.smartbill.api.test;

import br.com.smartbill.api.ws.Cliente;
import br.com.smartbill.api.ws.ClienteFilter;
import br.com.smartbill.api.ws.SmartbillAPI;
import br.com.smartbill.api.ws.SmartbillAPIMessage_Exception;

/**
 * @author Daniel
 *
 */
public class ExcluirClienteTeste {
	
	final static SmartbillAPI service = new SmartbillApiAuth().getSmartbillAPI();

	public static void main(String[] args) throws SmartbillAPIMessage_Exception {
		
		String chaveConhecido = "c7f5035b-5383-11e3-880f-5cf9dd5f0a07";
		
		Cliente cliente = service.obterCliente(chaveConhecido);
		
		// Ou
		
		ClienteFilter clienteFilter = new ClienteFilter();
		clienteFilter.setCpfCnpj("395.743.998-18");
		
		cliente = service.listarCliente(clienteFilter).get(0);
		
		try {
			service.removerCliente(cliente);
		} catch (SmartbillAPIMessage_Exception e) {
			System.out.println(e.getFaultInfo().getMensagem());
		}

	}

}
