
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for logVisualizacao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="logVisualizacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuidDownload" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuidRelatorioGerado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuidTransacaoBoleto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "logVisualizacao", propOrder = {
    "criacao",
    "gerenteUuid",
    "ip",
    "uuid",
    "uuidDownload",
    "uuidRelatorioGerado",
    "uuidTransacaoBoleto"
})
public class LogVisualizacao {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    protected String gerenteUuid;
    protected String ip;
    protected String uuid;
    protected String uuidDownload;
    protected String uuidRelatorioGerado;
    protected String uuidTransacaoBoleto;

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the ip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIp() {
        return ip;
    }

    /**
     * Sets the value of the ip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIp(String value) {
        this.ip = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the uuidDownload property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidDownload() {
        return uuidDownload;
    }

    /**
     * Sets the value of the uuidDownload property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidDownload(String value) {
        this.uuidDownload = value;
    }

    /**
     * Gets the value of the uuidRelatorioGerado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidRelatorioGerado() {
        return uuidRelatorioGerado;
    }

    /**
     * Sets the value of the uuidRelatorioGerado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidRelatorioGerado(String value) {
        this.uuidRelatorioGerado = value;
    }

    /**
     * Gets the value of the uuidTransacaoBoleto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidTransacaoBoleto() {
        return uuidTransacaoBoleto;
    }

    /**
     * Sets the value of the uuidTransacaoBoleto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidTransacaoBoleto(String value) {
        this.uuidTransacaoBoleto = value;
    }

}
