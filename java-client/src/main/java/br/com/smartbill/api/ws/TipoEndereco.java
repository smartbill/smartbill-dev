
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoEndereco.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoEndereco">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="RESIDENCIAL"/>
 *     &lt;enumeration value="COMERCIAL"/>
 *     &lt;enumeration value="COBRANCA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoEndereco")
@XmlEnum
public enum TipoEndereco {

    RESIDENCIAL,
    COMERCIAL,
    COBRANCA;

    public String value() {
        return name();
    }

    public static TipoEndereco fromValue(String v) {
        return valueOf(v);
    }

}
