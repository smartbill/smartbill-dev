
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for servicoAliquotaPrestador complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="servicoAliquotaPrestador">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aliquotaINSSEX" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaINSSPF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaINSSRetidoPJ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaINSSRetidoPU" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaIRRetidoPJ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaIRRetidoPU" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaISSEX" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaISSPF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaISSPJ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaISSPU" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaISSRetidoPJ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaISSRetidoPU" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="incideINSSPJ" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="incideINSSPU" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="retemINSSPJ" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemINSSPU" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemIRPJ" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemIRPU" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemISSPJ" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemISSPU" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemISSPrestadorCadastrado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemISSPrestadorMunicipio" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemISSPrestadorNaoCadastrado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemISSTomadorIgualPrestador" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemPCCPJ" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="retemPCCPU" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="servicoAliquotaTomadors" type="{http://ws.api.smartbill.com.br/}servicoAliquotaTomador" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tributacaoMunicipio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "servicoAliquotaPrestador", propOrder = {
    "aliquotaINSSEX",
    "aliquotaINSSPF",
    "aliquotaINSSRetidoPJ",
    "aliquotaINSSRetidoPU",
    "aliquotaIRRetidoPJ",
    "aliquotaIRRetidoPU",
    "aliquotaISSEX",
    "aliquotaISSPF",
    "aliquotaISSPJ",
    "aliquotaISSPU",
    "aliquotaISSRetidoPJ",
    "aliquotaISSRetidoPU",
    "codigo",
    "criacao",
    "descricao",
    "exclusao",
    "gerenteUuid",
    "incideINSSPJ",
    "incideINSSPU",
    "modificacao",
    "retemINSSPJ",
    "retemINSSPU",
    "retemIRPJ",
    "retemIRPU",
    "retemISSPJ",
    "retemISSPU",
    "retemISSPrestadorCadastrado",
    "retemISSPrestadorMunicipio",
    "retemISSPrestadorNaoCadastrado",
    "retemISSTomadorIgualPrestador",
    "retemPCCPJ",
    "retemPCCPU",
    "servicoAliquotaTomadors",
    "tributacaoMunicipio",
    "uuid"
})
public class ServicoAliquotaPrestador {

    protected BigDecimal aliquotaINSSEX;
    protected BigDecimal aliquotaINSSPF;
    protected BigDecimal aliquotaINSSRetidoPJ;
    protected BigDecimal aliquotaINSSRetidoPU;
    protected BigDecimal aliquotaIRRetidoPJ;
    protected BigDecimal aliquotaIRRetidoPU;
    protected BigDecimal aliquotaISSEX;
    protected BigDecimal aliquotaISSPF;
    protected BigDecimal aliquotaISSPJ;
    protected BigDecimal aliquotaISSPU;
    protected BigDecimal aliquotaISSRetidoPJ;
    protected BigDecimal aliquotaISSRetidoPU;
    protected String codigo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    protected String descricao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    protected boolean incideINSSPJ;
    protected boolean incideINSSPU;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected boolean retemINSSPJ;
    protected boolean retemINSSPU;
    protected boolean retemIRPJ;
    protected boolean retemIRPU;
    protected boolean retemISSPJ;
    protected boolean retemISSPU;
    protected boolean retemISSPrestadorCadastrado;
    protected boolean retemISSPrestadorMunicipio;
    protected boolean retemISSPrestadorNaoCadastrado;
    protected boolean retemISSTomadorIgualPrestador;
    protected boolean retemPCCPJ;
    protected boolean retemPCCPU;
    @XmlElement(nillable = true)
    protected List<ServicoAliquotaTomador> servicoAliquotaTomadors;
    protected String tributacaoMunicipio;
    protected String uuid;

    /**
     * Gets the value of the aliquotaINSSEX property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaINSSEX() {
        return aliquotaINSSEX;
    }

    /**
     * Sets the value of the aliquotaINSSEX property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaINSSEX(BigDecimal value) {
        this.aliquotaINSSEX = value;
    }

    /**
     * Gets the value of the aliquotaINSSPF property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaINSSPF() {
        return aliquotaINSSPF;
    }

    /**
     * Sets the value of the aliquotaINSSPF property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaINSSPF(BigDecimal value) {
        this.aliquotaINSSPF = value;
    }

    /**
     * Gets the value of the aliquotaINSSRetidoPJ property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaINSSRetidoPJ() {
        return aliquotaINSSRetidoPJ;
    }

    /**
     * Sets the value of the aliquotaINSSRetidoPJ property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaINSSRetidoPJ(BigDecimal value) {
        this.aliquotaINSSRetidoPJ = value;
    }

    /**
     * Gets the value of the aliquotaINSSRetidoPU property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaINSSRetidoPU() {
        return aliquotaINSSRetidoPU;
    }

    /**
     * Sets the value of the aliquotaINSSRetidoPU property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaINSSRetidoPU(BigDecimal value) {
        this.aliquotaINSSRetidoPU = value;
    }

    /**
     * Gets the value of the aliquotaIRRetidoPJ property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaIRRetidoPJ() {
        return aliquotaIRRetidoPJ;
    }

    /**
     * Sets the value of the aliquotaIRRetidoPJ property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaIRRetidoPJ(BigDecimal value) {
        this.aliquotaIRRetidoPJ = value;
    }

    /**
     * Gets the value of the aliquotaIRRetidoPU property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaIRRetidoPU() {
        return aliquotaIRRetidoPU;
    }

    /**
     * Sets the value of the aliquotaIRRetidoPU property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaIRRetidoPU(BigDecimal value) {
        this.aliquotaIRRetidoPU = value;
    }

    /**
     * Gets the value of the aliquotaISSEX property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISSEX() {
        return aliquotaISSEX;
    }

    /**
     * Sets the value of the aliquotaISSEX property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISSEX(BigDecimal value) {
        this.aliquotaISSEX = value;
    }

    /**
     * Gets the value of the aliquotaISSPF property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISSPF() {
        return aliquotaISSPF;
    }

    /**
     * Sets the value of the aliquotaISSPF property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISSPF(BigDecimal value) {
        this.aliquotaISSPF = value;
    }

    /**
     * Gets the value of the aliquotaISSPJ property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISSPJ() {
        return aliquotaISSPJ;
    }

    /**
     * Sets the value of the aliquotaISSPJ property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISSPJ(BigDecimal value) {
        this.aliquotaISSPJ = value;
    }

    /**
     * Gets the value of the aliquotaISSPU property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISSPU() {
        return aliquotaISSPU;
    }

    /**
     * Sets the value of the aliquotaISSPU property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISSPU(BigDecimal value) {
        this.aliquotaISSPU = value;
    }

    /**
     * Gets the value of the aliquotaISSRetidoPJ property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISSRetidoPJ() {
        return aliquotaISSRetidoPJ;
    }

    /**
     * Sets the value of the aliquotaISSRetidoPJ property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISSRetidoPJ(BigDecimal value) {
        this.aliquotaISSRetidoPJ = value;
    }

    /**
     * Gets the value of the aliquotaISSRetidoPU property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISSRetidoPU() {
        return aliquotaISSRetidoPU;
    }

    /**
     * Sets the value of the aliquotaISSRetidoPU property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISSRetidoPU(BigDecimal value) {
        this.aliquotaISSRetidoPU = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the descricao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Sets the value of the descricao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the incideINSSPJ property.
     * 
     */
    public boolean isIncideINSSPJ() {
        return incideINSSPJ;
    }

    /**
     * Sets the value of the incideINSSPJ property.
     * 
     */
    public void setIncideINSSPJ(boolean value) {
        this.incideINSSPJ = value;
    }

    /**
     * Gets the value of the incideINSSPU property.
     * 
     */
    public boolean isIncideINSSPU() {
        return incideINSSPU;
    }

    /**
     * Sets the value of the incideINSSPU property.
     * 
     */
    public void setIncideINSSPU(boolean value) {
        this.incideINSSPU = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the retemINSSPJ property.
     * 
     */
    public boolean isRetemINSSPJ() {
        return retemINSSPJ;
    }

    /**
     * Sets the value of the retemINSSPJ property.
     * 
     */
    public void setRetemINSSPJ(boolean value) {
        this.retemINSSPJ = value;
    }

    /**
     * Gets the value of the retemINSSPU property.
     * 
     */
    public boolean isRetemINSSPU() {
        return retemINSSPU;
    }

    /**
     * Sets the value of the retemINSSPU property.
     * 
     */
    public void setRetemINSSPU(boolean value) {
        this.retemINSSPU = value;
    }

    /**
     * Gets the value of the retemIRPJ property.
     * 
     */
    public boolean isRetemIRPJ() {
        return retemIRPJ;
    }

    /**
     * Sets the value of the retemIRPJ property.
     * 
     */
    public void setRetemIRPJ(boolean value) {
        this.retemIRPJ = value;
    }

    /**
     * Gets the value of the retemIRPU property.
     * 
     */
    public boolean isRetemIRPU() {
        return retemIRPU;
    }

    /**
     * Sets the value of the retemIRPU property.
     * 
     */
    public void setRetemIRPU(boolean value) {
        this.retemIRPU = value;
    }

    /**
     * Gets the value of the retemISSPJ property.
     * 
     */
    public boolean isRetemISSPJ() {
        return retemISSPJ;
    }

    /**
     * Sets the value of the retemISSPJ property.
     * 
     */
    public void setRetemISSPJ(boolean value) {
        this.retemISSPJ = value;
    }

    /**
     * Gets the value of the retemISSPU property.
     * 
     */
    public boolean isRetemISSPU() {
        return retemISSPU;
    }

    /**
     * Sets the value of the retemISSPU property.
     * 
     */
    public void setRetemISSPU(boolean value) {
        this.retemISSPU = value;
    }

    /**
     * Gets the value of the retemISSPrestadorCadastrado property.
     * 
     */
    public boolean isRetemISSPrestadorCadastrado() {
        return retemISSPrestadorCadastrado;
    }

    /**
     * Sets the value of the retemISSPrestadorCadastrado property.
     * 
     */
    public void setRetemISSPrestadorCadastrado(boolean value) {
        this.retemISSPrestadorCadastrado = value;
    }

    /**
     * Gets the value of the retemISSPrestadorMunicipio property.
     * 
     */
    public boolean isRetemISSPrestadorMunicipio() {
        return retemISSPrestadorMunicipio;
    }

    /**
     * Sets the value of the retemISSPrestadorMunicipio property.
     * 
     */
    public void setRetemISSPrestadorMunicipio(boolean value) {
        this.retemISSPrestadorMunicipio = value;
    }

    /**
     * Gets the value of the retemISSPrestadorNaoCadastrado property.
     * 
     */
    public boolean isRetemISSPrestadorNaoCadastrado() {
        return retemISSPrestadorNaoCadastrado;
    }

    /**
     * Sets the value of the retemISSPrestadorNaoCadastrado property.
     * 
     */
    public void setRetemISSPrestadorNaoCadastrado(boolean value) {
        this.retemISSPrestadorNaoCadastrado = value;
    }

    /**
     * Gets the value of the retemISSTomadorIgualPrestador property.
     * 
     */
    public boolean isRetemISSTomadorIgualPrestador() {
        return retemISSTomadorIgualPrestador;
    }

    /**
     * Sets the value of the retemISSTomadorIgualPrestador property.
     * 
     */
    public void setRetemISSTomadorIgualPrestador(boolean value) {
        this.retemISSTomadorIgualPrestador = value;
    }

    /**
     * Gets the value of the retemPCCPJ property.
     * 
     */
    public boolean isRetemPCCPJ() {
        return retemPCCPJ;
    }

    /**
     * Sets the value of the retemPCCPJ property.
     * 
     */
    public void setRetemPCCPJ(boolean value) {
        this.retemPCCPJ = value;
    }

    /**
     * Gets the value of the retemPCCPU property.
     * 
     */
    public boolean isRetemPCCPU() {
        return retemPCCPU;
    }

    /**
     * Sets the value of the retemPCCPU property.
     * 
     */
    public void setRetemPCCPU(boolean value) {
        this.retemPCCPU = value;
    }

    /**
     * Gets the value of the servicoAliquotaTomadors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servicoAliquotaTomadors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicoAliquotaTomadors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServicoAliquotaTomador }
     * 
     * 
     */
    public List<ServicoAliquotaTomador> getServicoAliquotaTomadors() {
        if (servicoAliquotaTomadors == null) {
            servicoAliquotaTomadors = new ArrayList<ServicoAliquotaTomador>();
        }
        return this.servicoAliquotaTomadors;
    }

    /**
     * Gets the value of the tributacaoMunicipio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTributacaoMunicipio() {
        return tributacaoMunicipio;
    }

    /**
     * Sets the value of the tributacaoMunicipio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTributacaoMunicipio(String value) {
        this.tributacaoMunicipio = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
