
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoParcela.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoParcela">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SERVICO"/>
 *     &lt;enumeration value="ATIVACAO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoParcela")
@XmlEnum
public enum TipoParcela {

    SERVICO,
    ATIVACAO;

    public String value() {
        return name();
    }

    public static TipoParcela fromValue(String v) {
        return valueOf(v);
    }

}
