
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for padraoPreenchimento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="padraoPreenchimento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ativo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="bloqueioFaturamento" type="{http://ws.api.smartbill.com.br/}bloqueioFaturamento" minOccurs="0"/>
 *         &lt;element name="cicloFaturamento" type="{http://ws.api.smartbill.com.br/}cicloFaturamento" minOccurs="0"/>
 *         &lt;element name="cidade" type="{http://ws.api.smartbill.com.br/}cidade" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="documentoFiscal" type="{http://ws.api.smartbill.com.br/}documentoFiscal" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="familia" type="{http://ws.api.smartbill.com.br/}familia" minOccurs="0"/>
 *         &lt;element name="fatorDeReajusteDaRenovacao" type="{http://ws.api.smartbill.com.br/}indiceEconomico" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="meioPagamento" type="{http://ws.api.smartbill.com.br/}meioPagamento" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="naturezaOperacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orgaoPublico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="periodicidade" type="{http://ws.api.smartbill.com.br/}periodicidade" minOccurs="0"/>
 *         &lt;element name="quantidadeTipoVigencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ramoAtividade" type="{http://ws.api.smartbill.com.br/}ramoAtividade" minOccurs="0"/>
 *         &lt;element name="recebeCobranca" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="renovacaoAutomatica" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="tipoContato" type="{http://ws.api.smartbill.com.br/}tipoContato" minOccurs="0"/>
 *         &lt;element name="tipoEndereco" type="{http://ws.api.smartbill.com.br/}tipoEndereco" minOccurs="0"/>
 *         &lt;element name="tipoFaturamento" type="{http://ws.api.smartbill.com.br/}tipoFaturamento" minOccurs="0"/>
 *         &lt;element name="tipoPessoa" type="{http://ws.api.smartbill.com.br/}tipoPessoa" minOccurs="0"/>
 *         &lt;element name="tipoServico" type="{http://ws.api.smartbill.com.br/}tipoServico" minOccurs="0"/>
 *         &lt;element name="tipoVigencia" type="{http://ws.api.smartbill.com.br/}tipoVigencia" minOccurs="0"/>
 *         &lt;element name="usarFatorDeReajusteAutomatico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "padraoPreenchimento", propOrder = {
    "ativo",
    "bloqueioFaturamento",
    "cicloFaturamento",
    "cidade",
    "criacao",
    "documentoFiscal",
    "exclusao",
    "familia",
    "fatorDeReajusteDaRenovacao",
    "gerenteUuid",
    "meioPagamento",
    "modificacao",
    "naturezaOperacao",
    "orgaoPublico",
    "periodicidade",
    "quantidadeTipoVigencia",
    "ramoAtividade",
    "recebeCobranca",
    "renovacaoAutomatica",
    "tipoContato",
    "tipoEndereco",
    "tipoFaturamento",
    "tipoPessoa",
    "tipoServico",
    "tipoVigencia",
    "usarFatorDeReajusteAutomatico",
    "uuid"
})
public class PadraoPreenchimento {

    protected boolean ativo;
    protected BloqueioFaturamento bloqueioFaturamento;
    protected CicloFaturamento cicloFaturamento;
    protected Cidade cidade;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    protected DocumentoFiscal documentoFiscal;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected Familia familia;
    protected IndiceEconomico fatorDeReajusteDaRenovacao;
    protected String gerenteUuid;
    protected MeioPagamento meioPagamento;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected String naturezaOperacao;
    protected boolean orgaoPublico;
    protected Periodicidade periodicidade;
    protected Integer quantidadeTipoVigencia;
    protected RamoAtividade ramoAtividade;
    protected boolean recebeCobranca;
    protected boolean renovacaoAutomatica;
    protected TipoContato tipoContato;
    protected TipoEndereco tipoEndereco;
    protected TipoFaturamento tipoFaturamento;
    protected TipoPessoa tipoPessoa;
    protected TipoServico tipoServico;
    protected TipoVigencia tipoVigencia;
    protected boolean usarFatorDeReajusteAutomatico;
    protected String uuid;

    /**
     * Gets the value of the ativo property.
     * 
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * Sets the value of the ativo property.
     * 
     */
    public void setAtivo(boolean value) {
        this.ativo = value;
    }

    /**
     * Gets the value of the bloqueioFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link BloqueioFaturamento }
     *     
     */
    public BloqueioFaturamento getBloqueioFaturamento() {
        return bloqueioFaturamento;
    }

    /**
     * Sets the value of the bloqueioFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link BloqueioFaturamento }
     *     
     */
    public void setBloqueioFaturamento(BloqueioFaturamento value) {
        this.bloqueioFaturamento = value;
    }

    /**
     * Gets the value of the cicloFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link CicloFaturamento }
     *     
     */
    public CicloFaturamento getCicloFaturamento() {
        return cicloFaturamento;
    }

    /**
     * Sets the value of the cicloFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link CicloFaturamento }
     *     
     */
    public void setCicloFaturamento(CicloFaturamento value) {
        this.cicloFaturamento = value;
    }

    /**
     * Gets the value of the cidade property.
     * 
     * @return
     *     possible object is
     *     {@link Cidade }
     *     
     */
    public Cidade getCidade() {
        return cidade;
    }

    /**
     * Sets the value of the cidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cidade }
     *     
     */
    public void setCidade(Cidade value) {
        this.cidade = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the documentoFiscal property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFiscal }
     *     
     */
    public DocumentoFiscal getDocumentoFiscal() {
        return documentoFiscal;
    }

    /**
     * Sets the value of the documentoFiscal property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFiscal }
     *     
     */
    public void setDocumentoFiscal(DocumentoFiscal value) {
        this.documentoFiscal = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the familia property.
     * 
     * @return
     *     possible object is
     *     {@link Familia }
     *     
     */
    public Familia getFamilia() {
        return familia;
    }

    /**
     * Sets the value of the familia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Familia }
     *     
     */
    public void setFamilia(Familia value) {
        this.familia = value;
    }

    /**
     * Gets the value of the fatorDeReajusteDaRenovacao property.
     * 
     * @return
     *     possible object is
     *     {@link IndiceEconomico }
     *     
     */
    public IndiceEconomico getFatorDeReajusteDaRenovacao() {
        return fatorDeReajusteDaRenovacao;
    }

    /**
     * Sets the value of the fatorDeReajusteDaRenovacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndiceEconomico }
     *     
     */
    public void setFatorDeReajusteDaRenovacao(IndiceEconomico value) {
        this.fatorDeReajusteDaRenovacao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the meioPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link MeioPagamento }
     *     
     */
    public MeioPagamento getMeioPagamento() {
        return meioPagamento;
    }

    /**
     * Sets the value of the meioPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link MeioPagamento }
     *     
     */
    public void setMeioPagamento(MeioPagamento value) {
        this.meioPagamento = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the naturezaOperacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNaturezaOperacao() {
        return naturezaOperacao;
    }

    /**
     * Sets the value of the naturezaOperacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNaturezaOperacao(String value) {
        this.naturezaOperacao = value;
    }

    /**
     * Gets the value of the orgaoPublico property.
     * 
     */
    public boolean isOrgaoPublico() {
        return orgaoPublico;
    }

    /**
     * Sets the value of the orgaoPublico property.
     * 
     */
    public void setOrgaoPublico(boolean value) {
        this.orgaoPublico = value;
    }

    /**
     * Gets the value of the periodicidade property.
     * 
     * @return
     *     possible object is
     *     {@link Periodicidade }
     *     
     */
    public Periodicidade getPeriodicidade() {
        return periodicidade;
    }

    /**
     * Sets the value of the periodicidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Periodicidade }
     *     
     */
    public void setPeriodicidade(Periodicidade value) {
        this.periodicidade = value;
    }

    /**
     * Gets the value of the quantidadeTipoVigencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQuantidadeTipoVigencia() {
        return quantidadeTipoVigencia;
    }

    /**
     * Sets the value of the quantidadeTipoVigencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQuantidadeTipoVigencia(Integer value) {
        this.quantidadeTipoVigencia = value;
    }

    /**
     * Gets the value of the ramoAtividade property.
     * 
     * @return
     *     possible object is
     *     {@link RamoAtividade }
     *     
     */
    public RamoAtividade getRamoAtividade() {
        return ramoAtividade;
    }

    /**
     * Sets the value of the ramoAtividade property.
     * 
     * @param value
     *     allowed object is
     *     {@link RamoAtividade }
     *     
     */
    public void setRamoAtividade(RamoAtividade value) {
        this.ramoAtividade = value;
    }

    /**
     * Gets the value of the recebeCobranca property.
     * 
     */
    public boolean isRecebeCobranca() {
        return recebeCobranca;
    }

    /**
     * Sets the value of the recebeCobranca property.
     * 
     */
    public void setRecebeCobranca(boolean value) {
        this.recebeCobranca = value;
    }

    /**
     * Gets the value of the renovacaoAutomatica property.
     * 
     */
    public boolean isRenovacaoAutomatica() {
        return renovacaoAutomatica;
    }

    /**
     * Sets the value of the renovacaoAutomatica property.
     * 
     */
    public void setRenovacaoAutomatica(boolean value) {
        this.renovacaoAutomatica = value;
    }

    /**
     * Gets the value of the tipoContato property.
     * 
     * @return
     *     possible object is
     *     {@link TipoContato }
     *     
     */
    public TipoContato getTipoContato() {
        return tipoContato;
    }

    /**
     * Sets the value of the tipoContato property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoContato }
     *     
     */
    public void setTipoContato(TipoContato value) {
        this.tipoContato = value;
    }

    /**
     * Gets the value of the tipoEndereco property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEndereco }
     *     
     */
    public TipoEndereco getTipoEndereco() {
        return tipoEndereco;
    }

    /**
     * Sets the value of the tipoEndereco property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEndereco }
     *     
     */
    public void setTipoEndereco(TipoEndereco value) {
        this.tipoEndereco = value;
    }

    /**
     * Gets the value of the tipoFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoFaturamento }
     *     
     */
    public TipoFaturamento getTipoFaturamento() {
        return tipoFaturamento;
    }

    /**
     * Sets the value of the tipoFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoFaturamento }
     *     
     */
    public void setTipoFaturamento(TipoFaturamento value) {
        this.tipoFaturamento = value;
    }

    /**
     * Gets the value of the tipoPessoa property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPessoa }
     *     
     */
    public TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }

    /**
     * Sets the value of the tipoPessoa property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPessoa }
     *     
     */
    public void setTipoPessoa(TipoPessoa value) {
        this.tipoPessoa = value;
    }

    /**
     * Gets the value of the tipoServico property.
     * 
     * @return
     *     possible object is
     *     {@link TipoServico }
     *     
     */
    public TipoServico getTipoServico() {
        return tipoServico;
    }

    /**
     * Sets the value of the tipoServico property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoServico }
     *     
     */
    public void setTipoServico(TipoServico value) {
        this.tipoServico = value;
    }

    /**
     * Gets the value of the tipoVigencia property.
     * 
     * @return
     *     possible object is
     *     {@link TipoVigencia }
     *     
     */
    public TipoVigencia getTipoVigencia() {
        return tipoVigencia;
    }

    /**
     * Sets the value of the tipoVigencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoVigencia }
     *     
     */
    public void setTipoVigencia(TipoVigencia value) {
        this.tipoVigencia = value;
    }

    /**
     * Gets the value of the usarFatorDeReajusteAutomatico property.
     * 
     */
    public boolean isUsarFatorDeReajusteAutomatico() {
        return usarFatorDeReajusteAutomatico;
    }

    /**
     * Sets the value of the usarFatorDeReajusteAutomatico property.
     * 
     */
    public void setUsarFatorDeReajusteAutomatico(boolean value) {
        this.usarFatorDeReajusteAutomatico = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
