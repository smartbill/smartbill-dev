
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for cicloFaturamento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cicloFaturamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="diaEnvioBoleto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diaEnvioBoletoFatorMesBase" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diaEnvioNFE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diaEnvioNFEFatorMesBase" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diaFechamentoConsumo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diaFechamentoConsumoFatorMesBase" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diaFechamentoFatura" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diaFechamentoFaturaFatorMesBase" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diaVencimento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diaVencimentoFatorMesBase" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cicloFaturamento", propOrder = {
    "criacao",
    "diaEnvioBoleto",
    "diaEnvioBoletoFatorMesBase",
    "diaEnvioNFE",
    "diaEnvioNFEFatorMesBase",
    "diaFechamentoConsumo",
    "diaFechamentoConsumoFatorMesBase",
    "diaFechamentoFatura",
    "diaFechamentoFaturaFatorMesBase",
    "diaVencimento",
    "diaVencimentoFatorMesBase",
    "exclusao",
    "gerenteUuid",
    "modificacao",
    "nome",
    "uuid"
})
public class CicloFaturamento {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    protected int diaEnvioBoleto;
    protected int diaEnvioBoletoFatorMesBase;
    protected int diaEnvioNFE;
    protected int diaEnvioNFEFatorMesBase;
    protected int diaFechamentoConsumo;
    protected int diaFechamentoConsumoFatorMesBase;
    protected int diaFechamentoFatura;
    protected int diaFechamentoFaturaFatorMesBase;
    protected int diaVencimento;
    protected int diaVencimentoFatorMesBase;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected String nome;
    protected String uuid;

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the diaEnvioBoleto property.
     * 
     */
    public int getDiaEnvioBoleto() {
        return diaEnvioBoleto;
    }

    /**
     * Sets the value of the diaEnvioBoleto property.
     * 
     */
    public void setDiaEnvioBoleto(int value) {
        this.diaEnvioBoleto = value;
    }

    /**
     * Gets the value of the diaEnvioBoletoFatorMesBase property.
     * 
     */
    public int getDiaEnvioBoletoFatorMesBase() {
        return diaEnvioBoletoFatorMesBase;
    }

    /**
     * Sets the value of the diaEnvioBoletoFatorMesBase property.
     * 
     */
    public void setDiaEnvioBoletoFatorMesBase(int value) {
        this.diaEnvioBoletoFatorMesBase = value;
    }

    /**
     * Gets the value of the diaEnvioNFE property.
     * 
     */
    public int getDiaEnvioNFE() {
        return diaEnvioNFE;
    }

    /**
     * Sets the value of the diaEnvioNFE property.
     * 
     */
    public void setDiaEnvioNFE(int value) {
        this.diaEnvioNFE = value;
    }

    /**
     * Gets the value of the diaEnvioNFEFatorMesBase property.
     * 
     */
    public int getDiaEnvioNFEFatorMesBase() {
        return diaEnvioNFEFatorMesBase;
    }

    /**
     * Sets the value of the diaEnvioNFEFatorMesBase property.
     * 
     */
    public void setDiaEnvioNFEFatorMesBase(int value) {
        this.diaEnvioNFEFatorMesBase = value;
    }

    /**
     * Gets the value of the diaFechamentoConsumo property.
     * 
     */
    public int getDiaFechamentoConsumo() {
        return diaFechamentoConsumo;
    }

    /**
     * Sets the value of the diaFechamentoConsumo property.
     * 
     */
    public void setDiaFechamentoConsumo(int value) {
        this.diaFechamentoConsumo = value;
    }

    /**
     * Gets the value of the diaFechamentoConsumoFatorMesBase property.
     * 
     */
    public int getDiaFechamentoConsumoFatorMesBase() {
        return diaFechamentoConsumoFatorMesBase;
    }

    /**
     * Sets the value of the diaFechamentoConsumoFatorMesBase property.
     * 
     */
    public void setDiaFechamentoConsumoFatorMesBase(int value) {
        this.diaFechamentoConsumoFatorMesBase = value;
    }

    /**
     * Gets the value of the diaFechamentoFatura property.
     * 
     */
    public int getDiaFechamentoFatura() {
        return diaFechamentoFatura;
    }

    /**
     * Sets the value of the diaFechamentoFatura property.
     * 
     */
    public void setDiaFechamentoFatura(int value) {
        this.diaFechamentoFatura = value;
    }

    /**
     * Gets the value of the diaFechamentoFaturaFatorMesBase property.
     * 
     */
    public int getDiaFechamentoFaturaFatorMesBase() {
        return diaFechamentoFaturaFatorMesBase;
    }

    /**
     * Sets the value of the diaFechamentoFaturaFatorMesBase property.
     * 
     */
    public void setDiaFechamentoFaturaFatorMesBase(int value) {
        this.diaFechamentoFaturaFatorMesBase = value;
    }

    /**
     * Gets the value of the diaVencimento property.
     * 
     */
    public int getDiaVencimento() {
        return diaVencimento;
    }

    /**
     * Sets the value of the diaVencimento property.
     * 
     */
    public void setDiaVencimento(int value) {
        this.diaVencimento = value;
    }

    /**
     * Gets the value of the diaVencimentoFatorMesBase property.
     * 
     */
    public int getDiaVencimentoFatorMesBase() {
        return diaVencimentoFatorMesBase;
    }

    /**
     * Sets the value of the diaVencimentoFatorMesBase property.
     * 
     */
    public void setDiaVencimentoFatorMesBase(int value) {
        this.diaVencimentoFatorMesBase = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
