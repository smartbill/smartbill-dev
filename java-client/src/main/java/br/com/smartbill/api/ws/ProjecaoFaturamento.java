
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for projecaoFaturamento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="projecaoFaturamento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REAL"/>
 *     &lt;enumeration value="PREVISTO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "projecaoFaturamento")
@XmlEnum
public enum ProjecaoFaturamento {

    REAL,
    PREVISTO;

    public String value() {
        return name();
    }

    public static ProjecaoFaturamento fromValue(String v) {
        return valueOf(v);
    }

}
