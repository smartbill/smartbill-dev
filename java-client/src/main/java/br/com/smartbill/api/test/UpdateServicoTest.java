package br.com.smartbill.api.test;

import br.com.smartbill.api.ws.Servico;
import br.com.smartbill.api.ws.ServicoFilter;
import br.com.smartbill.api.ws.SmartbillAPI;
import br.com.smartbill.api.ws.SmartbillAPIMessage_Exception;

public class UpdateServicoTest {
	
	final static SmartbillAPI service = new SmartbillApiAuth().getSmartbillAPI();

	public static void main(String[] args) throws SmartbillAPIMessage_Exception {
		
		String chaveConhecido = "c7f5035b-5383-11e3-880f-5cf9dd5f0a07";
		
		Servico servico = service.obterServico(chaveConhecido);
		
		//Ou
		
		ServicoFilter servicoFilter = new ServicoFilter();
		servicoFilter.setNome("Demostração Serviço");
		
		servico = service.listarServico(servicoFilter).get(0);
				
		servico.setCodigo("123");
		
		try {
			service.alterarServico(servico);
		} catch (SmartbillAPIMessage_Exception e) {
			System.out.println(e.getFaultInfo().getMensagem());
		}

	}

}
