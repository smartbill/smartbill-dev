
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoClienteCRTransacao.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoClienteCRTransacao">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BOLETO"/>
 *     &lt;enumeration value="CARTAO"/>
 *     &lt;enumeration value="DEPOSITO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoClienteCRTransacao")
@XmlEnum
public enum TipoClienteCRTransacao {

    BOLETO,
    CARTAO,
    DEPOSITO;

    public String value() {
        return name();
    }

    public static TipoClienteCRTransacao fromValue(String v) {
        return valueOf(v);
    }

}
