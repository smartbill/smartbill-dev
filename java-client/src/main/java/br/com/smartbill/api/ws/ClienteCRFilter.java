
package br.com.smartbill.api.ws;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteCRFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteCRFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anoReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="boleto" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="cicloFaturamento" type="{http://ws.api.smartbill.com.br/}cicloFaturamento" minOccurs="0"/>
 *         &lt;element name="clienteServicoCRUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clienteServicoUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clienteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enviado" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="fakeRangeDataPagamento" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="fakeRangeDataVencimento" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="fakeRangeDataVencimentoOriginal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="generalFilter" type="{http://ws.api.smartbill.com.br/}generalFilter" minOccurs="0"/>
 *         &lt;element name="mesReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="notaFiscal" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="pago" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="possuiNFEmitido" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="projecaoFaturamento" type="{http://ws.api.smartbill.com.br/}projecaoFaturamento" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rangeDataPagamento" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rangeDataVencimento" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rangeDataVencimentoOriginal" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tipoCREstado" type="{http://ws.api.smartbill.com.br/}tipoCREstado" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="vencido" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteCRFilter", propOrder = {
    "anoReferencia",
    "boleto",
    "cicloFaturamento",
    "clienteServicoCRUuid",
    "clienteServicoUuid",
    "clienteUuid",
    "enviado",
    "fakeRangeDataPagamento",
    "fakeRangeDataVencimento",
    "fakeRangeDataVencimentoOriginal",
    "generalFilter",
    "mesReferencia",
    "notaFiscal",
    "pago",
    "possuiNFEmitido",
    "projecaoFaturamento",
    "rangeDataPagamento",
    "rangeDataVencimento",
    "rangeDataVencimentoOriginal",
    "tipoCREstado",
    "vencido"
})
public class ClienteCRFilter {

    protected Integer anoReferencia;
    protected Long boleto;
    protected CicloFaturamento cicloFaturamento;
    protected String clienteServicoCRUuid;
    protected String clienteServicoUuid;
    protected String clienteUuid;
    protected Boolean enviado;
    protected Boolean fakeRangeDataPagamento;
    protected Boolean fakeRangeDataVencimento;
    protected Boolean fakeRangeDataVencimentoOriginal;
    protected GeneralFilter generalFilter;
    protected Integer mesReferencia;
    protected BigInteger notaFiscal;
    protected Boolean pago;
    protected Boolean possuiNFEmitido;
    @XmlElement(nillable = true)
    protected List<ProjecaoFaturamento> projecaoFaturamento;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected List<XMLGregorianCalendar> rangeDataPagamento;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected List<XMLGregorianCalendar> rangeDataVencimento;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected List<XMLGregorianCalendar> rangeDataVencimentoOriginal;
    @XmlElement(nillable = true)
    protected List<TipoCREstado> tipoCREstado;
    protected Boolean vencido;

    /**
     * Gets the value of the anoReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnoReferencia() {
        return anoReferencia;
    }

    /**
     * Sets the value of the anoReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnoReferencia(Integer value) {
        this.anoReferencia = value;
    }

    /**
     * Gets the value of the boleto property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBoleto() {
        return boleto;
    }

    /**
     * Sets the value of the boleto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBoleto(Long value) {
        this.boleto = value;
    }

    /**
     * Gets the value of the cicloFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link CicloFaturamento }
     *     
     */
    public CicloFaturamento getCicloFaturamento() {
        return cicloFaturamento;
    }

    /**
     * Sets the value of the cicloFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link CicloFaturamento }
     *     
     */
    public void setCicloFaturamento(CicloFaturamento value) {
        this.cicloFaturamento = value;
    }

    /**
     * Gets the value of the clienteServicoCRUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteServicoCRUuid() {
        return clienteServicoCRUuid;
    }

    /**
     * Sets the value of the clienteServicoCRUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteServicoCRUuid(String value) {
        this.clienteServicoCRUuid = value;
    }

    /**
     * Gets the value of the clienteServicoUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteServicoUuid() {
        return clienteServicoUuid;
    }

    /**
     * Sets the value of the clienteServicoUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteServicoUuid(String value) {
        this.clienteServicoUuid = value;
    }

    /**
     * Gets the value of the clienteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteUuid() {
        return clienteUuid;
    }

    /**
     * Sets the value of the clienteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteUuid(String value) {
        this.clienteUuid = value;
    }

    /**
     * Gets the value of the enviado property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnviado() {
        return enviado;
    }

    /**
     * Sets the value of the enviado property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnviado(Boolean value) {
        this.enviado = value;
    }

    /**
     * Gets the value of the fakeRangeDataPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFakeRangeDataPagamento() {
        return fakeRangeDataPagamento;
    }

    /**
     * Sets the value of the fakeRangeDataPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFakeRangeDataPagamento(Boolean value) {
        this.fakeRangeDataPagamento = value;
    }

    /**
     * Gets the value of the fakeRangeDataVencimento property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFakeRangeDataVencimento() {
        return fakeRangeDataVencimento;
    }

    /**
     * Sets the value of the fakeRangeDataVencimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFakeRangeDataVencimento(Boolean value) {
        this.fakeRangeDataVencimento = value;
    }

    /**
     * Gets the value of the fakeRangeDataVencimentoOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFakeRangeDataVencimentoOriginal() {
        return fakeRangeDataVencimentoOriginal;
    }

    /**
     * Sets the value of the fakeRangeDataVencimentoOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFakeRangeDataVencimentoOriginal(Boolean value) {
        this.fakeRangeDataVencimentoOriginal = value;
    }

    /**
     * Gets the value of the generalFilter property.
     * 
     * @return
     *     possible object is
     *     {@link GeneralFilter }
     *     
     */
    public GeneralFilter getGeneralFilter() {
        return generalFilter;
    }

    /**
     * Sets the value of the generalFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralFilter }
     *     
     */
    public void setGeneralFilter(GeneralFilter value) {
        this.generalFilter = value;
    }

    /**
     * Gets the value of the mesReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMesReferencia() {
        return mesReferencia;
    }

    /**
     * Sets the value of the mesReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMesReferencia(Integer value) {
        this.mesReferencia = value;
    }

    /**
     * Gets the value of the notaFiscal property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNotaFiscal() {
        return notaFiscal;
    }

    /**
     * Sets the value of the notaFiscal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNotaFiscal(BigInteger value) {
        this.notaFiscal = value;
    }

    /**
     * Gets the value of the pago property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPago() {
        return pago;
    }

    /**
     * Sets the value of the pago property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPago(Boolean value) {
        this.pago = value;
    }

    /**
     * Gets the value of the possuiNFEmitido property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPossuiNFEmitido() {
        return possuiNFEmitido;
    }

    /**
     * Sets the value of the possuiNFEmitido property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPossuiNFEmitido(Boolean value) {
        this.possuiNFEmitido = value;
    }

    /**
     * Gets the value of the projecaoFaturamento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the projecaoFaturamento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProjecaoFaturamento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProjecaoFaturamento }
     * 
     * 
     */
    public List<ProjecaoFaturamento> getProjecaoFaturamento() {
        if (projecaoFaturamento == null) {
            projecaoFaturamento = new ArrayList<ProjecaoFaturamento>();
        }
        return this.projecaoFaturamento;
    }

    /**
     * Gets the value of the rangeDataPagamento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rangeDataPagamento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRangeDataPagamento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XMLGregorianCalendar }
     * 
     * 
     */
    public List<XMLGregorianCalendar> getRangeDataPagamento() {
        if (rangeDataPagamento == null) {
            rangeDataPagamento = new ArrayList<XMLGregorianCalendar>();
        }
        return this.rangeDataPagamento;
    }

    /**
     * Gets the value of the rangeDataVencimento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rangeDataVencimento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRangeDataVencimento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XMLGregorianCalendar }
     * 
     * 
     */
    public List<XMLGregorianCalendar> getRangeDataVencimento() {
        if (rangeDataVencimento == null) {
            rangeDataVencimento = new ArrayList<XMLGregorianCalendar>();
        }
        return this.rangeDataVencimento;
    }

    /**
     * Gets the value of the rangeDataVencimentoOriginal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rangeDataVencimentoOriginal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRangeDataVencimentoOriginal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XMLGregorianCalendar }
     * 
     * 
     */
    public List<XMLGregorianCalendar> getRangeDataVencimentoOriginal() {
        if (rangeDataVencimentoOriginal == null) {
            rangeDataVencimentoOriginal = new ArrayList<XMLGregorianCalendar>();
        }
        return this.rangeDataVencimentoOriginal;
    }

    /**
     * Gets the value of the tipoCREstado property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipoCREstado property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipoCREstado().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoCREstado }
     * 
     * 
     */
    public List<TipoCREstado> getTipoCREstado() {
        if (tipoCREstado == null) {
            tipoCREstado = new ArrayList<TipoCREstado>();
        }
        return this.tipoCREstado;
    }

    /**
     * Gets the value of the vencido property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVencido() {
        return vencido;
    }

    /**
     * Sets the value of the vencido property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVencido(Boolean value) {
        this.vencido = value;
    }

}
