
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoContato.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoContato">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="RESPONSAVEL_LEGAL"/>
 *     &lt;enumeration value="TECNICO"/>
 *     &lt;enumeration value="COBRANCA"/>
 *     &lt;enumeration value="COMPRAS"/>
 *     &lt;enumeration value="FINANCEIRO"/>
 *     &lt;enumeration value="CONTABIL"/>
 *     &lt;enumeration value="RH"/>
 *     &lt;enumeration value="JURIDICO"/>
 *     &lt;enumeration value="OUTROS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoContato")
@XmlEnum
public enum TipoContato {

    RESPONSAVEL_LEGAL,
    TECNICO,
    COBRANCA,
    COMPRAS,
    FINANCEIRO,
    CONTABIL,
    RH,
    JURIDICO,
    OUTROS;

    public String value() {
        return name();
    }

    public static TipoContato fromValue(String v) {
        return valueOf(v);
    }

}
