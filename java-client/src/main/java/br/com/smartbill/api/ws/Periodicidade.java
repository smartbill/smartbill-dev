
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for periodicidade complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="periodicidade">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ativo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="exibirNoFormularioDeContratoExterno" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="percentualDesconto" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="tipoPeriodicidade" type="{http://ws.api.smartbill.com.br/}tipoPeriodicidade" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "periodicidade", propOrder = {
    "ativo",
    "criacao",
    "exclusao",
    "exibirNoFormularioDeContratoExterno",
    "gerenteUuid",
    "modificacao",
    "percentualDesconto",
    "tipoPeriodicidade",
    "uuid"
})
public class Periodicidade {

    protected boolean ativo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected boolean exibirNoFormularioDeContratoExterno;
    protected String gerenteUuid;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected Double percentualDesconto;
    protected TipoPeriodicidade tipoPeriodicidade;
    protected String uuid;

    /**
     * Gets the value of the ativo property.
     * 
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * Sets the value of the ativo property.
     * 
     */
    public void setAtivo(boolean value) {
        this.ativo = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the exibirNoFormularioDeContratoExterno property.
     * 
     */
    public boolean isExibirNoFormularioDeContratoExterno() {
        return exibirNoFormularioDeContratoExterno;
    }

    /**
     * Sets the value of the exibirNoFormularioDeContratoExterno property.
     * 
     */
    public void setExibirNoFormularioDeContratoExterno(boolean value) {
        this.exibirNoFormularioDeContratoExterno = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the percentualDesconto property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPercentualDesconto() {
        return percentualDesconto;
    }

    /**
     * Sets the value of the percentualDesconto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPercentualDesconto(Double value) {
        this.percentualDesconto = value;
    }

    /**
     * Gets the value of the tipoPeriodicidade property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPeriodicidade }
     *     
     */
    public TipoPeriodicidade getTipoPeriodicidade() {
        return tipoPeriodicidade;
    }

    /**
     * Sets the value of the tipoPeriodicidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPeriodicidade }
     *     
     */
    public void setTipoPeriodicidade(TipoPeriodicidade value) {
        this.tipoPeriodicidade = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
