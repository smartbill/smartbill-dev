
package br.com.smartbill.api.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteNFDTOFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteNFDTOFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anoReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cicloFaturamento" type="{http://ws.api.smartbill.com.br/}cicloFaturamento" minOccurs="0"/>
 *         &lt;element name="clienteCRUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clienteServicoCRUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cnpjCpfTomador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="generalFilter" type="{http://ws.api.smartbill.com.br/}generalFilter" minOccurs="0"/>
 *         &lt;element name="mesReferencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="rangeDataEmissao" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rangeDataVencimento" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rangeDataVencimentoOriginal" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tipoNFEstado" type="{http://ws.api.smartbill.com.br/}tipoNFEstado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteNFDTOFilter", propOrder = {
    "anoReferencia",
    "cicloFaturamento",
    "clienteCRUuid",
    "clienteServicoCRUuid",
    "cnpjCpfTomador",
    "generalFilter",
    "mesReferencia",
    "rangeDataEmissao",
    "rangeDataVencimento",
    "rangeDataVencimentoOriginal",
    "tipoNFEstado"
})
public class ClienteNFDTOFilter {

    protected Integer anoReferencia;
    protected CicloFaturamento cicloFaturamento;
    protected String clienteCRUuid;
    protected String clienteServicoCRUuid;
    protected String cnpjCpfTomador;
    protected GeneralFilter generalFilter;
    protected Integer mesReferencia;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected List<XMLGregorianCalendar> rangeDataEmissao;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected List<XMLGregorianCalendar> rangeDataVencimento;
    @XmlElement(nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected List<XMLGregorianCalendar> rangeDataVencimentoOriginal;
    protected TipoNFEstado tipoNFEstado;

    /**
     * Gets the value of the anoReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnoReferencia() {
        return anoReferencia;
    }

    /**
     * Sets the value of the anoReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnoReferencia(Integer value) {
        this.anoReferencia = value;
    }

    /**
     * Gets the value of the cicloFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link CicloFaturamento }
     *     
     */
    public CicloFaturamento getCicloFaturamento() {
        return cicloFaturamento;
    }

    /**
     * Sets the value of the cicloFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link CicloFaturamento }
     *     
     */
    public void setCicloFaturamento(CicloFaturamento value) {
        this.cicloFaturamento = value;
    }

    /**
     * Gets the value of the clienteCRUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteCRUuid() {
        return clienteCRUuid;
    }

    /**
     * Sets the value of the clienteCRUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteCRUuid(String value) {
        this.clienteCRUuid = value;
    }

    /**
     * Gets the value of the clienteServicoCRUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteServicoCRUuid() {
        return clienteServicoCRUuid;
    }

    /**
     * Sets the value of the clienteServicoCRUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteServicoCRUuid(String value) {
        this.clienteServicoCRUuid = value;
    }

    /**
     * Gets the value of the cnpjCpfTomador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnpjCpfTomador() {
        return cnpjCpfTomador;
    }

    /**
     * Sets the value of the cnpjCpfTomador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnpjCpfTomador(String value) {
        this.cnpjCpfTomador = value;
    }

    /**
     * Gets the value of the generalFilter property.
     * 
     * @return
     *     possible object is
     *     {@link GeneralFilter }
     *     
     */
    public GeneralFilter getGeneralFilter() {
        return generalFilter;
    }

    /**
     * Sets the value of the generalFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralFilter }
     *     
     */
    public void setGeneralFilter(GeneralFilter value) {
        this.generalFilter = value;
    }

    /**
     * Gets the value of the mesReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMesReferencia() {
        return mesReferencia;
    }

    /**
     * Sets the value of the mesReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMesReferencia(Integer value) {
        this.mesReferencia = value;
    }

    /**
     * Gets the value of the rangeDataEmissao property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rangeDataEmissao property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRangeDataEmissao().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XMLGregorianCalendar }
     * 
     * 
     */
    public List<XMLGregorianCalendar> getRangeDataEmissao() {
        if (rangeDataEmissao == null) {
            rangeDataEmissao = new ArrayList<XMLGregorianCalendar>();
        }
        return this.rangeDataEmissao;
    }

    /**
     * Gets the value of the rangeDataVencimento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rangeDataVencimento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRangeDataVencimento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XMLGregorianCalendar }
     * 
     * 
     */
    public List<XMLGregorianCalendar> getRangeDataVencimento() {
        if (rangeDataVencimento == null) {
            rangeDataVencimento = new ArrayList<XMLGregorianCalendar>();
        }
        return this.rangeDataVencimento;
    }

    /**
     * Gets the value of the rangeDataVencimentoOriginal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rangeDataVencimentoOriginal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRangeDataVencimentoOriginal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XMLGregorianCalendar }
     * 
     * 
     */
    public List<XMLGregorianCalendar> getRangeDataVencimentoOriginal() {
        if (rangeDataVencimentoOriginal == null) {
            rangeDataVencimentoOriginal = new ArrayList<XMLGregorianCalendar>();
        }
        return this.rangeDataVencimentoOriginal;
    }

    /**
     * Gets the value of the tipoNFEstado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoNFEstado }
     *     
     */
    public TipoNFEstado getTipoNFEstado() {
        return tipoNFEstado;
    }

    /**
     * Sets the value of the tipoNFEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoNFEstado }
     *     
     */
    public void setTipoNFEstado(TipoNFEstado value) {
        this.tipoNFEstado = value;
    }

}
