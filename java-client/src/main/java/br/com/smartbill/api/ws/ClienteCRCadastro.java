
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteCRCadastro complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteCRCadastro">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clienteCRUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientePrestador" type="{http://ws.api.smartbill.com.br/}clientePrestador" minOccurs="0"/>
 *         &lt;element name="clienteTomador" type="{http://ws.api.smartbill.com.br/}clienteTomador" minOccurs="0"/>
 *         &lt;element name="contatoPrestador" type="{http://ws.api.smartbill.com.br/}contatoPrestador" minOccurs="0"/>
 *         &lt;element name="contatoTomador" type="{http://ws.api.smartbill.com.br/}contatoTomador" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="enderecoPrestador" type="{http://ws.api.smartbill.com.br/}enderecoPrestador" minOccurs="0"/>
 *         &lt;element name="enderecoTomador" type="{http://ws.api.smartbill.com.br/}enderecoTomador" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteCRCadastro", propOrder = {
    "clienteCRUuid",
    "clientePrestador",
    "clienteTomador",
    "contatoPrestador",
    "contatoTomador",
    "criacao",
    "enderecoPrestador",
    "enderecoTomador",
    "exclusao",
    "gerenteUuid",
    "modificacao",
    "uuid"
})
public class ClienteCRCadastro {

    protected String clienteCRUuid;
    protected ClientePrestador clientePrestador;
    protected ClienteTomador clienteTomador;
    protected ContatoPrestador contatoPrestador;
    protected ContatoTomador contatoTomador;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    protected EnderecoPrestador enderecoPrestador;
    protected EnderecoTomador enderecoTomador;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected String gerenteUuid;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected String uuid;

    /**
     * Gets the value of the clienteCRUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteCRUuid() {
        return clienteCRUuid;
    }

    /**
     * Sets the value of the clienteCRUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteCRUuid(String value) {
        this.clienteCRUuid = value;
    }

    /**
     * Gets the value of the clientePrestador property.
     * 
     * @return
     *     possible object is
     *     {@link ClientePrestador }
     *     
     */
    public ClientePrestador getClientePrestador() {
        return clientePrestador;
    }

    /**
     * Sets the value of the clientePrestador property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientePrestador }
     *     
     */
    public void setClientePrestador(ClientePrestador value) {
        this.clientePrestador = value;
    }

    /**
     * Gets the value of the clienteTomador property.
     * 
     * @return
     *     possible object is
     *     {@link ClienteTomador }
     *     
     */
    public ClienteTomador getClienteTomador() {
        return clienteTomador;
    }

    /**
     * Sets the value of the clienteTomador property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClienteTomador }
     *     
     */
    public void setClienteTomador(ClienteTomador value) {
        this.clienteTomador = value;
    }

    /**
     * Gets the value of the contatoPrestador property.
     * 
     * @return
     *     possible object is
     *     {@link ContatoPrestador }
     *     
     */
    public ContatoPrestador getContatoPrestador() {
        return contatoPrestador;
    }

    /**
     * Sets the value of the contatoPrestador property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContatoPrestador }
     *     
     */
    public void setContatoPrestador(ContatoPrestador value) {
        this.contatoPrestador = value;
    }

    /**
     * Gets the value of the contatoTomador property.
     * 
     * @return
     *     possible object is
     *     {@link ContatoTomador }
     *     
     */
    public ContatoTomador getContatoTomador() {
        return contatoTomador;
    }

    /**
     * Sets the value of the contatoTomador property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContatoTomador }
     *     
     */
    public void setContatoTomador(ContatoTomador value) {
        this.contatoTomador = value;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the enderecoPrestador property.
     * 
     * @return
     *     possible object is
     *     {@link EnderecoPrestador }
     *     
     */
    public EnderecoPrestador getEnderecoPrestador() {
        return enderecoPrestador;
    }

    /**
     * Sets the value of the enderecoPrestador property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnderecoPrestador }
     *     
     */
    public void setEnderecoPrestador(EnderecoPrestador value) {
        this.enderecoPrestador = value;
    }

    /**
     * Gets the value of the enderecoTomador property.
     * 
     * @return
     *     possible object is
     *     {@link EnderecoTomador }
     *     
     */
    public EnderecoTomador getEnderecoTomador() {
        return enderecoTomador;
    }

    /**
     * Sets the value of the enderecoTomador property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnderecoTomador }
     *     
     */
    public void setEnderecoTomador(EnderecoTomador value) {
        this.enderecoTomador = value;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
