
package br.com.smartbill.api.ws;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for clienteServico complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteServico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acrescimos" type="{http://ws.api.smartbill.com.br/}clienteServicoAcrescimo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="aliquotaCOFINS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaCSLL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaINSS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaIR" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaISS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaISSRetido" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aliquotaPIS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="codigoConsolidaFatura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contatos" type="{http://ws.api.smartbill.com.br/}contato" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="criacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataAdesao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataCancelamento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataPrimeiroVencimento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataSuspensaoFim" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataSuspensaoInicio" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataVigenciaFim" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataVigenciaInicio" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descontos" type="{http://ws.api.smartbill.com.br/}clienteServicoDesconto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="exclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fatorDeReajusteDaRenovacao" type="{http://ws.api.smartbill.com.br/}indiceEconomico" minOccurs="0"/>
 *         &lt;element name="gerenteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modificacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="motivoCancelamento" type="{http://ws.api.smartbill.com.br/}motivoCancelamento" minOccurs="0"/>
 *         &lt;element name="motivoSuspensao" type="{http://ws.api.smartbill.com.br/}motivoSuspensao" minOccurs="0"/>
 *         &lt;element name="municipioPrestacaoServico" type="{http://ws.api.smartbill.com.br/}cidade" minOccurs="0"/>
 *         &lt;element name="naturezaOperacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parcelasAtivacao" type="{http://ws.api.smartbill.com.br/}clienteServicoParcelaAtivacao" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="parcelasServico" type="{http://ws.api.smartbill.com.br/}clienteServicoParcelaServico" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="periodicidade" type="{http://ws.api.smartbill.com.br/}periodicidade" minOccurs="0"/>
 *         &lt;element name="quantidade" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="quantidadeTipoVigencia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="renovacaoAutomatica" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="servico" type="{http://ws.api.smartbill.com.br/}servico" minOccurs="0"/>
 *         &lt;element name="servicoEstado" type="{http://ws.api.smartbill.com.br/}servicoEstado" minOccurs="0"/>
 *         &lt;element name="tarifaMinima" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="taxaAtivacao" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tipoConsumo" type="{http://ws.api.smartbill.com.br/}tipoConsumo" minOccurs="0"/>
 *         &lt;element name="tipoFaturamento" type="{http://ws.api.smartbill.com.br/}tipoFaturamento" minOccurs="0"/>
 *         &lt;element name="tipoVigencia" type="{http://ws.api.smartbill.com.br/}tipoVigencia" minOccurs="0"/>
 *         &lt;element name="unidadeMedida" type="{http://ws.api.smartbill.com.br/}unidadeMedida" minOccurs="0"/>
 *         &lt;element name="unidadeMetrica" type="{http://ws.api.smartbill.com.br/}unidadeMetrica" minOccurs="0"/>
 *         &lt;element name="usarFatorDeReajusteAutomatico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="uuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="valores" type="{http://ws.api.smartbill.com.br/}clienteServicoValor" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="vendedor" type="{http://ws.api.smartbill.com.br/}usuario" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteServico", propOrder = {
    "acrescimos",
    "aliquotaCOFINS",
    "aliquotaCSLL",
    "aliquotaINSS",
    "aliquotaIR",
    "aliquotaISS",
    "aliquotaISSRetido",
    "aliquotaPIS",
    "codigoConsolidaFatura",
    "codigoContrato",
    "contatos",
    "criacao",
    "dataAdesao",
    "dataCancelamento",
    "dataPrimeiroVencimento",
    "dataSuspensaoFim",
    "dataSuspensaoInicio",
    "dataVigenciaFim",
    "dataVigenciaInicio",
    "descontos",
    "exclusao",
    "fatorDeReajusteDaRenovacao",
    "gerenteUuid",
    "modificacao",
    "motivoCancelamento",
    "motivoSuspensao",
    "municipioPrestacaoServico",
    "naturezaOperacao",
    "observacao",
    "parcelasAtivacao",
    "parcelasServico",
    "periodicidade",
    "quantidade",
    "quantidadeTipoVigencia",
    "renovacaoAutomatica",
    "servico",
    "servicoEstado",
    "tarifaMinima",
    "taxaAtivacao",
    "tipoConsumo",
    "tipoFaturamento",
    "tipoVigencia",
    "unidadeMedida",
    "unidadeMetrica",
    "usarFatorDeReajusteAutomatico",
    "uuid",
    "valor",
    "valores",
    "vendedor"
})
public class ClienteServico {

    @XmlElement(nillable = true)
    protected List<ClienteServicoAcrescimo> acrescimos;
    protected BigDecimal aliquotaCOFINS;
    protected BigDecimal aliquotaCSLL;
    protected BigDecimal aliquotaINSS;
    protected BigDecimal aliquotaIR;
    protected BigDecimal aliquotaISS;
    protected BigDecimal aliquotaISSRetido;
    protected BigDecimal aliquotaPIS;
    protected String codigoConsolidaFatura;
    protected String codigoContrato;
    @XmlElement(nillable = true)
    protected List<Contato> contatos;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar criacao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAdesao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCancelamento;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataPrimeiroVencimento;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataSuspensaoFim;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataSuspensaoInicio;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataVigenciaFim;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataVigenciaInicio;
    @XmlElement(nillable = true)
    protected List<ClienteServicoDesconto> descontos;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exclusao;
    protected IndiceEconomico fatorDeReajusteDaRenovacao;
    protected String gerenteUuid;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modificacao;
    protected MotivoCancelamento motivoCancelamento;
    protected MotivoSuspensao motivoSuspensao;
    protected Cidade municipioPrestacaoServico;
    protected String naturezaOperacao;
    protected String observacao;
    @XmlElement(nillable = true)
    protected List<ClienteServicoParcelaAtivacao> parcelasAtivacao;
    @XmlElement(nillable = true)
    protected List<ClienteServicoParcelaServico> parcelasServico;
    protected Periodicidade periodicidade;
    protected BigDecimal quantidade;
    protected Integer quantidadeTipoVigencia;
    protected boolean renovacaoAutomatica;
    protected Servico servico;
    protected ServicoEstado servicoEstado;
    protected BigDecimal tarifaMinima;
    protected BigDecimal taxaAtivacao;
    protected TipoConsumo tipoConsumo;
    protected TipoFaturamento tipoFaturamento;
    protected TipoVigencia tipoVigencia;
    protected UnidadeMedida unidadeMedida;
    protected UnidadeMetrica unidadeMetrica;
    protected boolean usarFatorDeReajusteAutomatico;
    protected String uuid;
    protected BigDecimal valor;
    @XmlElement(nillable = true)
    protected List<ClienteServicoValor> valores;
    protected Usuario vendedor;

    /**
     * Gets the value of the acrescimos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acrescimos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcrescimos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteServicoAcrescimo }
     * 
     * 
     */
    public List<ClienteServicoAcrescimo> getAcrescimos() {
        if (acrescimos == null) {
            acrescimos = new ArrayList<ClienteServicoAcrescimo>();
        }
        return this.acrescimos;
    }

    /**
     * Gets the value of the aliquotaCOFINS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaCOFINS() {
        return aliquotaCOFINS;
    }

    /**
     * Sets the value of the aliquotaCOFINS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaCOFINS(BigDecimal value) {
        this.aliquotaCOFINS = value;
    }

    /**
     * Gets the value of the aliquotaCSLL property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaCSLL() {
        return aliquotaCSLL;
    }

    /**
     * Sets the value of the aliquotaCSLL property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaCSLL(BigDecimal value) {
        this.aliquotaCSLL = value;
    }

    /**
     * Gets the value of the aliquotaINSS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaINSS() {
        return aliquotaINSS;
    }

    /**
     * Sets the value of the aliquotaINSS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaINSS(BigDecimal value) {
        this.aliquotaINSS = value;
    }

    /**
     * Gets the value of the aliquotaIR property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaIR() {
        return aliquotaIR;
    }

    /**
     * Sets the value of the aliquotaIR property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaIR(BigDecimal value) {
        this.aliquotaIR = value;
    }

    /**
     * Gets the value of the aliquotaISS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISS() {
        return aliquotaISS;
    }

    /**
     * Sets the value of the aliquotaISS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISS(BigDecimal value) {
        this.aliquotaISS = value;
    }

    /**
     * Gets the value of the aliquotaISSRetido property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaISSRetido() {
        return aliquotaISSRetido;
    }

    /**
     * Sets the value of the aliquotaISSRetido property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaISSRetido(BigDecimal value) {
        this.aliquotaISSRetido = value;
    }

    /**
     * Gets the value of the aliquotaPIS property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAliquotaPIS() {
        return aliquotaPIS;
    }

    /**
     * Sets the value of the aliquotaPIS property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAliquotaPIS(BigDecimal value) {
        this.aliquotaPIS = value;
    }

    /**
     * Gets the value of the codigoConsolidaFatura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoConsolidaFatura() {
        return codigoConsolidaFatura;
    }

    /**
     * Sets the value of the codigoConsolidaFatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoConsolidaFatura(String value) {
        this.codigoConsolidaFatura = value;
    }

    /**
     * Gets the value of the codigoContrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoContrato() {
        return codigoContrato;
    }

    /**
     * Sets the value of the codigoContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoContrato(String value) {
        this.codigoContrato = value;
    }

    /**
     * Gets the value of the contatos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contatos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContatos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Contato }
     * 
     * 
     */
    public List<Contato> getContatos() {
        if (contatos == null) {
            contatos = new ArrayList<Contato>();
        }
        return this.contatos;
    }

    /**
     * Gets the value of the criacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCriacao() {
        return criacao;
    }

    /**
     * Sets the value of the criacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCriacao(XMLGregorianCalendar value) {
        this.criacao = value;
    }

    /**
     * Gets the value of the dataAdesao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAdesao() {
        return dataAdesao;
    }

    /**
     * Sets the value of the dataAdesao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAdesao(XMLGregorianCalendar value) {
        this.dataAdesao = value;
    }

    /**
     * Gets the value of the dataCancelamento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCancelamento() {
        return dataCancelamento;
    }

    /**
     * Sets the value of the dataCancelamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCancelamento(XMLGregorianCalendar value) {
        this.dataCancelamento = value;
    }

    /**
     * Gets the value of the dataPrimeiroVencimento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataPrimeiroVencimento() {
        return dataPrimeiroVencimento;
    }

    /**
     * Sets the value of the dataPrimeiroVencimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataPrimeiroVencimento(XMLGregorianCalendar value) {
        this.dataPrimeiroVencimento = value;
    }

    /**
     * Gets the value of the dataSuspensaoFim property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataSuspensaoFim() {
        return dataSuspensaoFim;
    }

    /**
     * Sets the value of the dataSuspensaoFim property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataSuspensaoFim(XMLGregorianCalendar value) {
        this.dataSuspensaoFim = value;
    }

    /**
     * Gets the value of the dataSuspensaoInicio property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataSuspensaoInicio() {
        return dataSuspensaoInicio;
    }

    /**
     * Sets the value of the dataSuspensaoInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataSuspensaoInicio(XMLGregorianCalendar value) {
        this.dataSuspensaoInicio = value;
    }

    /**
     * Gets the value of the dataVigenciaFim property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataVigenciaFim() {
        return dataVigenciaFim;
    }

    /**
     * Sets the value of the dataVigenciaFim property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataVigenciaFim(XMLGregorianCalendar value) {
        this.dataVigenciaFim = value;
    }

    /**
     * Gets the value of the dataVigenciaInicio property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataVigenciaInicio() {
        return dataVigenciaInicio;
    }

    /**
     * Sets the value of the dataVigenciaInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataVigenciaInicio(XMLGregorianCalendar value) {
        this.dataVigenciaInicio = value;
    }

    /**
     * Gets the value of the descontos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the descontos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescontos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteServicoDesconto }
     * 
     * 
     */
    public List<ClienteServicoDesconto> getDescontos() {
        if (descontos == null) {
            descontos = new ArrayList<ClienteServicoDesconto>();
        }
        return this.descontos;
    }

    /**
     * Gets the value of the exclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExclusao() {
        return exclusao;
    }

    /**
     * Sets the value of the exclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExclusao(XMLGregorianCalendar value) {
        this.exclusao = value;
    }

    /**
     * Gets the value of the fatorDeReajusteDaRenovacao property.
     * 
     * @return
     *     possible object is
     *     {@link IndiceEconomico }
     *     
     */
    public IndiceEconomico getFatorDeReajusteDaRenovacao() {
        return fatorDeReajusteDaRenovacao;
    }

    /**
     * Sets the value of the fatorDeReajusteDaRenovacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndiceEconomico }
     *     
     */
    public void setFatorDeReajusteDaRenovacao(IndiceEconomico value) {
        this.fatorDeReajusteDaRenovacao = value;
    }

    /**
     * Gets the value of the gerenteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGerenteUuid() {
        return gerenteUuid;
    }

    /**
     * Sets the value of the gerenteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGerenteUuid(String value) {
        this.gerenteUuid = value;
    }

    /**
     * Gets the value of the modificacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificacao() {
        return modificacao;
    }

    /**
     * Sets the value of the modificacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificacao(XMLGregorianCalendar value) {
        this.modificacao = value;
    }

    /**
     * Gets the value of the motivoCancelamento property.
     * 
     * @return
     *     possible object is
     *     {@link MotivoCancelamento }
     *     
     */
    public MotivoCancelamento getMotivoCancelamento() {
        return motivoCancelamento;
    }

    /**
     * Sets the value of the motivoCancelamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link MotivoCancelamento }
     *     
     */
    public void setMotivoCancelamento(MotivoCancelamento value) {
        this.motivoCancelamento = value;
    }

    /**
     * Gets the value of the motivoSuspensao property.
     * 
     * @return
     *     possible object is
     *     {@link MotivoSuspensao }
     *     
     */
    public MotivoSuspensao getMotivoSuspensao() {
        return motivoSuspensao;
    }

    /**
     * Sets the value of the motivoSuspensao property.
     * 
     * @param value
     *     allowed object is
     *     {@link MotivoSuspensao }
     *     
     */
    public void setMotivoSuspensao(MotivoSuspensao value) {
        this.motivoSuspensao = value;
    }

    /**
     * Gets the value of the municipioPrestacaoServico property.
     * 
     * @return
     *     possible object is
     *     {@link Cidade }
     *     
     */
    public Cidade getMunicipioPrestacaoServico() {
        return municipioPrestacaoServico;
    }

    /**
     * Sets the value of the municipioPrestacaoServico property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cidade }
     *     
     */
    public void setMunicipioPrestacaoServico(Cidade value) {
        this.municipioPrestacaoServico = value;
    }

    /**
     * Gets the value of the naturezaOperacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNaturezaOperacao() {
        return naturezaOperacao;
    }

    /**
     * Sets the value of the naturezaOperacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNaturezaOperacao(String value) {
        this.naturezaOperacao = value;
    }

    /**
     * Gets the value of the observacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Sets the value of the observacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Gets the value of the parcelasAtivacao property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parcelasAtivacao property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParcelasAtivacao().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteServicoParcelaAtivacao }
     * 
     * 
     */
    public List<ClienteServicoParcelaAtivacao> getParcelasAtivacao() {
        if (parcelasAtivacao == null) {
            parcelasAtivacao = new ArrayList<ClienteServicoParcelaAtivacao>();
        }
        return this.parcelasAtivacao;
    }

    /**
     * Gets the value of the parcelasServico property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parcelasServico property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParcelasServico().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteServicoParcelaServico }
     * 
     * 
     */
    public List<ClienteServicoParcelaServico> getParcelasServico() {
        if (parcelasServico == null) {
            parcelasServico = new ArrayList<ClienteServicoParcelaServico>();
        }
        return this.parcelasServico;
    }

    /**
     * Gets the value of the periodicidade property.
     * 
     * @return
     *     possible object is
     *     {@link Periodicidade }
     *     
     */
    public Periodicidade getPeriodicidade() {
        return periodicidade;
    }

    /**
     * Sets the value of the periodicidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Periodicidade }
     *     
     */
    public void setPeriodicidade(Periodicidade value) {
        this.periodicidade = value;
    }

    /**
     * Gets the value of the quantidade property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQuantidade() {
        return quantidade;
    }

    /**
     * Sets the value of the quantidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQuantidade(BigDecimal value) {
        this.quantidade = value;
    }

    /**
     * Gets the value of the quantidadeTipoVigencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQuantidadeTipoVigencia() {
        return quantidadeTipoVigencia;
    }

    /**
     * Sets the value of the quantidadeTipoVigencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQuantidadeTipoVigencia(Integer value) {
        this.quantidadeTipoVigencia = value;
    }

    /**
     * Gets the value of the renovacaoAutomatica property.
     * 
     */
    public boolean isRenovacaoAutomatica() {
        return renovacaoAutomatica;
    }

    /**
     * Sets the value of the renovacaoAutomatica property.
     * 
     */
    public void setRenovacaoAutomatica(boolean value) {
        this.renovacaoAutomatica = value;
    }

    /**
     * Gets the value of the servico property.
     * 
     * @return
     *     possible object is
     *     {@link Servico }
     *     
     */
    public Servico getServico() {
        return servico;
    }

    /**
     * Sets the value of the servico property.
     * 
     * @param value
     *     allowed object is
     *     {@link Servico }
     *     
     */
    public void setServico(Servico value) {
        this.servico = value;
    }

    /**
     * Gets the value of the servicoEstado property.
     * 
     * @return
     *     possible object is
     *     {@link ServicoEstado }
     *     
     */
    public ServicoEstado getServicoEstado() {
        return servicoEstado;
    }

    /**
     * Sets the value of the servicoEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicoEstado }
     *     
     */
    public void setServicoEstado(ServicoEstado value) {
        this.servicoEstado = value;
    }

    /**
     * Gets the value of the tarifaMinima property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaMinima() {
        return tarifaMinima;
    }

    /**
     * Sets the value of the tarifaMinima property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaMinima(BigDecimal value) {
        this.tarifaMinima = value;
    }

    /**
     * Gets the value of the taxaAtivacao property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxaAtivacao() {
        return taxaAtivacao;
    }

    /**
     * Sets the value of the taxaAtivacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxaAtivacao(BigDecimal value) {
        this.taxaAtivacao = value;
    }

    /**
     * Gets the value of the tipoConsumo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoConsumo }
     *     
     */
    public TipoConsumo getTipoConsumo() {
        return tipoConsumo;
    }

    /**
     * Sets the value of the tipoConsumo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConsumo }
     *     
     */
    public void setTipoConsumo(TipoConsumo value) {
        this.tipoConsumo = value;
    }

    /**
     * Gets the value of the tipoFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoFaturamento }
     *     
     */
    public TipoFaturamento getTipoFaturamento() {
        return tipoFaturamento;
    }

    /**
     * Sets the value of the tipoFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoFaturamento }
     *     
     */
    public void setTipoFaturamento(TipoFaturamento value) {
        this.tipoFaturamento = value;
    }

    /**
     * Gets the value of the tipoVigencia property.
     * 
     * @return
     *     possible object is
     *     {@link TipoVigencia }
     *     
     */
    public TipoVigencia getTipoVigencia() {
        return tipoVigencia;
    }

    /**
     * Sets the value of the tipoVigencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoVigencia }
     *     
     */
    public void setTipoVigencia(TipoVigencia value) {
        this.tipoVigencia = value;
    }

    /**
     * Gets the value of the unidadeMedida property.
     * 
     * @return
     *     possible object is
     *     {@link UnidadeMedida }
     *     
     */
    public UnidadeMedida getUnidadeMedida() {
        return unidadeMedida;
    }

    /**
     * Sets the value of the unidadeMedida property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnidadeMedida }
     *     
     */
    public void setUnidadeMedida(UnidadeMedida value) {
        this.unidadeMedida = value;
    }

    /**
     * Gets the value of the unidadeMetrica property.
     * 
     * @return
     *     possible object is
     *     {@link UnidadeMetrica }
     *     
     */
    public UnidadeMetrica getUnidadeMetrica() {
        return unidadeMetrica;
    }

    /**
     * Sets the value of the unidadeMetrica property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnidadeMetrica }
     *     
     */
    public void setUnidadeMetrica(UnidadeMetrica value) {
        this.unidadeMetrica = value;
    }

    /**
     * Gets the value of the usarFatorDeReajusteAutomatico property.
     * 
     */
    public boolean isUsarFatorDeReajusteAutomatico() {
        return usarFatorDeReajusteAutomatico;
    }

    /**
     * Sets the value of the usarFatorDeReajusteAutomatico property.
     * 
     */
    public void setUsarFatorDeReajusteAutomatico(boolean value) {
        this.usarFatorDeReajusteAutomatico = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValor(BigDecimal value) {
        this.valor = value;
    }

    /**
     * Gets the value of the valores property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valores property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValores().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClienteServicoValor }
     * 
     * 
     */
    public List<ClienteServicoValor> getValores() {
        if (valores == null) {
            valores = new ArrayList<ClienteServicoValor>();
        }
        return this.valores;
    }

    /**
     * Gets the value of the vendedor property.
     * 
     * @return
     *     possible object is
     *     {@link Usuario }
     *     
     */
    public Usuario getVendedor() {
        return vendedor;
    }

    /**
     * Sets the value of the vendedor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Usuario }
     *     
     */
    public void setVendedor(Usuario value) {
        this.vendedor = value;
    }

}
