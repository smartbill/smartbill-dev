
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoAmbiente.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoAmbiente">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PRODUCAO"/>
 *     &lt;enumeration value="HOMOLOGACAO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoAmbiente")
@XmlEnum
public enum TipoAmbiente {

    PRODUCAO,
    HOMOLOGACAO;

    public String value() {
        return name();
    }

    public static TipoAmbiente fromValue(String v) {
        return valueOf(v);
    }

}
