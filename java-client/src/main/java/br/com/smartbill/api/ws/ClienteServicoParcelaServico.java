
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for clienteServicoParcelaServico complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clienteServicoParcelaServico">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.api.smartbill.com.br/}clienteServicoParcela">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clienteServicoParcelaServico")
public class ClienteServicoParcelaServico
    extends ClienteServicoParcela
{


}
