
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enderecoFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enderecoFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clienteUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enderecoDeFaturamento" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="tipoEndereco" type="{http://ws.api.smartbill.com.br/}tipoEndereco" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enderecoFilter", propOrder = {
    "clienteUuid",
    "enderecoDeFaturamento",
    "tipoEndereco"
})
public class EnderecoFilter {

    protected String clienteUuid;
    protected Boolean enderecoDeFaturamento;
    protected TipoEndereco tipoEndereco;

    /**
     * Gets the value of the clienteUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteUuid() {
        return clienteUuid;
    }

    /**
     * Sets the value of the clienteUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteUuid(String value) {
        this.clienteUuid = value;
    }

    /**
     * Gets the value of the enderecoDeFaturamento property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnderecoDeFaturamento() {
        return enderecoDeFaturamento;
    }

    /**
     * Sets the value of the enderecoDeFaturamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnderecoDeFaturamento(Boolean value) {
        this.enderecoDeFaturamento = value;
    }

    /**
     * Gets the value of the tipoEndereco property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEndereco }
     *     
     */
    public TipoEndereco getTipoEndereco() {
        return tipoEndereco;
    }

    /**
     * Sets the value of the tipoEndereco property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEndereco }
     *     
     */
    public void setTipoEndereco(TipoEndereco value) {
        this.tipoEndereco = value;
    }

}
