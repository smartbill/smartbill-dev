
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for regimeTributarioFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regimeTributarioFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cidadeUuid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regimeTributarioFilter", propOrder = {
    "cidadeUuid"
})
public class RegimeTributarioFilter {

    protected String cidadeUuid;

    /**
     * Gets the value of the cidadeUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidadeUuid() {
        return cidadeUuid;
    }

    /**
     * Sets the value of the cidadeUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidadeUuid(String value) {
        this.cidadeUuid = value;
    }

}
