
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for documentoFiscal.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="documentoFiscal">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NFE"/>
 *     &lt;enumeration value="RECIBO"/>
 *     &lt;enumeration value="INVOICE"/>
 *     &lt;enumeration value="NAO_SE_APLICA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "documentoFiscal")
@XmlEnum
public enum DocumentoFiscal {

    NFE,
    RECIBO,
    INVOICE,
    NAO_SE_APLICA;

    public String value() {
        return name();
    }

    public static DocumentoFiscal fromValue(String v) {
        return valueOf(v);
    }

}
