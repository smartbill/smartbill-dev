
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for clientePrestador complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clientePrestador">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.api.smartbill.com.br/}copyFields">
 *       &lt;sequence>
 *         &lt;element name="cnpjCpf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emailGeral" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ieRg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="im" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeFantasia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orgaoPublico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="razaoSocial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoPessoa" type="{http://ws.api.smartbill.com.br/}tipoPessoa" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clientePrestador", propOrder = {
    "cnpjCpf",
    "emailGeral",
    "ieRg",
    "im",
    "nomeFantasia",
    "orgaoPublico",
    "razaoSocial",
    "tipoPessoa"
})
public class ClientePrestador
    extends CopyFields
{

    protected String cnpjCpf;
    protected String emailGeral;
    protected String ieRg;
    protected String im;
    protected String nomeFantasia;
    protected boolean orgaoPublico;
    protected String razaoSocial;
    protected TipoPessoa tipoPessoa;

    /**
     * Gets the value of the cnpjCpf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnpjCpf() {
        return cnpjCpf;
    }

    /**
     * Sets the value of the cnpjCpf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnpjCpf(String value) {
        this.cnpjCpf = value;
    }

    /**
     * Gets the value of the emailGeral property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailGeral() {
        return emailGeral;
    }

    /**
     * Sets the value of the emailGeral property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailGeral(String value) {
        this.emailGeral = value;
    }

    /**
     * Gets the value of the ieRg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIeRg() {
        return ieRg;
    }

    /**
     * Sets the value of the ieRg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIeRg(String value) {
        this.ieRg = value;
    }

    /**
     * Gets the value of the im property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIm() {
        return im;
    }

    /**
     * Sets the value of the im property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIm(String value) {
        this.im = value;
    }

    /**
     * Gets the value of the nomeFantasia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeFantasia() {
        return nomeFantasia;
    }

    /**
     * Sets the value of the nomeFantasia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeFantasia(String value) {
        this.nomeFantasia = value;
    }

    /**
     * Gets the value of the orgaoPublico property.
     * 
     */
    public boolean isOrgaoPublico() {
        return orgaoPublico;
    }

    /**
     * Sets the value of the orgaoPublico property.
     * 
     */
    public void setOrgaoPublico(boolean value) {
        this.orgaoPublico = value;
    }

    /**
     * Gets the value of the razaoSocial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazaoSocial() {
        return razaoSocial;
    }

    /**
     * Sets the value of the razaoSocial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazaoSocial(String value) {
        this.razaoSocial = value;
    }

    /**
     * Gets the value of the tipoPessoa property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPessoa }
     *     
     */
    public TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }

    /**
     * Sets the value of the tipoPessoa property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPessoa }
     *     
     */
    public void setTipoPessoa(TipoPessoa value) {
        this.tipoPessoa = value;
    }

}
