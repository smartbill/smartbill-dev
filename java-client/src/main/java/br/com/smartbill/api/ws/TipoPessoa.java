
package br.com.smartbill.api.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoPessoa.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoPessoa">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FISICA"/>
 *     &lt;enumeration value="JURIDICA"/>
 *     &lt;enumeration value="INTERNACIONAL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoPessoa")
@XmlEnum
public enum TipoPessoa {

    FISICA,
    JURIDICA,
    INTERNACIONAL;

    public String value() {
        return name();
    }

    public static TipoPessoa fromValue(String v) {
        return valueOf(v);
    }

}
