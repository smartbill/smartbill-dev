package br.com.smartbill.api.ws;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.4.4
 * 2013-11-25T14:18:50.152-02:00
 * Generated source version: 2.4.4
 * 
 */
@WebServiceClient(name = "ConfiguracaoWSService", 
                  wsdlLocation = "file:/C:/Users/Daniel/smartbill/workspace/smartbill-app/smartbill-app-web/target/wsprovide/resources/ConfiguracaoWSService.wsdl",
                  targetNamespace = "http://ws.api.smartbill.com.br/") 
public class ConfiguracaoWSService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://ws.api.smartbill.com.br/", "ConfiguracaoWSService");
    public final static QName ConfiguracaoWSPort = new QName("http://ws.api.smartbill.com.br/", "ConfiguracaoWSPort");
    static {
        URL url = null;
        try {
            url = new URL("file:/C:/Users/Daniel/smartbill/workspace/smartbill-app/smartbill-app-web/target/wsprovide/resources/ConfiguracaoWSService.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(ConfiguracaoWSService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:/C:/Users/Daniel/smartbill/workspace/smartbill-app/smartbill-app-web/target/wsprovide/resources/ConfiguracaoWSService.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public ConfiguracaoWSService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public ConfiguracaoWSService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ConfiguracaoWSService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public ConfiguracaoWSService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public ConfiguracaoWSService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public ConfiguracaoWSService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns ConfiguracaoWS
     */
    @WebEndpoint(name = "ConfiguracaoWSPort")
    public ConfiguracaoWS getConfiguracaoWSPort() {
        return super.getPort(ConfiguracaoWSPort, ConfiguracaoWS.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ConfiguracaoWS
     */
    @WebEndpoint(name = "ConfiguracaoWSPort")
    public ConfiguracaoWS getConfiguracaoWSPort(WebServiceFeature... features) {
        return super.getPort(ConfiguracaoWSPort, ConfiguracaoWS.class, features);
    }

}
